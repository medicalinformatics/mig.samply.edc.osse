$(function() {

	/* button click */

	$("#messageResumeButton").click(function() {
		if (!$('#messageResumeButton').hasClass("open")) {
			$("#messageResumePanel").fadeIn(500);
		} else {
			$("#messageResumePanel").fadeOut(700);
		}
	});

	$("#userButton").click(function() {
		if (!$('#userButton').hasClass("open")) {
			$("#userPanel").fadeIn(500);
		} else {
			$("#userPanel").fadeOut(700);
		}
	});

	/* hiding the menu panels */
	$("body").click(function() {
		if ($('#messageResumeButton').hasClass("open")) {
			$("#messageResumePanel").fadeOut(700);
		} else if ($('#userButton').hasClass("open")) {
			$("#userPanel").fadeOut(700);
		}
	});
});