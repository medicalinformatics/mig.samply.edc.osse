function fixDiv() {
    var $cache = $('#stickyButtonBar');
    var $stickyIsFixed = $cache.css('position')=='fixed';
    var $formheight=$('#patient').height() + $('#nav').height();
    var $episodeheight=$('#episode').height() + $('#nav').height();
    var $isEpisode = $('#stickyButtonBar').parents('.episode').length==1;
    var $stickyheight = $cache.height();

    //console.log("STICKY = "+$stickyheight+" ISFIXED = "+$stickyIsFixed+" PAGE = "+$pageheight+" WIN = "+$(window).height());
    switch ($isEpisode){
        case true:
            if($stickyIsFixed){
                if($episodeheight +$stickyheight + 20 > $(window).height()){
                    //console.log("fixed for long episode");
                }
                else{
                    $('#stickyButtonBar').css({'position': 'relative','top': 'auto' });
                }}
            else
            {
                if ($episodeheight > $(window).height()) {
                    $('#stickyButtonBar').css({'position': 'fixed', 'bottom': '0px'});
                    //console.log("fixed for long episode");
                }
            }
            break;
        case false:
            if($stickyIsFixed){
                if($formheight +$stickyheight + 20 > $(window).height()){
                    //console.log("fixed for long form");
                }
                else{
                    $('#stickyButtonBar').css({'position': 'relative','top': 'auto' });
                }}
            else
            {
                if ($formheight > $(window).height()) {
                    $('#stickyButtonBar').css({'position': 'fixed', 'bottom': '0px'});
                    //console.log("fixed for long form");
                }
            }
            break;
            $(".sticky_bar").width($(".right").width() - 20);
            fixRightDiv();
    }}
/* Sets the CSS max-width of the right div. This allows the right div to contain
 * overflowing elements without overflowing the container itself. */
function fixRightDiv() {
    $('.right').css('max-width', 
            $('.container').width() - $('.left').outerWidth());
}

function setTitleIfEllipsisActive($obj) {
    if (isEllipsisActive($obj)) {
        $obj.children().prop('title', $obj.children().text());
    } else {
        $obj.children().prop('title', '');
    }        
}

function isEllipsisActive($jQueryObject) {
    return ($jQueryObject.outerWidth() < $jQueryObject[0].scrollWidth);
}

(function() {
  var app;

  $(document).ready(function() {
	  $('.canBeEllipsed').each(function() {
	        setTitleIfEllipsisActive($(this));
	    });
	  
    return app.init();
  });

  $(window).load(function() {

	     $(".sticky_bar").addClass("sticky_bottom");
		 $(".sticky_bar").width($(".right").width()-20);


	    //return app.init();

	  });
  
  app = {
    init: function() {
//      $("#episode-slider").slick({
//        slidesToScroll: 1,
//        centerMode: true,
//        infinite: false,
//        slide: 'a',
//        variableWidth: true
//      });
    	
//    	alert($(".sticky_bar"));
//    	alert($(".sticky_bar").length);
//    	alert($(".sticky_bar").offset());
    	
    	if($(".sticky_bar").length == 0) {
    	}
    	else
    	{
	      this.bind_events();
	      //return window.offset_top = $(".sticky_bar").offset().top;
    	}
    },
    bind_events: function() {
//    	$(window).scroll(fixDiv);
    	$(window).resize(fixDiv);
    	fixDiv();

//      return $(window).scroll(function() {
//        //return app.sticky_nav_top();
//      });
    },
    sticky_nav_top: function() {
      if ($(window).scrollTop() >= window.offset_top) {
        $(".sticky_bar").addClass("sticky_top");
        return $(".sticky_spacer").show();
      } else {
        $(".sticky_bar").removeClass("sticky_top");
        return $(".sticky_spacer").hide();
      }
    }
  };

}).call(this);


formatDateNumber = function(d) {
    var digit = parseInt(d);

    if (typeof(d) =="string" && d.length > 1 && digit < 10)
        return d
	else if (typeof(d) =="string" && d.length == 1 && digit < 10)
		return "0" + d;
	else
		return d
}

function reRenderTableSurrounder(data, tablename) {
	if(data.status == "success") {
	    if ($('#rerender_'+tablename).length) {
            $('#rerender_'+tablename).click();
        } else {
            // The ids may be prefixed with the parent containers ids
	        var selector = 'rerender_' + tablename;
            $("input[id$=" + selector + "]").click();
        }
	}
}

function reInitTable(data, tablename) {
	if(data.status == "success") {
	    if ($('#'+tablename).length) {
            initRecordDataTable($('#' + tablename));
        } else {
	        // Table ids may be prefixed with the parent containers id
	        initRecordDataTable($("table[id$=" + tablename + "]"))
        }
		
		// recall this as record table might resize the form
		fixDiv();
	} else {
    $('#'+tablename).find('a').removeAttr('href');
    $('#'+tablename).find('a').removeAttr("onclick");
    $('#'+tablename).find('*').attr('disabled', true);
    $('#'+tablename).find('*').css("opacity",0);
    $('#'+tablename).addClass('spinner');
  }
} 

function addPsnToButton() {
    if ($('.upload-button').length) {
        var link = $('.upload-button').attr('href');
        $('.upload-button').attr('href', link + $('span[data-subject="pid"]').text());
    }
}