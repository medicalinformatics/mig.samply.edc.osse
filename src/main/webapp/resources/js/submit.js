$(document).ready(function() {
	// exclude certain pages
	var excludedPage = $("div.panel-body.form-visit-edit").length > 0;

  // detect changes
  $("#topFormPanel :input:not(input[type=submit]):not(input[type=button])").change(function(event) {
    // Filter programatically triggered changes to prevent false alarm when
    // form conditions are applied after loading the form.
    if (!event.isTrigger) {
      sessionStorage.setItem("somethingChangedInForm", "yes");
    }
  });

  // Changing a date by using a date picker does not trigger
  // the general change event, but dp.change
  $("#topFormPanel").on("dp.change", function() {
    sessionStorage.setItem("somethingChangedInForm", "yes");
  });

  //check if warning dialog is needed
  window.addEventListener('beforeunload', function(e) {
    var somethingChanged = sessionStorage.getItem("somethingChangedInForm");
    if(somethingChanged==="yes" && !['save', 'save_upgrade'].includes((e.target.document || e.target).activeElement.id) && !excludedPage) {
      e.preventDefault();
      e.returnValue = '';
    }
  });

  //if the user decides to leave the page
  window.addEventListener("unload", function (e) {
    sessionStorage.setItem("somethingChangedInForm","no");
  })

});
