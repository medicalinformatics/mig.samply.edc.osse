$(document).ready(function() {
//	$(".tooltipInput").tooltip({ placement: 'right', trigger: 'focus'});

	bindCaseTabSwitchEvent();
	bindSampleTabSwitchEvent();
	bindDeleteEntityEvent();
	registerDashboardClickHandlers();
});

function bindDeleteEntityEvent() {
	$('.deletable-entities').on('click', '.delete-entity', function(e) {
		if (confirm("Really delete?") == true) {
			deleteEntity([{name:'entityURL',value:$(this).parent().text()}, {name:'type',value:$(this).parent().attr('data-entityType')}]);
			reloadPage();
		}
	});
}

function bindCaseTabSwitchEvent() {
	$('#dataForm').off().on('click', '.case-tabs a', function (e) {
		var tabindex = $(e.target).closest('li').index();
		onCaseTabChange([{name:'caseIndex',value:tabindex}]);
		e.preventDefault();
		$(this).tab('show');
	});
}

function bindSampleTabSwitchEvent() {
	$('#caseTabContent').off().on('click', '.sample-tabs a', function (e) {
		var tabindex = $(e.target).closest('li').index();
		onSampleTabChange([{name:'sampleIndex',value:tabindex}]);
		e.preventDefault();
		$(this).tab('show');
	});
}

// TODO: not working!
function activateLastTab(caller) {
	var obj = $(caller).parentsUntil('.panel').parent();
	console.log($(caller).attr('id') + "---" + obj.attr('id'));
	obj.find('.tab-content').find('.active').removeClass('active');
	obj.find('.tab-content').find('.tab-pane').last().addClass('active');
	obj.find('.nav-tabs').find('.active').removeClass('active');
	obj.find('.nav-tabs').find('li').last().addClass('active');
}

function myChangeHandler(that) {
//   alert(that.checked);
}

function registerDashboardClickHandlers() {
	$('.dashboard-panel').click(function(event) {
		event.stopPropagation();
		$(this).siblings('.go-to-page').children('a').trigger("click");
	});
}
function copyInfo(element) {
	const $temp = $("<input>");
	$("body").append($temp);
	$temp.val($(element).text()).select();
	document.execCommand("copy");
	$temp.remove();
}
