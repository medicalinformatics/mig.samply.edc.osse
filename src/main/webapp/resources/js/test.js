// this does NOT work with JSF
// for some reasons the checkbox onchange is CALLED by loading of the page for the first checkbox no matter if it is set or not
// find out what mergels this by JSF before you can use nice stuff
// add in the header of a page this:  <h:outputScript library="js" name="test.js" />
// and replace body by: <h:body onload="mergel();">
//

function blockstuff(old, el)
{
	alert("blockstuff called by "+el.getAttribute("name"));
	var importbutton = document.getElementById('importbutton');
	var reportbutton = document.getElementById('reportbutton');
	
	importbutton.disabled = true;
	reportbutton.disabled = true;

	var savebutton = document.getElementById('savebutton');
	var old = savebutton.onclick;
	savebutton.onclick = unblockstuff(old, savebutton);
	
	old.call(el);
}

function unblockstuff(old, el)
{
	alert("unblockstuff called "+el.getAttribute("name"));
	var importbutton = document.getElementById('importbutton');
	var reportbutton = document.getElementById('reportbutton');
	
	importbutton.disabled = false;
	reportbutton.disabled = false;

	old.call(el);
}
 

function mergel()
{

var theForm = document.getElementById('theform');
var elCount = theForm.elements.length;
for (var i=0;i<elCount;i++){
	
	var el = theForm.elements[i];
	var old;
 	var elType = el.getAttribute('type');

 	switch (elType){
 	case 'text':
 		old = el.onkeypress;
   		el.onkeypress = blockstuff(old, el);
   	break;
 	case 'checkbox':
 		old = el.onclick,
  		//el.onclick = function(){  alert(el); old.call(el); };
 		el.onclick = blockstuff(old, el);
   	break;
 	case 'radio':
 		  el.onclick = function(){  el.onchange(); };
   	break;
 	case 'button':
 		alert("button "+el.getAttribute('name'));
 		if(el.getAttribute('name') == "savebutton" || el.getAttribute('name') == "savebuttonBottom" || el.getAttribute('name') == "savebuttonNoImport")
 			{
 				old = el.onclick;
 				el.onclick = unblockstuff(old, el);
 			}
 		
 	}
}
}