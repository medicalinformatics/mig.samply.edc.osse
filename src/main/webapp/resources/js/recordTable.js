$.fn.dataTable.ext.order['mdr-boolean'] = function (settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        return $('input', td).prop('checked') ? '1' : '0';
    });
};
// parses date to string with moment.js
$.fn.dataTable.ext.order['mdr-time'] = function (settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        var input = $('input', td);
        return moment(input.val(), input.attr("data-date-format")).format();
    });
};

$.fn.dataTable.ext.order['mdr-enumerated'] = function (settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        return $('select option:selected', td).text();
    });
};

$.fn.dataTable.ext.order['mdr-text'] = function (settings, col) {
    return this.api().column(col, {order: 'index'}).nodes().map(function (td, i) {
        return $('input', td).val();
    });
};

$.fn.dataTable.ext.order['mdr-numeric'] = function(settings, col) {
    return this.api().column(col, {order:'index'}).nodes().map(function (td, i) {
        return $('input', td).val() * 1;
    });
};

var initRecordDataTable = function(recordTable) {
    recordTable = $(recordTable);

    // determine type for ordering once for each column of the table
    var orderDataTypes = [];
    $("thead th", recordTable).each(function (index, th) {
        var dataTypeSpan = $(".mdrDataType", th);
        if (dataTypeSpan.length) {
            switch (dataTypeSpan[0].innerHTML) {
                case "DATE":
                case "DATETIME":
                    orderDataTypes.push("mdr-time");
                    break;
                case "INTEGER":
                case "INTEGERRANGE":
                case "FLOAT":
                case "FLOATRANGE":
                    orderDataTypes.push("mdr-numeric");
                    break;
                case "BOOLEAN":
                    orderDataTypes.push("mdr-boolean");
                    break;
                case "permissibleValues":
                    orderDataTypes.push("mdr-enumerated");
                    break;
                default:
                    orderDataTypes.push("mdr-text");
            }
        }
        else {
            orderDataTypes.push(null);
        }
    });

    recordTable.DataTable({
        "bPaginate": false,
        "searching": false,
        "columns": $.map(orderDataTypes, function (aType) {
            var column = {"orderDataType": aType, "defaultContent": ""};
            if (aType === "mdr-text" || aType === "mdr-time") {
                column["type"] = "string";
            }
            return column;
        }),
        "columnDefs": [
            {
                "targets": 0,
                "visible": false,
                "orderData": 0
            }
        ]
    });

    recordTable[0].setAttribute("role", "table");
    
    // workaround: fix bootstrap tooltips inside the table 
    // (Otherwise the overflow outside the table is hidden.)
    var tootippedElementsInTable = $(".hasTooltip", recordTable);
    tootippedElementsInTable.tooltip('destroy');
    tootippedElementsInTable.tooltip({container: 'body'});
}

$(document).ready(function() {
    // initalize all recordTable-Tables as jQuery DataTables
    $(".recordTable").each(function(index, recordTable) {
        initRecordDataTable(recordTable);
    });
    
	fixDiv();

});
