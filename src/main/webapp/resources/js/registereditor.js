/**
 * Activates the drag functionality in .sortableItemPanel
 * and .draggable
 */
function activateDrag() {

	$(".sortableItemPanelEpisode").sortable({
		start: function(e, ui ){
            ui.placeholder.height(ui.helper.outerHeight());
        },
		stop : function(event, ui) {
			refreshItems();
			refreshItemsEpisode();
			$("#refreshItems").click();
			$("#refreshItemsEpisode").click();
		}
	});
	
	$(".sortableItemPanel").sortable({
		start: function(e, ui ){
            ui.placeholder.height(ui.helper.outerHeight());
        },
		stop : function(event, ui) {
			refreshItems();
			refreshItemsEpisode();
			$("#refreshItems").click();
			$("#refreshItemsEpisode").click();
		}
	});
	
	$(".draggable").draggable({
		connectToSortable : ".sortableItemPanel,.sortableItemPanelEpisode",
		revert : "invalid",
		helper: "clone",
		stop : function(event, ui) {
			patformColors();
		}
	});

	$(".dragableInternal").draggable({
		connectToSortable : ".sortableItemPanel,.sortableItemPanelEpisode",
		revert : "invalid",
	});
	
	patformColors();
}

/**
 * Refreshes the items, using the .formItems
 */

function refreshItems() {
	var checkElements = this.checkElements($(".sortableItemPanel"));
	if (checkElements.isElementAlreadyUsed) {
		$.growl({
			title: '<strong>Error:</strong><br /> ',
			message: 'The form (in any version) is already used in your basic data forms'
		}, {
			type: 'info',
			animate: {
				enter: 'animated pulse',
				exit: 'animated zoomOut'
			}
		});
	}
	var elements = checkElements.getElements;
	if ($(".sortableItemPanel").find(".formItem").length > 0) {
		var newElements = new Array();
		var newElementsTemp = new Array();
		var i;
		for (i = 0; i < elements.length; i++) {
			var split = elements[i].split("-");
			if(!isInEpisodeForms(split[0])) {
				if(newElementsTemp.indexOf(split[0]) == -1) {
					newElements.push(elements[i]);
					newElementsTemp.push(split[0]);
				}
			}
			else {
				$.growl({
					title: '<strong>Error:</strong><br /> ',
					message: 'The form (in any version) is already used in your longitudinal data forms.'
				},{
					type: 'info',
					animate: {
						enter: 'animated pulse',
						exit: 'animated zoomOut'
					}
				});
			}
		}

		$("#items").val(newElements.join(","));
		$("#itemsTemp").val(newElementsTemp.join(","));

		$("#dragInfo").hide();
	} else {
		$("#items").val("");
		$("#itemsTemp").val("");
		$("#dragInfo").show();
	}
	
	patformColors();
}

function isInCaseForms(searchID) {
	var all = $("#itemsTemp").val().split(",");
	if(all.indexOf(searchID) == -1)
		return false;
	else
		return true;
}

function isInEpisodeForms(searchID) {
	var all = $("#itemsEpisodeTemp").val().split(",");
	if(all.indexOf(searchID) == -1)
		return false;
	else
		return true;
}

/**
 * Check each element to see if it is a duplicate. If it is an old version, it will be filtered.
 * @param id is the value of element's attribute from forms.xhtml.
 * @return ElementOrder as the info about order of element and isElementAlreadyUsed.
 */
function checkElements(id){
	var isDuplicated = false;
	var doNotPush = false;
	var elements = new Array();
	id.find(".formItem").each(function(i, el) {
		var urn = $(el).find(".urn").html();
		elements.push(urn);
	});
	var isVersionOld = false;
	var newElements = new Array();
	for (var y = 0; y < elements.length; y++) {
		var e1 = elements[y].split("-");
		var name1 = e1[0];
		var version1 = e1[1];
		for (var x = y + 1; x < elements.length; x++) {
			var e2 = elements[x].split("-");
			var name2 = e2[0];
			var version2 = e2[1];
			if(name1 == name2){
				if(version1 == version2){
					doNotPush = true;
					isDuplicated = true;
				} else {
					if (version1 < version2) {
						isVersionOld = true;
					} else {
						isVersionOld = false;
					}
					isDuplicated = false;
				}
				break;
			}
		}
		if (!isVersionOld && !doNotPush) {
			newElements.push(elements[y]);
		}
		isVersionOld = false;
		doNotPush = false;
	}
	return new ElementOrder(newElements, isDuplicated);
}

/**
 * Constructor ElementOrder
 * @param newElements
 * @param isElementAlreadyUsed
 */
function ElementOrder(newElements, isElementAlreadyUsed) {
	this.getElements = newElements;
	this.isElementAlreadyUsed = isElementAlreadyUsed;
}


/**
 * Refreshes the items for episode.
 */
function refreshItemsEpisode() {
	var checkElements = this.checkElements($(".sortableItemPanelEpisode"));
	if (checkElements.isElementAlreadyUsed) {
		$.growl({
			title: '<strong>Error:</strong><br /> ',
			message: 'The form (in any version) is already used in your longitudinal data forms'
		}, {
			type: 'info',
			animate: {
				enter: 'animated pulse',
				exit: 'animated zoomOut'
			}
		});
	}
	var elements = checkElements.getElements;
	if ($(".sortableItemPanelEpisode").find(".formItem").length > 0) {
		var newElements = new Array();
		var newElementsTemp = new Array();
		var i;
		for (i = 0; i < elements.length; i++) {
			var split = elements[i].split("-");
			
			if(!isInCaseForms(split[0])) {
				if(newElementsTemp.indexOf(split[0]) == -1) {
					newElements.push(elements[i]);
					newElementsTemp.push(split[0]);
				}
			}
			else {
				$.growl({
					title: '<strong>Error:</strong><br /> ',
					message: 'The form (in version) is already used in your basic data forms.'
				},{
					type: 'info',
					animate: {
						enter: 'animated pulse',
						exit: 'animated zoomOut'
					}
				});
			}
		}

		$("#itemsEpisode").val(newElements.join(","));
		$("#itemsEpisodeTemp").val(newElementsTemp.join(","));
		
		$("#dragInfoEpisode").hide();
	} else {
		$("#itemsEpisode").val("");
		$("#itemsEpisodeTemp").val("");
		$("#dragInfoEpisode").show();
	}
	
	patformColors();
}

/**
 * Called when a "remove" button has been clicken
 * @param element
 */
function removeItem(element) {
	//var urn = $(element).parent().parent().parent().remove();
	var urn = $(element).parent().parent().remove();
	refreshItems();
	$("#refreshItems").click();
}

function removeItemEpisode(element) {
	var urn = $(element).parent().parent().remove();
	refreshItemsEpisode();
	$("#refreshItemsEpisode").click();
}


function showResultTab() {
	$("#searchTabs a[href='#searchResultTab']").tab("show");
}

function toggleItem(element) {
	var urn = $(element).parent().parent().find(".urn").html();
	$(".inputStarItemUrn").val(urn);
	$(".inputStarItemBtn").click();
	
	
	var others = $(".urn").filter(function() { return $(this).html() == urn; });
	var i = $(element).find("i");
	
	if(i.hasClass("text-muted")) {
		others.each(function(index) {
			$(this).parent().parent().parent().find(".starItem").switchClass("text-muted", "text-primary");
		});
	} else {
		others.each(function(index) {
			$(this).parent().parent().parent().find(".starItem").switchClass("text-primary", "text-muted");
		});
	}
}

function collapseItem(element) {
	var divp = $(element).parent().parent().parent();
	var urn = divp.find(".urn").html();
	var d = divp.find("#details").first();
	var draggable = divp.find(".formItem").first().hasClass("ui-draggable");
	
	if(! d.hasClass("in")) {
		d.load("shortDetail.xhtml?draggable=" + draggable + "&urn=" + urn + " #detailView",
				function() {
			$(this).collapse('toggle');
			$(element).find("i").switchClass("fa-angle-down", "fa-angle-up");
			activateDrag();
		});
	} else {
		d.collapse('toggle');
		$(element).find("i").switchClass("fa-angle-up", "fa-angle-down");
	}
}

// Role Edit, hide/display permissions on rolename entry
function displayPermissions() {
	var feld = document.getElementById("rolename");
	var hiddenDiv = document.getElementById("predefinedPermissions");

    hiddenDiv.style.display = (feld.value == "") ? "none":"block";
}

function patformColors() {
    $("input[id*='patFormToggle']").each(function (i, el) {
	    if(el.checked)
	        el.parentElement.parentElement.parentElement.style.backgroundColor = "palegreen";
	    else
	        el.parentElement.parentElement.parentElement.style.background = "rgba(234, 239, 245, 0.71) none repeat scroll 0 0";

    });
}

function updateColor(val) {
    if(val.checked)
        val.parentElement.parentElement.parentElement.style.backgroundColor = "palegreen";
    else
        val.parentElement.parentElement.parentElement.style.background = "rgba(234, 239, 245, 0.71) none repeat scroll 0 0";
}



$(document).ready(function(){
    patformColors();
});
