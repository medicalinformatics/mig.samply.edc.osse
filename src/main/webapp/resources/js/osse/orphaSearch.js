/**
 * Add the modal html structure to be used by setOrphaField
 */
function addOrphaModal() {
  $("#topFormPanel").parents("div.right").append(`<div id="orphaModal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" data-localization=orphaModalTitle>ORPHAcode search</h4>
        </div>
        <div class="modal-body">
          <p data-localization=orphaModalSearchTextDescription>Here you can search for rare diseases in the Orphanet Nomenclature and retrieve the corresponding ORPHAcode.</p>
          <div class="form-group input-group data">
            <input type="text" id="orphaSeachText" >
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Dismiss</button>
          
        </div>
      </div>
    </div>
  </div>`);
}

/**
 * Looks for string (input) data elements which have a slot
 * "renderType: catalog_ORPHA" and activates the ORPHA search feature for them.
 */
function modificationsSetOrphaFields() {
  let regex = /^urn:(osse-\d+):dataelement:(\d+):\d+$/;
  $('input[data-slot-rendertype="catalog_ORPHA"]').each(function(i, e){
    let inputField = $(e);
    //make sure orpha modal exists
    if($("#orphaModal").length == 0) {
      addOrphaModal();
    }
    //make sure the registryNamespace is set, else try to impute
    let urn = inputField.attr('data-mdrid');
    if(registryNamespace=='') {
      registryNamespace=regex.exec(urn)[1]
    }
    if(inputField.attr('id')) {
      inputField.prop("readonly", true);
      inputField.css('cursor','pointer');
      //add description and listener
      updateDescriptionOrpha(inputField.get());
      $(inputField).on("change",function () {
        updateDescriptionOrpha($(this))
      })
      //activate modal logic
      getOrphaModal(inputField.get());
      //make sure to remeber this was already done for this field
      inputField.attr("is-orpha-set",true);
    }

  });
}

/**
 * Function to set the needed event listener to handle modal dialog for orpha search and deal with the results.
 * @param dataElementID the target dataelements id.
 */
function getOrphaModal(inputField) {
  var searchText = $("#orphaSeachText");
  $(inputField).focus(function () {
    $("#orphaModal").on('shown.bs.modal', function () {
      $('#orphaSeachText').focus();
    });
    $("#orphaModal").modal("show");
    searchText.val('');
    $(searchText).off('input');
    searchText.on('input', function() {
      queryOrphaTerm(searchText, $(inputField).get())});
    $('#orpha_search_results').remove();
  })
}

/**
 * Method to query for the correct orpha code.
 * @param {*} searchText
 * @param {*} inputField
 */
function queryOrphaTerm(searchText, inputField) {
  $.ajax({
    beforeSend: function(request) {
      request.setRequestHeader("apiKey", (Math.random() + 1).toString(36).substring(7));
      $('.orpha_selector').hide();
    },
    dataType: "json",
    url: "https://api.orphacode.org/"+getLanguageShort()+"/ClinicalEntity/ApproximateName/"  + $(searchText).val(),
    success: function(data) {
      if(data.length>20) {
        return;
      }
      $('.orpha').remove();
      selectionArray = [];
      linkValueArray = [];
      $.each(data, function (key, val) {
        selectionArray.push(val["Preferred term"] + " [" + val["ORPHAcode"] +"]");
        linkValueArray.push(val["ORPHAcode"]);
      });
      addSelectionToModal(inputField, selectionArray, linkValueArray);
    },
    error: function( jqxhr, textStatus, error ) {
      var err = textStatus + ", " + error;
      console.log( "Request Failed: " + err );
    }
  });
}

/**
 * Helper to update the description text for orpha terms.
 * @param ref reference to the input field that contains the orpha term.
 */
function updateDescriptionOrpha(inputField) {
  if(!$(inputField).next().hasClass('hpo_def')) {
    $('<a class="hpo_def" target="_blank" rel="noopener noreferrer" data-toggle="tooltip"></a>').attr('id', $(inputField).attr('id')+"_description").hide().insertAfter($(inputField));
  }
  let orpha_term = $(inputField).val()
  if(!( orpha_term=='')) {
    let description_field = $(inputField).next()
    let link = "https://www.orpha.net/consor/cgi-bin/OC_Exp.php?lng="+getLanguageShort()+"&Expert="+orpha_term;
    description_field.text("...")
    .attr('href',link).show();
    $.ajax({
      beforeSend: function(request) {
        request.setRequestHeader("apiKey", (Math.random() + 1).toString(36).substring(7));
      },
      dataType: "json",
      url: "https://api.orphacode.org/"+getLanguageShort()+"/ClinicalEntity/orphacode/"  + orpha_term + "/Name",
      success: function(data) {
        $(description_field).text(data["Preferred term"]);
      }
    });
  }
}

/**
 * Helper to add selection option to the orpha Modal.
 * @param {*} inputField
 * @param {*} selectionArray
 * @param {*} linkValueArray
 */
function addSelectionToModal(inputField, selectionArray, linkValueArray) {
  $('.orpha_selector').remove();
  var outer_div = $('<div class=orpha_selector style="margin-top: 20px;"/>');
  var terms_ul = $(
      '<ul id=orpha_search_results style="list-style-type:none;"/>');
  selectionArray.forEach(function(item, index){
    let terms_li = $('<a href="#"/>').append(item).wrap(
        '<li/>').parent().click(function (event) {
      event.preventDefault();
      $(inputField).attr("orpha-ref",linkValueArray[index]);
      $(inputField).val(linkValueArray[index]);
      $(inputField).trigger("change");
      $("#orphaModal").modal("hide");
    })
    terms_ul.append(terms_li);
  });
  outer_div.append(terms_ul);
  $("#orphaSeachText").parent().append(outer_div);
}

$(window).on('load', function() {
  modificationsSetOrphaFields();
  $("#topFormPanel").on('draw.dt', function () {
    modificationsSetOrphaFields();
  })
});