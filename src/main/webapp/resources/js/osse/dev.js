
// defining flags
var devHotkeyCtrl = false;
var devHotkeyShift = false;

$(document).ready(function() {
    // key release
    $(document).keyup(function(e) {
        if(e.which == 17) {
            devHotkeyCtrl = false;
        }
        if(e.which == 16) {
            devHotkeyShift = false;
        }
    });
    // key press
    $(document).keydown(function(e) {
        if(e.which == 17) {
            devHotkeyCtrl = true;
            console.log("Hotkey Ctrl");
        }
        if(e.which == 16) {
            devHotkeyShift = true;
            console.log("Hotkey Shift");
        }
        if(e.which == 115 && devHotkeyCtrl && devHotkeyShift) {
            // dev shortcut triggered
            console.log("Hotkey Ctrl+Shift+F4");
            showMdrIdTags();
        }
    });

});

function showMdrIdTags() {
    $("[data-mdrId*=':dataelement:'], [data-mdrId*=':record:']").each(function(index, element){
        if ($(element).is("input[type='radio']")) {
            return "radio";
        }

        var elementWithBadge = element;

        if ($(element).is("input[type='text']")) {
            var inputGroup = $(element).closest(".input-group");
            if (inputGroup.length > 0) {
                elementWithBadge = inputGroup;
            }
        } else if ($(element).is("input[type='checkbox']")) {
            var checkboxDiv = $(element).closest("div.checkbox");
            if (checkboxDiv.length > 0) {
                elementWithBadge = checkboxDiv;
            } else {
                return "checkbox";
            }
        }
        $(elementWithBadge).before("<span class=\"pull-right badge badge-mdr-id\">" + $(element).attr("data-mdrId") + "</span>");
    });
}