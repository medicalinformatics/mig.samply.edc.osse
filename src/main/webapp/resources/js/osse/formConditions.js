/**
 * The MDR namespace this registry uses.
 *
 * Example:
 * registryNamespace = "osse-123";
 *
 * @type {string}
 */
var registryNamespace = "";

/**
 * Conditions for data elements, defined as javascript object.
 *
 * formConditions = {
 *   forms: {
 *     f: {
 *       disable: {
 *         dataElements: [i, j],
 *         records: [i]
 *       },
 *       readOnly: {
 *         dataElements: [i, j],
 *         records: [i]
 *       },
 *       hide: {
 *         dataElements: [i],
 *         records: [i]
 *       },
 *       conditionalVisibility: [
 *         {
 *           dataElement: x,
 *           any: {
 *             eq: [
 *               {
 *                 form: f,
 *                 dataElement: y,
 *                 value: "v"
 *               }
 *             ]
 *           }
 *         },
 *         {
 *           record: x,
 *           any: {
 *             eq: [
 *               {
 *                 dataElement: y,
 *                 value: "v"
 *               }
 *             ]
 *           }
 *         }
 *       ],
 *       defaultValue: [
 *         {
 *           record: z,
 *           dataElement: x,
 *           value: y
 *         }
 *       ],
 *       storeValue: [
 *         {
 *           dataElement: x
 *         },
 *         {
 *           record: z,
 *           dataElement: x
 *         }
 *       ]
 *     }
 *   }
 * }
 *
 * Modifications are defined per form. f is the form to apply the modifications
 * to.
 *
 * For "disable", "readOnly" and "hide":
 * i and j are ids of data elements or records that should be disabled, set
 * read-only or hidden altogether. The difference between disabled and read-only
 * is that values of read-only inputs are submitted and stored upon saving the
 * form, while values of disabled inputs are only for display. The use case for
 * read-only data elements is to store auto-generated data in the form.
 * Limitation: "readOnly" only works with text inputs, not with other input
 * types like select lists etc (due to limitations of the HTML standard).
 *
 * For "conditionalVisibility":
 * x is the id of the data element or record to be hidden / to apply the
 * conditions to. y is the id of the data element that x depends on. v is the
 * value to compare y's value to.
 * "any" is currently a fixed keyword. If y is on a different form than x, then
 * f is the id of that form. v must be stored in that case (see storeValue).
 * "eq" ia a condition using equality (=). So x is visible when the value of y
 * equals v.
 * It works the same way for entire records.
 *
 * For "defaultValue":
 * Each object represents one data element to set a default value for. It
 * contains the data element's id (x) and the default value to set (y). If the
 * data element is part of a record, the record's id can also optionally be set
 * (z).
 * Values for radio buttons, checkboxes and select elements should be given as
 * an array.
 * The default value will only be set when the data element is "empty" (ie. no
 * value set or none selected).
 * If there are more than one default values for a single data element id (eg.
 * one with record and one without), the first one is used.
 *
 * For "storeValue":
 * Each object represents one data element of which to temporarily store the
 * value. It contains the data element's id (x) and if the data element is part
 * of a record, the record's id can also optionally be set (z).
 *
 * The id of a form is visible as tooltip of the version string in the form's
 * header.
 * The id of a data element or a record is part of its URN:
 * urn:<namespace>:<type>:<id>:<version>
 *
 * Example:
 *
 * formConditions = {
 *   forms: {
 *     123: {
 *       disable: {
 *         dataElements: [34, 56],
 *         records: [78]
 *       },
 *       readOnly: {
 *         dataElements: [35, 57],
 *         records: [79]
 *       },
 *       hide: {
 *         dataElements: [90],
 *         records: [12]
 *       },
 *       conditionalVisibility: [
 *         {
 *           dataElement: 23,
 *           any: {
 *             eq: [
 *               {
 *                 dataElement: 45,
 *                 value: "yes"
 *               }
 *             ]
 *           }
 *         },
 *         {
 *           record: 67,
 *           any: {
 *             eq: [
 *               {
 *                 dataElement: 89,
 *                 value: "1"
 *               }
 *             ]
 *           }
 *         }
 *       ],
 *       defaultValue: [
 *         {
 *           dataElement: 12,
 *           value: "default"
 *         },
 *         {
 *           record: 34,
 *           dataElement: 56,
 *           value: ["default", "another default"]
 *         }
 *       ],
 *       storeValue: [
 *         {
 *           dataElement: 12
 *         },
 *         {
 *           record: 34,
 *           dataElement: 56
 *         }
 *       ]
 *     }
 *   }
 * }
 *
 * @type {object}
 */
var formConditions = {
  forms: {}
};
