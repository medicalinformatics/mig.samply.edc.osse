function complete2faToggle() {
  if ($('#totpToggle_input').is(':checked')) {
    PF('mfa-dlg').show();
  }
}

function complete2faActivate(data) {
  if(data.status=="success") {
    PF('mfa-dlg').show();
  }
}
