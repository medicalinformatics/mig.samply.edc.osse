$(document).ready(function() {
    if ($('#patientlist-table-unsortable').length) {

        var tempIdResolver = tempIdResolverConstructor({
            useDefaultResolver:"mainzellisteResolver",
            mainzellisteURL: $('#mainzelliste-url').text()
        });

        var languageSetting = $('#language-setting').text() || "en";

        var patientListRawTable = $('#patientlist-table-unsortable').DataTable( {
            "ajax": {
                "url": "patientlist_datasource.xhtml"
            },
            "searching": false,
            "processing": true,
            "serverSide": true,
            "pageLength": 10,
            "ordering": false,
            "columns": [
                { "render": function (data, type, row, meta ) {
                    var patientAnchor = document.createElement('a');
                        patientAnchor.classList.add('patientLink');
                        patientAnchor.href = "#";

                        var spanResourceUri = document.createElement('span');
                        spanResourceUri.classList.add('resourceUri');
                        spanResourceUri.style.display = 'none';
                        spanResourceUri.textContent = row.resourceUri;
                        patientAnchor.appendChild(spanResourceUri);
                    if (meta.settings.json.isBridgehead || row.isImported) {
                        patientAnchor.appendChild(document.createTextNode(row.patientId));
                    } else {
                        if (meta.settings.json.showIdat) {
                            if (row.showIdat) {
                                var spanLastName = document.createElement('span');
                                spanLastName.setAttribute("id", "idatLastName");
                                spanLastName.dataset.tempid = row.psn;
                                spanLastName.dataset.subject = "nachname";

                                var spanFirstName = document.createElement('span');
                                spanFirstName.setAttribute("id", "idatFirstName");
                                spanFirstName.dataset.tempid = row.psn;
                                spanFirstName.dataset.subject = "vorname";

                                patientAnchor.appendChild(spanLastName);
                                patientAnchor.appendChild(document.createTextNode(', '));
                                patientAnchor.appendChild(spanFirstName);
                                patientAnchor.appendChild(document.createTextNode(' ( '));
                            } else {
                                patientAnchor.appendChild(document.createTextNode('( '));
                            }

                            var spanIdatPid = document.createElement('span');
                            spanIdatPid.setAttribute("id", "idatPID");
                            spanIdatPid.dataset.tempid = row.psn;
                            spanIdatPid.dataset.subject= "pid";
                            patientAnchor.appendChild(spanIdatPid);
                            patientAnchor.appendChild(document.createTextNode(' )'));
                        } else {
                            var spanPid = document.createElement('span');
                            spanPid.setAttribute("id", "noIdatPatientId");
                            spanPid.dataset.tempid = row.psn;
                            spanPid.dataset.subject = patientId;
                            patientAnchor.appendChild(spanPid);
                        }
                    }

                    return patientAnchor.outerHTML + ' <span class="fa fa-files-o clipboard" title="copy"></span>';
                  },
                    "orderable": false
                },
                { "render": function (data, type, row, meta ) {
                        return '<span id="idatBirthDay" data-tempid="' + row.psn + '" data-subject="geburtstag" data-postProcess="formatDateNumber"></span>.' +
                            '<span id="idatBirthMonth" data-tempid="' + row.psn + '" data-subject="geburtsmonat" data-postProcess="formatDateNumber"></span>.' +
                            '<span id="idatBirthYear" data-tempid="' + row.psn + '" data-subject="geburtsjahr" data-postProcess="formatDateNumber"></span>';
                  },
                    "orderable": false
                },
                {
                    "data": "location",
                    "orderable": false
                }
                // // Leave the action column out for now...
                // ,
                // { "render": function (data, type, row, meta ) {
                //         if (row.mayWrite) {
                //             return '<span class="fa fa-flask"></span>';
                //         }
                //         if (row.mayDeleteCase) {
                //             return '<span class="fa fa-edit"></span>';
                //         }
                //         // Without returning an empty String, the datatables plugin issues a warning
                //         if (!row.mayWrite && !row.mayDeleteCase) {
                //             return ' ';
                //         }
                //     },
                //     "orderable": false
                // }
            ],
            "fnDrawCallback": function(oSettings) {
                $('.amount-of-patients').text(oSettings.json.recordsTotal);
                tempIdResolver.resolve(this);
            },
            language: {
                "url":"javax.faces.resource/js/datatable_" + languageSetting + ".json"
            }
        } );
    }

    var str;

    if (getCookie("lang") == "en") str = "Copied!";
    if (getCookie("lang") =="de") str = "Kopiert!";

    var clipboard = new Clipboard('.clipboard', {
        text: function(trigger) {
            return $(trigger).parents("tr").find("[data-subject='pid']").text();
        }
    });

    clipboard.on('success', function(e) {
        $($(e.trigger).parents("tr").find("[data-subject='pid']")).tooltip({
            trigger: 'click',
            placement: 'bottom'
        });

        setTooltip($(e.trigger).parents("tr").find("[data-subject='pid']"), str);
        hideTooltip($(e.trigger).parents("tr").find("[data-subject='pid']"));
    });

    bindSwitchPatientHandlers();
});

function getCookie(name) {
    var value = "; " + document.cookie;
    var parts = value.split("; " + name + "=");
    if (parts.length == 2) return parts.pop().split(";").shift();
}

function setTooltip(btn, message) {
    $(btn).tooltip('hide')
        .attr('data-original-title', message)
        .tooltip('show');
}

function hideTooltip(btn) {
    setTimeout(function() {
        $(btn).tooltip('hide');
    }, 1000);
}

function bindSwitchPatientHandlers() {
    $('.patientListTable').on('click', '.patientLink', function () {
        var resourceUri = $(this).find('.resourceUri').text();
        switchPatient({ patientURI: resourceUri});
    })
}