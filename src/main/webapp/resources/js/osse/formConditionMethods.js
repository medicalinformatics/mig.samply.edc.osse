/**
 * Disabled the property of checkbox "indeterminate"
 * from mdrFaces -> formMdrField.xhtml -> Boolean.
 */
$(function() {
  $("input[data-indeterminate='true']").prop("indeterminate", false);
});
/*
 * Define includes function for arrays if needed (e.g. in IE 11).
 * Taken from https://stackoverflow.com/questions/31221341/ie-does-not-support-includes-method
 */
if (!Array.prototype.includes) {
  Object.defineProperty(Array.prototype, "includes", {
    enumerable: false,
    value: function(obj) {
      var newArr = this.filter(function(el) {
        return el == obj;
      });
      return newArr.length > 0;
    }
  });
}
/**
 * Callback function to initiate modifications after Mainzelliste completed its
 * calls.
 */
var modificationsMzlCallback = function() {
  modificationsTempStoreAge();
  $(document).ready(function () {
    if (typeof formConditions !== "undefined" && formConditions !== null
        && typeof registryNamespace !== "undefined" && registryNamespace !== "") {
      processConditions(formConditions);
    }
    $('.datepicker').each(function (index, element) {
      $(element).find('input').data("DateTimePicker").useCurrent(false);
    });
    modificationsSetHpoFields();
  });
};

/**
 * Array to temporarily store values of hidden elements.
 *
 * @type {Array}
 */
var tempFormValues = [];

/**
 * Find the ancestor of a dataelement that represents the whole row or cell.
 *
 * @param obj The data element whose HTML ancestor is to be returned.
 * @returns {*|jQuery|null} The ancestor HTML object (null if not found).
 */
function conditionsFindRow(obj) {
  var result = obj.parentsUntil("div.row").parent(".row");
  if (result.length == 0) {
    result = obj.closest("td");
  }

  return result;
}

/**
 * Extracts the current form id from the url of the page.
 *
 * @returns {*|jQuery|undefined} The current form's id, taken from its HTML.
 */
function conditionsGetCurrentFormId() {
  return $('#topFormPanel').attr("data-formid");
}

/**
 * Conditions for data elements.
 *
 * Parameter format:
 * conditions = {
 *     "urn:osse-x:datalement:y:": {
 *         "urn:osse-x:datalement:z:": "v"
 *     }
 * }
 * y is the id of the data element to be hidden. z is the id of the data element
 * that y depends on. v is the value, that
 * z has to have for y to be visible. Version number should be omitted.
 *
 * @param modifications The modifications object as defined in file
 * formConditions.js.
 */
function processConditions(modifications) {
  if (modifications.forms.hasOwnProperty(conditionsGetCurrentFormId())) {
    var currentFormModifications =
        modifications.forms[conditionsGetCurrentFormId()];

    // process value temp store
    if (currentFormModifications.hasOwnProperty("storeValue")) {
      modificationsProcessStoreValues(registryNamespace,
          conditionsGetCurrentFormId(),
          currentFormModifications.storeValue);
    }

    // process default values
    if (currentFormModifications.hasOwnProperty("defaultValue")) {
      modificationsProcessDefaultValues(currentFormModifications.defaultValue);
    }

    // conditional visibility
    if (currentFormModifications.hasOwnProperty("conditionalVisibility")) {
      // Remove existing event handlers for handling form conditions
      // to prevent duplicate handlers in case of multiple calls to
      // processFormConditions (which happens when adding a tab to a
      // repeatable record).
      $("#topFormPanel").off("change.formConditions");
      currentFormModifications.conditionalVisibility.forEach(
          function (conditionalVisibility) {
            // The data element that is to be hidden or shown.
            var conditionalElement;
            var conditionalElementHtml;
            // process conditional visibility of data elements
            if (conditionalVisibility.hasOwnProperty("dataElement")) {
              // check if conditional data element is part of a record
              if (conditionalVisibility.hasOwnProperty("record")) {
                conditionalElement = $dataElement(registryNamespace,
                    conditionalVisibility.dataElement,
                    conditionalVisibility.record);
              } else {
                conditionalElement = $dataElement(registryNamespace,
                    conditionalVisibility.dataElement);
              }
              if (conditionalElement !== undefined) {
                // if it's a single element, result is a div.row
                // if it's part of a repeatable, result is td
                conditionalElementHtml = conditionsFindRow(conditionalElement);
              }
            } // process conditional visibility of records
            else if (conditionalVisibility.hasOwnProperty("record")
                && !conditionalVisibility.hasOwnProperty("dataElement")) {
              conditionalElement = $record(registryNamespace,
                  conditionalVisibility.record);
              conditionalElementHtml = conditionalElement;
            }

            // process conditions
            var elementsToCheck = [];
            var storedElementsToCheck = [];
            var ageCheck = false;
            var conditionString = "";
            var conditionExpression = null;
            var conditionExpressionType = null;
            if (conditionalVisibility.hasOwnProperty("any")) {
              conditionExpression = conditionalVisibility.any;
              conditionExpressionType = "any";
            } else if (conditionalVisibility.hasOwnProperty("all")) {
              conditionExpression = conditionalVisibility.all;
              conditionExpressionType = "all";
            }
            if (conditionExpression != null) {
              var conditions = null;
              var conditionType = null;
              ["eq", "lt", "gt", "neq"].forEach(function (item, index) {
                if (conditionExpression.hasOwnProperty(item)) {
                  conditions = conditionExpression[item];
                  conditionType = item;

                  if (conditions != null) {
                    conditions.forEach(function (condition) {
                      if (condition.hasOwnProperty("form")) {
                        // if it's a data element on another form, that value has to
                        // be taken from the local storage
                        var conditionValueFromStorage;
                        if (condition.hasOwnProperty("record")) {
                          conditionValueFromStorage = modificationsGetStoredValue(
                              condition.form, condition.record,
                              condition.dataElement);
                        } else {
                          conditionValueFromStorage = modificationsGetStoredValue(
                              condition.form, null, condition.dataElement);
                        }
                        if (conditionValueFromStorage != undefined) {
                          // keep an array of all elements that are part of the
                          // condition
                          storedElementsToCheck.push(
                              modificationsGetUrnDataElement(registryNamespace,
                                  condition.dataElement));
                          // build condition string for eval()
                          conditionString = modificationsAppendConditionOperator(
                              conditionString, conditionExpressionType);
                          conditionString += "'" + conditionValueFromStorage + "' ";
                          conditionString += modificationsGetOperator(conditionType);
                          conditionString += " '" + condition.value + "'";
                        }
                      } else if (condition.hasOwnProperty("dataElement")) {
                        var conditionElement = $dataElement(registryNamespace,
                            condition.dataElement);
                        // if part of a repeatable record, the context (= row or tab of that entry) has to be given
                        var conditionElementString;
                        if (condition.hasOwnProperty("record")) {
                          if (modificationsRecordIsRepeatable(
                              $record(registryNamespace, condition.record))) {
                            // if the data element is part of a repeatable record,
                            // its row or tab needs to be identified
                            conditionElementString =
                                "$dataElement(registryNamespace, "
                                + condition.dataElement
                                + ", " + "null" // ignore the record because we use the row/tab as context
                                + ", " + "conditionalElement.closest('.repeatable-record-instance')" + ")";
                          } else {
                            conditionElementString =
                                "$dataElement(registryNamespace, "
                                + condition.dataElement
                                + ", " + condition.record + ")";
                          }
                        } else {
                          conditionElementString =
                              "$dataElement(registryNamespace, "
                              + condition.dataElement + ")";
                        }
                        if (conditionalElement.length > 0
                            && conditionElement.length > 0) {
                          // keep an array of all elements that are part of the
                          // condition
                          elementsToCheck.push(conditionElement);
                          // build a boolean expression to evaluate the condition
                          conditionString = modificationsAppendConditionOperator(
                              conditionString, conditionExpressionType);
                          conditionString += modificationsGetConditionString(
                              conditionElement, conditionElementString,
                              conditionType, condition.value);
                        }
                      } else if (condition.hasOwnProperty("age")
                          || condition.hasOwnProperty("ageAtEpisode")) {
                        // the patient's age has to be taken from the local
                        // storage
                        conditionString = modificationsAppendConditionOperator(
                            conditionString, conditionExpressionType);
                        var conditionValueFromStorage;
                        // choose age (method) according to definition (in months or in years, current age or age at episode date)
                        if (condition.hasOwnProperty("unit")) {
                          if (condition.unit == "days") {
                            if (condition.hasOwnProperty("age")) {
                              conditionValueFromStorage = modificationsGetPatientAgeDays();
                            } else if (condition.hasOwnProperty("ageAtEpisode")) {
                              conditionValueFromStorage = modificationsGetPatientAgeAtEpisodeDays();
                            }
                          } else if (condition.unit == "months") {
                            if (condition.hasOwnProperty("age")) {
                              conditionValueFromStorage = modificationsGetPatientAgeMonths();
                            } else if (condition.hasOwnProperty("ageAtEpisode")) {
                              conditionValueFromStorage = modificationsGetPatientAgeAtEpisodeMonths();
                            }
                          } else {
                            if (condition.hasOwnProperty("age")) {
                              conditionValueFromStorage = modificationsGetPatientAgeYears();
                            } else if (condition.hasOwnProperty("ageAtEpisode")) {
                              conditionValueFromStorage = modificationsGetPatientAgeAtEpisodeYears();
                            }
                          }
                        }
                        if (conditionValueFromStorage != undefined) {
                          // build condition string for eval()
                          conditionString += "'" + conditionValueFromStorage + "' ";
                          conditionString += modificationsGetOperator(conditionType);
                          // current age or age at episode date
                          if (condition.hasOwnProperty("age")) {
                            conditionString += " '" + condition.age + "'";
                          } else if (condition.hasOwnProperty("ageAtEpisode")) {
                            conditionString += " '" + condition.ageAtEpisode + "'";
                          }
                          ageCheck = true;
                        }
                      }
                    });
                  }
                }
              });
            }
            // have all relevant elements execute the check once and every time
            // they change
            storedElementsToCheck.forEach(function () {
              // if there's more than one element, it's part of a repeatable record
              conditionalElement.each(function (index, element) {
                modificationsConditionCheck($(element),
                    $(conditionalElementHtml[index]), conditionString);
              });
            });
            elementsToCheck.forEach(function (elementToCheck) {
              // if there's more than one element, it's part of a repeatable record
              conditionalElementHtml.each(function (index, elementHtml) {
                // conditionalElement may contain inputs belonging to multiple
                // instances of a repeatable record. Filter so that only those
                // are considered that belong to the current instance.
                // In case of a whole record, conditionalElement and elementHtml
                // are the same, this must considered also.
                var filteredConditionalElement = $(elementHtml).is(conditionalElement) ?
                    conditionalElement : $(elementHtml).find(conditionalElement);
                modificationsConditionCheck(filteredConditionalElement, $(elementHtml),
                    // conditionString contains event which does not exist in this case, doing a string replace works but is kind of dirty
                    conditionString.replace(/\$\(event.target\)/g,
                        "conditionalElement"));
              });
              $("#topFormPanel").on("change.formConditions", elementToCheck.selector, function (event) {
                // seems like conditionalElement and conditionalElementHtml need to be re-fetched
                conditionalElement.each(function (index, element) {
                  var conditionalElement = $(element);
                  // If both elements (the one to check and the one to show/hide)
                  // are part of a repeatable record, the following block ensures
                  // that the condition check only happens within each instance
                  // of the repeatable record
                  if (modificationsIsDataElementPartOfRecord(conditionalElement,
                      true) && modificationsIsDataElementPartOfRecord($(event.target))) {
                    // re-load elements in case the dataTable changed
                    conditionalElement = $dataElement(registryNamespace,
                        conditionalVisibility.dataElement,
                        conditionalVisibility.record);
                    if (conditionalElement !== undefined) {
                      conditionalElementHtml = conditionsFindRow(
                          conditionalElement);
                    }
                    // execute condition check
                    modificationsConditionCheck(
                        conditionalElement.filter(function () {
                          return $(this).closest(".repeatable-record-instance").is(
                              $(event.target).closest(".repeatable-record-instance"))
                        }),
                        conditionalElementHtml.filter(function () {
                          return $(this).closest(".repeatable-record-instance").is(
                              $(event.target).closest(".repeatable-record-instance"))
                        }), conditionString);
                  } else {
                    // not part of a repeatable record
                    modificationsConditionCheck(conditionalElement,
                        conditionalElementHtml, conditionString);
                  }
                });
              });
            });
            // the age doesn't change, so the check needs to be executed only once
            if (ageCheck) {
              conditionalElement.each(function (index, element) {
                if (modificationsIsDataElementPartOfRecord(conditionalElement,
                    true)) {
                  $("#topFormPanel").on("draw.dt", ".dataTable", function () {
                    conditionalElement = $dataElement(registryNamespace,
                        conditionalVisibility.dataElement,
                        conditionalVisibility.record);
                    if (conditionalElement !== undefined) {
                      conditionalElementHtml = conditionsFindRow(
                          conditionalElement);
                    }
                    // execute condition check
                    conditionalElement.each(function (index, element) {
                      modificationsConditionCheck($(element),
                          $(conditionalElementHtml[index]), conditionString);
                    });
                  });
                }
                modificationsConditionCheck($(element),
                    $(conditionalElementHtml[index]), conditionString);
              });
            }
          });
    }

    // process deactivation
    if (currentFormModifications.hasOwnProperty("disable")) {
      var disable = currentFormModifications.disable;
      if (disable.hasOwnProperty("dataElements")) {
        disable.dataElements.forEach(function (id) {
          $dataElement(registryNamespace, id).prop("disabled", true);
        });
      }
      if (disable.hasOwnProperty("records")) {
        disable.records.forEach(function (id) {
          $record(registryNamespace, id).find(":input").prop("disabled", true);
        });
      }
    }

    // process making fields read-only
    if (currentFormModifications.hasOwnProperty("readOnly")) {
      var readOnly = currentFormModifications.readOnly;
      if (readOnly.hasOwnProperty("dataElements")) {
        readOnly.dataElements.forEach(function (id) {
          $dataElement(registryNamespace, id).prop("readonly", true);
        });
      }
      if (readOnly.hasOwnProperty("records")) {
        readOnly.records.forEach(function (id) {
          $record(registryNamespace, id).find(":input").prop("readonly", true);
        });
      }
    }
    // process hiding
    if (currentFormModifications.hasOwnProperty("hide")) {
      var hide = currentFormModifications.hide;
      if (hide.hasOwnProperty("dataElements")) {
        hide.dataElements.forEach(function (id) {
          var elementToHide = conditionsFindRow($dataElement(registryNamespace, id));
          if (elementToHide.is('td')) {
            // If element to be hidden is part of a table, only hide the
            // contents of the table cell - otherwise the table layout gets
            // messed up.
            elementToHide.children().hide();
          } else {
            elementToHide.hide();
          }
        });
      }
      if (hide.hasOwnProperty("records")) {
        hide.records.forEach(function (id) {
          var recordToHide = conditionsFindRow($record(registryNamespace, id));
          recordToHide.hide();
        });
      }
    }

  }
}

/**
 * Checks the predefined condition.
 *
 * @param conditionalElement The element to hide or show.
 * @param conditionalElementHtml The HTML representation of the element to hide or show.
 * @param conditionString The string to execute.
 * @param event The event that triggered the form condition to check.
 */
function checkCondition(conditionalElement, conditionalElementHtml, conditionString, event) {
  if (conditionalElement.length > 1) {
    // part of a repeatable record
    // Filter elements to make sure that conditions are only processed
    // for elements that are within the same instance of the repeatable
    // record (identified by class "repeatable-record-instance")
    modificationsConditionCheck(conditionalElement.filter(function(){
      return $(this).closest(".repeatable-record-instance")
        .is($(event.target).closest(".repeatable-record-instance"))}),
        conditionalElementHtml.filter(function(){
          return $(this).closest(".repeatable-record-instance")
          .is($(event.target).closest(".repeatable-record-instance"))}),
        conditionString);
  } else {
    // not part of a repeatable record
    modificationsConditionCheck(conditionalElement,
        conditionalElementHtml, conditionString);
  }
}

/**
 * Appends a logical operator to the condition string according to the condition's definition.
 *
 * @param conditionString The current condition string.
 * @param conditionExpressionType The type of the condition.
 * @returns {*} The new condition string.
 */
function modificationsAppendConditionOperator(conditionString, conditionExpressionType) {
  if (conditionString.length > 0) {
    if (conditionExpressionType == "any") {
      conditionString += " || ";
    } else if (conditionExpressionType == "all") {
      conditionString += " && ";
    }
  }
  return conditionString;
}

/**
 * Sets the value of a data element.
 *
 * @param defaultValueDefinition The definition of the default values for the
 * current form.
 */
function modificationsProcessDefaultValues(defaultValueDefinition) {
  defaultValueDefinition.forEach(function (element) {
    if (element.hasOwnProperty("dataElement")
        && element.hasOwnProperty("value")) {
      var recordUrnSelector = "";
      if (element.hasOwnProperty("record")) {
        recordUrnSelector = "*[data-mdrid^='" + "urn:" + registryNamespace
            + ":record:" + element.record + ":']";
      }
      // Selector for finding an element within a record, i.e. the
      // record context is not part of the selector but handled outside
      var dataElementWithinRecordSelector = " *[data-mdrid^='" + "urn:"
          + registryNamespace + ":dataelement:" + element.dataElement + ":']"
      // Selector for finding the element from global context, i.e. record is
      // part of the selector
      var dataElementSelector = recordUrnSelector + dataElementWithinRecordSelector;
      // Loop over all instances of the current data element
      $(dataElementSelector).each(function() {
        var dataElement = $(dataElementSelector);
        // Find out if element is part of a repeatable record. If yes, the set
        // of elements (multiple in case of checkboxes and radio buttons) is
        // reduced to the one within the current instance of the repeatable.
        var repeatableRecordContext = $(this).closest(".repeatable-record-instance");
        if (repeatableRecordContext.length > 0) {
          dataElement = repeatableRecordContext.find(dataElementWithinRecordSelector);
        }
        if (($(this).is("select")
            && $(this).val() == "")
            || (dataElement.filter("[type='radio']").length > 0
                && !dataElement.filter("[type='radio']:checked").val())
            || (dataElement.filter("[type='checkbox']").length > 0
                && !dataElement.filter("[type='checkbox']:checked").val())
            || dataElement.filter(":text").val() == "") {
          if (dataElement.filter("[type='checkbox']").length == 1
              && dataElement.filter("[type='checkbox']").prop(
                  "indeterminate")
              == true) {
            dataElement.prop("indeterminate", false);
            dataElement.prop("checked", element.value);
          } else {
            dataElement.val(element.value);
          }
        }
      });
    }
  });
}

/**
 * Returns the proper operator as string for a given condition type.
 *
 * @param conditionType One of ["eq", "neq", "lt", "gt"].
 * @returns {string|null} The operator as string or null if something went wrong.
 */
function modificationsGetOperator(conditionType) {
  switch (conditionType) {
    case "eq":
      return "==";
      break;
    case "lt":
      return "<";
      break;
    case "gt":
      return ">";
      break;
    case "neq":
      return "!=";
      break;
    default:
  }
  return null;
}

/**
 * Returns a string representation of a boolean expression for a simple equals
 * condition.
 *
 * @param conditionElement The element whose value is to be compared.
 * @param conditionElementString A string representation of conditionElement.
 * @param conditionType The operator of the condition as string (eq, neq, lt, gt).
 * @param value The value conditionElement needs to have for the boolean
 * expression to be true.
 */
function modificationsGetConditionString(conditionElement,
    conditionElementString, conditionType, value) {
  var result = "";
  if (conditionElement.is("select")) {
    result += "(" + conditionElementString
        + ".children('option:selected').val() ";
    result += modificationsGetOperator(conditionType);
    result += " '" + value + "')";
  } else if (conditionElement.is("input[type='text']")) {
    result += "(" + conditionElementString + ".val() ";
    result += modificationsGetOperator(conditionType);
    result += " '" + value + "')";
  } else if (conditionElement.is("input[type='checkbox']")
      || conditionElement.is("input[type='radio']")) {
    result += "(" + conditionElementString
        + ".filter(':checked').map(function(){return $(this).val();}).get()"
        + ".includes('" + value + "')" + ")";
  }

  return result;
}

/**
 * Evaluates a boolean expression and calls the method to show or hide an
 * element accordingly.
 *
 * @param conditionalElement The element that needs to be shown or hidden.
 * @param conditionalElementHtml The actual HTML code that needs to be shown or
 * hidden.
 * @param conditionString A string containing the conditions for
 * conditionalElement.
 */
function modificationsConditionCheck(conditionalElement, conditionalElementHtml,
    conditionString) {
  if (conditionalElement.length == 0 || conditionalElementHtml.length == 0)
    return;
  if (eval(conditionString)) {
    conditionsShowElement(conditionalElementHtml, conditionalElement);
  } else {
    conditionsHideElement(conditionalElementHtml, conditionalElement);
  }
}

/**
 * Hide a specific element, store its value and clear it.
 *
 * @param elementToHide The data element to hide.
 * @param elementWithValue
 */
function conditionsHideElement(elementToHide, elementWithValue) {
  // determine visibility according to type of element
  var isVisible = false;
  if (elementToHide.is("div.row") || elementToHide.is("div.abschnitt")) {
    isVisible = !elementToHide.hasClass('formconditions-hidden');
  } else if (elementToHide.is("td")) {
    elementToHide.find("input, select").each(function () {
      if ($(this).prop("disabled") == false) {
        isVisible = true;
      }
    });
  } else {
    console.log("Element to hide has unknown type.");
  }

  if (isVisible) {
    conditionsStoreHiddenValue(elementWithValue);
    if (elementToHide.is("div.row") || elementToHide.is("div.abschnitt")) {
      elementToHide.hide();
    } else if (elementToHide.is("td")) {
      elementToHide.find("input, select").prop("disabled", true);
    } else {
      console.log("Element to hide has unknown type.");
    }
    elementToHide.addClass('formconditions-hidden');
  }
}

/**
 * Temporarily store the value of an element clear the element.
 * Accepts any of the following:
 *   * A simple input element.
 *   * A set of radio buttons or checkboxes (belonging to one data element).
 *   * A container (e.g. table) containing a set of radio buttons or check boxes.
 *   * A record div (in this case the function recurses into
 *     the record's members).
 * @param elementWithValue The input element whose value to store.
 */
function conditionsStoreHiddenValue(elementWithValue) {
  if (elementWithValue.is("div[data-mdrid*=':record:']")) {
    // If called on a record, recurse into members of the record
    conditionsRecurseIntoRecord(elementWithValue, conditionsStoreHiddenValue);
  }
  // If there are checkboxes or radio buttons in this element
  // (in which case it is the surrounding table), these input elements
  // have to be processed, otherwise the code to store the selected option(s)
  // does not work.
  if (elementWithValue.find(':checkbox, :radio').length > 0) {
    elementWithValue = elementWithValue.find(':checkbox, :radio');
  }
  // Check if the value already has been stored (can be the case
  // when an element is the target of several conditions. Checking
  // for a stored value is more robust than checking for visibility.
  // This comes after the code block above where radio buttons or check boxes
  // are determined because otherwise getTempFormIndex does not work.
  if (getTempFormIndex(elementWithValue) in tempFormValues) {
    return;
  }
  var oldValue;
  if (elementWithValue.is("select")) {
    oldValue = elementWithValue.val();
    elementWithValue.val(null).trigger("change");
  } else if (elementWithValue.is("input[type='text']")) {
    oldValue = elementWithValue.val();
    elementWithValue.val("").trigger("change");
  } else if (elementWithValue.is("input[type='checkbox']")
      || elementWithValue.is("input[type='radio']")) {
    if (elementWithValue.prop("indeterminate") == true) {
      oldValue = "indeterminate";
    } else {
      oldValue = elementWithValue.filter(":checked").map(function () {
        return $(this).val();
      }).get();
    }
    elementWithValue.filter(":checked").removeAttr("checked").trigger("change");
  }
  tempFormValues[getTempFormIndex(elementWithValue)] = oldValue;
}
/**
 * Show a hidden element and restore the value.
 *
 * @param elementToShow The data element to show.
 * @param elementWithValue
 */
function conditionsShowElement(elementToShow, elementWithValue) {
  // determin visibility according to type of element
  var isHidden = false;
  if (elementToShow.is("div.row") || elementToShow.is("div.abschnitt")) {
    isHidden = elementToShow.hasClass('formconditions-hidden');
  } else if (elementToShow.is("td")) {
    elementToShow.find("input, select").each(function () {
      if ($(this).prop("disabled") == true) {
        isHidden = true;
      }
    });
  } else {
    console.log("Element to show has unknown type.");
  }

  if (isHidden) {
    conditionsRestoreHiddenValue(elementWithValue);
    if (elementToShow.is("div.row") || elementToShow.is("div.abschnitt")) {
      elementToShow.show();
    } else if (elementToShow.is("td")) {
      elementToShow.find("input, select").prop("disabled", false);
    } else {
      console.log("Element to show has unknown type.");
    }
    elementToShow.removeClass('formconditions-hidden');
  }
}

/**
 * Restore a temporarily stored value to a data element.
 * Accepts any of the following:
 *   * A simple input element.
 *   * A set of radio buttons or checkboxes (belonging to one data element).
 *   * A container (e.g. table) containing a set of radio buttons or check boxes.
 *   * A record div (in this case the function recurses into
 *     the record's members).
 * @param elementWithValue The input element to restore.
 */
function conditionsRestoreHiddenValue(elementWithValue) {
  if (elementWithValue.is("div[data-mdrid*=':record:']")) {
    // If called on a record, recurse into members of the record
    conditionsRecurseIntoRecord(elementWithValue, conditionsRestoreHiddenValue);
  }
  // If there are checkboxes or radio buttons in this element
  // (in which case it is the surrounding table), process these input elements,
  // otherwise getTempFormIndex will not work
  if (elementWithValue.find(':checkbox, :radio').length > 0) {
    elementWithValue = elementWithValue.find(':checkbox, :radio');
  }
  // Restore only if there is actually a value to restore
  // This check is needed to make sure an already restored value is not
  // deleted in case this function is called again on the element
  // (which happens if the element is in a record and the element itself
  // and also the record as a whole are target of the condition).
  if (!(getTempFormIndex(elementWithValue) in tempFormValues)) {
    return;
  }
  if (elementWithValue.is("select")) {
    elementWithValue.val(
        tempFormValues[getTempFormIndex(elementWithValue)]).trigger(
        "change");
  } else if (elementWithValue.is("input[type='text']")) {
    elementWithValue.val(
        tempFormValues[getTempFormIndex(elementWithValue)]).change();
  } else if (elementWithValue.is("input[type='checkbox']")
      || elementWithValue.is("input[type='radio']")) {
    if (tempFormValues[getTempFormIndex(elementWithValue)] !== "indeterminate") {
      $(tempFormValues[getTempFormIndex(elementWithValue)]).each(
          function () {
            elementWithValue.filter("[value='" + this + "']").prop("checked",
                true).trigger("change");
          });
    }
  }
  // Delete stored value (needed so that check for already stored value
  // in conditionsStoreHiddenValue works
  delete tempFormValues[getTempFormIndex(elementWithValue)];
}

/**
 * Call the given function for every member of a record.
 * Searches for data elements (input with mdr-id) and calls the callback
 * function on on them. If the record is a repeatable records, the calls are
 * made individually for each entry (tab/row) in the record. Groups of
 * inputs with the same mdr-id (radio buttons and checkboxes) are handled in
 * one call to the callback function.
 *
 * @param record The record in which to recurse.
 * @param callback The callback function to call for every input or input group.
 */
function conditionsRecurseIntoRecord(record, callback) {
  // First, determine the set of URNs of data elements in the record
  var dataElementUrns = $.unique(record.find('[data-mdrid*=":dataelement:"]')
  .map(function() {return $(this).attr('data-mdrid');}));
  // Next, determine the context in which to handle data elements, either
  // the record div for a non-repeatable record...
  var context = record;
  if (record.find('.repeatable-record').length > 0) {
    // ...or, if it is a repeatable record, each instances (rows/tabs) on its own
    context = record.find('.repeatable-record-instance');
  }
  // Recurse within every context (only one for a non-repeatable record,
  // possibly several for a repeatable record)
  context.each(function(index, thisContext) {
    // Do recursion separately by URN, so that radiobutton or checkbox groups are handled together
    dataElementUrns.each(function(index, urn) {
      callback($(':input[data-mdrid="' + urn + '"]', thisContext));
    });
  });
}
/**
 * Get the index under which to store the value of an element.
 * Used for storing the values of elements while they are hidden. The former
 * mechanism of using the URN did not work for members of repeatable records
 * (as the same URN  appears multiple times when the repeatable records has
 * multiple entries).
 *
 * @param elementWithValue Input element as jQuery object.
 */
function getTempFormIndex(elementWithValue) {
  // For checkboxes or radiobuttons, use the id of the surrounding table,
  if (elementWithValue.is('[type=checkbox]') || elementWithValue.is('[type=radio]')) {
    return elementWithValue.closest('table').attr('id');
  } else {
    // Otherwise, use the id of the element
    return elementWithValue.attr('id');
  }
}

/**
 * Processes definitions of form values to be stored.
 *
 * @param registryNamespace The current MDR namespace.
 * @param formId The ID of the current form.
 * @param storeValueDefinition The json part containing the definition of values
 * to store from the current form.
 */
function modificationsProcessStoreValues(registryNamespace, formId,
    storeValueDefinition) {
  storeValueDefinition.forEach(function (element) {
    var recordUrnSelector = "";
    if (element.hasOwnProperty("dataElement")) {
      if (element.hasOwnProperty("record")) {
        recordUrnSelector = "*[data-mdrid^='" + "urn:" + registryNamespace
            + ":record:" + element.record + ":']";
      }
      var dataElementSelector = recordUrnSelector + " *[data-mdrid^='" + "urn:"
          + registryNamespace + ":dataelement:" + element.dataElement + ":']";

      // store value when loading form
      modificationsTempStoreValue(formId, element, dataElementSelector,
          recordUrnSelector);
      // store value on save form event
      $("#save").on("click", function () {
        modificationsTempStoreValue(formId, element, dataElementSelector,
            recordUrnSelector);
      });
    }
  });
}

/**
 * Returns whether the current form is an episode form.
 *
 * @returns {boolean} TRUE when the current form is an episode form, FALSE otherwise.
 */
function modificationsGetIsEpisodeForm() {
  return $("#topFormPanel").attr("data-isepisodeform") == "true";
}

/**
 * Returns the name of the current episode.
 *
 * @returns {*|jQuery} Name of the current episode. Undefined if not in episode.
 */
function modificationsGetEpisodeName() {
  return $("#topFormPanel").attr("data-episodename");
}

/**
 * Returns the URN of a data element.
 *
 * @param namespace The namespace of the data element.
 * @param id The ID of the data element.
 * @param version The version number of the data element. Optional.
 * @returns {string} The complete URN of the data element.
 */
function modificationsGetUrnDataElement(namespace, id, version) {
  if (version === undefined)
    version = "";
  return "urn:" + namespace + ":dataelement:" + id + ":" + version;
}

/**
 * Returns the URN of a record.
 *
 * @param namespace The namespace of the record.
 * @param id The ID of the record.
 * @param version The version number of the record. Optional.
 * @returns {string} The complete URN of the record.
 */
function modificationsGetUrnRecord(namespace, id, version) {
  if (version === undefined)
    version = "";
  return "urn:" + namespace + ":record:" + id + ":" + version;
}

/**
 * Returns the HTML element of a specific data element.
 *
 * @param namespace The namespace of the data element.
 * @param id The ID of the data element.
 * @param recordId The ID of the record containing the data element (optional).
 * @param context The part of the DOM to look for the data element (optional).
 * @returns {jQuery|HTMLElement} The HTML element for the specific URN.
 */
function $dataElement(namespace, id, recordId, context) {
  var selector = "";

  if (recordId !== undefined && recordId != null) {
    selector += "[data-mdrid^='"
        + modificationsGetUrnRecord(namespace, recordId) + "'] ";
  }
  selector += "[data-mdrid^='"
      + modificationsGetUrnDataElement(namespace, id) + "']";

  if (context !== undefined && context == null) {
    return $(selector);
  } else {
    return $(selector, context);
  }
}

/**
 * Returns the HTML element of a specific record.
 *
 * @param namespace The namespace of the record.
 * @param id The ID of the record.
 * @param version The version number of the record (optional).
 * @returns {jQuery|HTMLElement} The HTML element for the specific URN.
 */
function $record(namespace, id, version) {
  if (version === undefined)
    version = "";
  return $("[data-mdrid" + ((!version) ? "^" : "") + "='"
      + modificationsGetUrnRecord(namespace, id, version) + "']");
}

/**
 * Checks if a record is repeatable.
 *
 * @param record A jQuery object of the record.
 * @returns {boolean} True if the record is repeatable, false otherwise.
 */
function modificationsRecordIsRepeatable(record) {
  if (record.find(".repeatable-record").length > 0) {
    return true;
  } else {
    return false;
  }
}

/**
 * Returns the currently stored temp form values as json.
 *
 * @returns {string} The temporarily stored form value json.
 */
function modificationsGetOsseTempFormStorage() {
  var osseTempFormStorage = JSON.parse(
      localStorage.getItem("osseTempFormStorage"));
  if (osseTempFormStorage == null) {
    osseTempFormStorage = {};
  }
  return osseTempFormStorage;
}

/**
 * Sets the temporarily stored form value json in local storage.
 *
 * @param osseTempFormStorage A json string containing the form values.
 */
function modificationsSetOsseTempFormStorage(osseTempFormStorage) {
  localStorage.setItem("osseTempFormStorage",
      JSON.stringify(osseTempFormStorage));
}

/**
 * Calculate the age of the patient from the date of birth given in a patient
 * form. The value is stored in the localStorage. If there is no date of birth
 * available no values are stored.
 */
function modificationsTempStoreAge() {
  if ($("div.birth").length > 0) {
    var osseTempFormStorage = modificationsGetOsseTempFormStorage();
    // current age
    var age = moment.duration(moment().diff(modificationsGetPatientDob()));
    osseTempFormStorage.patientAgeYears = Math.floor(age.asYears());
    osseTempFormStorage.patientAgeMonths = Math.floor(age.asMonths());
    osseTempFormStorage.patientAgeDays = Math.floor(age.asDays());
    modificationsSetOsseTempFormStorage(osseTempFormStorage);
  }
}

/**
 * Gets the patient's date of birth from the page's content.
 * If the current user may not see IDAT, the method returns NULL.
 *
 * @returns {null|*} A Moment object representing the patient's DOB. NULL if no DOB available.
 */
function modificationsGetPatientDob() {
  if ($("div.birth").length > 0) {
    return moment(
        [$("#idatBirthYear").text(), $("#idatBirthMonth").text() - 1,
          $("#idatBirthDay").text()]);
  } else {
    return null;
  }
}

/**
 * Gets the current episode's date from the page's content.
 * If the episode has no specific date (ie. quarters or indexed), the method returns NULL.
 *
 * @returns {null|*} A Moment object representing the episode's date. NULL if no date available.
 */
function modificationsGetEpisodeDate() {
  if ($("a.tab.current-episode").length > 0) {
    var episodeDateText = $("a.tab.current-episode").text();
    var regexIso = /(\d{4}-\d{2}-\d{2})/g;
    var regexSlash = /(\d{2}\/\d{2}\/\d{4})/g;
    if (episodeDateText.match(regexSlash)) {
      return moment(episodeDateText.match(regexSlash), "DD/MM/YYYY");
    } else if (episodeDateText.match(regexIso)) {
      return moment(episodeDateText.match(regexIso), "YYYY-MM-DD");
    } else {
      return null;
    }
  } else {
    return null;
  }
}

/**
 * Stores the value of a data element in the local storage.
 *
 * @param formId The ID of the form which contains the data element.
 * @param element The definition of the element from the conditions object.
 * @param dataElementSelector The jQuery selector for the data element.
 * @param recordUrnSelector The jQuery selector for the record (can be omitted).
 */
function modificationsTempStoreValue(formId, element, dataElementSelector,
    recordUrnSelector) {
  // case or episode?
  var formSpace = (modificationsGetIsEpisodeForm())?"episodeForm":"caseForm";
  var episodeName = modificationsGetEpisodeName();
  if (recordUrnSelector === undefined)
    recordUrnSelector = "";
  var osseTempFormStorage = modificationsGetOsseTempFormStorage();
  // check if storage initialised
  if (!osseTempFormStorage.hasOwnProperty(formSpace)) {
    osseTempFormStorage[formSpace] = {};
  }
  let currentStore;
  if (modificationsGetIsEpisodeForm()) {
    if (!osseTempFormStorage[formSpace].hasOwnProperty(episodeName)) {
      osseTempFormStorage[formSpace][episodeName] = {};
    }
    if (!osseTempFormStorage[formSpace][episodeName].hasOwnProperty(formId)) {
      osseTempFormStorage[formSpace][episodeName][formId] = {};
    }
    currentStore = osseTempFormStorage[formSpace][episodeName][formId];
  } else {
    if (!osseTempFormStorage[formSpace].hasOwnProperty(formId)) {
      osseTempFormStorage[formSpace][formId] = {};
    }
    currentStore = osseTempFormStorage[formSpace][formId];
  }
  let valueToStore;
  if ($(dataElementSelector).is("input[type=radio]")) {
    valueToStore = $(dataElementSelector).find(":checked").val();
  } else if ($(dataElementSelector).is("input[type=checkbox]")) {
    valueToStore = [];
    $(dataElementSelector).find(":checked").each(function(index){
      valueToStore[index] = $(this).val();
    });
  } else {
    valueToStore = $(dataElementSelector).val();
  }
  if (recordUrnSelector == "") {
    // no record
    currentStore[modificationsGetUrnDataElement(
        registryNamespace, element.dataElement, "")] = valueToStore;
  } else {
    // in record
    if (!currentStore.hasOwnProperty(
        modificationsGetUrnRecord(registryNamespace, element.record, ""))) {
      currentStore[modificationsGetUrnRecord(
          registryNamespace, element.record, "")] = {};
    }
    currentStore[modificationsGetUrnRecord(
        registryNamespace,
        element.record, "")][modificationsGetUrnDataElement(registryNamespace,
        element.dataElement, "")] = valueToStore;
  }
  if (modificationsGetIsEpisodeForm()) {
    osseTempFormStorage[formSpace][episodeName][formId] = currentStore;
  } else {
    osseTempFormStorage[formSpace][formId] = currentStore;
  }
  modificationsSetOsseTempFormStorage(osseTempFormStorage);
}

/**
 * Retrieve a single form value that was temporarily stored.
 * If the data element is not part of the record, pass null as parameter
 * recordId.
 *
 * @param formId The ID of the form which contains the data element.
 * @param recordId The ID of the record which contains the data element or null.
 * @param dataElementId The ID of the data element of which to retrieve the
 * value.
 * @returns {*} The value if it was found.
 */
function modificationsGetStoredValue(formId, recordId, dataElementId) {
  var osseTempFormStorage = modificationsGetOsseTempFormStorage();
  var result = null;
  // current form case or episode?
  var formSpace;
  var episodeName = modificationsGetEpisodeName();
  if (modificationsGetIsEpisodeForm() == false) {
    formSpace = "caseForm"; // unknown which episode was opened last, so only check case forms
  } else if (modificationsGetIsEpisodeForm() == true
      && osseTempFormStorage.hasOwnProperty("episodeForm")
      && osseTempFormStorage["episodeForm"].hasOwnProperty(episodeName)
      && osseTempFormStorage["episodeForm"][episodeName].hasOwnProperty(formId)) {
    formSpace = "episodeForm";
  } else if (modificationsGetIsEpisodeForm() == true
      && osseTempFormStorage.hasOwnProperty("caseForm")
      && osseTempFormStorage["caseForm"].hasOwnProperty(formId)) {
    formSpace = "caseForm";
  }

  if (modificationsGetIsEpisodeForm() == true) {
    if (osseTempFormStorage.hasOwnProperty(formSpace)
        && osseTempFormStorage[formSpace].hasOwnProperty(episodeName)
        && osseTempFormStorage[formSpace][episodeName].hasOwnProperty(formId)) {
      if (recordId != null
          && osseTempFormStorage[formSpace][episodeName][formId].hasOwnProperty(
              modificationsGetUrnRecord(registryNamespace, recordId, ""))) {
        result = osseTempFormStorage[formSpace][episodeName][formId]
            [modificationsGetUrnRecord(registryNamespace, recordId, "")]
            [modificationsGetUrnDataElement(registryNamespace, dataElementId, "")];
      } else {
        result = osseTempFormStorage[formSpace][episodeName][formId]
            [modificationsGetUrnDataElement(registryNamespace, dataElementId, "")];
      }
    }
  }
  if (modificationsGetIsEpisodeForm() == false || result === null) {
    formSpace = "caseForm";
    if (osseTempFormStorage.hasOwnProperty(formSpace)
        && osseTempFormStorage[formSpace].hasOwnProperty(formId)) {
      if (recordId != null
          && osseTempFormStorage[formSpace][formId].hasOwnProperty(
              modificationsGetUrnRecord(registryNamespace, recordId, ""))) {
        result = osseTempFormStorage[formSpace][formId]
            [modificationsGetUrnRecord(registryNamespace, recordId, "")]
            [modificationsGetUrnDataElement(registryNamespace, dataElementId, "")];
      } else {
        result = osseTempFormStorage[formSpace][formId]
            [modificationsGetUrnDataElement(registryNamespace, dataElementId, "")];
      }
    }
  }

  return result;
}

/**
 * Returns a Moment object representing the patient's age at a specific date.
 * Returns NULL if the user is not allowed to read IDAT.
 *
 * @param date The date to calculate the patient's age at. If date is NULL, the current date will be used. If date is empty, returns NULL.
 * @param dateFormat The date format (see https://momentjs.com/docs/#/parsing/string-format/, default: ISO "YYYY-MM-DD").
 * @param unit The unit of measurement (see https://momentjs.com/docs/#/displaying/difference/).
 * @returns {null|*} A Moment object representing the patient's age at a given date in the indicated unit, NULL if no DOB or date present.
 */
function modificationsGetPatientAgeAtDate(date, unit, dateFormat = "YYYY-MM-DD") {
  var dob = modificationsGetPatientDob();
  if (dob == null) {
    return null;
  }
  var dateObject;
  if (date == null) {
    dateObject = moment();
  } else if (date == "") {
    return null;
  } else {
    dateObject = moment(date, dateFormat);
  }
  return dateObject.diff(dob, unit);
}

/**
 * Returns the patient's age at a given date in full years.
 * Returns NULL if the user is not allowed to read IDAT or if date is empty.
 *
 * @param date The date to calculate the patient's age at. If date is NULL, the current date will be used.
 * @param dateFormat The date format (see https://momentjs.com/docs/#/parsing/string-format/, default: ISO "YYYY-MM-DD").
 * @returns {number} The patient's age in years.
 */
function modificationsGetPatientAgeYears(date, dateFormat = "YYYY-MM-DD") {
  return modificationsGetPatientAgeAtDate(date, "years", dateFormat);
}

/**
 * Returns the patient's age at a given date in full months.
 * Returns NULL if the user is not allowed to read IDAT or if date is empty.
 *
 * @param date The date to calculate the patient's age at. If date is NULL, the current date will be used.
 * @param dateFormat The date format (see https://momentjs.com/docs/#/parsing/string-format/, default: ISO "YYYY-MM-DD").
 * @returns {number} The patient's age in months.
 */
function modificationsGetPatientAgeMonths(date, dateFormat = "YYYY-MM-DD") {
  return modificationsGetPatientAgeAtDate(date, "months", dateFormat);
}

/**
 * Returns the patient's age at a given date in days (first day of life = 0 days).
 * Returns NULL if the user is not allowed to read IDAT or if date is empty.
 *
 * @param date The date to calculate the patient's age at. If date is NULL, the current date will be used.
 * @param dateFormat The date format (see https://momentjs.com/docs/#/parsing/string-format/, default: ISO "YYYY-MM-DD").
 * @returns {number} The patient's age in days.
 */
function modificationsGetPatientAgeDays(date, dateFormat = "YYYY-MM-DD") {
  return modificationsGetPatientAgeAtDate(date, "days", dateFormat);
}

/**
 * Returns the patient's age at the date of the episode in years or null if no episode / episode date available.
 *
 * @returns {string|null} Patient's age at episode in years.
 */
function modificationsGetPatientAgeAtEpisodeYears() {
  if ($("a.tab.current-episode").length > 0) {
    var episodeDate = modificationsGetEpisodeDate();
    if (episodeDate != null) {
      return modificationsGetPatientAgeAtDate(episodeDate, "years");
    } else {
      return null
    }
  } else {
    return null
  }
}

/**
 * Returns the patient's age at the date of the episode in months null if no episode / episode date available.
 *
 * @returns {string|null} Patient's age at episode in months.
 */
function modificationsGetPatientAgeAtEpisodeMonths() {
  if ($("a.tab.current-episode").length > 0) {
    var episodeDate = modificationsGetEpisodeDate();
    if (episodeDate != null) {
      return modificationsGetPatientAgeAtDate(episodeDate, "months");
    } else {
      return null
    }
  } else {
    return null
  }
}

/**
 * Returns the patient's age at the date of the episode in days or null null if no episode / episode date available.
 *
 * @returns {null|*}
 */
function modificationsGetPatientAgeAtEpisodeDays() {
  if ($("a.tab.current-episode").length > 0) {
    var episodeDate = modificationsGetEpisodeDate();
    if (episodeDate != null) {
      return modificationsGetPatientAgeAtDate(episodeDate, "days");
    } else {
      return null
    }
  } else {
    return null
  }
}

/**
 * Clears all the temporarily stored values from localStorage.
 */
function modificationsClearLocalStorage() {
  localStorage.removeItem("osseTempFormStorage");
}

/**
 * Returns true if the given data element is part of a (repeatable) record.
 *
 * @param dataElement A jQuery object of the data element.
 * @param repeatable TRUE to find out whether the data element is part of a
 * repeatable record. FALSE for non-repeatable records. NULL for either.
 */
function modificationsIsDataElementPartOfRecord(dataElement,
    repeatable) {
  // non-repeatable record elements have an ancestor with a record urn but no
  // surrounding repeatable-record container (table or tab div)
  if ((repeatable === undefined || repeatable == null || !repeatable)
      && dataElement.closest("[data-mdrid*=':record:']").length == 1
      && dataElement.closest(".repeatable-record").length == 0) {
    return true;
  }
  // repeatable record elements have an ancestor with a record urn and a
  // surrounding repeatable-record container (table or tab div)
  if ((repeatable === undefined || repeatable == null || repeatable)
      && dataElement.closest(".repeatable-record").length == 1) {
    return true;
  }
  return false;
}

/**
 * Adds a custom message below a specified data element. The severity controls
 * the background color according to the bootstrap alert-x styles.
 *
 * @param dataElementIdOrObj The id or jQuery object of the data element below which the message should be shown.
 * @param message The message to show.
 * @param severity One of 'success', 'info' (default), 'warning', 'danger'.
 */
function modificationsShowCustomErrorMessage(dataElementIdOrObj, message, severity) {
  var dataElement = dataElementIdOrObj instanceof jQuery ?
      dataElementIdOrObj : $dataElement(registryNamespace, dataElementIdOrObj);
  var row = conditionsFindRow(dataElement);
  switch (severity) {
    case 'success':
    case 'warning':
    case 'danger':
      break;
    default:
      severity = 'info';
  }

  if (row.find(".customError").length == 0) {
    row.append("<div class='col-xs-9 col-xs-offset-3 customError' style='margin-top: 10px'></div>");
  } else {
    row.find(".customError").find("div").detach();
  }
  row.find(".customError").append('<div class="alert alert-' + severity + '">' + message + '</div>');
}

/**
 * Removes the custom message below a specified data element.
 *
 * @param dataElementIdOrObj The id or jQuery object of the data element to remove the message from.
 */
function modificationsRemoveCustomErrorMessage(dataElementIdOrObj) {
  var dataElement = dataElementIdOrObj instanceof jQuery ?
      dataElementIdOrObj : $dataElement(registryNamespace, dataElementIdOrObj);
  var row = conditionsFindRow(dataElement);

  row.find(".customError").detach();
}

/**
 * Sorts the selected element in the order given by correctOrder array.
 * Modified version of https://stackoverflow.com/questions/667010/sorting-dropdown-list-using-javascript/667323#667323
 * @param selElem mdr element with the permitted value list that should be sorted for example given by $dataElement("osse-14", "17")[0];
 * @param correctOrder array that contains the permitted values in the desired order e.g. ["", "very good", "good", "poor", "very poor"]
 */
function sortSelect(selElem, correctOrder) {
  if (typeof selElem !== 'undefined') {
    var changeState = sessionStorage.getItem("somethingChangedInForm");
    if (selElem.nodeName == "SELECT") {
      sortSelectElement(selElem, correctOrder);
    }
    if (selElem.nodeName == "TABLE") {
      sortTableElement(selElem, correctOrder);
    }

    sessionStorage.setItem("somethingChangedInForm", changeState);
  }
}

/**
 * Function to sort permitted value list with out a slot (drop down version)
 * @param selElem mdr element with the permitted value list that should be sorted for example given by $dataElement("osse-14", "17")[0];
 * @param correctOrder array that contains the permitted values in the desired order e.g. ["", "very good", "good", "poor", "very poor"]
 */
function sortSelectElement(selElem, correctOrder) {
  var selIndex = correctOrder.indexOf(selElem.selectedOptions[0].value);

  var ary = (function (nl) {
    var a = [];
    for (var i = 0, len = nl.length; i < len; i++) {
      a.push(nl.item(i));
    }
    return a;
  })(selElem.options);
  ary.sort(function (a, b) {
    return (correctOrder.indexOf(a.value) > correctOrder.indexOf(
        b.value)) ? 1
        : -1;
  });
  for (var i = 0, len = ary.length; i < len; i++) {
    selElem.remove(ary[i].index);
  }
  for (var i = 0, len = ary.length; i < len; i++) {
    selElem.add(ary[i], null);
  }
  selElem.selectedIndex = selIndex;
}

/**
 * Function to sort permitted value that is represented in a table like the versions where SELECT_ONE_RADIO or SELECT_MANY_CHECKBOX is given as a slot
 * @param selElem mdr element with the permitted value list that should be sorted for example given by $dataElement("osse-14", "17")[0];
 * @param correctOrder array that contains the permitted values in the desired order e.g. ["", "very good", "good", "poor", "very poor"]
 */
function sortTableElement(selElem, correctOrder) {
  tbody = $(selElem).find('tbody');
  test = tbody.find('tr').sort(function (a, b) {
    return (correctOrder.indexOf($(a).find('input').val())-correctOrder.indexOf(
        $(b).find('input').val()));
  }).appendTo(tbody);
}

/**
 * Querys localization file for the given key and sets text component of the ref_field to the found value.
 * @param ref_field the html elemnt that should be localized.
 * @param key the key to find the correct string inside the localization file.
 */
function getLocalizedString(ref_field, key) {
  $.getJSON('./resources/localization/localization.json', function (data) {
    ref_field.text(data[getLanguage()][key])
  });
}

/**
 * Extracts current language from dropdown menu
 * @returns string like "Deutsch" or "English"
 */
function getLanguage() {
  return $("#dropdownLanguageLink").attr("title").split(" ")[1];
}

/**
 * Helper to get short language strings.
 * @returns short language string like EN for English.
 */
function getLanguageShort() {
  if(getLanguage()=="Deutsch") return "DE";
  if(getLanguage()=="English") return "EN";

}

$(window).on('load', function() {
  $('*[data-localization]').each(function setLocalization() {
    getLocalizedString($(this),$(this).attr("data-localization"));
  });
});