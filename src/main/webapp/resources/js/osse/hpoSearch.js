/**
 * Add the modal html structure to be used by setHPOField
 */
function addHPOModal() {
  $("#topFormPanel").parents("div.right").append(`<div id="hpoModal" class="modal fade" role="dialog" tabindex="-1">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h4 class="modal-title" data-localization=hpoModalTitle>Human Phenotype Ontology Search</h4>
        </div>
        <div class="modal-body">
          <p data-localization=hpoModalSearchTextDescription>Here you can search for terms in the Human Phenotype Ontology (HPO) and retrieve the corresponding HPO identifier.</p>
          <div class="form-group input-group data">
            <input type="text" id="hpoSeachText" >
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Dismiss</button>
        </div>
      </div>
    </div>
  </div>`);
}

/**
 * Looks for string (input) data elements which have a slot
 * "renderType: catalog_HPO" and activates the HPO search feature for them.
 */
function modificationsSetHpoFields() {
  let regex = /^urn:(osse-\d+):dataelement:(\d+):\d+$/;
  $('input[data-slot-rendertype="catalog_HPO"]').each(function (i, e) {
    let inputField = $(e);
    //make sure hpo modal exists
    if ($("#hpoModal").length == 0) {
      addHPOModal();
    }
    //make sure the registryNamespace is set, else try to impute
    let urn = inputField.attr('data-mdrid');
    if (registryNamespace == '') {
      registryNamespace = regex.exec(urn)[1]
    }
    if (inputField.attr('id')) {
      inputField.prop("readonly", true);
      inputField.css('cursor', 'pointer');
      //add description and listener
      updateDescriptionHpo(inputField.get());
      $(inputField).on("change", function () {
        updateDescriptionHpo($(this))
      })
      //activate modal logic
      getHPOModal(inputField.get());
      //make sure to remeber this was already done for this field
      inputField.attr("is-hpo-set", true);
    }

  });
}

/**
 * Function to set the needed event listener to handle modal dialog for hpo search and deal with the results.
 * @param dataElementID the target dataelements id.
 */
function getHPOModal(inputField) {
  var searchText = $("#hpoSeachText");
  $(inputField).focus(function () {
    $("#hpoModal").on('shown.bs.modal', function () {
      $('#hpoSeachText').focus();
    });
    $("#hpoModal").modal("show");
    searchText.val('');
    $(searchText).off('input');
    searchText.on('input', function () {
      queryHPOTerm(searchText, $(inputField).get())
    });
    $('#hpo_search_results').remove();
  })
}

/**
 * Method to query for the hpo terms.
 * @param {*} searchText
 * @param {*} inputField
 */
function queryHPOTerm(searchText, inputField) {
  $.ajax({
    beforeSend: function (request) {
      $('.hpo_selector').hide();
    },
    dataType: "json",
    url: "https://hpo.jax.org/api/hpo/search/?q=" + $(searchText).val()
        + "&max=20&offset=0&category=terms",
    success: function (data) {
      $('.hpo_selector').remove();
      selectionArray = [];
      linkValueArray = [];
      $.each(data["terms"], function (key, val) {
        selectionArray.push(val.name + " [" + val.ontologyId + "]");
        linkValueArray.push(val.ontologyId);
      });
      addSelectionToModalHpo($(inputField).get(), selectionArray,
          linkValueArray);
    },
    error: function (jqxhr, textStatus, error) {
      var err = textStatus + ", " + error;
      console.log("Request Failed: " + err);
    }
  });
}

/**
 * Helper to update the description text for hpo terms.
 * @param ref reference to the input field that contains the hpo term.
 */
function updateDescriptionHpo(inputField) {
  if (!$(inputField).next().hasClass('hpo_def')) {
    $('<a class="hpo_def" target="_blank" rel="noopener noreferrer" data-toggle="tooltip"></a>').attr(
        'id', $(inputField).attr('id') + "_description").hide().insertAfter(
        $(inputField));
  }
  let hpo_term = $(inputField).val()
  if (!(hpo_term == '')) {
    let description_field = $(inputField).next()
    description_field.text("...")
    .attr('href', "https://hpo.jax.org/app/browse/term/" + hpo_term).show();
    hpoNameAndSynonyms(description_field, hpo_term)
    $.getJSON("https://hpo.jax.org/api/hpo/term/" + hpo_term, function (data) {
      let name = data.details.name;
      let synonyms = data.details.synonyms;
      let def = data.details.definition;
      $(description_field).text(name);
      description_field.attr('title', def + "\n [" + synonyms + "]")
    })
  }
}

/**
 * Function set name and synonyms as text for a given term.
 * @param ref_field element which text property should be set.
 * @param hpo_term to search.
 */
function hpoNameAndSynonyms(ref_field, hpo_term) {
  return
}

/**
 * Helper to add selection option to the Hpo Modal.
 * @param {*} inputField
 * @param {*} selectionArray
 * @param {*} linkValueArray
 */
function addSelectionToModalHpo(inputField, selectionArray, linkValueArray) {
  $('.hpo_selector').remove();
  var outer_div = $('<div class=hpo_selector style="margin-top: 20px;"/>');
  var terms_ul = $(
      '<ul id=hpo_search_results style="list-style-type:none;"/>');
  selectionArray.forEach(function (item, index) {
    let terms_li = $('<a href="#"/>').append(item).wrap(
        '<li/>').parent().click(function (event) {
      event.preventDefault();
      $(inputField).attr("hpo-ref", linkValueArray[index]);
      $(inputField).val(linkValueArray[index]);
      $(inputField).trigger("change");
      $("#hpoModal").modal("hide");
    })
    terms_ul.append(terms_li);
  });
  outer_div.append(terms_ul);
  $("#hpoSeachText").parent().append(outer_div);
}

$(window).on('load', function () {
  modificationsSetHpoFields();
  $("#topFormPanel").on('draw.dt', function () {
    modificationsSetHpoFields();
  })
});