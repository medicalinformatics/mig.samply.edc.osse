/**
 * Activates the drag functionality in .sortableItemPanel
 * and .draggable
 */
function activateDrag() {

	$(".sortableItemPanel").sortable({
		start: function(e, ui ){
            ui.placeholder.height(ui.helper.outerHeight());
        },
		stop : function(event, ui) {
			refreshItems();
			$("#refreshItems").click();
		}
	});
	
	$(".draggable").draggable({
		connectToSortable : ".sortableItemPanel",
		revert : "invalid",
		helper: "clone",
		stop : function(event, ui) {
		}
	});

}

/**
 * Refreshes the items, using the .formItems
 */

function refreshItems() {
	var data = "";

	if ($(".sortableItemPanel").find(".formItem").length > 0) {
		var newElements = new Array();
		
		$(".sortableItemPanel").find(".formItem").each(function(i, el) {
			var urn = $(el).find(".urn").html();
			
			if(newElements.indexOf(urn) == -1) {
				newElements.push(urn);
			}
		});

		$("#items").val(newElements.join(","));

		$("#dragInfo").hide();
	} else {
		$("#items").val("");
		$("#dragInfo").show();
	}
}

/**
 * Called when a "remove" button has been clicken
 * @param element
 */
function removeItem(element) {
	//var urn = $(element).parent().parent().parent().remove();
	var urn = $(element).parent().parent().remove();
	refreshItems();
	$("#refreshItems").click();
}

function showResultTab() {
	$("#searchTabs a[href='#searchResultTab']").tab("show");
}

function toggleItem(element) {
	var urn = $(element).parent().parent().find(".urn").html();
	$(".inputStarItemUrn").val(urn);
	$(".inputStarItemBtn").click();
	
	
	var others = $(".urn").filter(function() { return $(this).html() == urn; });
	var i = $(element).find("i");
	
	if(i.hasClass("text-muted")) {
		others.each(function(index) {
			$(this).parent().parent().parent().find(".starItem").switchClass("text-muted", "text-primary");
		});
	} else {
		others.each(function(index) {
			$(this).parent().parent().parent().find(".starItem").switchClass("text-primary", "text-muted");
		});
	}
}

function collapseItem(element) {
	var divp = $(element).parent().parent().parent();
	var urn = divp.find(".urn").html();
	var d = divp.find("#details").first();
	var draggable = divp.find(".formItem").first().hasClass("ui-draggable");
	
	if(! d.hasClass("in")) {
		d.load("shortDetail.xhtml?draggable=" + draggable + "&urn=" + urn + " #detailView",
				function() {
			$(this).collapse('toggle');
			$(element).find("i").switchClass("fa-angle-down", "fa-angle-up");
			activateDrag();
		});
	} else {
		d.collapse('toggle');
		$(element).find("i").switchClass("fa-angle-up", "fa-angle-down");
	}
}