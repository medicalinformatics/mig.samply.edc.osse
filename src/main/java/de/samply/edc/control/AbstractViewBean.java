/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.control;

import de.samply.edc.model.ContextMenuEntry;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.io.Serializable;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import javax.faces.bean.ManagedProperty;

/** Abstract class for view scoped beans. */
public abstract class AbstractViewBean implements Serializable {

  public static final String FORM_STATE_OPEN = "open";
  public static final String DATA_OBJECT = "dataObject";
  /** Field containing a list contextMenu entries for a current view. */
  protected LinkedList<ContextMenuEntry> contextMenuList = new LinkedList<>();

  /**
   * Data Object used in some forms to contain data that we yet don't know what instance of classes
   * they might be.
   */
  protected HashMap<String, Object> dataObject;
  /** The sessionBean. */
  @ManagedProperty(value = "#{sessionBean}")
  protected AbstractSessionBean sessionBean;
  /** Hashmap containing fixed values for the view/form given by the xhtml form. */
  private HashMap<String, String> formParam;
  private JSONResource config;

  /**
   * TODO: add javadoc.
   */
  public JSONResource getConfig() {
    if (config == null) {
      config = sessionBean.getConfig();
    }
    return config;
  }

  /**
   * Method to get the sessionBean always use this, never access it directly, as sessionBean may be
   * null.
   *
   * @return the session bean
   */
  protected AbstractSessionBean getSessionBean() {
    if (sessionBean == null) {
      sessionBean = Utils.getSB();
    }

    return sessionBean;
  }

  /**
   * Sets the session bean.
   *
   * @param sessionBean the new session bean
   */
  public void setSessionBean(AbstractSessionBean sessionBean) {
    this.sessionBean = sessionBean;
  }

  /** Initialization. */
  public void init() {
    dataObject = null;

    config = sessionBean.getConfig();

    // Set the locale of the form to the language we chose to play in
    Utils.changeLanguage(getSessionBean().getLanguage());

    // prepare the context menu for the current view
    makeContextMenu();
  }

  /** Method to calculate a list of context menu entries where needed. */
  public void makeContextMenu() {}

  /**
   * Gets the context menu list.
   *
   * @return the context menu list
   */
  public List<ContextMenuEntry> getContextMenuList() {
    return contextMenuList;
  }

  /**
   * Checks if the view has a context menu.
   *
   * @return the boolean
   */
  public Boolean hasContextMenu() {
    return (contextMenuList != null && !contextMenuList.isEmpty());
  }

  /**
   * Method returning the state of a form. It defaults to "open"
   *
   * @return the state of the form
   */
  public String getFormState() {
    return FORM_STATE_OPEN;
  }

  /**
   * Gets the form param.
   *
   * @return the form param
   */
  public HashMap<String, String> getFormParam() {
    if (formParam == null) {
      formParam = new HashMap<>();
    }

    return formParam;
  }

  /**
   * Gets the form param.
   *
   * @param key the key
   * @return the form param
   */
  public String getFormParam(String key) {
    if (formParam == null) {
      formParam = new HashMap<>();
    }

    return formParam.get(key);
  }

  /**
   * Sets the form param.
   *
   * @param formParam the form param
   */
  public void setFormParam(HashMap<String, String> formParam) {
    this.formParam = formParam;
  }

  /**
   * Sets the form param.
   *
   * @param key the key
   * @param value the value
   */
  public void setFormParam(String key, String value) {
    if (formParam == null) {
      formParam = new HashMap<>();
    }

    formParam.put(key, value);
  }

  /**
   * Action method: saves values of a form to "the DB" and recalculates the context menu.
   *
   * @return @
   */
  public String save() {
    makeContextMenu();
    return save(false);
  }

  /**
   * Method to save values either by import or by form saving.
   *
   * @param wasImported the was imported
   * @return the string
   */
  public String save(Boolean wasImported) {
    return null;
  }

  /**
   * Gets the dataObject container.
   *
   * @return the hashmap container
   */
  @SuppressWarnings("unchecked")
  public HashMap<String, Object> getDataObject() {
    if (dataObject == null) {
      Object temp = getSessionBean().clearTempObject(DATA_OBJECT);
      if (temp != null) {
        dataObject = (HashMap<String, Object>) temp;
      } else {
        dataObject = new HashMap<>();
      }
    }
    return dataObject;
  }

  /**
   * Action method: To add a row to a datatable.
   *
   * @param name Name of the datatable
   * @return the string
   */
  public String addAction(String name) {
    addRow(name);
    return null;
  }

  /**
   * Action method: to delete a row from a datatable.
   *
   * @param name Name of the datatable
   * @param weg Row to be removed
   * @return the string
   */
  public String deleteAction(String name, DatatableRow weg) {
    deleteRow(name, weg);
    return null;
  }

  /**
   * Method to add a row to a datatable.
   *
   * @param name the name of the datatable to add a row to
   */
  public void addRow(String name) {
    DatatableRows stl;

    if (!dataObject.containsKey(name)) {
      stl = new DatatableRows();
    } else {
      stl = (DatatableRows) dataObject.get(name);
    }

    stl.add(new DatatableRow());
    dataObject.put(name, stl);
  }

  /**
   * Method to delete a line from a datatable.
   *
   * @param name Name of the datatable
   * @param weg Line to be removed
   */
  public void deleteRow(String name, DatatableRow weg) {
    if (!dataObject.containsKey(name)) {
      return;
    }

    DatatableRows stl = (DatatableRows) dataObject.get(name);
    stl.remove(weg);

    dataObject.put(name, stl);
  }
}
