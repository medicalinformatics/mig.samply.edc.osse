/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.control;

import com.google.common.net.HttpHeaders;
import de.samply.edc.model.Entity;
import de.samply.edc.model.Formvars;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Locale;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.ws.rs.core.MediaType;
import org.apache.commons.io.IOUtils;

/** Abstract for sessionBeans. */
public abstract class AbstractSessionBean implements Serializable {

  public static final String CONFIG_NAME_OSSE = "osse";
  public static final String FORM_LOGBOOK = "logbook";
  public static final String FORM_FORGOTPASSWORD = "forgotpassword.xhtml";
  public static final String FORM_LOGIN = "login.xhtml";
  public static final String OUTCOME_LOGIN = "login";
  public static final String OUTCOME_LOGOUT = "logout";
  public static final String CURRENT_OBJECT_USER = "user";
  public static final String CURRENT_OBJECT_GROUP = "group";
  /** The currently set locale. */
  protected Locale locale;
  /** Has the user changed the locale? Used to not switch back to standard locale if so. */
  protected Boolean userChangedLocale = false;
  /** Name of the currently viewed form. */
  protected String currentFormName = null;
  /** Bean property to use for form values that need to be stored in more than a view state. */
  protected Formvars formvars = new Formvars();
  /**
   * HashMap of all "current" objects to be stored in a session view, like the currently logged in
   * user etc.
   */
  protected HashMap<String, Object> currentObjects;
  /**
   * Object to store something that is about to get deleted, this is used to be able to access it
   * from a confirmation page etc.
   */
  protected Object toDeleteObject;
  /** The database wrapper. */
  protected transient AbstractDatabase<?> database;
  private JSONResource config;
  /**
   * tempObject used to store objects into the session temporarily it is basically similar to the
   * sesionmap, just cleaner to working with our session stuff.
   */
  private HashMap<String, Object> tempObjects;

  public JSONResource getConfig() {
    return config;
  }

  /**
   * Method needed to be implemented to return the de.samply.store model In this method you should
   * create an instance of Database wrapper that you use in your project
   *
   * @return instance of the Database wrapper
   */
  public abstract AbstractDatabase<?> get();

  /**
   * Always call this if you need the database object.
   *
   * @return instance of the Database wrapper
   */
  public AbstractDatabase<?> getDatabase() {
    // necessary due to serialisation
    if (database == null) {
      database = get();
    }

    return database;
  }

  /**
   * Method to set a predefined database.
   *
   * @param database the new database
   */
  public void setDatabase(AbstractDatabase<?> database) {
    this.database = database;
  }

  /** PostConstruct to initiate variables. */
  public void init() {
    database = get();
    config = database.getConfig(CONFIG_NAME_OSSE);

    currentObjects = new HashMap<>();
  }

  /* GO methods, usually called from JSF pages. */

  /** Go to the logbook. */
  public void goLogbook() {
    Utils.goForm(FORM_LOGBOOK);
  }

  /** Go to forgot password. */
  public void goForgotPassword() {
    goPage(FORM_FORGOTPASSWORD);
  }

  /** Go to login page. */
  public void goLoginPage() {
    goPage(FORM_LOGIN);
  }

  /**
   * Go to a given page.
   *
   * @param page the page
   */
  public void goPage(String page) {
    try {
      Utils.redirectToPage(page);
    } catch (IOException e) {
      Utils.getLogger().warn("Unable to redirect to:" + page, e);
    }
  }

  /**
   * Login Form Action.
   *
   * @return String navigation string for JSF (see faces-config) @
   */
  public String login() {
    clearVariables();

    return OUTCOME_LOGIN;
  }

  /**
   * Logout action.
   *
   * @return String navigation string for JSF (see faces-config) @
   */
  public String logout() {
    clearVariables();

    Utils.killSession();

    getDatabase().logout();

    // reset registry to the default language
    userChangedLocale = false;
    Utils.changeLanguage(
        FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage());

    return OUTCOME_LOGOUT;
  }

  /**
   * Sets the current {@code Locale} for each user session.
   *
   * @param languageCode - ISO-639 language code
   */
  public void changeLanguage(String languageCode) {
    userChangedLocale = true;
    Utils.changeLanguage(languageCode);
    locale = new Locale(languageCode);
  }

  /** When logging out we need to devalue all our fields. */
  protected void clearVariables() {
    clearCurrentObjects();
  }

  /** Clears all current objects. */
  private void clearCurrentObjects() {
    if (currentObjects == null) {
      currentObjects = new HashMap<>();
    } else {
      currentObjects.clear();
    }
  }

  /**
   * Delete one form entry.
   *
   * @param key String
   */
  public void deleteEntry(String key) {
    formvars.deleteEntry(key);
  }

  /** Deletes all form var entries. */
  public void clearValues() {
    formvars.clearEntries();
  }

  /** Method called by webpage or javascript to refresh a user session. */
  public void refreshSession() {
    Utils.refreshSession();
  }

  /**
   * Offers a file to download Only for demonstration platform to download a test MDB file in
   * Adapthera.
   *
   * @param filename the filename
   * @throws IOException Signals that an I/O exception has occurred.
   */
  public void doDownload(String filename) {
    if (getCurrentObject(CURRENT_OBJECT_USER) == null) {
      return;
    }

    if (Utils.isNullOrEmpty(filename)) {
      return;
    }

    filename = Utils.getRealPath(filename);
    File file = new File(filename);
    if (!file.exists()) {
      Utils.getLogger().debug("File does not exist");
      return;
    }

    addLog("downloaded file " + file.getName() + " from " + Utils.getClientIP());

    FacesContext fc = FacesContext.getCurrentInstance();
    ExternalContext externalContext = fc.getExternalContext();

    externalContext.responseReset();
    externalContext.setResponseHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_OCTET_STREAM);
    externalContext.setResponseHeader(HttpHeaders.CONTENT_LENGTH, String.valueOf(file.length()));
    externalContext.setResponseHeader(
        HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=\"" + file.getName() + "\"");

    InputStream input = null;
    OutputStream output;

    try {
      input = new FileInputStream(file);
      output = externalContext.getResponseOutputStream();
      IOUtils.copy(input, output);
    } catch (IOException ex) {
      Utils.getLogger().debug("Unable to copy streams.", ex);
    } finally {
      IOUtils.closeQuietly(input);
    }

    fc.responseComplete();
  }

  /**
   * adds a logbook entry.
   *
   * @param action Free text what was done
   */
  public void addLog(String action) {
    if (getCurrentObject(CURRENT_OBJECT_USER) != null) {
      getDatabase().addLog(((Entity) getCurrentObject(CURRENT_OBJECT_USER)).getUri(), action);
    } else {
      getDatabase().addLog("unknown", action);
    }
  }

  /**
   * Clear a current object of a certain type.
   *
   * @param name the OSSEVocabulary.Type
   */
  public void clearCurrentObject(String name) {
    if (currentObjects == null) {
      return;
    }

    currentObjects.remove(name);
  }

  /**
   * Gets the current object of a certain type.
   *
   * @param name the OSSEVocabulary.Type
   * @return the current object
   */
  public Object getCurrentObject(String name) {
    if (currentObjects == null) {
      currentObjects = new HashMap<>();
    }

    if (currentObjects.containsKey(name)) {
      return currentObjects.get(name);
    }

    return null;
  }

  /**
   * Sets the current object for a given type.
   *
   * @param name the OSSEVocabulary.Type
   * @param currentObject the current Object
   */
  public void setCurrentObject(String name, Object currentObject) {
    if (currentObjects == null) {
      currentObjects = new HashMap<>();
    }
    currentObjects.put(name, currentObject);
  }

  /**
   * Returns the set locale.
   *
   * @return the locale
   */
  public Locale getLocale() {
    if (locale == null) {
      locale = Utils.getDefaultLocale();
    }
    return locale;
  }

  /**
   * Sets the locale.
   *
   * @param locale the new locale
   */
  public void setLocale(Locale locale) {
    this.locale = locale;
  }

  /**
   * Gets the set language.
   *
   * @return the language
   */
  public String getLanguage() {
    return getLocale().getLanguage();
  }

  /**
   * Sets the language.
   *
   * @param language the new language
   */
  public void setLanguage(String language) {
    locale = new Locale(language);
    FacesContext.getCurrentInstance().getViewRoot().setLocale(locale);
  }

  /**
   * Gets the current form name.
   *
   * @return the current form name
   */
  public String getCurrentFormName() {
    return currentFormName;
  }

  /**
   * Sets the current form name.
   *
   * @param currentFormName the new current form name
   */
  public void setCurrentFormName(String currentFormName) {
    this.currentFormName = currentFormName;
  }

  /**
   * Gets the current form name.
   *
   * @return the current form name
   */
  public String getForm() {
    return currentFormName;
  }

  /**
   * Sets the current form name.
   *
   * @param form the new form
   */
  public void setForm(String form) {
    this.currentFormName = form;
  }

  /**
   * Gets the formtitle in the set locale.
   *
   * @return the formtitle
   */
  public String getFormtitle() {
    if (currentFormName == null) {
      return "";
    }

    return Utils.getAB().getPageTitleName(currentFormName, Utils.getLocale());
  }

  /** Go start page. */
  public abstract void goStartPage();

  /** Go to main patient page. */
  public abstract void goMainPatientPage();

  /** What happens when you press cancel on a page. */
  public abstract void cancelPage();

  /**
   * Checks for current group.
   *
   * @return the boolean
   */
  public Boolean hasCurrentGroup() {
    return (getCurrentObject(CURRENT_OBJECT_GROUP) != null);
  }

  /**
   * Gets the formvars.
   *
   * @return the formvars
   */
  public Formvars getFormvars() {
    return formvars;
  }

  /**
   * Sets the formvars.
   *
   * @param formvars the new formvars
   */
  public void setFormvars(Formvars formvars) {
    this.formvars = formvars;
  }

  /**
   * Get all formular entries.
   *
   * @return the entries
   */
  public HashMap<String, Object> getEntries() {
    return formvars.getEntries();
  }

  /**
   * Sets all formular entries.
   *
   * @param entries the entries
   */
  public void setEntries(HashMap<String, Object> entries) {
    formvars.setEntries(entries);
  }

  /**
   * Get a formular entry.
   *
   * @param key the key
   * @return the entry
   */
  public Object getEntry(String key) {
    return formvars.getEntry(key);
  }

  /**
   * Sets a formular entry.
   *
   * @param key String
   * @param value Object
   */
  public void setEntry(String key, Object value) {
    formvars.setEntry(key, value);
  }

  /**
   * Gets the session timeout in Minutes.
   *
   * @return the session timeout in minutes
   */
  public int getSessionTimeoutMin() {
    return Utils.getSessionTimeoutMin();
  }

  /**
   * Gets the URL basedir in which this application is installed to.
   *
   * @return the URL basedir
   */
  public String getBasedir() {
    return Utils.getBasedir();
  }

  /**
   * Boolean if the user changed the locale manually.
   *
   * @return boolean
   */
  public Boolean getUserChangedLocale() {
    return userChangedLocale;
  }

  /**
   * Sets if the user has changed the locale manually.
   *
   * @param userChangedLocale the new user changed locale
   */
  public void setUserChangedLocale(Boolean userChangedLocale) {
    this.userChangedLocale = userChangedLocale;
  }

  /**
   * Gets the temp objects.
   *
   * @return the temp objects
   */
  public HashMap<String, Object> getTempObjects() {
    return tempObjects;
  }

  /**
   * Sets the temp objects.
   *
   * @param tempObjects the temp objects
   */
  public void setTempObjects(HashMap<String, Object> tempObjects) {
    this.tempObjects = tempObjects;
  }

  /**
   * Sets a given temp object.
   *
   * @param key the key
   * @param value the value
   */
  public void setTempObject(String key, Object value) {
    if (tempObjects == null) {
      tempObjects = new HashMap<>();
    }
    tempObjects.put(key, value);
  }

  /**
   * Gets a named temp object.
   *
   * @param key the key
   * @return the temp object
   */
  public Object getTempObject(String key) {
    if (tempObjects == null) {
      return null;
    }

    return tempObjects.get(key);
  }

  /**
   * Deletes a given temp object.
   *
   * @param key the key
   * @return the object
   */
  public Object clearTempObject(String key) {
    if (tempObjects == null) {
      return null;
    }

    return tempObjects.remove(key);
  }
}
