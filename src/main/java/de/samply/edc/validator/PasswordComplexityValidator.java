/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.validator;

import de.samply.edc.utils.Utils;
import java.text.MessageFormat;
import java.util.ArrayList;
import org.passay.CharacterRule;
import org.passay.EnglishCharacterData;
import org.passay.IllegalRegexRule;
import org.passay.LengthRule;
import org.passay.PasswordData;
import org.passay.PasswordGenerator;
import org.passay.PasswordUtils;
import org.passay.Rule;


public class PasswordComplexityValidator {

  private static PasswordComplexityValidator instance;

  private int minPasswordLength;
  private int maxPasswordLength;
  private int minLowerAlphaChars;
  private int minUpperAlphaChars;
  private int minSpecialChars;
  private int minNumericalChars;
  private boolean allowExtendedAsciiSymbols;
  private int lastPasswordDifferInChars;
  private int passwordHistoryLen;
  private boolean allowPhoneNumbers;
  private boolean allowDates;


  public void setMinPasswordLength(int minPasswordLength) {
    this.minPasswordLength = minPasswordLength;
  }

  public void setMaxPasswordLength(int maxPasswordLength) {
    this.maxPasswordLength = maxPasswordLength;
  }

  public void setMinLowerAlphaChars(int minLowerAlphaChars) {
    this.minLowerAlphaChars = minLowerAlphaChars;
  }

  public void setMinUpperAlphaChars(int minUpperAlphaChars) {
    this.minUpperAlphaChars = minUpperAlphaChars;
  }

  public void setMinSpecialChars(int minSpecialChars) {
    this.minSpecialChars = minSpecialChars;
  }

  public void setMinNumericalChars(int minNumericalChars) {
    this.minNumericalChars = minNumericalChars;
  }

  public void setAllowExtendedAsciiSymbols(boolean allowExtendedAsciiSymbols) {
    this.allowExtendedAsciiSymbols = allowExtendedAsciiSymbols;
  }

  public void setLastPasswordDifferInChars(int lastPasswordDifferInChars) {
    this.lastPasswordDifferInChars = lastPasswordDifferInChars;
  }

  public void setPasswordHistoryLen(int passwordHistoryLen) {
    this.passwordHistoryLen = passwordHistoryLen;
  }

  public void setAllowPhoneNumbers(boolean allowPhoneNumbers) {
    this.allowPhoneNumbers = allowPhoneNumbers;
  }

  public void setAllowDates(boolean allowDates) {
    this.allowDates = allowDates;
  }

  /**
   * EdcPasswortValidator singelton.
   *
   * @return EdcPasswortValidator
   */
  public static PasswordComplexityValidator getInstance() {
    if (PasswordComplexityValidator.instance == null) {
      PasswordComplexityValidator.instance = new PasswordComplexityValidator();
      PasswordComplexityValidator.instance.setMinPasswordLength(8);
      PasswordComplexityValidator.instance.setMaxPasswordLength(255);
      PasswordComplexityValidator.instance.setMinLowerAlphaChars(1);
      PasswordComplexityValidator.instance.setMinUpperAlphaChars(1);
      PasswordComplexityValidator.instance.setMinSpecialChars(1);
      PasswordComplexityValidator.instance.setMinNumericalChars(1);
      PasswordComplexityValidator.instance.setAllowExtendedAsciiSymbols(true);
    }
    return PasswordComplexityValidator.instance;
  }

  /**
   * Configures some basic settings.
   *
   * @param newMinPasswordLength         minimal password length.
   * @param newMaxPasswordLength         maximal password length.
   * @param newMinLowerAlphaChars        minimal number of lower case alpha characters.
   * @param newMinUpperAlphaChars        minimal number of upper case alpha characters.
   * @param newMinSpecialChars           minimal number of special characters.
   * @param newMinNumericalChars         minimal number of numerical characters.
   * @param newAllowExtendedAsciiSymbols allow extended ascii symbols.
   * @param newLastPasswordDifferInChars min different in characters.
   * @param newPasswordHistoryLen        not in use.
   * @param newAllowPhoneNumbers         allow phone numbers as password
   * @param newAllowDates                allow dates numbers as password
   * @param newRestrictedByDictionary    not in use.
   * @param newDictionaryAccuracy        not in use.
   * @param newDictionaryMinWordLength   not in use.
   */
  public static synchronized void configure(
      int newMinPasswordLength,
      int newMaxPasswordLength,
      int newMinLowerAlphaChars,
      int newMinUpperAlphaChars,
      int newMinSpecialChars,
      int newMinNumericalChars,
      boolean newAllowExtendedAsciiSymbols,
      int newLastPasswordDifferInChars,
      int newPasswordHistoryLen,
      boolean newAllowPhoneNumbers,
      boolean newAllowDates,
      boolean newRestrictedByDictionary,
      float newDictionaryAccuracy,
      int newDictionaryMinWordLength) {

    PasswordComplexityValidator.getInstance().setMinPasswordLength(newMinPasswordLength);
    PasswordComplexityValidator.getInstance().setMaxPasswordLength(newMaxPasswordLength);
    PasswordComplexityValidator.getInstance().setMinLowerAlphaChars(newMinLowerAlphaChars);
    PasswordComplexityValidator.getInstance().setMinUpperAlphaChars(newMinUpperAlphaChars);
    PasswordComplexityValidator.getInstance().setMinSpecialChars(newMinSpecialChars);
    PasswordComplexityValidator.getInstance().setMinNumericalChars(newMinNumericalChars);
    PasswordComplexityValidator.getInstance()
        .setAllowExtendedAsciiSymbols(newAllowExtendedAsciiSymbols);
    PasswordComplexityValidator.getInstance()
        .setLastPasswordDifferInChars(newLastPasswordDifferInChars);
    PasswordComplexityValidator.getInstance().setAllowPhoneNumbers(newAllowPhoneNumbers);
    PasswordComplexityValidator.getInstance().setAllowDates(newAllowDates);
  }

  /**
   * Validates the given password.
   *
   * @param password    new password
   * @param oldPassword old password
   * @throws Exception message on validation fail
   */
  public void validate(String password, String oldPassword) throws Exception {
    PasswordData pwData = new PasswordData();
    pwData.setPassword(password);

    boolean valid = true;
    String exceptionString = "";

    //length
    LengthRule lengthRule = new LengthRule(minPasswordLength, maxPasswordLength);
    if (!lengthRule.validate(pwData).isValid()) {
      exceptionString +=
          MessageFormat.format(Utils.getResourceText("new_password_length"), minPasswordLength,
              maxPasswordLength) + "\n";
      valid = false;
    }

    ArrayList<Rule> caseRules = new ArrayList<>();
    if (minLowerAlphaChars > 0) {
      CharacterRule lowerCaseRule = new CharacterRule(EnglishCharacterData.LowerCase,
          minLowerAlphaChars);
      if (!lowerCaseRule.validate(pwData).isValid()) {
        exceptionString +=
            MessageFormat
                .format(Utils.getResourceText("new_password_case_lower"), minLowerAlphaChars)
                + "\n";
        valid = false;
      }
    }
    if (minUpperAlphaChars > 0) {
      CharacterRule upperCaseRule = new CharacterRule(EnglishCharacterData.UpperCase,
          minUpperAlphaChars);
      if (!upperCaseRule.validate(pwData).isValid()) {
        exceptionString +=
            MessageFormat.format(Utils.getResourceText("new_password_case_upper"),
                minUpperAlphaChars) + "\n";
        valid = false;
      }
    }

    if (minNumericalChars > 0) {
      CharacterRule digitRule = new CharacterRule(EnglishCharacterData.Digit, minNumericalChars);
      if (!digitRule.validate(pwData).isValid()) {
        exceptionString +=
            MessageFormat.format(Utils.getResourceText("new_password_numeric"), minNumericalChars)
                + "\n";
        valid = false;
      }
    }

    if (minSpecialChars > 0) {
      CharacterRule minSpecialCharRule;
      if (allowExtendedAsciiSymbols) {
        minSpecialCharRule = new CharacterRule(AsciiSpecialCharacterData.SpecialExt,
            minSpecialChars);
      } else {
        minSpecialCharRule = new CharacterRule(AsciiSpecialCharacterData.Special, minSpecialChars);
      }
      if (!minSpecialCharRule.validate(pwData).isValid()) {
        exceptionString +=
            MessageFormat.format(Utils.getResourceText("new_password_special"), minSpecialChars)
                + "\n";
        valid = false;
      }
    }

    if (PasswordUtils.countMatchingCharacters(password, oldPassword) > lastPasswordDifferInChars) {
      exceptionString += Utils.getResourceText("new_password_similar");
      valid = false;
    }

    if (!allowPhoneNumbers) {
      final String regexPhoneNumber =
          ".*([0-9]{3}[\\-.]{1}" + "[0-9]{3}[\\-.]{1}[0-9]{4}).*";
      if (!new IllegalRegexRule(regexPhoneNumber).validate(pwData).isValid()) {
        exceptionString += Utils.getResourceText("new_password_phone");
        valid = false;
      }
    }

    if (!allowDates) {
      final String regexDate =
          ".*([0-9]{1,4}[\\-.\\/]{1}" + "[0-9]{1,2}[\\-.\\/]{1}[0-9]{1,4}).*";
      if (!new IllegalRegexRule(regexDate).validate(pwData).isValid()) {
        exceptionString += Utils.getResourceText("new_password_date");
        valid = false;
      }
    }

    if (!valid) {
      throw new Exception(exceptionString);
    }

  }

  /**
   * Generates a new random password.
   *
   * @return random password
   */
  public String getRandomPassword() {
    ArrayList<CharacterRule> characterRules = new ArrayList<>();
    characterRules.add(new CharacterRule(EnglishCharacterData.LowerCase, minLowerAlphaChars));
    characterRules.add(new CharacterRule(EnglishCharacterData.UpperCase, minUpperAlphaChars));
    characterRules.add(new CharacterRule(EnglishCharacterData.Digit, minNumericalChars));
    CharacterRule minSpecialCharRule;
    if (allowExtendedAsciiSymbols) {
      minSpecialCharRule = new CharacterRule(AsciiSpecialCharacterData.SpecialExt, minSpecialChars);
    } else {
      minSpecialCharRule = new CharacterRule(AsciiSpecialCharacterData.Special, minSpecialChars);
    }
    characterRules.add(minSpecialCharRule);
    PasswordGenerator passwordGenerator = new PasswordGenerator();
    String password = passwordGenerator.generatePassword(maxPasswordLength, characterRules);
    return password;
  }
}
