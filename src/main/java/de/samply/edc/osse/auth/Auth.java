/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.auth;

import de.samply.auth.client.AuthClient;
import de.samply.auth.client.AuthClientUtils;
import de.samply.auth.client.GrantType;
import de.samply.auth.client.InvalidTokenException;
import de.samply.auth.client.KeycloakAuthClient;
import de.samply.auth.client.RegistrationWrapper;
import de.samply.auth.client.SamplyAuthClient;
import de.samply.auth.client.jwt.JwtAccessToken;
import de.samply.auth.client.jwt.JwtException;
import de.samply.auth.client.jwt.JwtIdToken;
import de.samply.auth.client.jwt.JwtRefreshToken;
import de.samply.auth.client.jwt.KeyLoader;
import de.samply.auth.rest.AccessTokenDto;
import de.samply.auth.rest.AccessTokenRequestDto;
import de.samply.auth.rest.ClientDescriptionDto;
import de.samply.auth.rest.ClientListDto;
import de.samply.auth.rest.KeyIdentificationDto;
import de.samply.auth.rest.LoginDto;
import de.samply.auth.rest.LoginRequestDto;
import de.samply.auth.rest.OAuth2Key;
import de.samply.auth.rest.OAuth2Keys;
import de.samply.auth.rest.RegistrationDto;
import de.samply.auth.rest.RegistrationRequestDto;
import de.samply.auth.rest.SignRequestDto;
import de.samply.auth.rest.Usertype;
import de.samply.common.config.OAuth2Client;
import de.samply.common.http.HttpConnector;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.utils.Utils;
import java.math.BigInteger;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.SecureRandom;
import java.security.Signature;
import java.security.SignatureException;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.PKCS8EncodedKeySpec;
import java.util.HashMap;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.Response;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.configuration.Configuration;
import org.glassfish.jersey.client.ClientProperties;
import org.keycloak.representations.idm.UserRepresentation;

/** Class for communication with Samply.AUTH or Keycloak component. */
public class Auth {

  /**
   * Generates an RSA keypair.
   *
   * @return HashMap contains private (Vocabulary.Config.Auth.MyPrivKey) and public
   *     (Vocabulary.Config.Auth.MyPubKey) key
   * @throws NoSuchAlgorithmException the no such algorithm exception
   */
  public static HashMap<String, String> generateKeyPair() throws NoSuchAlgorithmException {
    HashMap<String, String> keyMap = new HashMap<>();

    KeyPairGenerator fac = KeyPairGenerator.getInstance("RSA");
    fac.initialize(4096);
    KeyPair keyPair = fac.generateKeyPair();

    PublicKey pubkey = keyPair.getPublic();
    PrivateKey privkey = keyPair.getPrivate();
    String pubkeyString = Base64.encodeBase64String(pubkey.getEncoded());
    String privkeyString = Base64.encodeBase64String(privkey.getEncoded());

    keyMap.put(Vocabulary.Config.Auth.MyPrivKey, privkeyString);
    keyMap.put(Vocabulary.Config.Auth.MyPubKey, pubkeyString);

    return keyMap;
  }

  /**
   * Registers a registry to Samply.AUTH component
   *
   * @param pubkeyString The private key of the Registry
   * @param email The email address provided by the admin
   * @param registryName The name of this Registry
   * @param configuration the configuration
   * @param usertype The type of registry (Usertype.OSSE_REGISTRY, Usertype.BRIDGEHEAD)
   * @return HashMap contains: (Vocabulary.Config.Auth.ServerPubKey) server public key,
   *     (Vocabulary.Config.Auth.KeyId) the registry account ID, (Vocabulary.Config.Auth.UserId) the
   *     registry account user ID, or an error reason
   */
  public static HashMap<String, String> registerAuth(
      String pubkeyString,
      String privkeyString,
      String email,
      String registryName,
      Configuration configuration,
      Usertype usertype) {

    Utils.getLogger().debug("Preparing to register with auth service...");

    if (pubkeyString == null || "".equals(pubkeyString)) {
      Utils.getLogger().error("No pubkey.");
      return null;
    }

    if (privkeyString == null || "".equals(privkeyString)) {
      Utils.getLogger().error("No privkey.");
      return null;
    }

    if (email == null || "".equals(email)) {
      Utils.getLogger().error("No email.");
      return null;
    }

    if (registryName == null || "".equals(registryName)) {
      Utils.getLogger().error("No registry name.");
      return null;
    }

    OAuth2Client oauth2Client = getOAuth2Client(configuration);
    String authUrl = oauth2Client.getHost();
    HttpConnector hc = ApplicationBean.getHttpConnector();
    HashMap<String, String> authMap = new HashMap<>();

    // first check if we have an internet connection at all
    if (!Utils.checkHostReachable(hc, oauth2Client.getHost())) {
      authMap.put("hostunreachable", "true");
      Utils.getLogger().error("Host unreachable.");
      return authMap;
    }

    Client client = hc.getJerseyClientForHTTPS(false);
    String state = new BigInteger(64, new SecureRandom()).toString(32);
    PrivateKey privKey = getPrivateKeyFromString(privkeyString);

    if (privKey == null) {
      Utils.getLogger().error("No private key loaded, aborting registration ...");
      return null;
    }

    RegistrationRequestDto regRequest = new RegistrationRequestDto();
    regRequest.setBase64EncodedPublicKey(pubkeyString);
    regRequest.setDescription(registryName);
    regRequest.setEmail(email);
    regRequest.setName(registryName);
    regRequest.setContactData("");
    regRequest.setUsertype(usertype);

    RegistrationDto registrationDto;
    Integer keyId = -1;
    Integer userId = -1;
    Response response;

    if (oauth2Client.isUseSamplyAuth()) {
      Utils.getLogger().debug("Using SamplyAuthClient...");
      AuthClient authcli = new SamplyAuthClient(privKey, oauth2Client, client, state);
      Utils.getLogger().debug("Calling register method of SamplyAuthClient...");
      response = authcli.register(new RegistrationWrapper(regRequest));
    } else {
      Utils.getLogger().debug("Reading password from config...");
      String password = configuration.getString(Vocabulary.Config.Auth.RegistryPassword);
      Utils.getLogger().debug("Creating UserRepresentation...");
      UserRepresentation user = AuthClientUtils.samplyRegistrationToKeycloak(regRequest, password);
      Utils.getLogger().debug("Using KeycloakAuthClient...");
      AuthClient authcli = new KeycloakAuthClient(oauth2Client, client, state);
      Utils.getLogger().debug("Calling register method of KeycloakAuthClient...");
      response = authcli.register(new RegistrationWrapper(user));
    }

    Utils.getLogger().debug("Register method complete. Status: " + response.getStatus()
        + " Reason: " + response.getStatusInfo().getReasonPhrase());

    if (response.getStatus() == 409) {
      // Conflict means the email address (or user) is already in use!
      authMap.put("conflict", "true");
      return authMap;
    } else if (response.getStatus() != 200 && response.getStatus() != 201) {
      Utils.getLogger()
          .error("Error code = " + response.getStatus()
              + " Reason: " + response.getStatusInfo().getReasonPhrase());
      return null;
    }

    if (oauth2Client.isUseSamplyAuth()) {
      registrationDto = response.readEntity(RegistrationDto.class);
      // Get and store pubkey of Auth server for future signature checks
      OAuth2Keys keys = client.target(authUrl + "/oauth2/certs").request().get(OAuth2Keys.class);
      OAuth2Key key = keys.getKeys().get(0);
      authMap.put(Vocabulary.Config.Auth.ServerPubKey, key.getDerFormat());
      keyId = registrationDto.getKeyId();
      userId = registrationDto.getUserId();
    } else {
      authMap.put(
          Vocabulary.Config.Auth.ServerPubKey,
          configuration.getString(Vocabulary.Config.Auth.Pubkey));
    }

    authMap.put(Vocabulary.Config.Auth.KeyId, keyId.toString());
    authMap.put(Vocabulary.Config.Auth.UserId, userId.toString());

    return authMap;
  }

  /**
   * Sets the OAuth2Client object.
   */
  public static OAuth2Client getOAuth2Client(Configuration config) {
    OAuth2Client oauth2Client = new OAuth2Client();
    oauth2Client.setClientId(config.getString(Vocabulary.Config.Auth.ClientId));
    oauth2Client.setClientSecret(config.getString(Vocabulary.Config.Auth.ClientSecret));
    oauth2Client.setUseSamplyAuth(config.getBoolean(Vocabulary.Config.Auth.UseSamplyAuth));
    oauth2Client.setHost(config.getString(Vocabulary.Config.Auth.REST));
    oauth2Client.setHostPublicKey(config.getString(Vocabulary.Config.Auth.Pubkey));
    if (!oauth2Client.isUseSamplyAuth()) {
      oauth2Client.setRealm(config.getString(Vocabulary.Config.Auth.REALM));
    }
    return oauth2Client;
  }

  /**
   * Converts a private key String into a PrivateKey object.
   *
   * @param privateKey string
   * @return PrivateKey
   */
  private static PrivateKey getPrivateKeyFromString(String privateKey) {
    try {
      byte[] encodedKey = Base64.decodeBase64(privateKey);
      PKCS8EncodedKeySpec keySpec = new PKCS8EncodedKeySpec(encodedKey);
      KeyFactory kf = KeyFactory.getInstance("RSA");
      return kf.generatePrivate(keySpec);
    } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
      Utils.getLogger().error("Could not load PrivateKey from string ...\n", e);
    }
    return null;
  }

  /**
   * Loads a list of all clients of the Samply.AUTH component
   *
   * @param client A Jersey client
   * @param configuration the configuration
   * @return The clientID, and its description
   */
  public static HashMap<String, ClientDescriptionDto> loadAllClients(
      Client client, Configuration configuration) {
    HashMap<String, ClientDescriptionDto> allClients = new HashMap<>();
    String authUrl = configuration.getString(Vocabulary.Config.Auth.REST);
    ClientListDto clientList =
        client
            .target(authUrl + "/oauth2/clients")
            .request("application/json")
            .accept("application/json")
            .get(ClientListDto.class);

    for (ClientDescriptionDto clientDescription : clientList.getClients()) {
      allClients.put(clientDescription.getClientId(), clientDescription);
    }

    return allClients;
  }

  /**
   * Returns a LoginDTO for MDRFaces.
   *
   * @param clientId The clientID of the MDR
   * @param configuration the configuration
   * @return LoginDTO
   * @throws InvalidKeyException the invalid key exception
   * @throws NoSuchAlgorithmException the no such algorithm exception
   * @throws SignatureException the signature exception
   * @throws JwtException the JWT exception
   */
  public static LoginDto getClientCommunicationCode(String clientId, Configuration configuration)
      throws InvalidKeyException, NoSuchAlgorithmException, SignatureException, JwtException {
    Client client = ApplicationBean.getHttpConnector().getJerseyClientForHTTPS(false);

    AccessTokenDto accessToken = getAccessToken(client, configuration);

    LoginRequestDto loginRequest = new LoginRequestDto();
    loginRequest.setClientId(clientId);
    String authUrl = configuration.getString(Vocabulary.Config.Auth.REST);

    return client
        .target(authUrl + "/oauth2/login")
        .request("application/json")
        .header("Authorization", accessToken.getHeader())
        .post(Entity.json(loginRequest), LoginDto.class);
  }

  public static AccessTokenDto getAccessToken(Client client, Configuration configuration)
      throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    return getAccessToken(client, configuration, GrantType.CLIENT_CREDENTIALS);
  }

  /**
   * Gets an AccessTokenDTO accessToken from Samply.AUTH component, used for communication with
   * other components.
   */
  public static AccessTokenDto getAccessToken(
      Client client, Configuration configuration, String grantType)
      throws NoSuchAlgorithmException, InvalidKeyException, SignatureException {
    if (client == null) {
      return null;
    }

    AccessTokenDto accessTokenDto;
    OAuth2Client oauth2Client = getOAuth2Client(configuration);

    if (!oauth2Client.isUseSamplyAuth()) {
      AuthClient authcli = new KeycloakAuthClient(oauth2Client, client);
      authcli.setGrantType(grantType);
      String registryName = configuration.getString(Vocabulary.Config.registryName);
      authcli.setUsername(registryName);
      String pw = configuration.getString(Vocabulary.Config.Auth.RegistryPassword);
      authcli.setPassword(pw);
      try {
        accessTokenDto = new AccessTokenDto();

        JwtAccessToken accessToken = authcli.getAccessToken();
        if (accessToken == null) {
          Utils.getLogger().error("Could not retrieve tokens from keycloak");
          return null;
        }
        accessTokenDto.setAccessToken(accessToken.getSerialized());

        JwtIdToken idToken = authcli.getIdToken();
        accessTokenDto.setIdToken(idToken.getSerialized());

        JwtRefreshToken refreshToken = authcli.getRefreshToken();
        accessTokenDto.setRefreshToken(refreshToken.getSerialized());
        // TODO: who knows when it expires?! authclient does, but returns JWTAccessToken without
        //  info about expires_in
        // accessTokenDTO.setExpiresIn();
      } catch (InvalidTokenException e) {
        Utils.getLogger().error("Could not retrieve tokens from keycloak",e);
        return null;
      }
    } else {
      KeyIdentificationDto identification = new KeyIdentificationDto();

      String keyId = configuration.getString(Vocabulary.Config.Auth.KeyId);
      if (keyId == null || "".equalsIgnoreCase(keyId)) {
        return null;
      }

      identification.setKeyId(Integer.parseInt(keyId));
      String authUrl = configuration.getString(Vocabulary.Config.Auth.REST);
      // set timeout and catch it to prevent waiting infinitely, if auth not available
      Response response;
      response =
          client
              .property(ClientProperties.CONNECT_TIMEOUT, 10000)
              .property(ClientProperties.READ_TIMEOUT, 10000)
              .target(authUrl + "/oauth2/signRequest")
              .request("application/json")
              .accept("application/json")
              .post(Entity.json(identification), Response.class);
      if (response.getStatus() != 200) {
        Utils.getLogger()
            .error(
                "Auth.getAccessToken returned "
                    + response.getStatus()
                    + " on signRequest bailing out!");
        return null;
      }
      SignRequestDto signRequest = response.readEntity(SignRequestDto.class);

      /* Sign the code and encode to base64 */
      Signature sig = Signature.getInstance(signRequest.getAlgorithm());
      PrivateKey privkey =
          KeyLoader.loadPrivateKey(configuration.getString(Vocabulary.Config.Auth.MyPrivKey));
      sig.initSign(privkey);
      sig.update(signRequest.getCode().getBytes());
      String signature = Base64.encodeBase64String(sig.sign());

      AccessTokenRequestDto accessRequest = new AccessTokenRequestDto();
      accessRequest.setCode(signRequest.getCode());
      accessRequest.setSignature(signature);

      response =
          client
              .property(ClientProperties.CONNECT_TIMEOUT, 10000)
              .property(ClientProperties.READ_TIMEOUT, 10000)
              .target(authUrl + "/oauth2/access_token")
              .request("application/json")
              .accept("application/json")
              .post(Entity.json(accessRequest), Response.class);

      if (response.getStatus() != 200) {
        Utils.getLogger()
            .error("Auth.getAccessToken returned " + response.getStatus() + " bailing out!");
        return null;
      }
      accessTokenDto = response.readEntity(AccessTokenDto.class);
    }
    return accessTokenDto;
  }

  /**
   * Checks whether the configured auth server is available. Checks if the server returns status
   * 200 for a default endpoint. No credentials are being checked.
   *
   * @return True if the server returns status 200.
   */
  public static boolean isAvailable() {
    return isAvailable(null);
  }

  /**
   * Checks whether a given auth server is available. Checks if the server returns status 200 for
   * a default endpoint. No credentials are being checked.
   *
   * @param authUrlString The URL of the auth server / the server's REST interface.
   * @return True if the server returns status 200.
   */
  public static boolean isAvailable(String authUrlString) {
    OAuth2Client oauth2Client = getOAuth2Client(Utils.getAB().getConfig());
    if (authUrlString == null) {
      authUrlString = oauth2Client.getHost();
    }
    HttpConnector hc = ApplicationBean.getHttpConnector();

    if (!Utils.checkHostReachable(hc, oauth2Client.getHost())) {
      Utils.getLogger().error("Auth server not reachable. Host: " + oauth2Client.getHost());
      Utils.getLogger().debug("Was trying to reach " + oauth2Client.getHost()
          + " Used proxy scheme: " + hc.getProxy(oauth2Client.getHost()).getSchemeName()
          + " host: " + hc.getProxy(oauth2Client.getHost()).getHostName()
          + " port: " + hc.getProxy(oauth2Client.getHost()).getPort());
      return false;
    }

    Client client = hc.getJerseyClientForHTTPS(false);
    client
        .property(ClientProperties.CONNECT_TIMEOUT, 10000)
        .property(ClientProperties.READ_TIMEOUT, 10000);
    Response response = null;
    URI authUri = null;
    String authUriString = null;
    if (oauth2Client.isUseSamplyAuth()) {
      // for samply.auth check if key is the same
      authUriString = authUrlString + "/oauth2/certs";
    } else {
      // for Keycloak check if it is indeed a keycloak server
      authUriString = authUrlString + "/realms/" + oauth2Client.getRealm()
          + "/.well-known/openid-configuration";
    }
    try {
      authUri = new URI(authUriString);
    } catch (URISyntaxException e) {
      Utils.getLogger().error(
          "Couldn't build auth URI. Host: " + authUri.getHost() + " Path: " + authUri.getPath(), e);
    }
    if (authUri != null) {
      try {
        response = client.target(authUri).request().get();
      } catch (Exception e) {
        Utils.getLogger().error("Couldn't retrieve information from auth server. Host: "
            + authUrlString + " Realm: " + oauth2Client.getRealm() + " samply.auth: "
            + oauth2Client.isUseSamplyAuth(), e);
        URI u = URI
            .create((String) client.getConfiguration().getProperty(ClientProperties.PROXY_URI));
        Utils.getLogger().debug("Proxy URI from client (URI) Host: " + u.getHost()
            + " Port: " + u.getPort() + " Scheme: " + u.getScheme());
      }
    }
    if (response != null && response.getStatus() != 200) {
      Utils.getLogger().error("Auth URL check returned status " + response.getStatus() + ".");
      return false;
    }

    return true;
  }
}
