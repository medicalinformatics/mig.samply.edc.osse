/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.components;

import de.samply.edc.utils.Utils;
import java.text.Collator;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;
import java.util.List;
import java.util.ResourceBundle;
import javax.faces.model.SelectItem;

/**
 * This Broker provides a List of SelectItems of countries with an ISO 3166 ALPHA-3 encoding based
 * on the message bundle de.samply.edc.osse.messages.countries
 *
 * <p>In OSSE it's build in as a taglib. See /WEB-INF/countries.taglib.xml
 *
 * <p>Usage in your XHTML: <html xmlns:countries="http://osse.edc.samply.de/countries">
 *
 * <p><h:form> <h:selectOneMenu id="countryList" value="#{someBean.someField}"> <f:selectItem
 * itemLabel="" itemValue="" /> <f:selectItems value="#{countries:getCountriesFortemplate()}" />
 * </h:selectOneMenu> </h:form>
 */
public final class CountryTaglib {

  /** The countries fortemplate. */
  private static List<SelectItem> countriesFortemplate;

  /**
   * Gets the countries fortemplate.
   *
   * @return the countries fortemplate
   */
  public static List<SelectItem> getCountriesFortemplate() {
    if (countriesFortemplate == null || countriesFortemplate.isEmpty()) {
      countriesFortemplate = new ArrayList<>();
      buildSelectItemList();
      sortSelectItemsByLabel();
    }
    return countriesFortemplate;
  }

  /** Builds the select item list. */
  private static void buildSelectItemList() {
    ResourceBundle countries = Utils.getResourceBundle("de.samply.edc.osse.messages.countries");
    Enumeration<String> codes = countries.getKeys();
    while (codes.hasMoreElements()) {
      String code = codes.nextElement();
      countriesFortemplate.add(new SelectItem(code, countries.getString(code)));
    }
  }

  /** Sort select items by label. */
  private static void sortSelectItemsByLabel() {
    Collections.sort(
        countriesFortemplate,
        new Comparator<SelectItem>() {
          @Override
          public int compare(SelectItem siOne, SelectItem siOther) {
            Collator collator = Collator.getInstance();
            return collator.compare(siOne.getLabel(), siOther.getLabel());
          }
        });
  }
}
