/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.components;

import de.samply.edc.utils.Utils;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import javax.faces.component.FacesComponent;
import javax.faces.component.NamingContainer;
import javax.faces.component.UIInput;
import javax.faces.component.UINamingContainer;
import javax.faces.context.FacesContext;
import javax.faces.convert.ConverterException;

/**
 * Faces component to present select-one fields for month + year and databind it to a Date Class.
 *
 * <p>Usage: <html xmlns:my="http://java.sun.com/jsf/composite/components" >
 * <my:inputDate value="#{bean.variable}" maxyear="2020" minyear="1990" />
 */
@FacesComponent("inputMonthYear")
public class InputMonthYear extends UIInput implements NamingContainer {

  /** The selected month. */
  private UIInput monthUI;

  /** The selected year. */
  private UIInput yearUI;

  /**
   * Create an integer array with values from specified begin to specified end, inclusive.
   *
   * @param begin the begin
   * @param end the end
   * @return the integer[]
   */
  private static Integer[] createIntegerArray(int begin, int end) {
    int direction = Integer.compare(end, begin);
    int size = Math.abs(end - begin) + 1;
    Integer[] array = new Integer[size];

    for (int i = 0; i < size; i++) {
      array[i] = begin + (i * direction);
    }

    return array;
  }

  /**
   * Returns the component family of {@link UINamingContainer}. (that's just required by composite
   * component)
   *
   * @return the family
   */
  @Override
  public String getFamily() {
    return UINamingContainer.COMPONENT_FAMILY;
  }

  /**
   * Set the selected and available values of the day, month and year fields based on the model.
   *
   * @param context the context
   * @throws IOException Signals that an I/O exception has occurred.
   */
  @Override
  public void encodeBegin(FacesContext context) throws IOException {
    Calendar calendar = Calendar.getInstance();
    int maxYear = getAttributeValue("maxyear", calendar.get(Calendar.YEAR));
    int minYear = getAttributeValue("minyear", maxYear - 100);
    Date date = (Date) getValue();

    if (date != null) {
      calendar.setTime(date);
      int year = calendar.get(Calendar.YEAR);

      if (year > maxYear || minYear > year) {
        throw new IllegalArgumentException(
            String.format("Year %d out of min/max range %d/%d.", year, minYear, maxYear));
      }
    }

    if (monthUI.getLocalValue() == null) {
      monthUI.setValue(calendar.get(Calendar.MONTH) + 1);
      yearUI.setValue(calendar.get(Calendar.YEAR));
    }

    setMonths(createIntegerArray(1, calendar.getActualMaximum(Calendar.MONTH) + 1));
    setYears(createIntegerArray(maxYear, minYear));
    super.encodeBegin(context);
  }

  /**
   * Returns the submitted value in MM-yyyy format.
   *
   * @return the submitted value
   */
  @Override
  public Object getSubmittedValue() {
    Object moo;

    moo = monthUI.getSubmittedValue() + "-" + yearUI.getSubmittedValue();

    return moo;
  }

  /**
   * Converts the submitted value to a concrete {@link Date} instance.
   *
   * @param context the context
   * @param submittedValue the submitted value
   * @return the converted value
   */
  @Override
  protected Object getConvertedValue(FacesContext context, Object submittedValue) {
    try {
      return new SimpleDateFormat("MM-yyyy").parse((String) submittedValue);
    } catch (ParseException e) {
      Utils.getLogger().error("Error while parsing date!");
      throw new ConverterException(e); // This is not to be expected in
      // normal circumstances.
    }
  }

  /**
   * Return specified attribute value or otherwise the specified default if it's null.
   *
   * @param <T> the generic type
   * @param key the key
   * @param defaultValue the default value
   * @return the attribute value
   */
  @SuppressWarnings("unchecked")
  private <T> T getAttributeValue(String key, T defaultValue) {
    T value = (T) getAttributes().get(key);
    return (value != null) ? value : defaultValue;
  }

  /**
   * Gets the selected month.
   *
   * @return the month
   */
  public UIInput getMonthUI() {
    return monthUI;
  }

  /**
   * Sets the selected month.
   *
   * @param month the new month
   */
  public void setMonthUI(UIInput month) {
    this.monthUI = month;
  }

  /**
   * Gets the selected year.
   *
   * @return the year
   */
  public UIInput getYearUI() {
    return yearUI;
  }

  /**
   * Sets the selected year.
   *
   * @param year the new year
   */
  public void setYearUI(UIInput year) {
    this.yearUI = year;
  }

  /**
   * Gets the list of valid months.
   *
   * @return the months
   */
  public Integer[] getMonths() {
    return (Integer[]) getStateHelper().get("months");
  }

  /**
   * Sets the list of valid months.
   *
   * @param months the new months
   */
  public void setMonths(Integer[] months) {
    getStateHelper().put("months", months);
  }

  /**
   * Gets the list of valid years.
   *
   * @return the years
   */
  public Integer[] getYears() {
    return (Integer[]) getStateHelper().get("years");
  }

  /**
   * Sets the list of valid years.
   *
   * @param years the new years
   */
  public void setYears(Integer[] years) {
    getStateHelper().put("years", years);
  }
}
