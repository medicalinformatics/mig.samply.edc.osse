/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.osse.model.Patient;
import de.samply.edc.osse.model.RoRForm;
import de.samply.edc.osse.model.RoRMap;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.ResourceQuery;
import java.util.ArrayList;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * View scoped bean to display RoR forms, which are a special version of the forms used in the
 * registry.
 */
@ManagedBean(name = "rorviewer")
@ViewScoped
public class RoRViewer extends AbstractViewBean {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7188838794010482359L;

  /** The RoR form data entries. */
  private RoRMap<String, Object> entries;

  /** The form we're looking at. */
  private RoRForm form = null;

  private AbstractDatabase<?> database;

  private JSONResource config;

  /**
   * Post-construct init loads the values of the form.
   *
   * @see de.samply.edc.control.AbstractViewBean#init()
   */
  @Override
  @PostConstruct
  public void init() {
    database = getSessionBean().getDatabase();
    config = database.getConfig("osse");
    loadValues();
  }

  /** Method to load data. */
  private void loadValues() {
    ArrayList<Resource> all = database.getResources(OSSEVocabulary.Type.Registry);

    if (all == null || all.isEmpty()) {
      // no data yet? create a new form
      form = new RoRForm(database);
      form.save();
    } else {
      form = new RoRForm(database, all.get(0).getValue());
    }

    form.load(config);
    entries = new RoRMap<>(form);

    // enter the amount of cases of this registry into the form
    entries.put("urn__osse-ror__dataelement__21__1", getAmountCases());
  }

  /**
   * Saves the form data.
   *
   * @param wasImported the was imported
   * @return the string
   * @see de.samply.edc.control.AbstractViewBean#save(java.lang.Boolean)
   */
  @Override
  public String save(Boolean wasImported) {
    if (form == null) {
      Utils.displayContextMessage("summary_savefailed", "error_general_noformorvisit");
      return null;
    }

    form.save();

    // say Yay!
    Utils.displayContextMessage("summary_savesuccess", "success_save");

    // logbook
    getSessionBean().addLog("saved RoRform.");

    return "save";
  }

  /**
   * Gets the amount of cases in this registry.
   *
   * @return the amount
   */
  public Integer getAmountCases() {
    Database database = new Database(true);
    ResourceQuery queryPatients = new ResourceQuery(OSSEVocabulary.Type.Patient);
    List<Resource> resultPatients = database.getResources(queryPatients);

    Integer amountCases = 0;

    for (Resource patientResource : resultPatients) {
      Patient patient = new Patient(database, patientResource);
      patient.load(config);

      if (patient.isDeleted()) {
        continue;
      }

      if (patient.getPatientID() == null) {
        continue;
      }

      // fetch all cases
      patient.loadChildren(OSSEVocabulary.Type.Case, false, config);
      amountCases += patient.getCases(config).size();
    }

    return amountCases;
  }

  /**
   * Method to add a row to a data table.
   *
   * @param name the name of the data table to add a row to
   */
  @Override
  public void addRow(String name) {
    DatatableRows stl = null;

    if (!entries.containsKey(name)) {
      stl = new DatatableRows();
    } else {
      stl = (DatatableRows) entries.get(name);
    }

    stl.add(new DatatableRow());
    entries.put(name, stl);
  }

  /**
   * Method to delete a row from a data table.
   *
   * @param tableName Name of the data table
   * @param rowToBeRemoved Row to be removed
   */
  @Override
  public void deleteRow(String tableName, DatatableRow rowToBeRemoved) {
    if (!entries.containsKey(tableName)) {
      return;
    }

    DatatableRows stl = (DatatableRows) entries.get(tableName);
    stl.remove(rowToBeRemoved);

    entries.put(tableName, stl);
  }

  /**
   * Gets the RoR form data entries.
   *
   * @return the entries
   */
  public RoRMap<String, Object> getEntries() {
    if (form == null) {
      Utils.getLogger().debug("getEntries called with form = null");
      return entries;
    }
    if (entries == null) {
      entries = new RoRMap<>(form);
    }
    return entries;
  }

  /**
   * Sets the RoR form data entries.
   *
   * @param entries the entries
   */
  public void setEntries(RoRMap<String, Object> entries) {
    this.entries = entries;
  }

  /**
   * Gets the form.
   *
   * @return the form
   */
  public RoRForm getForm() {
    return form;
  }
}
