/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import com.google.common.base.Splitter;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.osse.model.Form;
import de.samply.edc.osse.model.MyMap;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

/**
 * A quick hack to enable showing entries from external forms.
 *
 * <p>Set the variables for formname and formtype in the generated form. E.g.
 *
 * <p><ui:composition> <c:set var="externalForms" value="form_1_ver-2:episode::form_2_ver-4::case"
 * scope="request" /> </ui:composition>
 *
 * <p>WARNING: experimental. You HAVE to make sure, the form names are correct, and the form type is
 * correct. You can only use repeatable elements to generate a list of select items of now. Records
 * (and worse: repeatable records) are not yet supported at all!
 *
 * <p>To access a single dataelement, use:
 *
 * <p><h:outputText
 * value="#{externalFormsBean.entries['form_2_ver-1']['urn__osse-1__dataelement__2__1']}" />
 *
 * <p>To access the select items, use:
 *
 * <p>For Repeatable Dataelements <h:selectOneMenu><f:selectItems
 * value="#{externalFormsBean.getSelectItems('form_5_ver-1', 'urn__osse-1__dataelement__1__1')}" />
 * </h:selectOneMenu>
 *
 * <p>For entries from repeatable records <h:selectOneMenu> <f:selectItems
 * value="#{externalFormsBean.getSelectItemsFromRecord('form_4_ver-1', 'urn__osse-1__record__1__1',
 * 'urn__osse-1__dataelement__1__1')}"></f:selectItems></h:selectOneMenu>
 *
 * <p>...substituting the formnames and dataelement names of course
 */
@ManagedBean(name = "externalFormsBean")
@ViewScoped
public class ExternalFormsBean implements Serializable {

  private AbstractDatabase<?> database;

  private JSONResource config;

  private Map<String, MyMap<String, Object>> entries;

  public Map<String, MyMap<String, Object>> getEntries() {
    return entries;
  }

  public void setEntries(Map<String, MyMap<String, Object>> entries) {
    this.entries = entries;
  }

  /**
   * Only use this on repeatable records.
   */
  public List<SelectItem> getSelectItemsFromRecord(String form, String recordName, String key) {
    List<SelectItem> items = new ArrayList<>();
    MyMap<String, Object> formEntries = entries.get(form);

    // If the form is not found or does not contain the record...return an empty list
    if (formEntries == null || !formEntries.containsKey(recordName)) {
      return items;
    }

    DatatableRows datatableRows = (DatatableRows) formEntries.get(recordName);

    for (DatatableRow row : datatableRows.getRows()) {
      Iterator it = row.getColumns().entrySet().iterator();
      while (it.hasNext()) {
        Map.Entry pair = (Map.Entry) it.next();
        if (key.equalsIgnoreCase((String) pair.getKey())) {
          items.add(new SelectItem(pair.getValue()));
        }
      }
    }

    return items;
  }

  /**
   * Only use this on repeatable dataelements (not records!).
   */
  public List<SelectItem> getSelectItems(String form, String key) {
    List<SelectItem> items = new ArrayList<>();
    MyMap<String, Object> formEntries = entries.get(form);

    // If the form is not found or does not contain the record...return an empty list
    if (formEntries == null || !formEntries.containsKey(key)) {
      return items;
    }

    DatatableRows datatableRows = (DatatableRows) formEntries.get(key);

    for (DatatableRow row : datatableRows.getRows()) {
      Iterator it = row.getColumns().entrySet().iterator();
      while (it.hasNext()) {
        Map.Entry pair = (Map.Entry) it.next();
        items.add(new SelectItem(pair.getValue()));
      }
    }

    return items;
  }

  /**
   * Get a list of select items, that can be filled from multiple sources
   * (ONLY repeatable elements, not records!).*
   * Split Entries with "::" and forms/elements with ":"
   * e.g.
   * form_5_ver-7:urn__osse-1__dataelement__24__2::form_7_ver-5:urn__osse-1__dataelement__50__2
   */
  public List<SelectItem> getSelectItemsFromMultipleForms(
      String concatenatedListOfFormsAndElements) {
    List<SelectItem> selectItems = new ArrayList<>();
    try {
      List<String> entries =
          Splitter.on("::")
              .omitEmptyStrings()
              .trimResults()
              .splitToList(concatenatedListOfFormsAndElements);

      for (String entry : entries) {
        List<String> parts = Splitter.on(":").omitEmptyStrings().trimResults().splitToList(entry);

        if (parts.size() != 2) {
          continue;
        }
        selectItems.addAll(getSelectItems(parts.get(0), parts.get(1)));
      }
      return selectItems;
    } catch (Exception e) {
      Utils.getLogger().warn("Unable to get selected items.",e);
      return selectItems;
    }
  }

  /**
   * TODO: add javadoc.
   */
  @PostConstruct
  public void init() {
    config = Utils.getSB().getConfig();
    HttpServletRequest request =
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    String formnames = (String) request.getAttribute("externalForms");

    entries = new HashMap<>();

    List<String> forms = Splitter.on("::").splitToList(formnames);
    for (String f : forms) {
      List<String> formNameParts = Splitter.on(":").splitToList(f);
      if (formNameParts == null || formNameParts.size() != 2) {
        continue;
      }
      Form form;
      if ("episode".equalsIgnoreCase(formNameParts.get(1))) {
        form =
            ((SessionBean) Utils.getSB()).getCurrentEpisode().getForm(formNameParts.get(0), config);
      } else {
        form = ((SessionBean) Utils.getSB()).getCurrentCase().getForm(formNameParts.get(0), config);
      }
      if (form != null) {
        form.load(config);
        MyMap<String, Object> entry = new MyMap<>(form);
        entries.put(formNameParts.get(0), entry);
      }
    }
  }
}
