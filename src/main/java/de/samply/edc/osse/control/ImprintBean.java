/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.samply.edc.utils.Utils;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

@ManagedBean
@ViewScoped
public class ImprintBean {

  ApplicationBean applicationBean;
  Map<String, String> privacyLinks;
  Map<String, String> imprintLinks;
  String currentLanguage;
  List<String> languages;
  ObjectMapper mapper = new ObjectMapper();

  /**
   * Postconstruct init, loads the current configuration.
   */
  //@Override
  @PostConstruct
  public void init() {
    //super.init();
    applicationBean = (ApplicationBean) Utils.getAB();
    languages = new ArrayList<>();
    languages.add("default");
    languages.addAll(applicationBean.getSupportedLocales());

    currentLanguage = languages.get(0);

    String imprintJson = applicationBean.getOsseConfig().getString("link.imprint");
    String privacyJson = applicationBean.getOsseConfig().getString("link.privacy");
    try {
      if (imprintJson != null) {
        imprintLinks = mapper.readValue(imprintJson, HashMap.class);
      } else {
        imprintLinks = new HashMap<>();
        imprintLinks.put("default", "");
      }
      if (privacyJson != null) {
        privacyLinks = mapper.readValue(privacyJson, HashMap.class);
      } else {
        privacyLinks = new HashMap<>();
        privacyLinks.put("default", "");
      }
    } catch (IOException e) {
      Utils.getLogger().warn("Unable to load privacy and imprint links!", e);
    }

  }

  public void setPrivacyLink(String language, String link) {
    privacyLinks.put(language, link);
  }

  public void setImprintLink(String language, String link) {
    imprintLinks.put(language, link);
  }

  /**
   * Get the current correct privacy link based on language settings.
   */
  public String getPrivacyLink() {
    String language = Utils.getSB().getLocale().getLanguage();
    String link = privacyLinks.get(language);
    if (link == null || link.equals("")) {
      link = privacyLinks.get("default");
    }
    return link;
  }

  /**
   * Get the current correct imprint link based on language settings.
   */
  public String getImprintLink() {
    String language = Utils.getSB().getLocale().getLanguage();
    String link = imprintLinks.get(language);
    if (link == null || link.equals("")) {
      link = imprintLinks.get("default");
    }
    return link;
  }

  /**
   * Store the link settings.
   */
  public void saveSettings() {
    try {
      String imprintJson = mapper.writeValueAsString(imprintLinks);
      String privacyJson = mapper.writeValueAsString(privacyLinks);
      applicationBean.getOsseConfig().removeProperties("link.imprint");
      applicationBean.getOsseConfig().removeProperties("link.privacy");
      applicationBean.getOsseConfig().setProperty("link.imprint", imprintJson);
      applicationBean.getOsseConfig().setProperty("link.privacy", privacyJson);
      Utils.getSB().getDatabase().saveConfig("osse", applicationBean.getOsseConfig());
      Utils.addContextMessage("Saving successful", "Your changes have been saved.");
    } catch (JsonProcessingException e) {
      Utils.getLogger().warn("Unable to store privacy and imprint link settings!", e);
    }
  }


  public List<String> getLanguages() {
    return languages;
  }

  public void setLanguages(List<String> languages) {
    this.languages = languages;
  }

  public String getCurrentLanguage() {
    return currentLanguage;
  }

  public void setCurrentLanguage(String currentLanguage) {
    this.currentLanguage = currentLanguage;
  }

  public Map<String, String> getPrivacyLinks() {
    return privacyLinks;
  }

  public void setPrivacyLinks(Map<String, String> privacyLinks) {
    this.privacyLinks = privacyLinks;
  }

  public Map<String, String> getImprintLinks() {
    return imprintLinks;
  }

  public void setImprintLinks(Map<String, String> imprintLinks) {
    this.imprintLinks = imprintLinks;
  }

}
