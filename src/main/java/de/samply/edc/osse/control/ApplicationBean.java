/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.auth.client.GrantType;
import de.samply.auth.client.jwt.JwtException;
import de.samply.auth.rest.AccessTokenDto;
import de.samply.auth.rest.LoginDto;
import de.samply.common.http.HttpConnector;
import de.samply.common.http.HttpConnectorException;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.catalog.Vocabulary.Config;
import de.samply.edc.catalog.Vocabulary.Config.FormEditor;
import de.samply.edc.catalog.Vocabulary.Config.Mdr;
import de.samply.edc.control.AbstractApplicationBean;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.catalog.EpisodePatterns;
import de.samply.edc.osse.dto.teiler.TeilerAccountDto;
import de.samply.edc.osse.dto.teiler.TeilerDto;
import de.samply.edc.osse.model.InitDbModel;
import de.samply.edc.osse.upgrade.UpgradeExecution;
import de.samply.edc.osse.upgrade.UpgradeExecutionException;
import de.samply.edc.osse.upgrade.UpgradeExecutionReconfigure;
import de.samply.edc.osse.upgrade.dto.Upgrades;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.osse.utils.FormRecreationHelper;
import de.samply.edc.osse.utils.OsseUtils;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.edc.validator.PasswordComplexityValidator;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.TablesNotFoundException;
import de.samply.store.osse.OSSEModel;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.ResourceQuery;
import de.samply.web.mdrfaces.MdrContext;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.concurrent.ExecutionException;
import java.util.jar.Manifest;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.bean.ApplicationScoped;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.ws.rs.client.Client;
import javax.ws.rs.client.Entity;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import org.apache.commons.configuration.Configuration;
import org.glassfish.jersey.client.ClientProperties;

/** The application scoped Bean. */
@ManagedBean(name = "applicationBean", eager = true)
@ApplicationScoped
public class ApplicationBean extends AbstractApplicationBean {

  /** Internal var to force a recreation of forms. only used for DEVELOPMENT purposes */
  private static final Boolean redoForms = false;

  /**
   * This number defines how many upgrades shall be executed, so when there is a new upgrade to be
   * executed on application start, upgrade this number to the necessary internal Revision.
   */
  private static final Integer requiredVersion = 21;

  private static final String CONFIG_FILENAME_BACKEND = Utils.findConfigurationFile("backend.xml");
  private static HttpConnector httpConnector;
  private static boolean isBridgehead;
  /** Contains the configuration of the OSSE Register in a JSONResource format. */
  private JSONResource osseConfig;
  /** Contains the configuration of Design of the OSSE Register in a JSONResource format. */
  private JSONResource designConfig;
  /** Contains the xhtml of all forms (old and current) ever created for this registry. */
  private JSONResource osseFormStorage;
  /** Contains all import XSDs used in the registry. */
  private JSONResource osseXsdStorage;
  /** Contains additional information about the build. */
  private Properties osseProperties = new Properties();
  /** A HashMap of all status a form may have and which resource Id it is linked to. */
  private HashMap<Integer, String> formStatus = new HashMap<>();
  /** A reverse of the map above. */
  private HashMap<String, Integer> formStatusReverse = new HashMap<>();
  /** The standard status number of a form. */
  private String formStandardStatusNumber;
  /**
   * A map of status and the valid status they may transition to e.g. "reported":
   * ["validated","open"]
   */
  private HashMap<String, ArrayList<String>> statusTransitions;
  /**
   * The formulars available in the registry by language code The LinkedHashMap: key = formname,
   * value = form title.
   */
  private HashMap<String, LinkedHashMap<String, String>> formulars = new HashMap<>();
  /**
   * The archived formulars (old versions of formulars). The LinkedHashMap: key = formname, value =
   * form title
   */
  private HashMap<String, LinkedHashMap<String, String>> archivedFormulars = new HashMap<>();
  /** The episode formulars. */
  private HashMap<String, LinkedHashMap<String, String>> visitFormulars = new HashMap<>();
  /** The archived episode formulars. */
  private HashMap<String, LinkedHashMap<String, String>> archivedVisitFormulars = new HashMap<>();
  /** The patient forms. */
  private ArrayList<String> patientForms = new ArrayList<>();
  /** A map storing which mdr entity is in which form. */
  private HashMap<String, List<String>> mdrEntityIsInForm = new HashMap<>();
  /** A map storing which form contains which mdr entities. */
  private HashMap<String, List<String>> formContainsMdrEntity = new HashMap<>();
  /** List of MDR entities in a record. */
  private HashMap<String, List<String>> recordHasMdrEntities = new HashMap<>();
  /** Name of the main patientuser case form. */
  private String patientUserPatientMainPage;
  /** Name of the main patientuser episode form. */
  private String patientUserEpisodeMainPage;
  /** The project's manifest. */
  private Manifest manifest;

  /**
   * Check if this application is a Bridgehead of OSSE.
   *
   * @return boolean
   */
  public static Boolean isBridgehead() {
    return ApplicationBean.isBridgehead;
  }

  public static HttpConnector getHttpConnector() {
    return httpConnector;
  }

  public static String getConfigFilenameBackend() {
    return CONFIG_FILENAME_BACKEND;
  }

  /**
   * Reads the config and form storage and stores it in the JSONResources above. It also copies the
   * Apache configuration data into the OSSEconfig JSONResource.
   *
   * @param model the backend database model
   * @throws DatabaseException if a database error occurs
   */
  public void readConfig(OSSEModel model) throws DatabaseException {
    osseXsdStorage = model.getConfig("osse.xsd.storage");
    if (osseXsdStorage == null) {
      osseXsdStorage = new JSONResource();
    }

    osseFormStorage = model.getConfig("osse.form.storage");
    if (osseFormStorage == null) {
      osseFormStorage = new JSONResource();
    }

    osseConfig = model.getConfig("osse");
    if (osseConfig == null) {
      osseConfig = new JSONResource();
    }

    try {
      osseProperties.load(this.getClass().getClassLoader().getResourceAsStream("osse.properties"));
    } catch (Exception e) {
      Utils.getLogger().error("Could not load osse.properties file.", e);
    }

    designConfig = model.getConfig("design");

    isBridgehead = Utils.isBridgehead(osseConfig);

    String[] proxySettings = {
      Vocabulary.Config.Standard.proxyHTTPHost,
      Vocabulary.Config.Standard.proxyHTTPSHost,
      Vocabulary.Config.Standard.proxyHTTPPort,
      Vocabulary.Config.Standard.proxyHTTPSPort,
      Vocabulary.Config.Standard.proxyHTTPUsername,
      Vocabulary.Config.Standard.proxyHTTPPassword,
      Vocabulary.Config.Standard.proxyHTTPSUsername,
      Vocabulary.Config.Standard.proxyHTTPSPassword,
      Vocabulary.Config.Standard.proxyRealm
    };

    // inject proxy settings, so they dont get overloaded in the next step
    for (String propertySet : proxySettings) {
      String setting = config.getString(propertySet);
      if (setting == null || setting.equals("")) {
        osseConfig.setProperty(propertySet, "");
      } else {
        osseConfig.setProperty(propertySet, setting);
      }
    }

    // inject osseConfig into Config, so both ways of configuration are
    // returning the same values
    for (String key : osseConfig.getDefinedProperties()) {
      config.setProperty(key, osseConfig.getProperty(key).getValue());
    }

    // Get formstatus und transitions
    formStatus = new HashMap<>();
    formStatusReverse = new HashMap<>();

    // for now the standard status of a form has the number 1
    setFormStandardStatusNumber("1");

    // Get statuses
    ResourceQuery statusQuery = new ResourceQuery(OSSEVocabulary.Type.Status);
    statusQuery.setFetchAdjacentResources(false);

    ArrayList<Resource> statusResources = model.getResources(statusQuery);
    for (Resource statusResource : statusResources) {
      formStatus.put(
          statusResource.getProperty(OSSEVocabulary.ID).asInteger(),
          statusResource.getProperty("name").getValue());
      formStatusReverse.put(
          statusResource.getProperty("name").getValue(),
          statusResource.getProperty(OSSEVocabulary.ID).asInteger());
    }

    if (formStatus.isEmpty()) {
      Utils.getLogger().warn("*** There were no formStatus defined! At least one has to be!");
    }

    // Transitions are yet only predefined
    HashMap<String, ArrayList<String>> statusTransitions = new HashMap<>();
    ArrayList<String> temp = new ArrayList<>();
    temp.add("reported");
    statusTransitions.put("open", temp);

    temp.clear();

    temp.add("open");
    temp.add("validated");
    statusTransitions.put("reported", temp);

    temp.clear();
    temp.add("open");
    statusTransitions.put("validated", temp);
  }

  /**
   * Post-construct initialization. Reads in the configuration, but also initiates the installation
   * if the registry has not yet completely been installed
   *
   * @see de.samply.edc.control.AbstractApplicationBean#init()
   */
  @Override
  @PostConstruct
  public void init() {
    super.init();

    OSSEModel model = null;
    if (osseConfig == null) {
      osseConfig = new JSONResource();
    }

    loadManifest();

    Utils.getLogger().debug("USING BACKEND FILE : " + CONFIG_FILENAME_BACKEND);
    Boolean configWasRead = false;

    try {
      model = new OSSEModel(CONFIG_FILENAME_BACKEND);
      model.testTables();

      model.injectLogin("admin");
      readConfig(model);
      model.logout();
      model.close();
      configWasRead = true;
    } catch (TablesNotFoundException e) {
      // This means, the registry was freshly installed, but the database
      // was not created yet, so we create it now, and inject the
      // necessary
      // base users and roles necessary for first administration

      Utils.getLogger().debug("===========================================");
      Utils.getLogger().info("Installing Database");
      Utils.getLogger().debug("===========================================");

      try {
        if (model != null) {
          model.close();
        }
        InitDbModel tmodel = new InitDbModel(CONFIG_FILENAME_BACKEND);
        tmodel.installDatabase();
        tmodel.insertAdminAndDev();
        tmodel.insertStatuses();
        tmodel.insertOsseConfig(this.config);

        tmodel.logout();
        tmodel.close();
      } catch (Exception ee) {
        Utils.getLogger().error("Error while init Database!", ee);
      }
    } catch (DatabaseException e) {
      e.printStackTrace();
      throw new Error("Database Exception prevents start of OSSE.");
    }

    if (!configWasRead) {
      try {
        model = new OSSEModel(CONFIG_FILENAME_BACKEND);
        model.injectLogin("admin");
        readConfig(model);
        model.logout();
        model.close();
      } catch (DatabaseException e) {
        Utils.getLogger().error("Error while creating new OsseModel!",e);
      }
    }

    httpConnector = new HttpConnector(getConfig());

    // Init MdrFacesClient
    MdrClient mdrClient;

    if (httpConnector == null) {
      Utils.getLogger()
          .error("HTTPclient could not be started. MDR Client in context (used by MDRFaces) "
              + "will not support proxy connection!");
      mdrClient = new MdrClient(osseConfig.getProperty(Vocabulary.Config.Mdr.REST).getValue());
    } else {
      Client mdrJerseyClient =
          httpConnector.getJerseyClient(
              osseConfig.getProperty(Vocabulary.Config.Mdr.REST).getValue(), false);
      mdrJerseyClient.property(ClientProperties.CONNECT_TIMEOUT, 5000);

      mdrClient =
          new MdrClient(
              osseConfig.getProperty(Vocabulary.Config.Mdr.REST).getValue(), mdrJerseyClient);
    }

    try {
      JSONResource usedMdrKeys =
          getOsseConfig().getProperty(Vocabulary.Config.mdrEntityIsInForm).asJSONResource();
      // Using a (wrongly sized) String array is better (performance-wise) than instantiating one
      // with the correct size,
      // according to
      // https://stackoverflow.com/questions/4042434/converting-arrayliststring-to-string-in-java
      mdrClient.preCacheKeys(
          usedMdrKeys.getDefinedProperties(), supportedLocales.toArray(new String[0]));
    } catch (ExecutionException | MdrConnectionException | MdrInvalidResponseException e) {
      Utils.getLogger().warn("Could not pre-cache keys",e);
    } catch (NullPointerException npe) {
      Utils.getLogger()
          .warn("NPE caught while trying to get list of used mdr keys. Probably none have been "
              + "set yet.");
    }

    // Check if there are any upgrades to be executed
    checkForUpgrades(CONFIG_FILENAME_BACKEND);

    MdrContext.getMdrContext().init(mdrClient);

    // Initialise the List of Forms
    initFormLists();

    // Pre caching mdr fields, so forms don't take ages to load on first
    // access
    // TODO: needs to check for records to load them "different" etc.
    // TODO: needs more mdrclient/faces support as this currently does not
    // function properly in it,
    // so I commented this out for now

    // for(String key: mdrEntityIsInForm.keySet()) {
    // Utils.getLogger().debug("MDR client for key "+key);
    // Definition dataElementDefinition;
    // try {
    // dataElementDefinition = mdrClient.getDataElementDefinition(key,
    // Utils.getLanguage());
    // Utils.getLogger().debug("DataDefinition: "+dataElementDefinition);
    // } catch (MdrConnectionException | MdrInvalidResponseException
    // | ExecutionException e) {
    // // TODO Auto-generated catch block
    // e.printStackTrace();
    // }
    // }

    // initialize password complexity validator
    initPasswordComplexityValidator();

    //initalize api settings
    initEdcApiSettings();

    // TODO: add config (or config entry) for catalogues to preload.
    //        ServletContext servletContext =
    // (ServletContext)FacesContext.getCurrentInstance().getExternalContext().getContext();
    //        if ("kidsafe".equalsIgnoreCase(servletContext.getServletContextName()) ) {
    //            for (Value catalogueValue : OSSEKidsafeConfig.getProperties("preload-catalogues"))
    // {
    //                CatalogueBean.preloadCatalogue(catalogueValue.getValue(), Utils.getLanguage(),
    // null, null);
    //            }
    //        }
  }

  /**
   * Save config.
   *
   * @param name the name
   * @param config the config
   * @throws DatabaseException the database exception
   */
  private void saveConfig(String name, JSONResource config) throws DatabaseException {
    InitDbModel model = new InitDbModel(CONFIG_FILENAME_BACKEND);
    model.saveOsseConfig(name, config);
    model.logout();
    model.close();
  }

  /** Inits api settings. */
  private void initEdcApiSettings() {
    // generate a new api key if none is set
    if (getOsseConfig().getProperty("importer.apikey") == null) {
      String apikey = ConfigurationBean.getNewApikey();
      setOsseConfig("importer.apikey",apikey);
    }
  }

  /** Inits the password complexity validator. */
  private void initPasswordComplexityValidator() {
    // Minimum length of a password (minimal 4 is accepted, standard is 8)
    Integer minPasswordLength = getOsseConfigAsInteger(Vocabulary.Config.Password.minLength);
    if (minPasswordLength == null) {
      // not initialized yet, so we do it here to standard settings
      setOsseConfig(Vocabulary.Config.Password.minLength, 8);
      setOsseConfig(Vocabulary.Config.Password.maxLength, 255);
      setOsseConfig(Vocabulary.Config.Password.minLowercase, 1);
      setOsseConfig(Vocabulary.Config.Password.minUppercase, 1);
      setOsseConfig(Vocabulary.Config.Password.minSpecial, 1);
      setOsseConfig(Vocabulary.Config.Password.minNumerical, 1);
      try {
        saveConfig("osse", osseConfig);
      } catch (DatabaseException e) {
        Utils.getLogger().error("Unable to save password complexity check settings",e);
      }
      minPasswordLength = getOsseConfigAsInteger(Vocabulary.Config.Password.minLength);
    }

    if (minPasswordLength < 4) {
      minPasswordLength = 4;
    }
    // Maximum length of a password (if 0 then it can be huge = 255 chars
    // long)
    Integer maxPasswordLength = getOsseConfigAsInteger(Vocabulary.Config.Password.maxLength);
    if (maxPasswordLength == null) {
      maxPasswordLength = 255;
    }
    if (maxPasswordLength < 1) {
      maxPasswordLength = 255;
    }
    // Minimum amount of lowercase alpha characters in the password
    Integer minLowerAlphaChars = getOsseConfigAsInteger(Vocabulary.Config.Password.minLowercase);
    if (minLowerAlphaChars == null) {
      minLowerAlphaChars = 1;
    }
    // Minimum amount of uppercase alpha characters in the password
    Integer minUpperAlphaChars = getOsseConfigAsInteger(Vocabulary.Config.Password.minUppercase);
    // Minimum amount of special characters in the password
    Integer minSpecialChars = getOsseConfigAsInteger(Vocabulary.Config.Password.minSpecial);
    // Minimum amount of numerical characters in the password
    Integer minNumericalChars = getOsseConfigAsInteger(Vocabulary.Config.Password.minNumerical);

    // The following configurations are yet not changeable by the
    // adminstrator and are mostly unused in the registry

    // Allow extended ascii characters in the password?
    boolean allowExtendedAsciiSymbols = false;
    // The password must differ by X amount of characters compared to the
    // last one
    int lastPasswordDifferInChars = 4;
    // Validate we haven't used this password in this many iterations (we
    // use the list of old pw's you pass in for this)
    int passwordHistoryLen = 4;
    // Allow phone numbers in the password?
    boolean allowPhoneNumbers = false;
    // Allow dates in the password?
    boolean allowDates = false;
    // Deny dictionary words within the password?
    boolean restrictedByDictionary = false;
    // Default Bloom Filter settings
    float dictionaryAccuracy = 17;
    // Default dictionary word length. Anything smaller and you'll get lots
    // of hits
    int dictionaryMinWordLength = 4;

    PasswordComplexityValidator.configure(
        minPasswordLength,
        maxPasswordLength,
        minLowerAlphaChars,
        minUpperAlphaChars,
        minSpecialChars,
        minNumericalChars,
        allowExtendedAsciiSymbols,
        lastPasswordDifferInChars,
        passwordHistoryLen,
        allowPhoneNumbers,
        allowDates,
        restrictedByDictionary,
        dictionaryAccuracy,
        dictionaryMinWordLength);
  }

  /**
   * Check for upgrades and executes them.
   *
   * @param configFile the config file
   */
  @SuppressWarnings("unchecked")
  private void checkForUpgrades(String configFile) {
    Utils.getLogger().debug("--------------------\n --- Upgrades starting\n--------------------");

    String upgradeFileName = "theOneUpgrade.xml";
    String upgradeFile = Utils.findConfigurationFile(upgradeFileName);
    Upgrades upgrades = null;

    if (upgradeFile == null) {
      Utils.getLogger().error("Cannot execute any upgrade: There was no upgrades file present!");
      return;
    }

    JAXBContext jaxbContext;
    try {
      jaxbContext = JAXBContext.newInstance(Upgrades.class);
      Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
      upgrades = (Upgrades) jaxbUnmarshaller.unmarshal(new File(upgradeFile));
    } catch (JAXBException e1) {
      Utils.getLogger().error("Unable to unmarshal upgrade file", e1);
    }

    if (upgrades == null) {
      Utils.getLogger()
          .error("Cannot execute any upgrade: The upgrades file could not be read as proper XML!");
      return;
    }

    // Check if the upgradeUUID in the upgrade.xml is the same as in our DB,
    // then we already done this upgrade
    // and can assume nothing has to be done.
    if (osseConfig.getProperty(Vocabulary.Config.executedUpgradeUUID) != null
        && upgrades
            .getUpdateUuid()
            .equalsIgnoreCase(
                osseConfig.getProperty(Vocabulary.Config.executedUpgradeUUID).getValue())) {
      Utils.getLogger()
          .debug("The upgrade with UUID " + upgrades.getUpdateUuid() + " was already done.");
      return;
    }

    // Put Upgrades into a HashMap<key, upgrade> to reference a wanted one
    HashMap<String, Upgrade> upgradeMap = new HashMap<>();
    for (Upgrade entry : upgrades.getUpgrade()) {
      String internalRevision = entry.getVersion().getInternalRevision();
      upgradeMap.put(internalRevision, entry);
    }

    Boolean didUpgrade = false;

    // run through the upgrades until the preset requiredVersion internal
    // revision number defined above
    for (int i = 1; i <= requiredVersion; i++) {
      Utils.getLogger().debug("Running UpgradeExecution" + i + ".java");
      Class<? extends UpgradeExecution> c = null;
      String javaFile =
          "de.samply.edc."
              + getConfig().getString("instance.project")
              + ".upgrade.UpgradeExecution"
              + i;

      try {
        c = (Class<? extends UpgradeExecution>) Class.forName(javaFile);
      } catch (ClassNotFoundException e) {
        Utils.getLogger().warn("Did not find class "
            + javaFile
            + ", but there should be one! I will check in osse as instance project name as "
            + "fallback.",e);
        try {
          javaFile = "de.samply.edc.osse.upgrade.UpgradeExecution" + i;
          c = (Class<? extends UpgradeExecution>) Class.forName(javaFile);
        } catch (ClassNotFoundException ee) {
          Utils.getLogger()
              .error("Did not find class " + javaFile + ", but there should be one!", ee);
          throw new Error("There is no UpgradeExecution" + i + ".java available!");
        }
      }

      if (c != null) {
        try {
          UpgradeExecution upgradeExecution = null;
          if (upgradeMap.get("" + i) != null) {
            Constructor<? extends UpgradeExecution> cons =
                c.getConstructor(Upgrade.class, JSONResource.class, ApplicationBean.class);
            upgradeExecution = cons.newInstance(upgradeMap.get("" + i), osseConfig, this);
          } else {
            Constructor<? extends UpgradeExecution> cons =
                c.getConstructor(JSONResource.class, ApplicationBean.class);
            upgradeExecution = cons.newInstance(osseConfig, this);
          }

          Boolean executedUpgrade;
          try {
            executedUpgrade = upgradeExecution.doUpgrade();
          } catch (UpgradeExecutionException e) {
            Utils.getLogger()
                .debug("An upgrade could not be finished successfully. Bailing out of the "
                    + "complete set of upgrading!",e);
            return;
          }

          didUpgrade = didUpgrade | executedUpgrade;

        } catch (NoSuchMethodException
            | SecurityException
            | InstantiationException
            | IllegalAccessException
            | IllegalArgumentException
            | InvocationTargetException e) {
          e.printStackTrace();
          Utils.getLogger().error("Could not setup upgrade execution.");
        }
      }
    }

    // Insert the reconfigure settings
    UpgradeExecution upgradeExecution =
        new UpgradeExecutionReconfigure(upgradeMap.get("reconfigure"), osseConfig, this);
    Boolean executedUpgrade;
    try {
      executedUpgrade = upgradeExecution.doUpgrade();
    } catch (UpgradeExecutionException e) {
      Utils.getLogger()
          .debug("An reconfigure upgrade could not be finished successfully. Bailing out of the "
              + "complete set of upgrading!",e);
      return;
    }

    didUpgrade = didUpgrade | executedUpgrade;

    // After upgrades were done, the config etc needs to be reloaded.
    // the easiest way to make sure all is reloaded is to call init again.
    if (didUpgrade) {
      Utils.getLogger().debug("Any upgrade was done, so calling init");

      osseConfig.setProperty(Vocabulary.Config.executedUpgradeUUID, upgrades.getUpdateUuid());
      try {
        InitDbModel tmodel = new InitDbModel(configFile);
        tmodel.saveOsseConfig("osse", osseConfig);
        tmodel.logout();
        tmodel.close();
      } catch (DatabaseException e) {
        Utils.getLogger().error("Unable to init db model or save osse config after upgrade.",e);
      }

      init();
    }

    Utils.getLogger().debug("--------------------\n --- Upgrades finished\n--------------------");
  }

  /**
   * Copies the current config to the factory standard.
   *
   * @param model the backend database model
   * @throws DatabaseException if a database error occurs
   */
  public void copyConfigToStandard(OSSEModel model) throws DatabaseException {
    osseConfig = model.getConfig("osse");
    model.saveConfig("osse.factory.settings", osseConfig);
  }

  /**
   * Gets the forms of this registry TODO: "Formulars" ist kein englisches Wort. Besser in
   * "getForms" umbenennen, folgende Methoden entsprechend.
   *
   * @param locale the locale
   * @return the forms.
   */
  public LinkedHashMap<String, String> getFormulars(Locale locale) {
    if (!isASupportedLocale(locale)) {
      Utils.getLogger().warn(
          "System tried to get formulars with locale "
              + locale.getLanguage()
              + " which is not supported! Giving back standard language.");
      return formulars.get(
          FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage());
    }

    return formulars.get(locale.getLanguage());
  }

  /**
   * Gets the case forms, HashMap of laguagecode, and a LinkedHashMap containing: key = formname,
   * value = form title.
   *
   * @return the forms
   */
  public HashMap<String, LinkedHashMap<String, String>> getFormulars() {
    return formulars;
  }

  /**
   * Gets the amount of visit forms (visit was renamed episode, so here for compatibility reasons).
   *
   * @return the amount visit forms
   */
  @Deprecated
  public Integer getAmountVisitFormulars() {
    if (visitFormulars == null || visitFormulars.isEmpty()) {
      return 0;
    }

    return visitFormulars.get(supportedLocales.get(0)).size();
  }

  /**
   * Gets the amount of episode forms.
   *
   * @return the amount episode forms
   */
  public Integer getAmountEpisodeFormulars() {
    if (visitFormulars == null || visitFormulars.isEmpty()) {
      return 0;
    }

    return visitFormulars.get(supportedLocales.get(0)).size();
  }

  /**
   * Gets the visit forms for a certain locale. If the locale is not supported, the default locale
   * will be used.
   *
   * @param locale the locale
   * @return the visit formulars
   */
  public LinkedHashMap<String, String> getVisitFormulars(Locale locale) {
    if (!isASupportedLocale(locale)) {
      Utils.getLogger().warn(
          "System tried to get formulars with locale "
              + locale.getLanguage()
              + " which is not supported!  Giving back standard language.");
      return visitFormulars.get(
          FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage());
    }
    return visitFormulars.get(locale.getLanguage());
  }

  /**
   * Gets the episode forms, HashMap of laguagecode, and a LinkedHashMap containing: key = formname,
   * value = form title.
   *
   * @return the episode forms
   */
  public HashMap<String, LinkedHashMap<String, String>> getVisitFormulars() {
    return visitFormulars;
  }

  /**
   * Checks if a form is a case form (previously called "unique" form).
   *
   * @param formName the form name
   * @return true|false
   */
  public Boolean isUniqueForm(String formName) {
    return formulars
            .get(
                FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage())
            .containsKey(formName)
        || archivedFormulars
            .get(
                FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage())
            .containsKey(formName);
  }

  /**
   * Checks if a form is a visit form.
   *
   * @param formName the form name
   * @return true|false
   */
  @Deprecated
  public Boolean isVisitForm(String formName) {
    return visitFormulars
            .get(
                FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage())
            .containsKey(formName)
        || archivedVisitFormulars
            .get(
                FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage())
            .containsKey(formName);
  }

  /**
   * Checks if a form is an episode form.
   *
   * @param formName the form name
   * @return true|false
   */
  public Boolean isEpisodeForm(String formName) {
    return visitFormulars
            .get(
                FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage())
            .containsKey(formName)
        || archivedVisitFormulars
            .get(
                FacesContext.getCurrentInstance().getApplication().getDefaultLocale().getLanguage())
            .containsKey(formName);
  }

  /**
   * Checks if a form is a medical form (either a case or an episode form).
   *
   * @param formName the form name
   * @return true|false
   */
  public Boolean isMedicalForm(String formName) {
    return (isUniqueForm(formName) || isEpisodeForm(formName) || isPatientForm(formName));
  }

  /** Loads the form lists of all supported languages. */
  public void initFormLists() {
    clearPatientUserForms();

    mdrEntityIsInForm = new HashMap<>();
    formContainsMdrEntity = new HashMap<>();
    JSONResource mdrEntityIsInFormJson = new JSONResource();
    if (osseConfig.getProperty(Vocabulary.Config.mdrEntityIsInForm) != null) {
      mdrEntityIsInFormJson =
          osseConfig.getProperty(Vocabulary.Config.mdrEntityIsInForm).asJSONResource();
      if (mdrEntityIsInFormJson != null) {
        for (String key : mdrEntityIsInFormJson.getDefinedProperties()) {
          List<Value> moo = mdrEntityIsInFormJson.getProperties(key);
          List<String> forms = new ArrayList<>();
          for (Value me : moo) {
            forms.add(me.getValue());
            // check if there is already a list for this form and if not, add one
            if (!formContainsMdrEntity.containsKey(
                me.asJSONResource().getProperty("formName").getValue())) {
              formContainsMdrEntity.put(
                  me.asJSONResource().getProperty("formName").getValue(), new ArrayList<String>());
            }
            // add element to form's element list
            formContainsMdrEntity
                .get(me.asJSONResource().getProperty("formName").getValue())
                .add(key);
          }
          mdrEntityIsInForm.put(key, forms);
        }
      }
    }

    recordHasMdrEntities = new HashMap<>();
    JSONResource recordHasMdrEntitiesJson = new JSONResource();
    if (osseConfig.getProperty(Vocabulary.Config.recordHasMdrEntities) != null) {
      recordHasMdrEntitiesJson =
          osseConfig.getProperty(Vocabulary.Config.recordHasMdrEntities).asJSONResource();
      if (recordHasMdrEntitiesJson != null) {
        for (String key : recordHasMdrEntitiesJson.getDefinedProperties()) {
          List<Value> moo = recordHasMdrEntitiesJson.getProperties(key);
          List<String> forms = new ArrayList<>();
          for (Value me : moo) {
            forms.add(me.getValue());
          }
          recordHasMdrEntities.put(key, forms);
        }
      }
    }

    for (String locale : supportedLocales) {
      Locale theLocale = new Locale(locale);

      if (osseConfig.getProperty(Vocabulary.Config.Form.patient) == null) {
        break;
      }

      LinkedHashMap<String, String> forms;
      LinkedHashMap<String, String> archivedForms = new LinkedHashMap<>();
      LinkedHashMap<String, String> visitForms = new LinkedHashMap<>();
      LinkedHashMap<String, String> archivedVisitForms = new LinkedHashMap<>();

      forms =
          getPageTitleNamesByLocale(
              theLocale, osseConfig.getProperty(Vocabulary.Config.Form.patient).getValue());

      if (osseConfig.getProperty(Vocabulary.Config.Form.archivedPatient) != null) {
        archivedForms =
            getPageTitleNamesByLocale(
                theLocale,
                osseConfig.getProperty(Vocabulary.Config.Form.archivedPatient).getValue());
      }

      if (osseConfig.getProperty(Vocabulary.Config.Form.visit) != null) {
        visitForms =
            getPageTitleNamesByLocale(
                theLocale, osseConfig.getProperty(Vocabulary.Config.Form.visit).getValue());

        if (osseConfig.getProperty(Vocabulary.Config.Form.archivedVisit) != null) {
          archivedVisitForms =
              getPageTitleNamesByLocale(
                  theLocale,
                  osseConfig.getProperty(Vocabulary.Config.Form.archivedVisit).getValue());
        }
      }

      formulars.put(locale, forms);
      archivedFormulars.put(locale, archivedForms);
      visitFormulars.put(locale, visitForms);
      archivedVisitFormulars.put(locale, archivedVisitForms);
    }

    // fill list of patientforms
    patientForms = new ArrayList<>();

    String patientFormList = osseConfig.getString(Vocabulary.Config.PatientForm.list);
    String patientFormArchivedList =
        osseConfig.getString(Vocabulary.Config.PatientForm.archivedList);
    patientFormList = (patientFormList == null) ? "" : patientFormList;
    patientFormArchivedList = (patientFormArchivedList == null) ? "" : patientFormArchivedList;
    String concatString =
        (!patientFormList.isEmpty() && !patientFormArchivedList.isEmpty()) ? "," : "";
    patientForms =
        new ArrayList<>(
            Arrays.asList(
                patientFormList.concat(concatString).concat(patientFormArchivedList).split(",")));

    // In case of a redeploy the xhtml files were deleted, so we have to
    // recover them from the DB
    recoveryCheck();

    // DEVELOPMENT PURPOSES ONLY: remake all forms from scratch
    if (redoForms) {
      remakeForms();
    }

    getPatientUserEpisodeMainPage();
    getPatientUserPatientMainPage();
  }

  /**
   * Takes a csv-type string of form ids and creates a map of the form's titles.
   *
   * @param locale The language in which the form titles should be.
   * @param formsString A comma separated string of form ids.
   * @return A map containing form titles assigned to the form's ids.
   */
  private LinkedHashMap<String, String> getPageTitleNamesByLocale(
      Locale locale, String formsString) {
    LinkedHashMap<String, String> result = new LinkedHashMap<>();

    if (formsString instanceof String) {
      for (String formString : formsString.split(",")) {
        if (formString != null && !formString.isEmpty()) {
          result.put(formString, getPageTitleName(formString, locale));
        }
      }
    }

    return result;
  }

  /** Clear patient user forms. */
  public void clearPatientUserForms() {
    patientUserPatientMainPage = null;
    patientUserEpisodeMainPage = null;
  }

  /**
   * Gets the patient user patient main page.
   *
   * @return the patient user patient main page
   */
  public String getPatientUserPatientMainPage() {
    if (patientUserPatientMainPage != null) {
      return patientUserPatientMainPage;
    }

    LinkedHashMap<String, String> list = getFormulars(Utils.getDefaultLocale());

    patientUserPatientMainPage = "noform";

    if (list != null && !list.isEmpty()) {
      for (String formName : list.keySet()) {
        if (isPatientForm(formName)) {
          patientUserPatientMainPage = formName;
        }
      }
    }

    return patientUserPatientMainPage;
  }

  /**
   * Gets the patient user episode main page.
   *
   * @return the patient user episode main page
   */
  public String getPatientUserEpisodeMainPage() {
    if (patientUserEpisodeMainPage != null) {
      return patientUserEpisodeMainPage;
    }

    LinkedHashMap<String, String> list = getVisitFormulars(Utils.getDefaultLocale());

    patientUserEpisodeMainPage = "noform";

    if (list != null && !list.isEmpty()) {
      for (String formName : list.keySet()) {
        if (isPatientForm(formName)) {
          patientUserEpisodeMainPage = formName;
        }
      }
    }

    return patientUserEpisodeMainPage;
  }

  /** DEVELOPMENT PURPOSES ONLY Recreates all forms from scratch. */
  private void remakeForms() {
    FormRecreationHelper rch = new FormRecreationHelper(this);
    rch.recreateForms();
  }

  /**
   * Checks if the form file exists of a given form name.
   *
   * @param formName the form name
   * @return the boolean
   */
  private Boolean doesFormFileExist(String formName) {
    String saveFileName = "/forms/" + formName + ".xhtml";
    saveFileName = Utils.getRealPath(saveFileName);
    File file = new File(saveFileName);
    if (file.exists()) {
      Utils.getLogger()
          .debug(
              "Form "
                  + formName
                  + " exists as form in "
                  + saveFileName
                  + ". No recovery necessary.");
      return true;
    }

    return false;
  }

  /** Check and recover lost xhtml form files. */
  private void recoveryCheck() {
    // Check that the files of the forms are available
    // After redeploying they might have been deleted, so we have to recover
    // them

    // Do we have a storage?
    if (osseFormStorage.getProperty(Vocabulary.Config.Form.storage) == null) {
      return;
    }

    JSONResource formStorage =
        osseFormStorage.getProperty(Vocabulary.Config.Form.storage).asJSONResource();
    if (formStorage == null) {
      return;
    }

    // if any form is missing, we will just remake all forms right away
    for (String formName : archivedFormulars.get("en").keySet()) {
      if (!doesFormFileExist(formName)) {
        Utils.getLogger()
            .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
        remakeForms();
        return;
      }
    }

    for (String formName : formulars.get("en").keySet()) {
      if (!doesFormFileExist(formName)) {
        Utils.getLogger()
            .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
        remakeForms();
        return;
      }
    }

    for (String formName : archivedVisitFormulars.get("en").keySet()) {
      if (!doesFormFileExist(formName)) {
        Utils.getLogger()
            .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
        remakeForms();
        return;
      }
    }

    for (String formName : visitFormulars.get("en").keySet()) {
      if (!doesFormFileExist(formName)) {
        Utils.getLogger()
            .debug("RecoveryCheck: Form " + formName + " is not existing, remaking all forms now.");
        remakeForms();
        return;
      }
    }
  }

  /**
   * Gets the form title of a form.
   *
   * @param formName the form name
   * @return the form title
   */
  public String getFormTitle(String formName) {
    String result = "";

    if (osseFormStorage instanceof JSONResource) {
      Value formNameMatrix = osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix);
      if (formNameMatrix instanceof Value) {
        Value formNamePropertyLocalized =
            formNameMatrix
                .asJSONResource()
                .getProperty(formName + "_" + ((SessionBean) Utils.getSB()).getCurrentLanguage());
        if (!(formNamePropertyLocalized instanceof Value)) {
          Value formNameProperty = formNameMatrix.asJSONResource().getProperty(formName);
          if (formNameProperty instanceof Value) {
            result = formNameProperty.getValue();
          }
        }
      }
    }

    return result;
  }

  /**
   * Creates an account in a registry of registers (RoR) to be used by the "Teiler"/Samply.Share
   *
   * @param url the URL to the RoR
   * @param username the username
   * @param password the password
   * @return created|updated|failed
   * @throws HttpConnectorException the http connector exception
   */
  public String createTeilerAccount(String url, String username, String password) {
    String teilerUrl = getConfig().getString(Vocabulary.Config.Teiler.REST);
    Client client = httpConnector.getJerseyClient(teilerUrl, false);

    TeilerAccountDto accountData = new TeilerAccountDto();
    accountData.setAddress(url);
    accountData.setUsername(username);
    accountData.setPassword(password);

    Response response =
        client
            .target(teilerUrl + "/rest/osse-ror")
            .request(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .put(Entity.json(accountData), Response.class);

    if (response.getStatus() == 201) {
      return "created";
    }

    if (response.getStatus() == 204) {
      return "updated";
    }

    return "failed";
  }

  /**
   * Redirects the user to the Teiler and makes sure the user is logged in.
   *
   * @return the string
   * @throws HttpConnectorException the http connector exception
   */
  public String goTeiler() throws HttpConnectorException {
    return goTeiler(false);
  }

  /**
   * Redirects the user to the Teiler and makes sure the user is logged in.
   *
   * @param goExport Redirect to data export or to teiler main page
   * @return the string
   * @throws HttpConnectorException the http connector exception
   */
  private String goTeiler(Boolean goExport) {
    // get configured values
    String teilerUrl = OsseUtils.getTeilerRestUrl();
    String teilerInternalPort = getConfig().getString(Vocabulary.Config.Teiler.internalTeilerPort);

    // if an internal port is defined, we go via localhost
    String internalUrl = teilerUrl;
    if (teilerInternalPort != null && !teilerInternalPort.equals("")) {
      internalUrl = "http://localhost:" + teilerInternalPort + "/osse-share";
    }

    Client client = httpConnector.getJerseyClient(internalUrl, false);
    String apiKey = null;
    if (goExport) {
      apiKey = getConfig().getString(Vocabulary.Config.Teiler.apiKeyExport);
    } else {
      apiKey = getConfig().getString(Vocabulary.Config.Teiler.apiKey);
    }

    Response response =
        client
            .target(internalUrl + "/rest/tokens")
            .request(MediaType.APPLICATION_JSON)
            .header("apiKey", apiKey)
            .accept(MediaType.APPLICATION_JSON)
            .get(Response.class);
    if (response.getStatus() != 200) {
      throw new RuntimeException("Failed : HTTP error code : " + response.getStatus());
    }

    TeilerDto theToken = response.readEntity(TeilerDto.class);
    try {
      Utils.redirectToPage(teilerUrl + "/index.xhtml?signinToken=" + theToken.getSigninToken());
    } catch (IOException e) {
      Utils.getLogger().warn("Unable to redirect to teiler page.",e);
    }

    return "";
  }

  /**
   * Redirects the user to the Teiler Exporter and makes sure the user is logged in.
   *
   * @return the string
   * @throws HttpConnectorException the http connector exception
   */
  public String goTeilerExport() throws HttpConnectorException {
    return goTeiler(true);
  }

  /**
   * Redirects the user to the form repository and provides a Samply.AUTH login code.
   *
   * @return the string
   */
  public String goFormRepository() {
    try {
      String feClientId = getConfig().getString(Vocabulary.Config.FormEditor.formEditorClientId);

      String feUrl = getConfig().getString(FormEditor.formEditorBASE);
      Configuration config = Utils.getAB().getConfig();
      if (!Auth.getOAuth2Client(config).isUseSamplyAuth()) {
        Client client = ApplicationBean.getHttpConnector().getJerseyClientForHTTPS(false);
        AccessTokenDto accessTokenDto = Auth.getAccessToken(client, config, GrantType.PASSWORD);
        feUrl += "?accessToken=" + accessTokenDto.getAccessToken();
        feUrl += "&idToken=" + accessTokenDto.getIdToken();
        feUrl += "&refreshToken=" + accessTokenDto.getRefreshToken();
      } else {
        LoginDto loginDto = Auth.getClientCommunicationCode(feClientId, getConfig());
        feUrl += "?code=" + loginDto.getCode();
      }
      Utils.redirectToPage(feUrl);
    } catch (InvalidKeyException
        | NoSuchAlgorithmException
        | SignatureException
        | JwtException
        | IOException e) {
      Utils.getLogger().warn("Unable to redirect to forms repository.", e);
    }

    return "";
  }

  /**
   * Redirects the user to the MDR and provides a Samply.AUTH login code.
   *
   * @return the string
   */
  public String goMdr() {
    try {
      String feClientId = getConfig().getString(Vocabulary.Config.FormEditor.formEditorClientId);

      String feUrl =
          getConfig().getString(Mdr.REST);
      feUrl = feUrl.replace("/rest/api/mdr","/samplyLogin.xhtml");
      Configuration config = Utils.getAB().getConfig();
      if (!Auth.getOAuth2Client(config).isUseSamplyAuth()) {
        Client client = ApplicationBean.getHttpConnector().getJerseyClientForHTTPS(false);
        AccessTokenDto accessTokenDto = Auth.getAccessToken(client, config, GrantType.PASSWORD);
        feUrl += "?accessToken=" + accessTokenDto.getAccessToken();
        feUrl += "&idToken=" + accessTokenDto.getIdToken();
        feUrl += "&refreshToken=" + accessTokenDto.getRefreshToken();
      } else {
        LoginDto loginDto = Auth.getClientCommunicationCode(feClientId, getConfig());
        feUrl += "?code=" + loginDto.getCode();
      }
      Utils.redirectToPage(feUrl);
    } catch (InvalidKeyException
        | NoSuchAlgorithmException
        | SignatureException
        | JwtException
        | IOException e) {
      Utils.getLogger().warn("Unable to redirect to mdr.", e);
    }

    return "";
  }

  /**
   * Gets the OSSE form storage, i.e. the xhtml of all forms (old and current) ever created for this
   * registry.
   *
   * @return the OSSE form storage
   */
  public JSONResource getOsseFormStorage() {
    return osseFormStorage;
  }

  /**
   * Gets the OSSE XSD storage, i.e. the import XSD ever created for this registry.
   *
   * @return the OSSE XSD storage
   */
  public JSONResource getOsseXsdStorage() {
    return osseXsdStorage;
  }

  /**
   * Gets the OSSE config.
   *
   * @return the OSSE config
   */
  public JSONResource getOsseConfig() {
    return osseConfig;
  }

  /**
   * Gets the Design config.
   *
   * @return the design config
   */
  public JSONResource getDesignConfig() {
    if (designConfig == null) {
      designConfig = DesignBean.getDefault(osseConfig.getString(Config.registryName));
    }
    return designConfig;
  }

  /**
   * Gets the name of the patient main page.
   *
   * @return the patient main page name
   */
  public String getPatientMainPage() {
    return osseConfig.getString(Vocabulary.Config.Form.patientStandard);
  }

  /**
   * Gets the title of a page in a certain locale.
   *
   * @param form the form
   * @param locale the locale
   * @return the page title name
   */
  @Override
  public String getPageTitleName(String form, Locale locale) {
    String title = "";

    JSONResource formNames = osseConfig.getProperty(Vocabulary.Config.formNames).asJSONResource();
    if (formNames != null && formNames.getProperty(form) != null) {
      Value titleProperty = formNames.getProperty(form + "_" + locale.getLanguage());
      if (titleProperty instanceof Value) {
        title = titleProperty.getValue();
      }
      if (title == null || title.trim().isEmpty()) {
        title = formNames.getProperty(form).getValue();
      }
    }

    if (title == null || "".equals(title)) {
      if (osseConfig.getProperty(Vocabulary.Config.archivedFormNames) != null) {
        // check in archived Vocabulary.Config.archivedFormNames
        formNames = osseConfig.getProperty(Vocabulary.Config.archivedFormNames).asJSONResource();
        if (formNames != null && formNames.getProperty(form) != null) {
          title = formNames.getProperty(form).getValue();
        }
      }

      if (title == null || "".equals(title)) {
        // Fallback to messagebundle
        try {
          title = Utils.getResourceBundleString("de.samply.edc.osse.messages.formnames", form);
        } catch (MissingResourceException e) {
          Utils.getLogger().error("No entry in formnames bundle for form " + form, e);
        }

        if (title == null || "".equals(title)) {
          Utils.getLogger().error("Form " + form + " has no title");
          return "No Title";
        }
      }
    }

    return title;
  }

  /**
   * Gets the archived forms, HashMap of laguagecode, and a LinkedHashMap containing: key =
   * formname, value = form title.
   *
   * @return the archived forms
   */
  public HashMap<String, LinkedHashMap<String, String>> getArchivedFormulars() {
    return archivedFormulars;
  }

  /**
   * Sets the archived forms, HashMap of laguagecode, and a LinkedHashMap containing: key =
   * formname, value = form title.
   *
   * @param archivedFormulars the archived forms
   */
  public void setArchivedFormulars(
      HashMap<String, LinkedHashMap<String, String>> archivedFormulars) {
    this.archivedFormulars = archivedFormulars;
  }

  /**
   * Gets the archived episode forms, HashMap of laguagecode, and a LinkedHashMap containing: key =
   * formname, value = form title.
   *
   * @return the archived visit forms
   */
  public HashMap<String, LinkedHashMap<String, String>> getArchivedVisitFormulars() {
    return archivedVisitFormulars;
  }

  /**
   * Gets the archived episode forms, HashMap of laguagecode, and a LinkedHashMap containing: key =
   * formname, value = form title.
   *
   * @param archivedVisitFormulars the archived visit forms
   */
  public void setArchivedVisitFormulars(
      HashMap<String, LinkedHashMap<String, String>> archivedVisitFormulars) {
    this.archivedVisitFormulars = archivedVisitFormulars;
  }

  /**
   * Gets the OSSE version.
   *
   * @return the OSSE version
   */
  public String getOsseVersion() {
    if (osseConfig.getProperty(Vocabulary.Config.version) == null) {
      return "1.0.0";
    }

    return osseConfig.getProperty(Vocabulary.Config.version).getValue();
  }

  /**
   * Gets the OSSE version.
   *
   * @return the OSSE version
   */
  public String getOsseBuildVersion() {
    return osseProperties.getProperty("build.version");
  }

  /**
   * Gets the executed upgrade uuid as stored in the DB.
   *
   * @return the executed upgrade uuid
   */
  public String getExecutedUpgradeUuid() {
    if (osseConfig.getProperty(Vocabulary.Config.executedUpgradeUUID) == null) {
      return "";
    }

    return osseConfig.getProperty(Vocabulary.Config.executedUpgradeUUID).getValue();
  }

  /**
   * This is the port that the Mainzelliste shall use in its callback to access a registry via
   * "localhost" on a special port. If you do not set a port here, the URL is calculated, by what
   * you have called your registry in the browser
   *
   * @return the internal mainzelliste port
   */
  public String getInternalMainzellistePort() {
    if (osseConfig.getProperty(Vocabulary.Config.Mainzelliste.RESTInternalPort) == null) {
      return null;
    } else {
      return osseConfig.getProperty(Vocabulary.Config.Mainzelliste.RESTInternalPort).getValue();
    }
  }

  /**
   * Gets the form status.
   *
   * @return the form status
   */
  public HashMap<Integer, String> getFormStatus() {
    return formStatus;
  }

  /**
   * Gets the form status reverse.
   *
   * @return the form status reverse
   */
  public HashMap<String, Integer> getFormStatusReverse() {
    return formStatusReverse;
  }

  /**
   * Determines if we have any mdr entities in our registry yet.
   */
  public Boolean hasMdrEntities() {
    return mdrEntityIsInForm != null && !mdrEntityIsInForm.isEmpty();
  }

  /**
   * Gets the mdr entity is in form.
   *
   * @return the mdr entity is in form
   */
  public HashMap<String, List<String>> getMdrEntityIsInForm() {
    return mdrEntityIsInForm;
  }

  /**
   * Sets the mdr entity is in form.
   *
   * @param mdrEntityIsInForm the mdr entity is in form
   */
  public void setMdrEntityIsInForm(HashMap<String, List<String>> mdrEntityIsInForm) {
    this.mdrEntityIsInForm = mdrEntityIsInForm;
  }

  /**
   * Gets the mdr entities of a record.
   *
   * @return the list of mdr entities of a record
   */
  public HashMap<String, List<String>> getRecordHasMdrEntities() {
    return recordHasMdrEntities;
  }

  /**
   * Sets the mdr entities of a record.
   *
   * @param recordHasMdrEntities the recordHasMdrEntities to set for a record
   */
  public void setRecordHasMdrEntities(HashMap<String, List<String>> recordHasMdrEntities) {
    this.recordHasMdrEntities = recordHasMdrEntities;
  }

  /**
   * Gets the case forms, HashMap of laguagecode, and a LinkedHashMap containing: key = formname,
   * value = form title.
   *
   * @param formulars the formulars
   */
  public void setFormulars(HashMap<String, LinkedHashMap<String, String>> formulars) {
    this.formulars = formulars;
  }

  /**
   * Sets the episode forms, HashMap of laguagecode, and a LinkedHashMap containing: key = formname,
   * value = form title.
   *
   * @param visitFormulars the visit forms
   */
  public void setVisitFormulars(HashMap<String, LinkedHashMap<String, String>> visitFormulars) {
    this.visitFormulars = visitFormulars;
  }

  /**
   * Gets the OSSE config as integer.
   *
   * @param key the key
   * @return the OSSE config as integer
   */
  public Integer getOsseConfigAsInteger(String key) {
    if (osseConfig.getProperty(key) != null) {
      return osseConfig.getProperty(key).asInteger();
    } else {
      return null;
    }
  }

  /**
   * Sets the osse config.
   *
   * @param key the key
   * @param value the value
   */
  public void setOsseConfig(String key, Integer value) {
    osseConfig.setProperty(key, value);
  }

  /**
   * Sets the osse config.
   *
   * @param key the key
   * @param value the value
   */
  public void setOsseConfig(String key, String value) {
    osseConfig.setProperty(key, value);
  }

  /**
   * Checks if a registry has case forms defined.
   *
   * @return the boolean
   */
  public Boolean hasCaseForms() {
    String formlist = config.getString(Vocabulary.Config.Form.patient);
    return formlist != null && !"".equals(formlist);
  }

  /**
   * Checks if a registry has episode forms defined.
   *
   * @return the boolean
   */
  public Boolean hasEpisodeForms() {
    String formlist = config.getString(Vocabulary.Config.Form.visit);
    return formlist != null && !"".equals(formlist);
  }

  /**
   * Gets the status transitions.
   *
   * @return the statusTransitions
   */
  public HashMap<String, ArrayList<String>> getStatusTransitions() {
    return statusTransitions;
  }

  /**
   * Sets the status transitions.
   *
   * @param statusTransitions the statusTransitions to set
   */
  public void setStatusTransitions(HashMap<String, ArrayList<String>> statusTransitions) {
    this.statusTransitions = statusTransitions;
  }

  /**
   * Gets the form standard status number.
   *
   * @return the formStandardStatusNumber
   */
  public String getFormStandardStatusNumber() {
    return formStandardStatusNumber;
  }

  /**
   * Sets the form standard status number.
   *
   * @param formStandardStatusNumber the formStandardStatusNumber to set
   */
  public void setFormStandardStatusNumber(String formStandardStatusNumber) {
    this.formStandardStatusNumber = formStandardStatusNumber;
  }

  /**
   * Checks if this app is larger than the given version.
   *
   * @param version the version to check for
   * @return the boolean
   */
  public Boolean largerThanVersion(String version) {
    if (version == null || version.equals("")) {
      return false;
    }

    VersionNumber checkVersion = new VersionNumber(version);
    VersionNumber currentVersion = new VersionNumber(getOsseVersion());

    return currentVersion.compareTo(checkVersion) >= 0;
  }

  /**
   * Checks if the registry has registered in the AUTH service.
   *
   * @return the boolean
   */
  public Boolean isInstallationAuthDone() {
    if (hasCaseForms()) {
      Utils.getLogger().debug("Case forms present => registry account must be present.");
      return true;
    }

    if (!doWeHaveAuthUserId()) {
      Utils.getLogger().debug("No auth user ID found in config "
          + "=> registry not registered with auth.");
      return false;
    }

    if (((SessionBean)Utils.getSB()).getCurrentRole().isAdminRole()) {
      AccessTokenDto accessToken = null;
      try {
        // check if we can get an auth token, if not then we probably have
        // the account not yet activated
        Client client = httpConnector.getJerseyClientForHTTPS(false);
        accessToken = Auth.getAccessToken(client, getConfig());
      } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
        Utils.getLogger().error("Couldn't get accessToken due to an error.", e);
      }
      Utils.getLogger().debug("Couldn't get accessToken, registry account might not be active.");
      return accessToken != null;
    }

    return true;
  }

  /**
   * Checks if we have the User ID of the Samply.AUTH service saved.
   *
   * @return the boolean
   */
  public Boolean doWeHaveAuthUserId() {
    Value authUserID = getOsseConfig().getProperty(Vocabulary.Config.Auth.UserId);

    return authUserID != null && !authUserID.getValue().equals("");
  }

  /**
   * Checks if an email address has been saved for the registry.
   *
   * @return the boolean
   */
  public Boolean isInstallationEmailDone() {
    Value email = getOsseConfig().getProperty(Vocabulary.Config.registryEmail);

    return email != null && !email.getValue().equals("");
  }

  /**
   * Checks if the URL for the form repository has been saved.
   *
   * @return the boolean
   */
  public Boolean isInstallationFormRepositoryDone() {
    Value formRepositoryUrl =
        getOsseConfig().getProperty(Vocabulary.Config.FormEditor.formEditorBASE);

    return formRepositoryUrl != null && !formRepositoryUrl.getValue().equals("");
  }

  /**
   * Checks if we yet have case forms defined in the registry.
   *
   * @return the boolean
   */
  public Boolean hasForms() {
    HashMap<String, String> formulars = getFormulars().get("en");

    return formulars != null && !formulars.isEmpty();
  }

  /**
   * Does mainzelliste want idat.
   *
   * @return the boolean
   */
  public Boolean doesMainzellisteWantIdat() {
    return !Utils.getBooleanOfJsonResource(osseConfig, Vocabulary.Config.Mainzelliste.noIdat);
  }

  /**
   * Gets the episode pattern name.
   *
   * @return the episode pattern name
   */
  public String getEpisodePatternName() {
    return Utils.getStringOfJsonResource(osseConfig, Vocabulary.Config.Episode.pattern);
  }

  /**
   * Gets the episode pattern upper.
   *
   * @return the episode pattern upper
   */
  public String getEpisodePatternUpper() {
    return EpisodePatterns.patterns.get(getEpisodePatternName()).getPattern().toUpperCase();
  }

  /**
   * Gets the episode pattern label.
   *
   * @return the episode pattern label
   */
  public String getEpisodePatternLabel() {
    return EpisodePatterns.patterns.get(getEpisodePatternName()).getLabel();
  }

  /**
   * Gets the episode pattern.
   *
   * @return the episode pattern
   */
  public String getEpisodePattern() {
    return EpisodePatterns.patterns.get(getEpisodePatternName()).getPattern();
  }

  /**
   * Gets the episode pattern j query.
   *
   * @return the episode pattern j query
   */
  public String getEpisodePatternJQuery() {
    return EpisodePatterns.patterns.get(getEpisodePatternName()).getPatternJQuery();
  }

  /**
   * Checks if is episode date pattern.
   *
   * @return the boolean
   */
  public Boolean isEpisodeDatePattern() {
    return EpisodePatterns.patterns.get(getEpisodePatternName()).getIsDatePattern();
  }

  /**
   * Gets the url to web service.
   *
   * @return the url to web service
   */
  public String getUrlToWebService() {
    return Utils.generateUrlToWebservice();
  }

  /**
   * Checks for patient forms.
   *
   * @return the boolean
   */
  public Boolean hasPatientForms() {
    return patientForms != null && !patientForms.isEmpty();
  }

  /**
   * Checks for patient user episode form.
   *
   * @return the boolean
   */
  public Boolean hasPatientUserEpisodeForm() {
    return !"noform".equals(patientUserEpisodeMainPage);
  }

  /**
   * Checks for patient user case form.
   *
   * @return the boolean
   */
  public Boolean hasPatientUserCaseForm() {
    return !"noform".equals(patientUserPatientMainPage);
  }

  /**
   * Checks if a form name is a patientform or a default form.
   *
   * @param name full form name in pattern "form_NAME_ver-VERSION" or "NAME-VERSION"
   * @return the boolean
   */
  public Boolean isPatientForm(String name) {
    if (!hasPatientForms()) {
      return false;
    }

    Pattern pattern = Pattern.compile("^form_([^-]+)_ver-([^-]+)$");
    Matcher matcher = pattern.matcher(name);
    if (!matcher.find()) {
      // maybe it is already in the goal pattern NAME-VERSION, so check that
      return patientForms.contains(name);
    }

    // convert to NAME-VERSION pattern
    name = matcher.group(1) + "-" + matcher.group(2);
    return patientForms.contains(name);
  }

  public HashMap<String, List<String>> getFormContainsMdrEntity() {
    return formContainsMdrEntity;
  }

  /**
   * Checks whether a file exists in the images/osse folder.
   *
   * @param fileName The complete file name including type (eg. imagefile.png).
   * @return True if the file exists, false otherwise.
   */
  public boolean osseImageFileExists(String fileName) {
    File file = new File(Utils.getRealPath("resources/images/osse/" + fileName));

    return file.exists();
  }

  /**
   * Loads the project's manifest file.
   */
  private void loadManifest() {
    try {
      InputStream is = FacesContext.getCurrentInstance().getExternalContext()
          .getResourceAsStream("/META-INF/MANIFEST.MF");
      manifest = new Manifest();
      manifest.read(is);
    } catch (IOException ioe) {
      Utils.getLogger().error("Error loading manifest file.", ioe);
    }
  }

  /**
   * Get the last commit hash from the manifest.
   *
   * @return Shortened hash of the last git commit.
   */
  public String getBuildCommitHashFromManifest() {
    try {
      return manifest.getMainAttributes().getValue("SCM-Version");
    } catch (NullPointerException e) {
      return "Error reading manifest.";
    }
  }

  /**
   * Get the current branch from the manifest.
   *
   * @return Name of the current git branch.
   */
  public String getBuildCommitBranchFromManifest() {
    try {
      return manifest.getMainAttributes().getValue("SCM-Branch");
    } catch (NullPointerException e) {
      return "Error reading manifest.";
    }
  }

  /**
   * Get the version string from the manifest.
   *
   * @return The project's version.
   */
  public String getVersionFromManifest() {
    try {
      return manifest.getMainAttributes().getValue("Implementation-Version");
    } catch (NullPointerException e) {
      return "Error reading manifest.";
    }
  }

  /**
   * Get the build timestamp from the manifest in UTC.
   *
   * @return The project's build timestamp.
   */
  public String getBuildTimeFromManifest() {
    try {
      return manifest.getMainAttributes().getValue("Build-Timestamp");
    } catch (NullPointerException e) {
      return "Error reading manifest.";
    }
  }
}
