/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import com.fasterxml.jackson.core.JsonProcessingException;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.utils.DashboardDesign;
import de.samply.edc.osse.utils.DashboardDesignLocal;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import org.apache.commons.lang.StringUtils;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;

/**
 * Session scoped bean for configuration pages.
 */
@ManagedBean
@ViewScoped
public class DesignBean extends AbstractViewBean {

  public static final String dashboardImage = "design.dashboard_image";
  public static final String dashboardImageType = "design.dashboard_image_type";
  public static final String loginImage = "design.login_image";
  public static final String loginImageType = "design.login_image_type";
  public static final String registryName = "design.registry_name";
  public static final String useDashboard = "design.useDashboard";
  public static final String dashboardGretting = "design.gretting_text";
  ApplicationBean applicationBean;
  String currentLanguage;
  List<String> languages;
  DashboardDesign dashboardDesign;

  /**
   * The Constant serialVersionUID.
   */
  private static final long serialVersionUID = 7706545600753861168L;

  /**
   * Postconstruct init, loads the current configuration.
   */
  @Override
  @PostConstruct
  public void init() {
    applicationBean = (ApplicationBean) Utils.getAB();
    languages = new ArrayList<>();
    //languages.add("default");
    languages.addAll(applicationBean.getSupportedLocales());
    currentLanguage = languages.get(0);
    super.init();
    loadConfiguration();
  }

  /**
   * reads the configuration from the application scoped bean and puts it into the form param map.
   */
  private void loadConfiguration() {
    JSONResource designConfigConfig = ((ApplicationBean) Utils.getAB()).getDesignConfig();
    for (String key : designConfigConfig.getDefinedProperties()) {
      if (!(designConfigConfig.getProperty(key) instanceof JSONResource)) {
        setFormParam(key, designConfigConfig.getProperty(key).getValue());
      }
    }

    if (getFormParam("dashboardDesign") == null) {
      dashboardDesign = getDefaultDashboardDesign();
    } else {
      try {
        dashboardDesign = DashboardDesign.marshalToInstance(getFormParam("dashboardDesign"));
      } catch (JsonProcessingException e) {
        Utils.getLogger().error("Unable to deserialize dashboardDesign create a new instance.");
      }
    }
  }

  /**
   * Saves the form param to the configuration.
   *
   * @return the string
   */
  public String saveConfiguration() {
    return saveConfiguration(true);
  }

  /**
   * Save the form param to the configuration.
   *
   * @param showHappy display a save successful message
   * @return JSF outcome string (unused)
   */
  public String saveConfiguration(Boolean showHappy) {
    JSONResource designConfig = ((ApplicationBean) Utils.getAB()).getDesignConfig();

    try {
      getFormParam().put("dashboardDesign", dashboardDesign.marshalToJson());
    } catch (JsonProcessingException e) {
      Utils.getLogger().error("Unable to serialize dashboardDesign");
    }

    for (String key : getFormParam().keySet()) {
      if (getFormParam(key) == null) {
        // never store null!
      } else if (designConfig.getProperty(key) == null) {
        designConfig.setProperty(key, getFormParam(key));
      } else if (!designConfig.getProperty(key).getValue().equalsIgnoreCase(getFormParam(key))) {
        designConfig.setProperty(key, getFormParam(key));
      }
    }

    // save the osse config, and reload it in the application scoped bean
    getSessionBean().getDatabase().saveConfig("design", designConfig);
    Utils.getAB().init();
    loadConfiguration();

    if (showHappy) {
      Utils.addContextMessage("Saving successful", "Your changes have been saved.");
    }
    return "";
  }

  /**
   * upload action for dashboard image.
   */
  public void uploadDashboardImage(FileUploadEvent event) {
    FacesMessage msg =
        new FacesMessage("Success! ", event.getFile().getFileName() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, msg);
    UploadedFile file = event.getFile();
    this.setFormParam(DesignBean.dashboardImageType, file.getContentType());
    this.storeImage(DesignBean.dashboardImage, file.getContents());
  }

  /**
   * upload action for login image.
   */
  public void uploadLoginImage(FileUploadEvent event) {
    FacesMessage msg =
        new FacesMessage("Success! ", event.getFile().getFileName() + " is uploaded.");
    FacesContext.getCurrentInstance().addMessage(null, msg);
    UploadedFile file = event.getFile();
    this.setFormParam(DesignBean.loginImageType, file.getContentType());
    this.storeImage(DesignBean.loginImage, file.getContents());
  }

  /**
   * Gets the name of the current form.
   *
   * @return the form
   */
  public String getForm() {
    if (StringUtils.isEmpty(Utils.getSB().getCurrentFormName())) {
      if (((ApplicationBean) Utils.getAB()).isInstallationEmailDone()) {
        if (((ApplicationBean) Utils.getAB()).isInstallationAuthDone()) {
          if (!((ApplicationBean) Utils.getAB()).isBridgehead()) {
            Utils.getSB().setForm("forms");
          } else {
            Utils.getSB().setForm("register");
          }
        } else {
          Utils.getSB().setForm("auth");
        }
      } else {
        Utils.getSB().setForm("register");
      }
    }

    return Utils.getSB().getCurrentFormName();
  }

  /**
   * Cancel button action.
   *
   * @return JSF outcome string
   */
  public String cancel() {
    Utils.goAdminForm(getForm());
    Utils.addContextMessage(
        "Changes reverted", "All your changes have been reverted to the current state.");
    return "cancel";
  }

  /**
   * Checks if a certain form is the currently active form. Used to call the css class in the xhtml
   *
   * @param formName the form name
   * @return active|""
   */
  public String isActiveForm(String formName) {
    String currentFormName = Utils.getSB().getForm();

    if (currentFormName != null && currentFormName.equalsIgnoreCase(formName)) {
      return "active";
    }

    if (currentFormName == null && formName.equalsIgnoreCase("register")) {
      return "active";
    }

    return "";
  }

  /**
   * Saves the FormRepository settings.
   */
  public void saveFR() {
    saveConfiguration();
    Utils.refreshView();
  }

  /**
   * Gives a default version of the design.
   *
   * @return default design version.
   */
  public static JSONResource getDefault(String registryName) {
    JSONResource config = new JSONResource();
    config.setProperty(DesignBean.useDashboard, "false");
    config.setProperty(
        DesignBean.dashboardGretting,
        Utils.getResourceBundle("de.samply.edc.osse.messages.osse_messages")
            .getString("dashboard_welcome"));
    config.setProperty(DesignBean.registryName, registryName);

    return config;
  }

  /**
   * Creates the default DashboardDesign, containing if existing previouse entries as well as the
   * links that where previously present.
   *
   * @return the default dashboardDesign.
   */
  private DashboardDesign getDefaultDashboardDesign() {
    DashboardDesign dashboardDesign = new DashboardDesign();
    for (String language : languages) {
      DashboardDesignLocal currLocalDesign = new DashboardDesignLocal();

      if (getFormParam(registryName) != null) {
        currLocalDesign.setRegistryName(getFormParam(registryName));
      }
      if (getFormParam(dashboardGretting) != null) {
        currLocalDesign.setGreetingsText(getFormParam(dashboardGretting));
      }
      if (language == "de") {
        Map<String, String> document = new HashMap<>();
        document.put("name", "Handbuch Datenmanagement (en)");
        document.put("url",
            "https://download.osse-register.de/OSSE%20User%20Guide%20(en)%20-%20Data%20Management.pdf");
        currLocalDesign.getDocumentList().add(document);

        Map<String, String> firstLink = new HashMap<>();
        firstLink.put("name", "OSSE Website");
        firstLink.put("url", "https://www.osse-register.de/de/");
        currLocalDesign.getLinkList().add(firstLink);

        Map<String, String> secondLink = new HashMap<>();
        secondLink.put("name", "European Platform on Rare Disease Registration");
        secondLink.put("url", "https://eu-rd-platform.jrc.ec.europa.eu/_de");
        currLocalDesign.getLinkList().add(secondLink);
      }
      if (language == "en") {
        Map<String, String> document = new HashMap<>();
        document.put("name", "Manual for data management");
        document.put("url",
            "https://download.osse-register.de/OSSE%20User%20Guide%20(en)%20-%20Data%20Management.pdf");
        currLocalDesign.getDocumentList().add(document);

        Map<String, String> firstLink = new HashMap<>();
        firstLink.put("name", "OSSE Website");
        firstLink.put("url", "https://www.osse-register.de/en/");
        currLocalDesign.getLinkList().add(firstLink);

        Map<String, String> secondLink = new HashMap<>();
        secondLink.put("name", "European Platform on Rare Disease Registration");
        secondLink.put("url", "https://eu-rd-platform.jrc.ec.europa.eu/_en");
        currLocalDesign.getLinkList().add(secondLink);
      }
      dashboardDesign.getLocalizedDesign().put(language, currLocalDesign);
    }
    return dashboardDesign;
  }

  /**
   * Get the current (localized) DashboardDesign.
   *
   * @return localized DashboardDesign.
   */
  public DashboardDesignLocal getCurrDashboardDesignLocal() {
    return dashboardDesign.getLocalizedDesign().get(getSessionBean().getLanguage());
  }

  /**
   * Get the current selected DashboardDesign.
   *
   * @return localized DashboardDesign.
   */
  public DashboardDesignLocal getCurrSelectedDashboardDesignLocal() {
    return dashboardDesign.getLocalizedDesign().get(currentLanguage);
  }

  /**
   * Encode and store the given image.
   *
   * @param name  json id string.
   * @param image as byte array.
   */
  public void storeImage(String name, byte[] image) {
    String encodedImage = Base64.getEncoder().encodeToString(image);
    this.setFormParam(name, encodedImage);
  }

  /**
   * Get the image as base64 encoded string from database. Its additionally annotated with as
   * "data:'image_type';base64:'base64 string'".
   *
   * @param name json id string.
   * @return image string for src in graphicImage.
   */
  public String getImageString(String name) {
    String encodedImage = getFormParam(name);
    encodedImage = "data:" + this.getFormParam(name + "_type") + ";base64," + encodedImage;
    return encodedImage;
  }

  /**
   * Checks if logo is set.
   */
  public boolean isLogoSet(String name) {
    if (this.getFormParam(name) != null && !this.getFormParam(name).equals("")) {
      return true;
    }
    return false;
  }

  /**
   * Encode and store the given image.
   *
   * @param name json id string.
   */
  public void deleteImage(String name) {
    this.setFormParam(name, "");
  }

  public String getCurrentLanguage() {
    return currentLanguage;
  }

  public void setCurrentLanguage(String currentLanguage) {
    this.currentLanguage = currentLanguage;
  }

  public List<String> getLanguages() {
    return languages;
  }

  public void setLanguages(List<String> languages) {
    this.languages = languages;
  }

  public DashboardDesign getDashboardDesign() {
    return dashboardDesign;
  }

  public void setDashboardDesign(DashboardDesign dashboardDesign) {
    this.dashboardDesign = dashboardDesign;
  }
}
