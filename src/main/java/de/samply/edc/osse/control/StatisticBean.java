/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.bean.ViewScoped;

/**
 * Bean for some basic statistic functions.
 */
@ViewScoped
public class StatisticBean {

  @PostConstruct
  public void init() {
  }

  /**
   * Function to convert a target entry of a record to a list of values.
   * @param record the record.
   * @param target the target string (urn without version).
   * @return the extracted list.
   */
  public static List<String> getRecordElementFlat(JSONResource record, String target) {
    ObjectMapper mapper = new ObjectMapper();
    List<String> result2 = new ArrayList<>();

    Pattern pattern = Pattern.compile("(" + target + ":\\d+)");
    Matcher matcher = pattern.matcher(record.toString());
    String realTarget = "";
    if (matcher.find()) {
      realTarget = matcher.group(1);
    } else {
      return result2;
    }

    try {
      List<JsonNode> recordNodes = mapper.readTree(record.toString()).findValues(realTarget);
      for (JsonNode currNode : recordNodes) {
        if (currNode.has("arrayEntry")) {
          JsonNode currArrayEntryNode = currNode.findValue("arrayEntry");
          if (currArrayEntryNode.isTextual()) {
            result2.add(currArrayEntryNode.textValue());
          } else {
            currArrayEntryNode.forEach(node -> result2
                .add(node.textValue()));
          }
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
    return result2;
  }

  /**
   * Checks if a property exists containing the target string in its name of a resource.
   * @param resource the input resource.
   * @param target the target string that should be contained.
   */
  public static boolean propertiesContains(Resource resource, String target) {
    if (resource == null || target == null) {
      return false;
    }
    return resource.getDefinedProperties().stream().filter(s -> s.contains(target)).count() > 0;
  }

  /**
   * Value of a property containing the target string in its name of a resource. Also deals with
   * dataelements in records.
   *
   * @param resource the input resource.
   * @param target   the target string that should be contained.
   * @return the value.
   */
  public static Value propertiesValue(Resource resource, String target) {
    List<Value> results = propertiesValueList(resource, target);
    if (results.isEmpty()) {
      return null;
    } else {
      return results.get(0);
    }
  }

  /**
   * Values of a property containing the target string in its name of a resource. Also deals with
   * dataelements in records.
   * @param resource the input resource.
   * @param target the target string that should be contained.
   * @return the values.
   */
  public static List<Value> propertiesValueList(Resource resource, String target) {
    List<Value> results = new ArrayList<>();
    for (String key : resource.getDefinedProperties()) {
      if (key.contains(target)) {
        results.addAll(resource.getProperties(key));
      } else if (key.contains("record")) {
        resource.getProperties(key).stream().forEach(value -> results.addAll(propertiesValueList(
            value.asJSONResource().getProperty("column").asJSONResource(), target)));
      }
    }
    return results;
  }

  /**
   * Values of a property containing the target string in its name of a resource. Also deals with
   * dataelements in records.
   * @param resource the input resource.
   * @param target the target string that should be contained.
   * @return the values.
   */
  public static List<Value> propertiesValueList(JSONResource resource, String target) {
    List<Value> results = new ArrayList<>();
    for (String key : resource.getDefinedProperties()) {
      if (key.contains(target)) {
        results.addAll(resource.getProperties(key));
      }
    }
    return results;
  }

  /**
   * Count the occurrences in a list and return them as a count map incl. a "total" count.
   * @param objectList the input list.
   * @return count map.
   */
  public static Map<String, Integer> listToIntMap(List<Object> objectList) {
    Map<String, Integer> namedResult = new HashMap<>();
    namedResult.put("total", 0);
    List<Object> results = new ArrayList<>();
    for (Object currResult : objectList) {
      namedResult.put("total", namedResult.get("total") + 1);
      String key = currResult.toString();
      if (namedResult.containsKey(key)) {
        namedResult.put(key, namedResult.get(key) + 1);
      } else {
        namedResult.put(key, 1);
      }
    }
    return namedResult;
  }
}