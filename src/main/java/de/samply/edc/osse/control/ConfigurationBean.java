/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.auth.rest.ClientDescriptionDto;
import de.samply.auth.rest.OAuth2Key;
import de.samply.auth.rest.OAuth2Keys;
import de.samply.auth.rest.Usertype;
import de.samply.common.http.HttpConnector;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.catalog.Vocabulary.Config;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.catalog.EpisodePatterns;
import de.samply.edc.osse.utils.OsseUtils;
import de.samply.edc.osse.validator.FairValidator;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.SignatureException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Base64;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import javax.ws.rs.ClientErrorException;
import javax.ws.rs.client.Client;
import org.apache.commons.lang.StringUtils;

/** Session scoped bean for configuration pages. */
@ManagedBean
@ViewScoped
public class ConfigurationBean extends AbstractViewBean {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7706545600753861168L;

  /** Is the URL to the Samply.AUTH service ok? */
  private Boolean authUrlisOk = false;

  /** Map of all clients known to the Samply.AUTH service */
  private HashMap<String, ClientDescriptionDto> allClients;

  /** A SelectItem list of all form repositories defined in Samply.AUTH service */
  private List<SelectItem> formRepositorySelectItems;

  /** A SelectItem list of all MDRs defined in the Samply.AUTH service */
  private List<SelectItem> mdrSelectItems;

  /** Postconstruct init, loads the current configuration. */
  @Override
  @PostConstruct
  public void init() {
    super.init();
    loadConfiguration();
  }

  /**
   * reads the configuration from the application scoped bean and puts it into the form param map.
   */
  private void loadConfiguration() {
    JSONResource osseConfig = ((ApplicationBean) Utils.getAB()).getOsseConfig();
    for (String key : osseConfig.getDefinedProperties()) {
      if (!(osseConfig.getProperty(key) instanceof JSONResource)) {
        setFormParam(key, osseConfig.getProperty(key).getValue());
      }
    }
  }

  /**
   * JSF action method of the admin registry page.
   *
   * @return the string
   */
  public String saveRegister() {
    saveConfiguration(false);
    if (((ApplicationBean) Utils.getAB()).isInstallationEmailDone()) {
      registerAuth();
    }

    return "";
  }

  /**
   * Saves the form param to the configuration.
   *
   * @return the string
   */
  public String saveConfiguration() {
    return saveConfiguration(true);
  }

  /**
   * Save the form param to the configuration.
   *
   * @param showHappy display a save successful message
   * @return JSF outcome string (unused)
   */
  public String saveConfiguration(Boolean showHappy) {
    JSONResource osseConfig = ((ApplicationBean) Utils.getAB()).getOsseConfig();

    for (String key : getFormParam().keySet()) {
      if (getFormParam(key) == null) {
        // never store null!
      } else if (osseConfig.getProperty(key) == null) {
        osseConfig.setProperty(key, getFormParam(key));
      } else if (!osseConfig.getProperty(key).getValue().equalsIgnoreCase(getFormParam(key))) {
        osseConfig.setProperty(key, getFormParam(key));
      }
    }

    // save the osse config, and reload it in the application scoped bean
    getSessionBean().getDatabase().saveConfig("osse", osseConfig);
    Utils.getAB().init();
    loadConfiguration();

    if (showHappy) {
      Utils.addContextMessage("Saving successful", "Your changes have been saved.");
    }
    return "";
  }

  /**
   * Generates a new random api key.
   * @return new api key
   */
  public static String getNewApikey() {
    SecureRandom rng = new SecureRandom();
    byte[] key = new byte[64];
    rng.nextBytes(key);
    String apikey = Base64.getEncoder().encodeToString(key);
    return apikey;
  }

  /**
   * Generates a new random api key.
   */
  public void setNewApikey() {
    String apikey = ConfigurationBean.getNewApikey();
    setFormParam("importer.apikey",apikey);
  }

  /**
   * Gets the name of the current form.
   *
   * @return the form
   */
  public String getForm() {
    if (StringUtils.isEmpty(Utils.getSB().getCurrentFormName())) {
      if (((ApplicationBean) Utils.getAB()).isInstallationEmailDone()) {
        if (((ApplicationBean) Utils.getAB()).isInstallationAuthDone()) {
          if (!((ApplicationBean) Utils.getAB()).isBridgehead()) {
            Utils.getSB().setForm("forms");
          } else {
            Utils.getSB().setForm("register");
          }
        } else {
          Utils.getSB().setForm("auth");
        }
      } else {
        Utils.getSB().setForm("register");
      }
    }

    return Utils.getSB().getCurrentFormName();
  }

  /**
   * Cancel button action.
   *
   * @return JSF outcome string
   */
  public String cancel() {
    Utils.goAdminForm(getForm());
    Utils.addContextMessage(
        "Changes reverted", "All your changes have been reverted to the current state.");
    return "cancel";
  }

  /**
   * Checks if a certain form is the currently active form. Used to call the css class in the xhtml
   *
   * @param formName the form name
   * @return active|""
   */
  public String isActiveForm(String formName) {
    String currentFormName = Utils.getSB().getForm();

    if (currentFormName != null && currentFormName.equalsIgnoreCase(formName)) {
      return "active";
    }

    if (currentFormName == null && formName.equalsIgnoreCase("register")) {
      return "active";
    }

    return "";
  }

  /**
   * Checks if the currently active form is part of the list. Used to call the css class in the
   * xhtml
   *
   * @param formNames the form name
   * @return active|""
   */
  public boolean isActive(String... formNames) {
    String currentFormName = Utils.getSB().getForm();
    for (String formName : formNames) {
      if (currentFormName != null && currentFormName.equalsIgnoreCase(formName)) {
        return true;
      }
    }
    return false;
  }

  /**
   * Map of all clients known to the Samply.AUTH service
   *
   * @return the all clients
   */
  public HashMap<String, ClientDescriptionDto> getAllClients() {
    if (allClients == null) {
      try {
        HttpConnector hc = ApplicationBean.getHttpConnector();
        Client client = hc.getJerseyClientForHTTPS(false);
        allClients = Auth.loadAllClients(client, Utils.getAB().getConfig());
      } catch (Exception e) {
        Utils.getLogger().warn("Unable to get all clients, returning empty map", e);
        allClients = new HashMap<>();
      }
    }

    return allClients;
  }

  /**
   * Gets the form repository SelectItem list.
   *
   * @return the form repository select items
   */
  public List<SelectItem> getFormRepositorySelectItems() {
    if (formRepositorySelectItems == null) {
      formRepositorySelectItems = new ArrayList<>();

      SelectItem item;

      for (String clientID : getAllClients().keySet()) {
        ClientDescriptionDto clientDto = getAllClients().get(clientID);

        // TODO: Check for type FormRepository, yet Samply.AUTH does not
        // have client-Types
        String redirectUrl = clientDto.getRedirectUrl();
        redirectUrl = Utils.removeFinalTrailFromUrl(redirectUrl);

        item = new SelectItem();
        item.setLabel(redirectUrl);
        item.setValue(redirectUrl);
        formRepositorySelectItems.add(item);
      }
    }
    return formRepositorySelectItems;
  }

  /**
   * Gets the mdr SelectItem list.
   *
   * @return the mdr select items
   */
  public List<SelectItem> getMdrSelectItems() {
    if (mdrSelectItems == null) {
      mdrSelectItems = new ArrayList<>();

      SelectItem item;

      for (String clientID : getAllClients().keySet()) {
        ClientDescriptionDto clientDto = getAllClients().get(clientID);

        // TODO: Check for type MDR, yet Samply.AUTH does not have
        // client-types
        item = new SelectItem();
        item.setLabel(clientDto.getRedirectUrl());
        item.setValue(clientDto.getRedirectUrl());
        mdrSelectItems.add(item);
      }
    }
    return mdrSelectItems;
  }

  /** Saves the FormRepository settings. */
  public void saveFR() {
    saveConfiguration();
    Utils.refreshView();
  }

  /** Checks if a Samply.AUTH URL actually points to a Samply.AUTH service, and saves the value. */
  public boolean checkAuthUrl(String authUrl) {
    if (authUrl == null || "".equals(authUrl)) {
      return false;
    }

    if (getFormParam(Config.Auth.UseSamplyAuth).equals("true")) {
      try {
        OAuth2Keys keys = getSamplyAuthOAuth2Keys(authUrl);

        if (keys != null && keys.getKeys() != null && !keys.getKeys().isEmpty()) {
          authUrlisOk = true;
        } else {
          authUrlisOk = false;
        }
      } catch (Exception e) {
        authUrlisOk = false;

        e.printStackTrace();
      }
    } else {
      authUrlisOk = Auth.isAvailable(authUrl);
    }

    return authUrlisOk;
  }

  /**
   * Saves the auth URL from the 'Component Authentication' form, after checking whether it's valid.
   */
  public void saveAuthUrl() {
    if (checkAuthUrl(getFormParam(Vocabulary.Config.Auth.REST))) {
      String oldEntry = getFormParam(Vocabulary.Config.Auth.ServerPubKey);

      OAuth2Keys keys = getSamplyAuthOAuth2Keys(getFormParam(Vocabulary.Config.Auth.REST));
      if (keys != null && keys.getKeys() != null && !keys.getKeys().isEmpty()) {
        OAuth2Key key = keys.getKeys().get(0);

        if (oldEntry != null && !"".equals(oldEntry)
            && !oldEntry.equalsIgnoreCase(key.getDerFormat())) {
          // new AUTH, delete user ID, key ID, and serverpubkey,
          // but keep own priv/pub key
          setFormParam(Vocabulary.Config.Auth.UserId, "");
          setFormParam(Vocabulary.Config.Auth.KeyId, "");
          setFormParam(Vocabulary.Config.Auth.ServerPubKey, "");
        }
        saveConfiguration();
      } else {
        Utils.getLogger().error("No auth keys found.");
      }
    }
  }

  /**
   * Retrieve the OAuth2 keys from samply.auth.
   *
   * @param authUrl Base URL for samply.auth REST API.
   * @return The keys returned by samply.auth.
   */
  private OAuth2Keys getSamplyAuthOAuth2Keys(String authUrl) {
    HttpConnector hc = ApplicationBean.getHttpConnector();
    Client client = hc.getJerseyClientForHTTPS(false);
    OAuth2Keys keys = null;
    try {
      keys = client.target(authUrl + "/oauth2/certs")
          .request().get(OAuth2Keys.class);
    } catch (ClientErrorException e) {
      Utils.getLogger().error("Unable to get get oauthkey.", e);
    }

    return keys;
  }

  /**
   * Was the authURL correct and saved?.
   *
   * @return the boolean
   */
  public Boolean isAuthUrlcorrect() {
    return authUrlisOk;
  }

  /** Registers the registry to the Samply.AUTH service. */
  public void registerAuth() {
    JSONResource osseConfig = ((ApplicationBean) Utils.getAB()).getOsseConfig();

    HashMap<String, String> authstuff = new HashMap<>();
    String pubkey = null;
    String privkey = null;

    Utils.getLogger().debug("Trying to register the registry with the auth service...");

    if (osseConfig.getProperty(Vocabulary.Config.Auth.MyPubKey) != null
        && osseConfig.getProperty(Vocabulary.Config.Auth.MyPrivKey) != null) {
      pubkey = osseConfig.getProperty(Vocabulary.Config.Auth.MyPubKey).getValue();
      privkey = osseConfig.getProperty(Vocabulary.Config.Auth.MyPrivKey).getValue();
    }
    if (pubkey != null
        && !"".equalsIgnoreCase(pubkey)
        && privkey != null
        && !"".equalsIgnoreCase(privkey)) {
      authstuff.put(Vocabulary.Config.Auth.MyPubKey, pubkey);
      authstuff.put(Vocabulary.Config.Auth.MyPrivKey, privkey);
    } else {
      try {
        authstuff = Auth.generateKeyPair();
        pubkey = authstuff.get(Vocabulary.Config.Auth.MyPubKey);
        privkey = authstuff.get(Vocabulary.Config.Auth.MyPrivKey);
        osseConfig.setProperty(Vocabulary.Config.Auth.MyPubKey, pubkey);
        osseConfig.setProperty(Vocabulary.Config.Auth.MyPrivKey, privkey);
      } catch (NoSuchAlgorithmException e) {
        Utils.getLogger().error("Error while register the registry to samply auth!",e);
      }
    }

    String email = osseConfig.getProperty(Vocabulary.Config.registryEmail).getValue();
    String registryName = osseConfig.getProperty(Vocabulary.Config.registryName).getValue();

    if (((ApplicationBean) Utils.getAB()).isBridgehead()) {
      Utils.getLogger().debug("Calling registerAuth method for bridgehead...");
      authstuff =
          Auth.registerAuth(
              pubkey, privkey, email, registryName, Utils.getAB().getConfig(), Usertype.BRIDGEHEAD);
    } else {
      Utils.getLogger().debug("Calling registerAuth method for registry...");
      authstuff =
          Auth.registerAuth(
              pubkey,
              privkey,
              email,
              registryName,
              Utils.getAB().getConfig(),
              Usertype.OSSE_REGISTRY);
    }

    if (authstuff == null) {
      String subject = "Authentication registration failed";
      String message = "Something went wrong in registering your OSSE to the authentication "
          + "service.";
      Utils.addContextMessage(
          subject,
          message,
          FacesMessage.SEVERITY_FATAL);
      Utils.getLogger().error(subject + ": " + message);
      return;
    }

    Utils.getLogger().debug("registerAuth method complete.");

    if (authstuff.containsKey("hostunreachable")) {
      String subject = "AUTH Host could not be reached";
      String message = "The AUTH service could not be reached. This could be caused if your server "
          + "has no access to the internet or if the proxy settings are incorrect.";
      Utils.addContextMessage(
          subject,
          message,
          FacesMessage.SEVERITY_ERROR);
      Utils.getLogger().error(subject + ": " + message);
      return;
    }

    if (authstuff.containsKey("conflict")) {
      String subject = "Email or username already in use";
      String message = "The email or username you provided is already in use and cannot be used to "
          + "create a new registry. Please change to the Registry setting and enter a "
          + "different email address and/or username.";
      Utils.addContextMessage(
          subject,
          message,
          FacesMessage.SEVERITY_ERROR);
      Utils.getLogger().error(subject + ": " + message);
      return;
    }

    osseConfig.setProperty(
        Vocabulary.Config.Auth.KeyId, authstuff.get(Vocabulary.Config.Auth.KeyId));
    osseConfig.setProperty(
        Vocabulary.Config.Auth.UserId, authstuff.get(Vocabulary.Config.Auth.UserId));
    osseConfig.setProperty(
        Vocabulary.Config.Auth.ServerPubKey, authstuff.get(Vocabulary.Config.Auth.ServerPubKey));

    getSessionBean().getDatabase().saveConfig("osse", osseConfig);
    getSessionBean().getDatabase().saveConfig("osse.factory.settings", osseConfig);
    Utils.getAB().init();
    loadConfiguration();

    String subject = "Authentication registration successful";
    String message = "Your account has been created in the authentication service.";
    Utils.addContextMessage(
        subject,
        message);
    Utils.getLogger().debug(subject + ": " + message);
    try {
      OsseUtils.mdrFacesClientStuff();
    } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
      Utils.getLogger().error("Error while register the registry to samply auth!", e);
    }
  }

  /**
   * Do we have locations in the registry?.
   *
   * @return the boolean
   */
  public Boolean hasLocations() {
    List<Resource> resultLocations = ((Database) getSessionBean().getDatabase()).getLocations();

    return !resultLocations.isEmpty();
  }

  /**
   * Gets the episode pattern possibilities.
   *
   * @return the episode pattern possibilities
   */
  public List<SelectItem> getEpisodePatternPossibilities() {
    List<SelectItem> locationsSelectItems = new ArrayList<>();

    SelectItem item;

    for (String patternName : EpisodePatterns.patterns.keySet()) {
      item = new SelectItem();

      item.setLabel(
          EpisodePatterns.patterns.get(patternName).getLabel()
              + " : "
              + EpisodePatterns.patterns.get(patternName).getPattern());
      item.setValue(patternName);
      locationsSelectItems.add(item);
    }

    return locationsSelectItems;
  }

  /**
   * TODO: add javadoc.
   */
  public void checkFairData() {

    JSONResource osseConfig = ((ApplicationBean) Utils.getAB()).getOsseConfig();

    for (String key : getFormParam().keySet()) {
      if (getFormParam(key) == null) {
        // never store null!
        // test
      } else if (osseConfig.getProperty(key) == null) {
        osseConfig.setProperty(key, getFormParam(key));
      } else if (!osseConfig.getProperty(key).getValue().equalsIgnoreCase(getFormParam(key))) {
        osseConfig.setProperty(key, getFormParam(key));
      }
    }

    // teste, ob Issued gefüllt ist, wenn nicht, dann ändere es

    String timeStamp =
        new SimpleDateFormat("yyyy-MM-dd'T' HH:mm:ss").format(Calendar.getInstance().getTime());
    Value property = osseConfig.getProperty("fair.fdpmetadata.issued");
    if (property == null) {

      osseConfig.setProperty("fair.fdpmetadata.issued", timeStamp);
      osseConfig.setProperty("fair.catalog.issued", timeStamp);
      osseConfig.setProperty("fair.dataset.issued", timeStamp);
      osseConfig.setProperty("fair.distribution.issued", timeStamp);
    }

    osseConfig.setProperty("fair.fdpmetadata.modified", timeStamp);
    osseConfig.setProperty("fair.catalog.modified", timeStamp);
    osseConfig.setProperty("fair.dataset.modified", timeStamp);
    osseConfig.setProperty("fair.distribution.modified", timeStamp);

    FairValidator fairValidator = new FairValidator();
    boolean testFdp = fairValidator.checkFdpMetaData(osseConfig);
    boolean testCatalog = fairValidator.checkCatalogMetaData(osseConfig);
    boolean testDataset = fairValidator.checkDataSetMetaData(osseConfig);
    boolean testDistribution = fairValidator.checkDistributionMetaData(osseConfig);

    if (!testFdp || !testCatalog || !testDataset || !testDistribution) {

      Utils.addContextMessage(
          "Saving failed, please fill out the necassary fields",
          "Your changes have not been saved",
          FacesMessage.SEVERITY_ERROR);

    } else {

      saveFairConfiguration(osseConfig, true);
    }
  }

  /**
   * * Need extra method for Save FAIR Configuration.
   */
  public String saveFairConfiguration(JSONResource osseConfig, boolean showHappy) {

    // save the osse config, and reload it in the application scoped bean
    getSessionBean().getDatabase().saveConfig("osse", osseConfig);
    Utils.getAB().init();
    loadConfiguration();

    if (showHappy) {
      Utils.addContextMessage("Saving successful", "Your changes have been saved.");
    }
    return "";
  }
}
