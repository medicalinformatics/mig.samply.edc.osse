/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.Permission;
import de.samply.edc.osse.model.Role;
import de.samply.edc.osse.utils.OsseUtils;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.model.SelectItem;
import org.apache.commons.lang.StringUtils;

/** View scoped bean to manage roles. */
@ManagedBean
@ViewScoped
public class RoleBean extends AbstractViewBean {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1003623973539279000L;

  /**
   * A list of permission quickies.
   *
   * <p>A permission quicky is basically a collection of predefined permissions under a name, for
   * example the quicky "DataEntry" containing permissions to write patients and their medical
   * data).
   */
  private static List<HashMap<String, String>> allPermissionQuickies = new ArrayList<>();

  /* to populate "allPermissionQuickies" list */
  static {

    // TODO: Yet a hardcoded list of quickies, in the future maybe this can
    // be also made freely configurable
    HashMap<String, String> permissionQuicky = new HashMap<>();

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "Z");
    permissionQuicky.put("designation", "CreatePatients");
    permissionQuicky.put("definition", "Permission to add new patients to your own location");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "B");
    permissionQuicky.put("designation", "DataEntry");
    permissionQuicky.put("definition", "Permission to enter and edit medical data of your own "
        + "location");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "Y");
    permissionQuicky.put("designation", "See my location's patients");
    permissionQuicky.put("definition", "Permission to read data of your own location");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "X");
    permissionQuicky.put("designation", "See other locations' patients");
    permissionQuicky.put("definition",
        "Permission to read data of any patient, i.e. of those of other locations as well");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "L");
    permissionQuicky.put("designation", "See my IDAT");
    permissionQuicky.put(
        "definition", "Permission to see the IDAT of patients of your own location");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "I");
    permissionQuicky.put("designation", "See all IDAT");
    permissionQuicky.put(
        "definition",
        "Permission to see the IDAT of any patient, i.e. of those of other locations as well");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "E");
    permissionQuicky.put("designation", "DataExport");
    permissionQuicky.put("definition", "Permission to export all medical data");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "R");
    permissionQuicky.put("designation", "DataReport");
    permissionQuicky.put(
        "definition", "Permission to change the form status from open to reported");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "S");
    permissionQuicky.put("designation", "DataValidation");
    permissionQuicky.put(
        "definition", "Permission to change the form status from reported to validated");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "T");
    permissionQuicky.put("designation", "RemoveValidation");
    permissionQuicky.put(
        "definition", "Permission to change the form status from validated to open again");
    allPermissionQuickies.add(permissionQuicky);

    permissionQuicky = new HashMap<>();
    permissionQuicky.put("urn", "P");
    permissionQuicky.put("designation", "PatientAccounts");
    permissionQuicky.put("definition", "Permission to handle patient accounts");
    allPermissionQuickies.add(permissionQuicky);
  }

  /**
   * The list of roles as a HashMap, which contain the role data.
   *
   * <p>Keys are:
   *
   * <p>"name" - Name of the role
   *
   * <p>"rolelocation" - Location of the role
   *
   * <p>"permissions" - List of permission entities
   */
  private ArrayList<HashMap<String, Object>> roles;
  /** List of locations as SelectItems (used in JSF components). */
  private List<SelectItem> locationsSelectItems;
  /** A comma seperated list of items (used in Javascript). */
  private String items = "";
  /** A list of items linked to 'items' above. */
  private List<HashMap<String, String>> elements = new ArrayList<>();
  /** A list of permissions. */
  private List<Permission> createdPermissions;

  /**
   * Post-construct init defines the permission quickies.
   *
   * @see de.samply.edc.control.AbstractViewBean#init()
   */
  @Override
  @PostConstruct
  public void init() {
    items = "";
    roles = null;
    locationsSelectItems = null;
    super.init();
  }

  /** Load all roles. */
  private void loadRoles() {
    roles = new ArrayList<>();

    sessionBean.getDatabase().startCaching();

    /* Preload all locations */
    ResourceQuery locationCacheQuery = new ResourceQuery(OSSEVocabulary.Type.Location);
    locationCacheQuery.setFetchAdjacentResources(false);
    sessionBean.getDatabase().getResources(locationCacheQuery);

    List<Resource> resultRoles = ((Database) getSessionBean().getDatabase()).getRoles();
    for (Resource roleResource : resultRoles) {
      Role role = new Role(getSessionBean().getDatabase(), roleResource);
      role.load(false, getConfig());

      // only display custom roles
      if (!role.isCustomRole()) {
        continue;
      }

      role.loadPermissions(getConfig());

      dataObject = new HashMap<>();
      dataObject.put("name", role.getName());
      if (role.getLocation() != null) {
        dataObject.put("rolelocation", role.getLocation().getName());
      } else {
        dataObject.put("rolelocation", "N/A");
      }
      dataObject.put("roleResource", roleResource);

      DatatableRows permissions = new DatatableRows();
      for (Entity permission : role.getPermissions(getConfig())) {
        HashMap<String, Object> columns = new HashMap<>();

        if (((Permission) permission).getLocation() != null) {
          columns.put("location", ((Permission) permission).getLocation().getName());
        }

        columns.put("entityPatient", ((Permission) permission).getEntityPatient());
        columns.put("entityCase", ((Permission) permission).getEntityCase());
        columns.put("entityEpisode", ((Permission) permission).getEntityEpisode());
        columns.put("entityForm", ((Permission) permission).getEntityForm());
        columns.put("entityUser", ((Permission) permission).getEntityUser());

        String access = ((Permission) permission).getAccess();

        // translate access to a status if we got a statusID permission
        if (((Permission) permission).getStatusID() != null) {
          access =
              ((ApplicationBean) Utils.getAB())
                  .getFormStatus()
                  .get(((Permission) permission).getStatusID());
        }

        columns.put("access", access);
        columns.put("name", ((Permission) permission).getName());
        DatatableRow permissionRow = new DatatableRow();
        permissionRow.setColumns(columns);
        permissions.add(permissionRow);
      }

      dataObject.put("permissions", permissions);
      roles.add(dataObject);
    }
    sessionBean.getDatabase().stopCaching();

    dataObject = null;
  }

  /**
   * Gets the dataObject, but also "translates" it to whatever quicky used.
   *
   * @return the data object
   * @see de.samply.edc.control.AbstractViewBean#getDataObject()
   */
  @Override
  public HashMap<String, Object> getDataObject() {
    if (dataObject == null) {
      dataObject = super.getDataObject();

      checkRoleForQuickies();
      refreshItems();
    }

    return dataObject;
  }

  /** Checks a role for quickies. */
  public void checkRoleForQuickies() {

    Boolean createPatient = false;
    Boolean readPatient = false;
    Boolean writePatient = false;
    Boolean fullMedicalReadAccess = false;
    Boolean fullMedicalWriteAccess = false;
    Boolean fullMedicalCreateAccess = false;

    Boolean reporter = false;
    Boolean signer = false;
    Boolean breaker = false;
    Boolean exporter = false;
    Boolean myIdatReader = false;
    Boolean idatReader = false;
    Boolean patientAccounts = false;
    Boolean seeMyLocPatients = false;
    Boolean seeAllLocPatients = false;

    DatatableRows permissions = (DatatableRows) dataObject.get("permissions");
    DatatableRows temp = new DatatableRows();
    List<DatatableRow> storedPermission = new ArrayList<>();

    if (permissions != null && permissions.getRows() != null) {
      for (DatatableRow permission : permissions.getRows()) {
        HashMap<String, Object> permissionColumns = permission.getColumns();

        if ("executeAction".equals(permissionColumns.get("access"))) {
          if ("createPatientUser".equals(permissionColumns.get("name"))) {
            patientAccounts = true;
          }
          continue;
        }

        if (permissionColumns.get("location") == null || dataObject.get("rolelocation") == null) {
          if ("read".equals(permissionColumns.get("access"))) {
            seeAllLocPatients = true;
          }
          continue;
        }

        // If the role location and permission location is the same,
        // check if we got a quickie
        if (permissionColumns.get("location").equals(dataObject.get("rolelocation"))) {
          if ("write".equals(permissionColumns.get("access"))) {
            if ((Boolean) permissionColumns.get("entityCase")
                && (Boolean) permissionColumns.get("entityPatient")) {
              writePatient = true;
            }

            if ((Boolean) permissionColumns.get("entityEpisode")
                && (Boolean) permissionColumns.get("entityForm")) {
              fullMedicalWriteAccess = true;
            }

            if (writePatient && fullMedicalWriteAccess) {
              // we found quickie A fully or partially

            } else if (!writePatient && fullMedicalWriteAccess) {
              // We maybe got a partial B quickie, but store it
              // for later in case it is not a B
              storedPermission.add(permission);
            } else if (writePatient && !fullMedicalWriteAccess) {
              // We maybe got a partial Z quickie, but store it
              // for later in case it is not a Z
              storedPermission.add(permission);
            } else {
              temp.add(permission);
            }
          } else if ("create".equals(permissionColumns.get("access"))) {
            if ((Boolean) permissionColumns.get("entityCase")
                && (Boolean) permissionColumns.get("entityPatient")) {
              createPatient = true;
            }

            if ((Boolean) permissionColumns.get("entityEpisode")
                && (Boolean) permissionColumns.get("entityForm")) {
              fullMedicalCreateAccess = true;
            }

            if (createPatient && fullMedicalCreateAccess) {
              //  quickie A found completely or partially
            } else if (!createPatient && fullMedicalCreateAccess) {
              // We maybe got a partial B quickie, but store it
              // for later in case it is not a B
              storedPermission.add(permission);
            } else if (createPatient && !fullMedicalCreateAccess) {
              // We maybe got a partial Z quickie,but store it
              // for later in case it is not a  Z
              storedPermission.add(permission);
            } else {
              temp.add(permission);
            }
          } else if ("read".equals(permissionColumns.get("access"))) {
            if ((Boolean) permissionColumns.get("entityCase")
                && (Boolean) permissionColumns.get("entityPatient")) {
              readPatient = true;
            }

            if ((Boolean) permissionColumns.get("entityEpisode")
                && (Boolean) permissionColumns.get("entityForm")) {
              fullMedicalReadAccess = true;
            }

            if (readPatient && fullMedicalReadAccess) {
              // at least quickie C found or partial quickie A
              seeMyLocPatients = true;
            } else if (readPatient && !fullMedicalReadAccess) {
              //  We maybe got a Z or B quickie
              storedPermission.add(permission);
            } else {
              temp.add(permission);
            }
          } else if ("open".equals(permissionColumns.get("access"))) {
            reporter = true;
          } else if ("reported".equals(permissionColumns.get("access"))) {
            signer = true;
          } else if ("validated".equals(permissionColumns.get("access"))) {
            breaker = true;
          } else if ("export".equals(permissionColumns.get("access"))) {
            exporter = true;
          } else if ("myIdatReader".equals(permissionColumns.get("access"))) {
            myIdatReader = true;
          } else if ("idatReader".equals(permissionColumns.get("access"))) {
            idatReader = true;
          } else {
            temp.add(permission);
          }
        }
      }
    }

    ArrayList<String> myItems = new ArrayList<>();

    if (fullMedicalWriteAccess && fullMedicalCreateAccess) {
      myItems.add("B");
    }
    if (writePatient && createPatient) {
      myItems.add("Z");
    }
    if (seeMyLocPatients) {
      myItems.add("Y");
    }

    if (!(myItems.contains("B")
        || myItems.contains("Z")
        || myItems.contains("A")
        || myItems.contains("C"))) {
      for (DatatableRow permission : storedPermission) {
        temp.add(permission);
      }
    }

    if (reporter) {
      myItems.add("R");
    }
    if (signer) {
      myItems.add("S");
    }
    if (breaker) {
      myItems.add("T");
    }
    if (exporter) {
      myItems.add("E");
    }
    if (idatReader) {
      myItems.add("I");
    }
    if (myIdatReader && !idatReader) {
      myItems.add("L");
    }
    if (patientAccounts) {
      myItems.add("P");
    }
    if (seeAllLocPatients) {
      myItems.add("X");
    }
    items = StringUtils.join(myItems, ",");

    dataObject.put("permissions", temp);
  }

  /**
   * Returns a list of select items of available locations. Used in JSF-Selects.
   *
   * @return the location list
   */
  public List<SelectItem> getLocationList() {
    if (locationsSelectItems == null) {
      locationsSelectItems =
          OsseUtils.getLocationsAsSelectItems((Database) getSessionBean().getDatabase());
      Collections.sort(
          locationsSelectItems,
          new Comparator<SelectItem>() {
            @Override
            public int compare(SelectItem o1, SelectItem o2) {
              return o1.getLabel().compareToIgnoreCase(o2.getLabel());
            }
          });
    }
    return locationsSelectItems;
  }

  /**
   * Returns a list of select items of available modes. Used in JSF-Selects.
   *
   * @return the mode list
   */
  public List<SelectItem> getModeList() {
    List<SelectItem> selectItemsModes = new ArrayList<>();

    SelectItem item;
    ArrayList<String> modes = new ArrayList<>();
    modes.add("");

    // When write then add automatic create, so we won't make create
    // available by itself
    modes.add("write");
    modes.add("read");

    // special rights for forms (transitions) - not yet used
    // HashMap<String, HashMap<String, String>> transitionNames =
    // ((ApplicationBean) Utils.getAB()).getRulenames();
    // for(String transitionName : transitionNames.keySet()) {
    // modes.add(transitionName);
    // }

    // special rights for forms (status)
    HashMap<Integer, String> formStatus = ((ApplicationBean) Utils.getAB()).getFormStatus();
    for (Integer statusID : formStatus.keySet()) {
      modes.add(formStatus.get(statusID));
    }

    for (String mode : modes) {
      item = new SelectItem();
      item.setLabel(mode);
      item.setValue(mode);
      selectItemsModes.add(item);
    }

    return selectItemsModes;
  }

  /**
   * Saves role data.
   *
   * @return the string
   * @see de.samply.edc.control.AbstractViewBean#save()
   */
  @Override
  public String save() {
    createdPermissions = new ArrayList<>();
    Role role = new Role(getSessionBean().getDatabase());
    String errorSummary = Utils.getResourceBundleString("addrole_summary_fail");
    String roleName = (String) dataObject.get("name");
    Resource roleResource = (Resource) dataObject.get("roleResource");

    // check if we add a new role (then roleResource is null), or we've
    // changed our own name
    if (roleResource != null
        && !roleResource
        .getProperty(OSSEVocabulary.Role.Name)
        .getValue()
        .equalsIgnoreCase(roleName)) {
      // check if that new name already exists
      if (role.entityExistsByProperty(OSSEVocabulary.Role.Name, roleName) != null) {
        String[] replaceArray = {roleName};
        String errorMessage =
            Utils.getResourceBundleStringWithPlaceholders("addrole_role_exists", replaceArray);

        Utils.addContextMessage(errorSummary, errorMessage);
        return "";
      }
    } else if (roleResource == null) {
      // check if that new name already exists
      if (role.entityExistsByProperty(OSSEVocabulary.Role.Name, roleName) != null) {
        String[] replaceArray = {roleName};
        String errorMessage =
            Utils.getResourceBundleStringWithPlaceholders("addrole_role_exists", replaceArray);

        Utils.addContextMessage(errorSummary, errorMessage);
        return "";
      }
    }

    getSessionBean().getDatabase().beginTransaction();

    String[] replaceArray = {roleName};
    String report = Utils.getResourceBundleStringWithPlaceholders("addrole_save", replaceArray);

    if (roleResource != null) {
      role.setResource(roleResource);
      report = Utils.getResourceBundleStringWithPlaceholders("editrole_save", replaceArray);

      // Delete all old permissions
      ArrayList<Resource> permissionResources =
          roleResource.getResources(
              OSSEVocabulary.Role.ReadOnly.Permissions,
              getSessionBean().getDatabase().getDatabaseModel());
      if (permissionResources != null && !permissionResources.isEmpty()) {
        getSessionBean()
            .getDatabase()
            .deleteResource(permissionResources.toArray(new Resource[permissionResources.size()]));
      }
    }

    role.setProperty(OSSEVocabulary.Role.Name, roleName);

    Location roleLocation = new Location(getSessionBean().getDatabase());
    String roleLocationName = (String) dataObject.get("rolelocation");
    Resource roleLocationResource =
        roleLocation.entityExistsByProperty(OSSEVocabulary.Location.Name, roleLocationName);
    role.setProperty(OSSEVocabulary.Role.Location, roleLocationResource);

    role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.CUSTOM);
    role.setProperty(OSSEOntology.Role.IsUserRole, false);

    role.saveOrUpdate();
    if (roleResource == null) {
      roleResource = role.getResource();
    }

    DatatableRows permissions = (DatatableRows) dataObject.get("permissions");
    if (permissions != null && !permissions.toString().equalsIgnoreCase("Empty Row")) {
      for (DatatableRow permissionRow : permissions.getRows()) {
        String access =
            (String) permissionRow.getColumns().get("access"); // setting access of permission here

        // Check if access from the form is a statusname
        Integer statusID = ((ApplicationBean) Utils.getAB()).getFormStatusReverse().get(access);
        if (statusID != null) {
          access = "executeAction";
          if (!createPermission(
              role,
              access,
              (Boolean) permissionRow.getColumns().get("entityPatient"),
              (Boolean) permissionRow.getColumns().get("entityCase"),
              (Boolean) permissionRow.getColumns().get("entityEpisode"),
              (Boolean) permissionRow.getColumns().get("entityForm"),
              (String) permissionRow.getColumns().get("location"),
              statusID)) {
            Utils.addContextMessage(
                errorSummary, Utils.getResourceBundleString("addrole_cannot_save_permission"));
            return null;
          }
        } else {
          if (!createPermission(
              role,
              access,
              (Boolean) permissionRow.getColumns().get("entityPatient"),
              (Boolean) permissionRow.getColumns().get("entityCase"),
              (Boolean) permissionRow.getColumns().get("entityEpisode"),
              (Boolean) permissionRow.getColumns().get("entityForm"),
              (String) permissionRow.getColumns().get("location"))) {
            Utils.addContextMessage(
                errorSummary, Utils.getResourceBundleString("addrole_cannot_save_permission"));
            return null;
          }

          // since access=write also exist in other type of permissions or quickies(e.g.
          // "DataEntry:propertyBased" or "DataEntry")
          // bellow condition only caters for the DataEntry+ case
          if ("write".equalsIgnoreCase((String) permissionRow.getColumns().get("access"))
              && permissionRow.getColumns().get("mdrId") == null
              && (Boolean) permissionRow.getColumns().get("entityPatient")
              && (Boolean) permissionRow.getColumns().get("entityCase")
              && (Boolean) permissionRow.getColumns().get("entityEpisode")
              && (Boolean) permissionRow.getColumns().get("entityForm")) {
            if (!createPermission(
                role,
                "create",
                (Boolean) permissionRow.getColumns().get("entityPatient"),
                (Boolean) permissionRow.getColumns().get("entityCase"),
                (Boolean) permissionRow.getColumns().get("entityEpisode"),
                (Boolean) permissionRow.getColumns().get("entityForm"),
                (String) permissionRow.getColumns().get("location"))) {
              return null;
            }
          }
        }
      }
    }

    // Save quickies
    String[] items = this.items.split(",");
    Utils.getLogger().debug("items: " + items);
    List<String> itemList = Arrays.asList(items);
    Collections.sort(itemList);
    Boolean higherDone = false;

    Boolean doAddIdatReader = false;
    Boolean doAddMyIdatReader = false;

    for (String item : itemList) {
      if (item.equalsIgnoreCase("B")) {
        if (itemList.contains("Z")) {
          createPermission(role, "write", true, true, true, true, roleLocationName);
          createPermission(role, "create", true, true, true, true, roleLocationName);

          higherDone = true;
        } else {
          createPermission(role, "write", false, false, true, true, roleLocationName);
          createPermission(role, "create", false, false, true, true, roleLocationName);

          higherDone = true;
        }

      } else if (item.equalsIgnoreCase("Z") && !higherDone) {
        // allow creating new patients
        createPermission(role, "write", true, true, false, false, roleLocationName);
        createPermission(role, "create", true, true, false, false, roleLocationName);
      } else if (item.equalsIgnoreCase("Y")) {
        // see my location's patients
        createPermission(role, "read", true, true, true, true, roleLocationName);
      } else if (item.equalsIgnoreCase("X")) {
        // new permission: see other locations' patients
        createPermission(role, "read", true, true, true, true, null);
      } else if (item.equalsIgnoreCase("R")) {
        createPermission(role, "executeAction", false, false, false, false, roleLocationName, 1);
      } else if (item.equalsIgnoreCase("S")) {
        createPermission(role, "executeAction", false, false, false, false, roleLocationName, 2);
      } else if (item.equalsIgnoreCase("T")) {
        createPermission(role, "executeAction", false, false, false, false, roleLocationName, 3);
      } else if (item.equalsIgnoreCase("E")) {
        createPermission(role, "export", false, false, false, false, roleLocationName);
      } else if (item.equalsIgnoreCase("I")) {
        doAddIdatReader = true;
      } else if (item.equalsIgnoreCase("L")) {
        doAddMyIdatReader = true;
      } else if (item.equalsIgnoreCase("P")) {
        createPatientUserPermission(role, roleLocationName);
      }
    }

    if (doAddIdatReader) {
      createPermission(role, "idatReader", false, false, false, false, roleLocationName);
    } else if (doAddMyIdatReader) {
      createPermission(role, "myIdatReader", false, false, false, false, roleLocationName);
    }

    getSessionBean().getDatabase().commit();

    String summary = Utils.getResourceBundleString("addrole_summary_success");
    Utils.addContextMessage(summary, report);

    goRoleAdminList();
    return null;
  }

  /**
   * Check if a permission is already saved so we don't save it twice.
   *
   * @param permission the permission
   * @return the boolean
   */
  private Boolean checkAlreadySaved(Permission permission) {
    for (Permission done : createdPermissions) {
      if (done.equals(permission)) {
        Utils.getLogger().debug("\nPermission:" + permission + "\nequals    :" + done);
        return true;
      }
    }

    return false;
  }

  /**
   * Creates a permission.
   *
   * @param role the role
   * @param access the access
   * @param entityPatient the entity patient
   * @param entityCase the entity case
   * @param entityEpisode the entity episode
   * @param entityForm the entity form
   * @param location the location
   * @return the boolean
   */
  private Boolean createPermission(
      Role role,
      String access,
      Boolean entityPatient,
      Boolean entityCase,
      Boolean entityEpisode,
      Boolean entityForm,
      String location) {
    return createPermission(
        role, access, entityPatient, entityCase, entityEpisode, entityForm, location, null);
  }

  /**
   * Creates a permission.
   *
   * @param role the role
   * @param access the access
   * @param entityPatient the entity patient
   * @param entityCase the entity case
   * @param entityEpisode the entity episode
   * @param entityForm the entity form
   * @param location the location
   * @param statusID the status id
   * @return the boolean
   */
  private Boolean createPermission(
      Role role,
      String access,
      Boolean entityPatient,
      Boolean entityCase,
      Boolean entityEpisode,
      Boolean entityForm,
      String location,
      Integer statusID) {
    // TODO: check if the "same" permission is already saved, then don't
    // save another one (no doubles please)

    Permission permission = new Permission(getSessionBean().getDatabase());

    permission.setStatusID(statusID);
    if (statusID != null) {
      permission.setName("changeStatus");
    }

    permission.setAccess(access);
    permission.setEntityPatient(entityPatient);
    // TODO: For now if the user may do stuff about the patient, he may also
    // do the same about the cases
    permission.setEntityCase(entityPatient);

    permission.setEntityEpisode(entityEpisode);
    permission.setEntityForm(entityForm);

    if (location == null) {
      permission.setShowIdat(false);
    } else {
      permission.setShowIdat(true);

      Location permissionLocation = new Location(getSessionBean().getDatabase());
      Resource permissionLocationResource =
          permissionLocation.entityExistsByProperty(OSSEVocabulary.Location.Name, location);

      if (permissionLocationResource == null) {
        getSessionBean().getDatabase().rollback();
        throw new Error("Meh, Location not found");
      }
      permissionLocation.setResource(permissionLocationResource);
      permissionLocation.setProperty(OSSEVocabulary.Location.Name, location);
      permission.setLocation(permissionLocation);
    }

    permission.setParent(role);

    if (!checkAlreadySaved(permission)) {
      permission.saveOrUpdate(getConfig());
      createdPermissions.add(permission);
    }

    return true;
  }

  /**
   * Creates the patient user permission.
   *
   * @param role the role
   * @param location the location
   */
  private void createPatientUserPermission(Role role, String location) {
    Permission permission = new Permission(getSessionBean().getDatabase());
    permission.setName("createPatientUser");
    permission.setAccess("executeAction");
    permission.setEntityUser(true);
    permission.setParent(role);

    Location permissionLocation = new Location(getSessionBean().getDatabase());
    Resource permissionLocationResource =
        permissionLocation.entityExistsByProperty(OSSEVocabulary.Location.Name, location);

    if (permissionLocationResource == null) {
      getSessionBean().getDatabase().rollback();
      throw new Error("Meh, Location not found");
    }
    permissionLocation.setResource(permissionLocationResource);
    permissionLocation.setProperty(OSSEVocabulary.Location.Name, location);
    permission.setLocation(permissionLocation);

    if (!checkAlreadySaved(permission)) {
      permission.saveOrUpdate(getConfig());
      createdPermissions.add(permission);
    }
  }

  /** Redirect user to the role admin list. */
  public void goRoleAdminList() {
    Utils.goAdminForm("rolelist");
  }

  /**
   * JSF action after pressing edit of a selected role.
   *
   * @param roleData the role data
   * @return the string
   */
  public String editRole(HashMap<String, Object> roleData) {
    getSessionBean().setTempObject("dataObject", roleData);
    Utils.goAdminForm("role_edit");
    return "editRole";
  }

  /**
   * redirects the user when wanting to add a new role.
   *
   * @return the string
   */
  public String goAddRole() {
    getSessionBean().clearTempObject("dataObject");
    Utils.goAdminForm("role_edit");
    return "addRole";
  }

  /**
   * Sets the role to be deleted.
   *
   * @param toDeleteRole the new to delete role
   */
  public void setToDeleteRole(Object toDeleteRole) {
    getSessionBean().setTempObject("dataObject", toDeleteRole);
  }

  /**
   * Deletes a role.
   *
   * @return the string
   */
  @SuppressWarnings("unchecked")
  public String deleteRole() {
    dataObject = (HashMap<String, Object>) getSessionBean().clearTempObject("dataObject");
    if (dataObject == null) {
      return "";
    }

    ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Role);
    query.add(
        Criteria.Equal(
            OSSEVocabulary.Type.Role,
            OSSEVocabulary.Role.Name,
            (String) dataObject.get(OSSEVocabulary.Role.Name)));
    ArrayList<Resource> found = getSessionBean().getDatabase().getResources(query);
    for (Resource deleteMe : found) {
      // TODO: delete permission first, if it's not used in another role
      getSessionBean().getDatabase().deleteResource(deleteMe);
    }

    loadRoles();
    return "deleteRole";
  }

  /** Loads the javascript items list and puts its data into the java elements list. */
  public void refreshItems() {
    String[] items = this.items.split(",");
    elements = new ArrayList<>();

    for (String s : items) {
      if (s.trim().length() == 0) {
        continue;
      }

      elements.add(findElement(s));
    }
  }

  /**
   * Finds an element by its "urn" property, and returns the element as a HashMap containing its
   * data.
   *
   * @param searchFor the urn property to search for
   * @return the hash map
   */
  private HashMap<String, String> findElement(String searchFor) {
    for (HashMap<String, String> element : allPermissionQuickies) {
      if (searchFor.equals(element.get("urn"))) {
        return element;
      }
    }

    return null;
  }

  /**
   * Gets the roles.
   *
   * @return the roles
   */
  public ArrayList<HashMap<String, Object>> getRoles() {
    if (roles == null) {
      loadRoles();
    }
    return roles;
  }

  /**
   * Gets the items. A comma seperated list of items (used in Javascript)
   *
   * @return the items
   */
  public String getItems() {
    return items;
  }

  /**
   * Sets the items. A comma seperated list of items (used in Javascript)
   *
   * @param items the new items
   */
  public void setItems(String items) {
    this.items = items;
  }

  /**
   * Gets the elements. A list of elements linked to 'items' above
   *
   * @return the elements
   */
  public List<HashMap<String, String>> getElements() {
    return elements;
  }

  /**
   * Sets the elements. A list of items linked to 'items' above
   *
   * @param elements the elements
   */
  public void setElements(List<HashMap<String, String>> elements) {
    this.elements = elements;
  }

  /**
   * Gets all permission quickies.
   *
   * @return the list of all permission quickies
   */
  public List<HashMap<String, String>> getAllPermissionQuickies() {
    return RoleBean.allPermissionQuickies;
  }

  /**
   * Sets all permission quickies.
   *
   * @param allPermissionQuickies the list of all permission quickies
   */
  public void setAllPermissionQuickies(List<HashMap<String, String>> allPermissionQuickies) {
    RoleBean.allPermissionQuickies = allPermissionQuickies;
  }
}
