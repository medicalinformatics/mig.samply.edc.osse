/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.edc.osse.model.TempTokenExchangeStorage;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.codehaus.jettison.json.JSONException;
import org.codehaus.jettison.json.JSONObject;

/** REST interface for callback requests by the Mainzelliste. */
@Path("")
public class MainzellisteInterface {

  /**
   * New patient.
   *
   * @param dataString JSON data as submitted by the callback request of the Mainzelliste.
   * @return the response
   */
  @POST
  @Path("/patients")
  public Response newPatient(String dataString) {
    try {
      JSONObject data = new JSONObject(dataString);
      String patientId;
      // Read id of new patient
      if (data.has("id")) {
        // API version 1
        patientId = data.getString("id");
      } else { // API version 2
        patientId = data.getJSONArray("ids").getJSONObject(0).getString("idString");
      }
      String tokenId = data.getString("tokenId");
      TempTokenExchangeStorage.instance.setCurrentObject(tokenId, patientId);

      return Response.ok().build();
    } catch (JSONException e) {
      return Response.status(Status.BAD_REQUEST)
          .entity(
              "Error while parsing input data. Please provide a legal JSON object. Error message: "
                  + e.getMessage())
          .build();
    }
  }
}
