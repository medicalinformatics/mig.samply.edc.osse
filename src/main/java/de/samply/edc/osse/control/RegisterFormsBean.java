/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.auth.client.GrantType;
import de.samply.auth.rest.AccessTokenDto;
import de.samply.common.http.HttpConnector;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.catalog.Vocabulary.Config.FormEditor;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.exceptions.OsseException;
import de.samply.edc.osse.model.MdrKeyUsageData;
import de.samply.edc.osse.model.MdrKeyUsageStore;
import de.samply.edc.osse.utils.FormImportHelper;
import de.samply.edc.osse.utils.FormRecreationHelper;
import de.samply.edc.utils.Utils;
import de.samply.form.dto.FormDetails;
import de.samply.form.dto.FormOverview;
import de.samply.store.JSONResource;
import de.samply.store.Value;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.StringUtils;

/** View-scoped Bean to control which forms are in the registry. */
@ManagedBean
@ViewScoped
public class RegisterFormsBean extends AbstractViewBean {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -8635494505863836665L;
  /**
   * Helper Class containing all important methods to import forms from the FormRepository and
   * create files from them.
   */
  protected FormImportHelper fih = new FormImportHelper((ApplicationBean) Utils.getAB());
  /** A comma-seperated string of case form IDs (used in javascript). */
  private String items = "";
  /** A uniqued comma-seperated string of unique case form IDs (used in javascript). */
  private String itemsTemp = "";
  /** A comma-seperated string of episode form IDs (used in javascript). */
  private String itemsEpisode = "";
  /** A comma-seperated string of unique episode form IDs (used in javascript). */
  private String itemsEpisodeTemp = "";
  /** Map of all forms. */
  private HashMap<String, HashMap<String, HashMap<String, String>>> allTempForms = new HashMap<>();
  /** List of all forms available in the form repository. */
  private List<HashMap<String, String>> allForms = new ArrayList<>();
  /** A list of case form elements (linked to 'items'). */
  private List<HashMap<String, String>> elements = new ArrayList<>();
  /** A list of episode form elements (linked to 'itemsEpisode'). */
  private List<HashMap<String, String>> elementsEpisode = new ArrayList<>();
  /** The patient form selection. */
  private HashMap<String, Boolean> patientFormSelection = new HashMap<>();

  /**
   * Loads all forms from the form repository REST interface.
   *
   * @param client JerseyClient
   * @return FormOverview[] array of Forms (as provided by XML)
   */
  protected FormOverview[] getForms(Client client) {
    return getForms(client, null);
  }

  /**
   * Loads all forms from the form repository REST interface.
   *
   * @param client JerseyClient
   * @param accessToken Samply.AUTH access token
   * @return FormOverview[] array of Forms (as provided by XML)
   */
  protected FormOverview[] getForms(Client client, AccessTokenDto accessToken) {
    try {
      if (accessToken == null) {
        Configuration config = Utils.getAB().getConfig();
        accessToken =
            Auth.getOAuth2Client(config).isUseSamplyAuth()
                ? Auth.getAccessToken(client, config)
                : Auth.getAccessToken(client, config, GrantType.PASSWORD);
      }

      if (accessToken == null) {
        // AUTH seems to be gone
        ((SessionBean) Utils.getSB()).setCriticalAuthError(true);
        return null;
      }

      // XXX: For debugging purposes
      // client.addFilter(new LoggingFilter(System.out));

      String urlFR =
          Utils.getAB().getConfig().getProperty(Vocabulary.Config.FormEditor.formEditorRestUrl)
              + "/forms";
      String token = "Bearer " + accessToken.getAccessToken();

      try {
        Response response =
            client.target(urlFR)
                .request(MediaType.APPLICATION_JSON)
                .header(HttpHeaders.AUTHORIZATION, token)
                .accept(MediaType.APPLICATION_JSON)
                .get(Response.class);
        return response.readEntity(FormOverview[].class);
      } catch (Exception e) {
        ((SessionBean)Utils.getSB()).setCriticalFormEditorError(true);
        Utils.getLogger().error("Error while communicating with forms repo at:" + urlFR, e);
        return null;
      }

    } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
      Utils.getLogger().warn("Unable to get forms from form repository", e);
      return null;
    }
  }

  /**
   * Loads all forms from the form repository REST interface.
   *
   * @param client JerseyClient
   * @param formId the form id
   * @param formVersion the form version
   * @return FormOverview[] array of Forms (as provided by XML)
   */
  private FormDetails getForm(Client client, String formId, String formVersion) {
    try {
      Configuration config = Utils.getAB().getConfig();
      AccessTokenDto accessToken =
          Auth.getOAuth2Client(config).isUseSamplyAuth()
              ? Auth.getAccessToken(client, config)
              : Auth.getAccessToken(client, config, GrantType.PASSWORD);
      if (accessToken == null) {
        // AUTH seems to be gone
        ((SessionBean) Utils.getSB()).setCriticalAuthError(true);
        return null;
      }

      // XXX: For debugging purposes
      // client.addFilter(new LoggingFilter(System.out));

      String urlFR =
          Utils.getAB().getConfig().getProperty(FormEditor.formEditorRestUrl)
              + "/form/"
              + formId
              + "/"
              + formVersion;
      String token = "Bearer " + accessToken.getAccessToken();
      try {
        return client.target(urlFR).request().header("authorization", token).get(FormDetails.class);
      } catch (Exception e) {
        Utils.getLogger().error("Error while communicating with forms repo at:" + urlFR, e);
        return null;
      }

    } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
      Utils.getLogger().warn("Unable to get forms from form repository", e);
      return null;
    }
  }

  /** Action for button "reload forms". */
  public void reloadAllForms() {

    if (((SessionBean)Utils.getSB()).getCurrentRole().isAdminRole()) {
      FormOverview[] theForms;

      HttpConnector hc = ApplicationBean.getHttpConnector();
      String urlFR =
          Utils.getAB().getConfig().getProperty(FormEditor.formEditorRestUrl) + "";
      Client client = hc.getJerseyClient(urlFR, false);
      theForms = getForms(client);

      if (theForms == null) {
        allForms = null;
        Utils.addContextMessage("Error loading forms", "The forms list could not be loaded.");
        return;
      } else {
        allForms = new ArrayList<>();
      }

      allTempForms = new HashMap<>();
      HashMap<String, String> form;
      for (FormOverview fo : theForms) {
        form = new HashMap<>();
        form.put("name", fo.getName());
        form.put("id", "" + fo.getId());
        form.put("osseid", "" + fo.getId() + "-" + fo.getVersion());
        form.put("version", "" + fo.getVersion());
        form.put("description", fo.getDescription());
        HashMap<String, HashMap<String, String>> temp = allTempForms.get("" + fo.getId());

        if (temp == null) {
          temp = new HashMap<>();
        }
        temp.put("" + fo.getVersion(), form);
        allTempForms.put("" + fo.getId(), temp);
      }

      if (this.items != null && !"".equals(this.items)) {
        String[] allFormIds = this.items.split(",");

        runThrough(allFormIds);
      }

      if (this.itemsEpisode != null && !"".equals(this.itemsEpisode)) {
        String[] allFormIds = this.itemsEpisode.split(",");

        runThrough(allFormIds);
      }

      addRest();
    }
  }

  /**
   * Method to load a single form that was deleted before and thus is not yet in our list of all
   * forms. This is necessary if someone used a form in a registry and deleted it in the form
   * repository
   *
   * @param formIdAndVersion FormID and Version e.g. "42-8" means formId = 42, version = 8
   */
  private void loadDeletedForm(String formIdAndVersion) {
    String[] formIdAndVersionParts = formIdAndVersion.split("-");
    String formId = formIdAndVersionParts[0];
    String formVersion = formIdAndVersionParts[1];

    HttpConnector httpConnector = ApplicationBean.getHttpConnector();
    String urlFR =
        Utils.getAB().getConfig().getProperty(FormEditor.formEditorRestUrl) + "";
    Client client = httpConnector.getJerseyClient(urlFR, false);
    FormDetails formDetails = getForm(client, formId, formVersion);
    client.close();
    httpConnector.closeClients();
    HashMap<String, String> form = new HashMap<>();
    form.put("name", formDetails.getName());
    form.put("id", "" + formDetails.getId());
    form.put("osseid", "" + formDetails.getId() + "-" + formDetails.getVersion());
    form.put("version", "" + formDetails.getVersion());
    form.put("description", formDetails.getDescription());
    HashMap<String, HashMap<String, String>> temp = allTempForms.get("" + formDetails.getId());

    if (temp == null) {
      temp = new HashMap<>();
    }
    temp.put("" + formDetails.getVersion(), form);
    allTempForms.put("" + formDetails.getId(), temp);
  }

  /**
   * Method to fill the allForms and allTempForms array for the UI. It provides all forms that are
   * currently in the registry, and their latest versions.
   *
   * @param allFormIds List of all forms
   */
  private void runThrough(String[] allFormIds) {
    HashMap<String, String> form;

    // run through allIds
    for (String id : allFormIds) {
      String[] formIdVer = id.split("-");
      try {
        form = allTempForms.get(formIdVer[0]).get(formIdVer[1]);
      } catch (NullPointerException e) {
        // Nullpointer means this form is not in our list of all
        // which can happen if it was for example deleted in the
        // form repository but is still used in our registry
        // so we have to load its data solo
        Utils.getLogger().debug("Form not found, maybe it was deleted from the repo." + e);
        loadDeletedForm(id);
        form = allTempForms.get(formIdVer[0]).get(formIdVer[1]);
      }

      allForms.add(form);

      // now add the latest version of this id as well
      Integer highestVersion = Integer.parseInt(formIdVer[1]);
      for (String moo : allTempForms.get(formIdVer[0]).keySet()) {
        Integer thisVersion = Integer.parseInt(moo);
        if (thisVersion > highestVersion) {
          highestVersion = thisVersion;
        }
      }

      if (highestVersion > Integer.parseInt(formIdVer[1])) {
        allForms.add(allTempForms.get(formIdVer[0]).get("" + highestVersion));
      }

      // and now remove this form id completely from our list
      allTempForms.remove(formIdVer[0]);
    }
  }

  /**
   * Method to fill the allForms and allTempForms array for the UI. Puts in all forms in their
   * latest version that are not yet in the registry.
   */
  private void addRest() {
    // now add the remaining yet unused forms
    for (String tempForm : allTempForms.keySet()) {
      Integer highestVersion = 1;
      for (String moo : allTempForms.get(tempForm).keySet()) {
        Integer thisVersion = Integer.parseInt(moo);
        if (thisVersion > highestVersion) {
          highestVersion = thisVersion;
        }
      }

      allForms.add(allTempForms.get(tempForm).get("" + highestVersion));
    }
  }

  /**
   * Post-construct init. Loads all forms from the repository and prepares the other UI values to
   * display the current configuration of forms
   *
   * @see de.samply.edc.control.AbstractViewBean#init()
   */
  @SuppressWarnings({"unchecked"})
  @Override
  @PostConstruct
  public void init() {
    FormOverview[] theForms = null;

    HttpConnector httpConnector = ApplicationBean.getHttpConnector();
    String urlFR = Utils.getAB().getConfig()
        .getProperty(Vocabulary.Config.FormEditor.formEditorRestUrl).toString();
    Client client = httpConnector.getJerseyClient(urlFR, false);
    theForms = getForms(client);
    client.close();
    httpConnector.closeClients();

    if (theForms == null) {
      allForms = null;
      Utils.addContextMessage("Error loading forms", "The forms list could not be loaded.");
      ((SessionBean)Utils.getSB()).setCriticalFormEditorError(true);
      return;
    } else {
      allForms = new ArrayList<>();
    }

    HashMap<String, String> form;
    for (FormOverview formData : theForms) {
      form = new HashMap<>();
      form.put("name", formData.getName());
      form.put("id", "" + formData.getId());
      form.put("osseid", "" + formData.getId() + "-" + formData.getVersion());
      form.put("version", "" + formData.getVersion());
      form.put("description", formData.getDescription());

      // read translations
      for (FormOverview.I18n i18n : formData.getI18ns()) {
        form.put(i18n.getLanguage() + "_" + "name", i18n.getName());
        form.put(i18n.getLanguage() + "_" + "description", i18n.getDescription());
      }

      HashMap<String, HashMap<String, String>> temp = allTempForms.get("" + formData.getId());

      if (temp == null) {
        temp = new HashMap<>();
      }
      temp.put("" + formData.getVersion(), form);
      allTempForms.put("" + formData.getId(), temp);

      allForms.add(form);
    }

    // reading list of patforms
    String[] patformItems = null;

    if (Utils.getAB().getConfig().getProperty(Vocabulary.Config.PatientForm.list) == null) {
      patformItems = null;
    } else if (Utils.getAB()
        .getConfig()
        .getProperty(Vocabulary.Config.PatientForm.list)
        .getClass()
        .isArray()) {
      patformItems =
          (String[]) Utils.getAB().getConfig().getProperty(Vocabulary.Config.PatientForm.list);
    } else if (Utils.getAB().getConfig().getProperty(Vocabulary.Config.PatientForm.list)
        instanceof ArrayList) {
      ArrayList<String> temp =
          (ArrayList<String>)
              Utils.getAB().getConfig().getProperty(Vocabulary.Config.PatientForm.list);
      patformItems = temp.toArray(new String[temp.size()]);
    } else {
      String patformItemsString =
          (String) Utils.getAB().getConfig().getProperty(Vocabulary.Config.PatientForm.list);
      patformItems = patformItemsString.split(",");
    }

    if (patformItems != null) {
      for (String key : patformItems) {
        patientFormSelection.put(key, true);
      }
    }

    String[] items = null;
    String configItems = null;

    if (Utils.getAB()
        .getConfig()
        .getProperty(Vocabulary.Config.Form.patient)
        .getClass()
        .isArray()) {
      items = (String[]) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.patient);
    } else if (Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.patient)
        instanceof ArrayList) {
      ArrayList<String> temp =
          (ArrayList<String>) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.patient);
      items = temp.toArray(new String[temp.size()]);
    } else {
      configItems = (String) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.patient);
      items = configItems.split(",");
    }

    ArrayList<String> allCaseFormIds = new ArrayList<>();
    ArrayList<String> allIdsTemp = new ArrayList<>();

    for (String s : items) {
      Integer verPos = s.lastIndexOf("-");
      if (verPos < 0) {
        continue;
      }
      String version = s.substring(verPos);

      String[] formsplitted = s.split("_");
      String formID = "";
      if (formsplitted.length > 1) {
        formID = formsplitted[1] + version;
        allCaseFormIds.add(formID);
        allIdsTemp.add(formsplitted[1]);
      }
    }

    this.items = StringUtils.join(allCaseFormIds, ",");
    this.setItemsTemp(StringUtils.join(allIdsTemp, ","));

    if (Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.visit).getClass().isArray()) {
      items = (String[]) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.visit);
    } else if (Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.visit)
        instanceof ArrayList) {
      ArrayList<String> temp =
          (ArrayList<String>) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.visit);
      items = temp.toArray(new String[temp.size()]);
    } else {
      configItems = (String) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.visit);
      items = configItems.split(",");
    }

    ArrayList<String> allEpisodeFormIds = new ArrayList<>();
    allIdsTemp = new ArrayList<>();

    for (String s : items) {
      Integer verPos = s.lastIndexOf("-");
      if (verPos < 0) {
        continue;
      }
      String version = s.substring(verPos);

      String[] formsplitted = s.split("_");
      String formID = "";
      if (formsplitted.length > 1) {
        formID = formsplitted[1] + version;
        allEpisodeFormIds.add(formID);
        allIdsTemp.add(formsplitted[1]);
      }
    }

    this.itemsEpisode = StringUtils.join(allEpisodeFormIds, ",");
    this.setItemsEpisodeTemp(StringUtils.join(allIdsTemp, ","));

    Boolean hasForms = allForms.size() > 0;

    allForms.clear();

    if (hasForms) {
      // Now filter the allForms so that only currently used and newer
      // versions of a form are displayed
      // TODO: Switchbutton to access older versions
      if (this.items != null && !"".equals(this.items)) {
        runThrough(this.items.split(","));
      }
      if (this.itemsEpisode != null && !"".equals(this.itemsEpisode)) {
        runThrough(this.itemsEpisode.split(","));
      }
      addRest();
    }

    // Update the item lists of case forms and episode forms for the
    // javascript stuff
    refreshItems();
    refreshItemsEpisode();
  }

  /**
   * Reset.
   *
   * @return the string
   */
  public String reset() {
    elements.clear();
    elementsEpisode.clear();
    items = "";
    itemsEpisode = "";
    return "";
  }

  /**
   * Loads the Javascript-filled list of case forms and fills it into the elements list used by
   * Java.
   */
  public void refreshItems() {
    String[] items = this.items.split(",");
    elements = new ArrayList<>();

    HashMap<String, String> foundElements;

    for (String s : items) {
      if (s.trim().length() == 0) {
        continue;
      }

      foundElements = findElement(s);
      if (foundElements != null) {
        elements.add(foundElements);
      }
    }
  }

  /**
   * Loads the Javascript-filled list of episode forms and fills it into the elements list used by
   * Java.
   */
  public void refreshItemsEpisode() {
    String[] items = this.itemsEpisode.split(",");
    elementsEpisode = new ArrayList<>();

    for (String s : items) {
      if (s.trim().length() == 0) {
        continue;
      }

      elementsEpisode.add(findElement(s));
    }
  }

  /**
   * Finds a certain form element in our list.
   *
   * @param findMe the id of the element to find
   * @return the hash map
   */
  private HashMap<String, String> findElement(String findMe) {
    for (HashMap<String, String> element : allForms) {
      if (findMe.equals(element.get("osseid"))) {
        return element;
      }
    }

    return null;
  }

  /** Remakes all forms, they get re-imported from the form repository. */
  public void remakeForms() {
    FormRecreationHelper rch = new FormRecreationHelper((ApplicationBean) Utils.getAB());
    rch.recreateForms();
  }

  /**
   * Saves the selected forms to the registry config and converts them to XHTML to be stored on
   * harddisk and as fallback-DB entry.
   *
   * @return JSF outcome string
   */
  @Override
  public String save() {
    Configuration configuration = Utils.getAB().getConfig();
    fih.clearData();

    JSONResource osseFormStorage = ((ApplicationBean) Utils.getAB()).getOsseFormStorage();

    JSONResource formStorage = null;
    if (osseFormStorage.getProperty(Vocabulary.Config.Form.storage) != null) {
      formStorage = osseFormStorage.getProperty(Vocabulary.Config.Form.storage).asJSONResource();
    } else {
      formStorage = new JSONResource();
    }

    JSONResource formNameMatrix = null;
    if (osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix) != null) {
      formNameMatrix =
          osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix).asJSONResource();
    } else {
      formNameMatrix = new JSONResource();
    }

    fih.setFormNameMatrix(formNameMatrix);

    HttpConnector httpConnector = null;
    Client client = null;
    AccessTokenDto accessToken = null;

    try {
      httpConnector = ApplicationBean.getHttpConnector();
      // Get a HTTPS jersey client (AUTH is always https)
      client = httpConnector.getJerseyClientForHTTPS(false);
      accessToken =
          Auth.getOAuth2Client(configuration).isUseSamplyAuth()
              ? Auth.getAccessToken(client, configuration)
              : Auth.getAccessToken(client, configuration, GrantType.PASSWORD);
    } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
      Utils.getLogger().error("Unable to get accessToken while saving forms.", e);
    }

    if (httpConnector == null) {
      Utils.getLogger().error("HTTPclient could not be started");
      Utils.addContextMessage(
          "Failed", "A critical error occured (HTTPclient could not be started.");
      return "failed";
    }

    if (accessToken == null) {
      Utils.getLogger().error("No access token could be gained");
      Utils.addContextMessage("Failed", "A critical error occured (No access token was returned.");
      return "failed";
    }

    // Get a Jersey client for the FormRepository
    String urlFR = configuration.getProperty(FormEditor.formEditorRestUrl) + "";
    Client frJerseyClient = httpConnector.getJerseyClient(urlFR, false);

    // Get a Jersey client for the MDR
    String urlMdr = configuration.getProperty(Vocabulary.Config.Mdr.REST) + "";
    Client mdrJerseyClient = httpConnector.getJerseyClient(urlMdr, false);
    String mdrUrl = (String) configuration.getProperty(Vocabulary.Config.Mdr.REST);
    MdrClient mdrClient = new MdrClient(mdrUrl, mdrJerseyClient);

    // import case forms
    for (HashMap<String, String> form : elements) {
      if (form == null) {
        continue;
      }

      formStorage =
          fih.doImport(
              frJerseyClient,
              mdrClient,
              accessToken,
              form.get("id"),
              form.get("version"),
              false,
              formStorage,
              true,
              false);
    }

    // import episode forms
    for (HashMap<String, String> form : elementsEpisode) {
      if (form == null) {
        continue;
      }

      formStorage =
          fih.doImport(
              frJerseyClient,
              mdrClient,
              accessToken,
              form.get("id"),
              form.get("version"),
              true,
              formStorage,
              true,
              false);
    }

    // fill out old form validation stuff and formstorage with old forms
    try {
      for (String formName :
          ((ApplicationBean) Utils.getAB()).getArchivedFormulars().get("en").keySet()) {
        formStorage =
            readoutFormDataIntoFih(
                formStorage, mdrClient, frJerseyClient, accessToken, formName, false);
      }

      for (String formName :
          ((ApplicationBean) Utils.getAB()).getArchivedVisitFormulars().get("en").keySet()) {
        formStorage =
            readoutFormDataIntoFih(
                formStorage, mdrClient, frJerseyClient, accessToken, formName, true);
      }
    } catch (FileNotFoundException e) {
      Utils.getLogger().warn("Unable to readout form data.", e);
    }
    // store the forms and names in the osse form storage for later recovery
    osseFormStorage.setProperty(Vocabulary.Config.Form.storage, formStorage);
    osseFormStorage.setProperty(Vocabulary.Config.Form.formNameMatrix, fih.getFormNameMatrix());
    getSessionBean().getDatabase().saveConfig("osse.form.storage", osseFormStorage);

    try {
      fih.fillMdrEntityHasValidation(mdrClient, accessToken);
    } catch (OsseException e) {
      Utils.getLogger().warn("Unable to fill mdr has validation.", e);
    }

    saveConfig(mdrJerseyClient, accessToken);

    // (re)create csv file
    try {
      fih.createDataElementListCsv(mdrClient, accessToken);
    } catch (MdrConnectionException e) {
      Utils.getLogger().warn("Unable to create data element csv list.", e);
    } catch (ExecutionException e) {
      Utils.getLogger().warn("Unable to create data element csv list.", e);
    } catch (MdrInvalidResponseException e) {
      Utils.getLogger().warn("Unable to create data element csv list.", e);
    } catch (OsseException e) {
      Utils.getLogger().warn("Unable to create data element csv list.", e);
    }

    Utils.addContextMessage(
        "Success", "Your selected forms have been imported into your registry.");
    return "save";
  }

  /**
   * Readout form data and adds them to a given form storage.
   *
   * @param formStorage the form storage to fill up
   * @param mdrClient the mdr client
   * @param frJerseyClient the fr jersey client
   * @param accessToken the access token
   * @param formName the form name
   * @param episodeForm the episode form
   * @return the resulting formStorage
   * @throws FileNotFoundException the file not found exception
   */
  private JSONResource readoutFormDataIntoFih(
      JSONResource formStorage,
      MdrClient mdrClient,
      Client frJerseyClient,
      AccessTokenDto accessToken,
      String formName,
      Boolean episodeForm)
      throws FileNotFoundException {
    Pattern pattern = Pattern.compile("^form_([^-]+)_ver-([^-]+)$");
    Matcher matcher = pattern.matcher(formName);
    if (!matcher.find()) {
      Utils.getLogger().debug("Formname " + formName + " does not match pattern.");
      return formStorage;
    }

    formStorage =
        fih.doImport(
            frJerseyClient,
            mdrClient,
            accessToken,
            matcher.group(1),
            matcher.group(2),
            episodeForm,
            formStorage,
            false,
            true);

    return formStorage;
  }

  /**
   * Cancel button action.
   *
   * @return JSF outcome string
   */
  public String cancel() {
    Utils.goAdminForm("forms");
    Utils.addContextMessage(
        "Changes reverted", "All your changes have been reverted to the current state.");
    return "cancel";
  }

  /**
   * Method to store old forms into an archive in case they get replaced so we still are able to
   * render them in case they were used to store data in older versions.
   *
   * @param osseConfig the osse config
   * @return the JSON resource
   */
  private JSONResource storeOldForms(JSONResource osseConfig) {
    // Store the form.matrix
    Value oldFormNameMatrix = osseConfig.getProperty(Vocabulary.Config.formNames);
    if (oldFormNameMatrix == null) {
      oldFormNameMatrix = new JSONResource();
    }

    JSONResource archivedFormMatrix = null;
    if (osseConfig.getProperty(Vocabulary.Config.archivedFormNames) != null) {
      archivedFormMatrix =
          osseConfig.getProperty(Vocabulary.Config.archivedFormNames).asJSONResource();
    } else {
      archivedFormMatrix = new JSONResource();
    }

    for (String formNameAndVersion : oldFormNameMatrix.asJSONResource().getDefinedProperties()) {
      archivedFormMatrix.setProperty(
          formNameAndVersion, oldFormNameMatrix.asJSONResource().getProperty(formNameAndVersion));
    }

    osseConfig.setProperty(Vocabulary.Config.archivedFormNames, archivedFormMatrix);

    // Store caseForms
    String oldForms = osseConfig.getProperty(Vocabulary.Config.Form.patient).getValue();
    String[] forms = oldForms.split(",");
    Set<String> formsList = new HashSet<>(Arrays.asList(forms));

    if (osseConfig.getProperty(Vocabulary.Config.Form.archivedPatient) != null) {
      String archivedForms =
          osseConfig.getProperty(Vocabulary.Config.Form.archivedPatient).getValue();
      forms = archivedForms.split(",");
      Collections.addAll(formsList, forms);
    }
    oldForms = StringUtils.join(formsList, ",");
    osseConfig.setProperty(Vocabulary.Config.Form.archivedPatient, oldForms);

    // Store episodeForms
    oldForms = osseConfig.getProperty(Vocabulary.Config.Form.visit).getValue();
    forms = oldForms.split(",");
    formsList = new HashSet<>(Arrays.asList(forms));

    if (osseConfig.getProperty(Vocabulary.Config.Form.archivedVisit) != null) {
      String archivedForms =
          osseConfig.getProperty(Vocabulary.Config.Form.archivedVisit).getValue();
      forms = archivedForms.split(",");
      Collections.addAll(formsList, forms);
    }
    oldForms = StringUtils.join(formsList, ",");
    osseConfig.setProperty(Vocabulary.Config.Form.archivedVisit, oldForms);

    // Read out old MDR Entity Form List
    JSONResource mdrEntityIsInFormJson = null;
    if (osseConfig.getProperty(Vocabulary.Config.mdrEntityIsInForm) != null) {
      mdrEntityIsInFormJson =
          osseConfig.getProperty(Vocabulary.Config.mdrEntityIsInForm).asJSONResource();
    }

    if (mdrEntityIsInFormJson != null) {
      MdrKeyUsageStore store = new MdrKeyUsageStore();
      store.inputAsJsonResource(mdrEntityIsInFormJson);
      fih.getMdrKeyUsageStore().merge(store);
    }

    return osseConfig;
  }

  /**
   * Saves the form-setup to the OSSEConfig DB.
   *
   * @param mdrJerseyClient the mdr jersey client
   * @param accessToken the access token
   */
  protected void saveConfig(Client mdrJerseyClient, AccessTokenDto accessToken) {
    JSONResource osseConfig = ((ApplicationBean) Utils.getAB()).getOsseConfig();
    osseConfig = storeOldForms(osseConfig);

    osseConfig.setProperty(Vocabulary.Config.formNames, fih.getFormNames());

    // Saving patforms list
    ArrayList<String> patforms = new ArrayList<>();
    for (String key : patientFormSelection.keySet()) {
      if (patientFormSelection.get(key)) {
        patforms.add(key);
      }
    }
    osseConfig.setProperty(Vocabulary.Config.PatientForm.list, StringUtils.join(patforms, ","));

    // store caseforms
    LinkedList<String> uniqueList = new LinkedList<>();
    Boolean firstDone = false;

    for (String caseForm : fih.getCaseForms()) {
      // set the first item to blah
      if (!firstDone) {
        firstDone = true;
        osseConfig.setProperty(Vocabulary.Config.Form.patientStandard, caseForm);
      }

      if (!uniqueList.contains(caseForm)) {
        uniqueList.add(caseForm);
      }
    }

    String formString = StringUtils.join(uniqueList, ",");
    osseConfig.setProperty(Vocabulary.Config.Form.patient, formString);

    // store episodeforms
    uniqueList = new LinkedList<>();
    firstDone = false;

    for (String anEpisodeForm : fih.getEpisodeForms()) {
      // set the first item to blah
      if (!firstDone) {
        firstDone = true;
        osseConfig.setProperty(Vocabulary.Config.Form.visitStandard, anEpisodeForm);
      }

      if (!uniqueList.contains(anEpisodeForm)) {
        uniqueList.add(anEpisodeForm);
      }
    }

    formString = StringUtils.join(uniqueList, ",");
    osseConfig.setProperty(Vocabulary.Config.Form.visit, formString);

    osseConfig.setProperty(
        Vocabulary.Config.mdrEntityIsInForm, fih.getMdrKeyUsageStore().asJsonResource());

    JSONResource mdrs = new JSONResource();
    for (String key : fih.getRecordHasMdrEntities().keySet()) {
      LinkedHashSet<String> moo = fih.getRecordHasMdrEntities().get(key);
      for (String me : moo) {
        mdrs.addProperty(key, me);
      }
    }
    osseConfig.setProperty(Vocabulary.Config.recordHasMdrEntities, mdrs);

    mdrs = new JSONResource();
    for (String key : fih.getWarnDoubleUsage().keySet()) {
      Set<MdrKeyUsageData> moo = fih.getWarnDoubleUsage().get(key);
      for (MdrKeyUsageData me : moo) {
        mdrs.addProperty(key, me.asJsonResource());
      }
    }
    osseConfig.setProperty(Vocabulary.Config.warnDoubleMDRKeyUsage, mdrs);

    osseConfig.setProperty(
        Vocabulary.Config.mdrEntityHasValidation, fih.getMdrEntityHasValidation());

    // XXX: Get rid of old XSD storage
    // TODO: Put these in an upgrader
    osseConfig.removeProperties(Vocabulary.Config.specificXSD);

    getSessionBean().getDatabase().saveConfig("osse", osseConfig);

    // TODO: Needs versionizing
    JSONResource osseXsdStorage = ((ApplicationBean) Utils.getAB()).getOsseXsdStorage();
    if (osseXsdStorage == null) {
      osseXsdStorage = new JSONResource();
    }

    try {
      String mdrUrl = osseConfig.getProperty(Vocabulary.Config.Mdr.REST).getValue();
      MdrClient mdrClient = new MdrClient(mdrUrl, mdrJerseyClient);
      String specificXsd = fih.makeV2Xsd(mdrClient, accessToken);
      osseXsdStorage.setProperty(Vocabulary.Config.specificXSD, specificXsd);
    } catch (Exception e) {
      Utils.getLogger().warn("Unable to create XSD for the imported forms.", e);
    }
    getSessionBean().getDatabase().saveConfig("osse.xsd.storage", osseXsdStorage);

    Utils.getAB().init();
  }

  /** provides the XSD file for downloading. */
  public void downloadXsd() {
    String xsd =
        ((ApplicationBean) Utils.getAB())
            .getOsseXsdStorage()
            .getProperty(Vocabulary.Config.specificXSD)
            .getValue();

    if (xsd != null) {
      try {
        HttpServletResponse response =
            (HttpServletResponse)
                FacesContext.getCurrentInstance().getExternalContext().getResponse();

        response.setContentType("text/xml");
        response.setHeader("Content-Disposition", "attachment;filename=import.xsd");
        response.getOutputStream().write(xsd.getBytes());
        response.getOutputStream().flush();
        response.getOutputStream().close();
        FacesContext.getCurrentInstance().responseComplete();
      } catch (IOException e) {
        Utils.getLogger().error("Error while setting up response to download XSD", e);
      }
    }
  }

  /** Provides the CSV file for downloading. */
  public void downloadCsv() {
    String csv;

    byte[] encoded = new byte[0];
    try {
      encoded =
          Files.readAllBytes(Paths.get(Utils.getRealPath("/schemata/" + "data_element_list.csv")));
    } catch (IOException e) {
      Utils.getLogger()
          .error(
              "Unable to read from:" + Utils.getRealPath("/schemata/" + "data_element_list.csv"),
              e);
    }
    csv = new String(encoded, StandardCharsets.UTF_8);

    if (csv != null) {
      try {
        HttpServletResponse response =
            (HttpServletResponse)
                FacesContext.getCurrentInstance().getExternalContext().getResponse();

        response.setContentType("text/csv");
        response.setHeader("Content-Disposition", "attachment;filename=data_element_list.csv");
        response.getOutputStream().write(csv.getBytes());
        response.getOutputStream().flush();
        response.getOutputStream().close();
        FacesContext.getCurrentInstance().responseComplete();
      } catch (IOException e) {
        Utils.getLogger().error("Error while setting up response to download CSV", e);
      }
    }
  }

  /**
   * Gets the episode form ids.
   *
   * @return a comma-seperated string of episode form IDs (used in javascript)
   */
  public String getItemsEpisode() {
    return itemsEpisode;
  }

  /**
   * Sets the episode form ids.
   *
   * @param itemsEpisode a comma-seperated string of episode form IDs (used in javascript)
   */
  public void setItemsEpisode(String itemsEpisode) {
    this.itemsEpisode = itemsEpisode;
  }

  /**
   * Gets the episode form elements.
   *
   * @return a list of episode form elements
   */
  public List<HashMap<String, String>> getElementsEpisode() {
    return elementsEpisode;
  }

  /**
   * Sets the episode form elements.
   *
   * @param elementsEpisode a list of episode form elements
   */
  public void setElementsEpisode(List<HashMap<String, String>> elementsEpisode) {
    this.elementsEpisode = elementsEpisode;
  }

  /**
   * A uniqued comma-seperated string of unique case form IDs (used in javascript).
   *
   * @return the items temp
   */
  public String getItemsTemp() {
    return itemsTemp;
  }

  /**
   * Sets the items temp.
   *
   * @param itemsTemp the new items temp
   */
  public void setItemsTemp(String itemsTemp) {
    this.itemsTemp = itemsTemp;
  }

  /**
   * A uniqued comma-seperated string of unique episode form IDs (used in javascript).
   *
   * @return the items episode temp
   */
  public String getItemsEpisodeTemp() {
    return itemsEpisodeTemp;
  }

  /**
   * Sets the items episode temp.
   *
   * @param itemsEpisodeTemp the new items episode temp
   */
  public void setItemsEpisodeTemp(String itemsEpisodeTemp) {
    this.itemsEpisodeTemp = itemsEpisodeTemp;
  }

  /**
   * Gets the case form elements.
   *
   * @return a list of case form elements
   */
  public List<HashMap<String, String>> getElements() {
    return elements;
  }

  /**
   * Sets case form elements.
   *
   * @param elements a list of case form elements
   */
  public void setElements(List<HashMap<String, String>> elements) {
    this.elements = elements;
  }

  /**
   * Gets the case form IDs.
   *
   * @return a comma-seperated string of case form IDs (used in javascript)
   */
  public String getItems() {
    return items;
  }

  /**
   * Sets the case form IDs.
   *
   * @param items a comma-seperated string of case form IDs (used in javascript)
   */
  public void setItems(String items) {
    this.items = items;
  }

  /**
   * Gets all available forms.
   *
   * @return List of all forms available in the form repository
   */
  public List<HashMap<String, String>> getAllForms() {
    if (allForms == null || allForms.isEmpty()) {
      init();
    }

    return allForms;
  }

  /**
   * Gets the patient form selection.
   *
   * @return the patient form selection
   */
  public HashMap<String, Boolean> getPatientFormSelection() {
    return patientFormSelection;
  }

  /**
   * Sets the patient form selection.
   *
   * @param patientFormSelection the patient form selection
   */
  public void setPatientFormSelection(HashMap<String, Boolean> patientFormSelection) {
    this.patientFormSelection = patientFormSelection;
  }

  /**
   * Returns the translated form name.
   *
   * @param form The form values as a HashMap.
   * @param locale ISO... code of the language to fetch.
   * @return The translated form name (or the default name if no translation available).
   */
  public String getFormTranslatedName(HashMap<String, String> form, String locale) {
    return getFormTranslatedProperty(form, locale, "name");
  }

  /**
   * Returns the translated form description.
   *
   * @param form The form values as a HashMap.
   * @param locale ISO... code of the language to fetch.
   * @return The translated form description (or the default description if no translation
   *     available).
   */
  public String getFormTranslatedDescription(HashMap<String, String> form, String locale) {
    return getFormTranslatedProperty(form, locale, "description");
  }

  /**
   * Returns the translation of a form property, if available.
   *
   * @param form The form values as a HashMap.
   * @param locale ISO... code of the language to fetch.
   * @param property The name of the property to fetch.
   * @return The translated property or, if not available, the property itself.
   */
  private String getFormTranslatedProperty(
      HashMap<String, String> form, String locale, String property) {
    String exactResult = form.get(locale + "_" + property);
    return (exactResult != null) ? exactResult : form.get(property);
  }
}
