/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.LocationContact;
import de.samply.edc.osse.model.Role;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/** View scoped bean for location management. */
@ManagedBean
@ViewScoped
public class LocationBean extends AbstractViewBean {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -92673420997266639L;

  /** The locations. */
  private ArrayList<HashMap<String, Object>> locations;

  /**
   * Postconstruct init.
   *
   * @see de.samply.edc.control.AbstractViewBean#init()
   */
  @Override
  @PostConstruct
  public void init() {
    locations = null;
    super.init();
  }

  /** Loads all locations including related data (eg. contact information). */
  private void loadLocations() {
    locations = new ArrayList<>();

    ResourceQuery locationQuery = new ResourceQuery(OSSEVocabulary.Type.Location);
    locationQuery.setFetchAdjacentResources(true);
    List<Resource> resultLocations = sessionBean.getDatabase().getResources(locationQuery);

    for (Resource locationResource : resultLocations) {
      Location location = new Location(getSessionBean().getDatabase(), locationResource);
      location.load(getConfig());

      dataObject = new HashMap<>();
      dataObject.put("name", location.getName());
      dataObject.put("locationResource", locationResource);
      dataObject.put("amountPatients", "N/A");
      dataObject.put("amountUsers", "N/A");

      dataObject.put("street", location.getStreet());
      dataObject.put("plz", location.getPlz());
      dataObject.put("city", location.getCity());
      dataObject.put("freetext", location.getFreetext());

      locations.add(dataObject);
    }
    dataObject = null;
  }

  /**
   * Saves location data.
   *
   * @return the string
   * @see de.samply.edc.control.AbstractViewBean#save()
   */
  @Override
  public String save() {
    final String name = (String) dataObject.get("name");
    final String street = (String) dataObject.get("street");
    final String plz = (String) dataObject.get("plz");
    final String city = (String) dataObject.get("city");
    final String freetext = (String) dataObject.get("freetext");

    Resource locationResource = (Resource) dataObject.get("locationResource");
    Location location = new Location(getSessionBean().getDatabase());

    String errorSummary = Utils.getResourceBundleString("summary_addusergroupfailed");
    String errorGeneral = Utils.getResourceBundleString("error_general_noformorvisit");

    // check if we've added a new user or changed our own name
    if (locationResource != null
        && !locationResource
            .getProperty(OSSEVocabulary.Location.Name)
            .getValue()
            .equalsIgnoreCase(name)) {
      // nope, we changed our own name, so check if that new name already
      // exists

      if (location.entityExistsByProperty(OSSEVocabulary.Location.Name, name) != null
          || location.entityExistsByProperty(OSSEVocabulary.Location.Name, name.toLowerCase())
              != null) {
        String errorMessage = Utils.getResourceBundleString("error_usergroupname_already_in_use");
        Utils.addContextMessage(errorSummary, errorMessage);
        return "";
      }
    } else if (locationResource == null) {
      if (location.entityExistsByProperty(OSSEVocabulary.Location.Name, name) != null
          || location.entityExistsByProperty(OSSEVocabulary.Location.Name, name.toLowerCase())
              != null) {
        String errorMessage = Utils.getResourceBundleString("error_usergroupname_already_in_use");
        Utils.addContextMessage(errorSummary, errorMessage);
        return "";
      }
    }

    LocationContact contact = null;
    getSessionBean().getDatabase().beginTransaction();

    String summary = Utils.getResourceBundleString("summary_addusergroupsuccess");
    String report = Utils.getResourceBundleString("success_addusergroup");

    String oldName = null;

    if (locationResource != null) {
      summary = Utils.getResourceBundleString("summary_editusergroupsuccess");
      report = Utils.getResourceBundleString("success_editusergroup");

      location.setResource(locationResource);

      // get contact resource
      location.load(getConfig());
      contact = location.getContact();
      oldName = location.getName();
      location.unload();
    }

    Boolean addingNewLocation = false;

    if (locationResource == null) {
      // a new location is added
      addingNewLocation = true;
    }

    location.setProperty(OSSEVocabulary.Location.Name, name);
    location.saveOrUpdate();

    if (locationResource == null) {
      locationResource = location.getResource();
    }

    if (locationResource == null) {
      getSessionBean().getDatabase().rollback();
      Utils.addContextMessage(errorSummary, errorGeneral);
      return "";
    }

    if (contact == null) {
      contact = new LocationContact(getSessionBean().getDatabase());
      contact.setLocation(location);
    }

    contact.setProperty(Vocabulary.Attributes.contactStreet, street);
    contact.setProperty(Vocabulary.Attributes.contactPLZ, plz);
    contact.setProperty(Vocabulary.Attributes.contactCity, city);
    contact.setProperty(Vocabulary.Attributes.contactFreetext, freetext);
    contact.saveOrUpdate();

    if (addingNewLocation) {
      // Now add a local admin!
      Role role = new Role(getSessionBean().getDatabase());
      role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.LOCAL_ADMINISTRATOR);
      role.setProperty(OSSEOntology.Role.IsUserRole, false);
      role.setProperty(OSSEVocabulary.Role.Location, location.getResource());
      role.setProperty(OSSEVocabulary.Role.Name, name + " Admin");
      role.saveOrUpdate();
    } else {
      if (oldName == null) {
        // Location had no old name?
        getSessionBean().getDatabase().rollback();
        Utils.addContextMessage(errorSummary, errorGeneral);
        Utils.getLogger().error("Location had no old name.");
        return "";
      }
      // RENAME the local admin role
      // In case of a name collision, we add a number at the end
      Role role = new Role(getSessionBean().getDatabase());

      ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Role);
      query.add(
          Criteria.Equal(OSSEVocabulary.Type.Role, OSSEVocabulary.Role.Location, locationResource));
      query.add(
          Criteria.Equal(
              OSSEVocabulary.Type.Role, OSSEOntology.Role.RoleType, "LOCAL_ADMINISTRATOR"));
      query.add(
          Criteria.Equal(OSSEVocabulary.Type.Role, OSSEVocabulary.Role.Name, oldName + " Admin"));

      ArrayList<Resource> found = getSessionBean().getDatabase().getResources(query);

      if (found.size() > 0) {
        role.setResource(found.get(0));
        role.setProperty(OSSEVocabulary.Role.Name, name + " Admin");
        role.saveOrUpdate();
      } else {
        // Location had no old name?
        // TODO: Then do create a new one
        getSessionBean().getDatabase().rollback();
        Utils.addContextMessage(errorSummary, errorGeneral);
        Utils.getLogger()
            .error("Did not find any old local-admin role with the name " + oldName + " Admin.");
        return "";
      }
    }
    getSessionBean().getDatabase().commit();

    Utils.addContextMessage(summary, report);
    goLocationList();
    return null;
  }

  /**
   * JSF Button: edit location.
   *
   * @param dataObject the data object
   * @return JSF outcome
   */
  public String editLocation(Object dataObject) {
    getSessionBean().setTempObject("dataObject", dataObject);
    Utils.goAdminForm("location_edit");
    return "editLocation";
  }

  /**
   * JSF Button: add location.
   *
   * @return JSF outcome
   */
  public String goAddLocation() {
    getSessionBean().clearTempObject("dataObject");
    Utils.goAdminForm("location_edit");
    return "";
  }

  /**
   * JSF Button: delete location.
   *
   * @param toDeleteLocation the new to delete location
   */
  public void setToDeleteLocation(Object toDeleteLocation) {
    getSessionBean().setTempObject("dataObject", toDeleteLocation);
  }

  /**
   * Delete location (not yet implemented). TODO: Implementation to follow
   *
   * @return the string
   */
  public String deleteLocation() {
    return "";
  }

  /** JSF Button: goLocationList. */
  public void goLocationList() {
    Utils.goAdminForm("locationlist");
  }

  /**
   * Gets the locations.
   *
   * @return The list of locations.
   */
  public ArrayList<HashMap<String, Object>> getLocations() {
    if (locations == null) {
      loadLocations();
    }
    return locations;
  }
}
