/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.exceptions.FormException;
import de.samply.edc.model.ContextMenuEntry;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.model.Case;
import de.samply.edc.osse.model.CaseForm;
import de.samply.edc.osse.model.Episode;
import de.samply.edc.osse.model.EpisodeForm;
import de.samply.edc.osse.model.Form;
import de.samply.edc.osse.model.FormComment;
import de.samply.edc.osse.model.FormName;
import de.samply.edc.osse.model.MyMap;
import de.samply.edc.osse.model.Patient;
import de.samply.edc.osse.model.User;
import de.samply.edc.osse.utils.FormUtils;
import de.samply.edc.utils.Utils;
import de.samply.form.dto.FormDetails;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;
import java.io.IOException;
import java.lang.reflect.Array;
import java.nio.charset.Charset;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;

// import org.primefaces.model.chart.CartesianChartModel;
// import org.primefaces.model.chart.LineChartSeries;

/** the viewscoped bean formviewer, managing forms. */
@ManagedBean(name = "formviewer")
@ViewScoped
public class Formviewer extends AbstractViewBean {

  /** Is the current form an episode form?. */
  private Boolean isEpisodeForm = false;

  /** The form entries/data. */
  private MyMap<String, Object> entries;

  /** Entries that cannot be mapped to the form definition. */
  private ConcurrentHashMap<String, Object> notMappedEntries;

  /** Whether there is a form upgrade in progress (after mapping, but before saving). */
  private boolean upgradeInProgress = false;

  /** The form we're looking at. */
  private Form form = null;

  /** The visit we're in. */
  private Episode episode = null;

  /** The case we're in. */
  private Case currentCase = null;

  /** If all forms are validated it counts as if the visit was validated. */
  private Boolean episodeIsValidated = false;

  /** If all forms are reported it counts as if the visit was reported. */
  private Boolean episodeIsReported = false;

  /* Linear model used to display form data as a chart. */
  // private CartesianChartModel linearModel;

  /** defines the new status on a status change. */
  private String newStatus;

  /** a selectitem list of accessible new status. */
  private List<SelectItem> possibleNewStatus;

  /** A list of comments. */
  private TreeSet<FormComment> formComments;

  /**
   * Stores information about whether a refresh of the case and episode info was already done in
   * this request.
   */
  private boolean reinitCaseAndEpisodeDone = false;

  /**
   * Postconstruct init. If the current form has a name, try to load its values.
   *
   * @see AbstractViewBean#init()
   */
  @Override
  public void init() {
    // If we got an episode selected, we are trying to call an episode form
    isEpisodeForm = (((SessionBean) getSessionBean()).getCurrentEpisode() != null);

    super.init();

    // And load our form
    if (getSessionBean().getCurrentFormName() != null) {
      loadValues();
    }
  }

  /** Method to init the form and load the data. */
  private void loadValues() {
    // no patient selected, so this cannot be a medical form, bail out
    if (((SessionBean) getSessionBean()).getCurrentPatient() == null) {
      return;
    }

    // no medical form, bail out
    if (!((SessionBean) getSessionBean()).isCurrentFormMedical()) {
      return;
    }

    // Init the form in case we didn't do it yet
    if (form == null) {
      initializeForm(false);

      if (form == null) {
        // init form failed or no medical form is displayed, bail out
        return;
      }

      // prepare comments
      formComments = new TreeSet<>();
      if (form.hasProperty(Vocabulary.Form.formComments)) {
        JSONResource commentsResource =
            (JSONResource) form.getProperty(Vocabulary.Form.formComments);

        List<Value> allComments = commentsResource.getProperties(Vocabulary.Form.formComment);
        for (Value comment : allComments) {
          FormComment fc = new FormComment((JSONResource) comment);
          formComments.add(fc);
        }
      }

      if (isUpgradeRequested()) {
        upgradeFormData();
      }

      entries = new MyMap<>(form);
      if (episode != null) {
        episodeIsValidated = episode.allFormsAreValidated();
        episodeIsReported = episode.allFormsAreReported();
      } else {
        episodeIsValidated = false;
        episodeIsReported = false;
      }

      newStatus = form.getFormState();
      preparePossibleStatusList();
    }
  }

  public Boolean getIsEpisodeForm() {
    return isEpisodeForm;
  }

  /** Prepare possible status list. */
  private void preparePossibleStatusList() {
    possibleNewStatus = new ArrayList<>();
    SelectItem item = null;

    if (form == null || form.getFormState().equalsIgnoreCase("open")) {
      item = new SelectItem();
      item.setLabel("open");
      item.setValue("open");

      possibleNewStatus.add(item);

      item = new SelectItem();
      item.setLabel("reported");
      item.setValue("reported");

      possibleNewStatus.add(item);

    } else if (form.getFormState().equalsIgnoreCase("reported")) {
      item = new SelectItem();
      item.setLabel("reported");
      item.setValue("reported");

      possibleNewStatus.add(item);

      item = new SelectItem();
      item.setLabel("open");
      item.setValue("open");

      possibleNewStatus.add(item);

      item = new SelectItem();
      item.setLabel("validated");
      item.setValue("validated");

      possibleNewStatus.add(item);
    } else if (form.getFormState().equalsIgnoreCase("validated")) {
      item = new SelectItem();
      item.setLabel("validated");
      item.setValue("validated");

      possibleNewStatus.add(item);

      item = new SelectItem();
      item.setLabel("open");
      item.setValue("open");

      possibleNewStatus.add(item);
    }
  }

  /*
   * Gets the previous episode based on episode`s name.
   *
   *  @return the last Episode
   */
  private Episode getTheLastEpisodeByName() {

    String errorNoData = Utils.getResourceBundleString("error_formimportnodata");
    String summary = Utils.getResourceBundleString("summary_importfailed");

    // Get the previous episode (based on the name), we use a treemap for this
    TreeMap<Long, Episode> myEpisodes = new TreeMap<>(Collections.reverseOrder());

    // reload the current case and episode data, in case meanwhile someone else changed values (only
    // if not already done)
    // what if it's a save request? should it be reloaded then? method save() reloads form anyway...
    if (!reinitCaseAndEpisodeDone && !((SessionBean) getSessionBean()).reinitCaseAndEpisode()) {
      Utils.getLogger().error("FV.importLastYearData: sb.reinitCaseAndEpisode returned false.");
      return null;
    } else {
      // refresh done, remember that
      reinitCaseAndEpisodeDone = true;
    }

    currentCase = ((SessionBean) getSessionBean()).getCurrentCase();

    // fill the tree
    for (Entity theEpisode : currentCase.getChildren(OSSEVocabulary.Type.Episode, getConfig())) {

      Date theDate = Episode.getDateByPattern(((Episode) theEpisode).getName(), null);
      if (theDate != null) {
        Long compareKey = theDate.getTime();
        myEpisodes.put(compareKey, (Episode) theEpisode);
      }
    }

    Entry<Long, Episode> theLast = null;
    Date myDate =
        Episode.getDateByPattern(
            ((SessionBean) getSessionBean()).getCurrentEpisode().getName(), null);
    if (myDate != null) {
      theLast = myEpisodes.higherEntry(myDate.getTime());
    }

    Episode theLastEpisode = null;

    // If there are no data to be imported go away
    if (theLast == null) {
      return null;
    } else {
      return theLastEpisode = theLast.getValue();
    }
  }

  /**
   * Imports the data of a/all previous episode's form(s) (historically called last year) that
   * is/are not in a "reported"- or "validated"-State.
   *
   * <p>For now we say that data may not be imported into forms that are not "open"
   *
   * @param allOpenForms if true, all forms (if false,just the current form ) of the current episode
   *     will be overwritten.
   * @return JSF outcome
   */
  public String importLastYearData(Boolean allOpenForms) {

    Boolean imported = false;
    String summaryFailed = Utils.getResourceBundleString("summary_importfailed");
    String summarySucceeded = Utils.getResourceBundleString("summary_importsuccess");
    String errorNotImported = Utils.getResourceBundleString("error_noimport");
    String successImport = Utils.getResourceBundleString("success_import");
    String errorNotOpen = Utils.getResourceBundleString("error_formnotopen");
    String errorNoData = Utils.getResourceBundleString("error_formimportnodata");
    String errorNoMatchingData =
        Utils.getResourceBundleString(
            "de.samply.edc.osse.messages.osse_messages", "error_formImportNoMatchingData");
    String infoPartialData =
        Utils.getResourceBundleString(
            "de.samply.edc.osse.messages.osse_messages", "info_formImportPartialData");

    if (episode == null) {
      throw new Error("formviewer.importLastYearData: " + "Cannot proceed without an episode");
    }

    // 1) Get the previous episode (based on it's name)
    Episode theLastEpisode = getTheLastEpisodeByName();
    if (theLastEpisode == null) {
      Utils.addContextMessage(summaryFailed, errorNoData);
      return "";
    }

    // 2) Get the previous form
    theLastEpisode.reloadResource();
    theLastEpisode.load(getConfig());
    theLastEpisode.loadEpisodeForms(getConfig());

    // just the data of the current Form should to be imported
    if (!allOpenForms) {
      if (form == null) {
        throw new Error("formviewer.importLastYearData: Cannot proceed without a form");
      }

      // if the current Form isn't in "open"-State inform the user; return nothing
      if (!form.isOpen()) {
        Utils.addContextMessage(summaryFailed, errorNotOpen);
        return "";
      }

      Form theLastForm = theLastEpisode.getForm(getSessionBean().getCurrentFormName(), getConfig());
      if (theLastForm == null) {
        theLastForm =
            findLowerVersionForm(
                getSessionBean().getCurrentFormName(), false, null, theLastEpisode);
      }

      // if the last Form doesn't exist inform the user; return nothing
      if (theLastForm == null) {
        Utils.addContextMessage(summaryFailed, errorNoData);
        return "";
      }

      // overwrite all data values of the current Form
      Utils.getLogger().debug("properties b4" + form.getProperties().toString());
      //            form.importPreviousFormData(theLastForm);
      String status = form.importPreviousFormData2(theLastForm);
      Utils.getLogger().debug("properties after" + form.getProperties().toString());

      // inform the user about the import from previous form according to status received

      if (status.equals("matched_none")) {
        // if no matched elements from previous version
        Utils.addContextMessage(summaryFailed, errorNoMatchingData);
        return "";
      }

      if (status.equals("matched_fully")) {
        // if previous and current form versions matched
        Utils.addContextMessage(summarySucceeded, successImport);
      }

      if (status.equals("matched_partially")) {
        // if some elements of the previous form match with current form
        Utils.addContextMessage(summarySucceeded, infoPartialData);
      }

      // inform the user that the import was successful
      //           Utils.addContextMessage(summary_succeeded, successImport);

      if ((status.equals("matched_fully")) || (status.equals("matched_partially"))) {
        // point the entries to the newly filled form data
        entries = new MyMap<>(form);

        Resource statusRes =
            Utils.findResourceByProperty(OSSEVocabulary.Type.Status, OSSEVocabulary.ID, "1");
        form.setProperty(OSSEVocabulary.EpisodeForm.Status, statusRes);

        getSessionBean()
            .addLog(
                "imported last version for form "
                    + form.getFormID()
                    + " (visit "
                    + episode.getName()
                    + ") from "
                    + theLastForm.getFormID()
                    + " (visit "
                    + theLastEpisode.getName()
                    + ") of patient "
                    + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

        save(true);
      }
    }

    if (allOpenForms) {
      // current Form's name, needed to redireckt
      String currentForm = getSessionBean().getCurrentFormName();

      // load current episode's Forms
      episode.loadEpisodeForms(getConfig());

      // get Forms of the last episode
      List<Entity> lastEpisodeForms = theLastEpisode.getForms(getConfig());

      // if there are no Forms of the last Episode inform the user; return nothing
      if (lastEpisodeForms == null || lastEpisodeForms.isEmpty()) {
        Utils.addContextMessage(summaryFailed, errorNoData);
        return "";
      }

      // import the Forms of the last Episode that are in "open"-State
      //            episode.importAllPreviousEpisodeForms(lastEpisodeForms, getConfig());
      Boolean isSuccess = episode.importAllPreviousEpisodeForms2(lastEpisodeForms, getConfig());

      if (!isSuccess) {
        Utils.addContextMessage(
            summaryFailed, "No matching elements found with previous episode forms");
      } else {

        // inform the user that the import was successful
        Utils.addContextMessage(summarySucceeded, successImport);
        getSessionBean()
            .addLog(
                "imported last versions of open forms of the "
                    + " (visit "
                    + episode.getName()
                    + ") from last "
                    + " (visit "
                    + theLastEpisode.getName()
                    + ") of patient "
                    + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

        episode.load(getConfig());
        episode.loadEpisodeForms(getConfig());

        // redirect to the current form
        // form = episode.getForm(currentForm, getConfig());
        redirectToForm(currentForm, true, false);
        entries = new MyMap<>(form);
      }
    }
    return null;
  }

  /**
   * Change to new status.
   *
   * @return the string
   */
  public String changeToNewStatus() {
    if (newStatus == null || "".equals(newStatus)) {
      return "";
    }

    //        if (form.getFormState().equalsIgnoreCase(newStatus)) {
    //            if (entries.get("statusChangeComment") != null ||
    // !entries.get("statusChangeComment").equals("")) {
    //                Utils.addContextMessage(Utils.getResourceBundleString("osse_messages",
    // "no_status_change_subject"),
    //                        Utils.getResourceBundleString("osse_messages",
    // "no_change_no_comment"));
    //            }
    //            return "";
    //        }
    return changeStatus(newStatus);
  }

  /**
   * Action method: tries to change the status of a form.
   *
   * @param newStatus the new status
   * @return JSF outcome string
   */
  public String changeStatus(String newStatus) {
    if (form == null) {
      Utils.addContextMessage(
          Utils.getResourceBundleString("summary_reportfailed"),
          Utils.getResourceBundleString("error_general_noformorvisit"));
      throw new Error(
          "formviewer.reportform: Cancelling reportform since it's neither form nor visit");
    }

    addFormComment();

    String oldStatus = form.getFormState();

    if (newStatus.equalsIgnoreCase(oldStatus)) {
      return oldStatus;
    }

    try {
      form.setFormState(newStatus);
    } catch (FormException | DatabaseException e) {
      loadValues();
      Utils.getLogger().warn("Unable to set form state.", e);
      Utils.addContextMessage(
          Utils.getResourceBundleString("formstatus_unchangedSummary"),
          Utils.getResourceBundleString("formstatus_unchangedDetail"));

      return form.getFormState();
    }

    Object[] params = {oldStatus, newStatus};
    String text = Utils.getResourceBundleString("general_statuschange_text", params);

    Utils.addContextMessage(Utils.getResourceBundleString("general_statuschange_successful"), text);

    redirectToForm(sessionBean.getCurrentFormName(), isEpisodeForm, false);

    return newStatus;
  }

  private void addFormComment() {
    if (entries.get("statusChangeComment") != null
        && !"".equals(entries.get("statusChangeComment"))) {
      String comment = (String) entries.get("statusChangeComment");

      FormComment fc =
          new FormComment(
              comment,
              ((SessionBean) getSessionBean()).getCurrentUser().getResourceUri(),
              new Date().getTime());

      JSONResource commentsResource;
      if (form.hasProperty(Vocabulary.Form.formComments)) {
        commentsResource = (JSONResource) form.getProperty(Vocabulary.Form.formComments);
      } else {
        commentsResource = new JSONResource();
      }

      commentsResource.addProperty(Vocabulary.Form.formComment, fc.toJsonResource());

      form.setProperty(Vocabulary.Form.formComments, commentsResource);
      form.getResource().setProperty(Vocabulary.Form.formComments, commentsResource);
      form.getDatabase().save(form.getResource());
      // Add new comment to cache, otherwise a page reload would be necessary to make it visible.
      this.formComments.add(fc);
    }
  }

  /**
   * Saves the form values to the DB.
   *
   * @param wasImported the was imported
   * @return the string
   * @see AbstractViewBean#save(Boolean)
   */
  @Override
  public String save(Boolean wasImported) {
    String summaryFailed = Utils.getResourceBundleString("summary_savefailed");

    if (form == null) {
      Utils.addContextMessage(
          summaryFailed, Utils.getResourceBundleString("error_general_noformorvisit"));
      return null;
    }

    // forms that are not open may not be saved
    // TODO: Future workflow generic definitions may need to change this!
    if (!form.isOpen()) {
      if (form.isReported()) {
        Utils.addContextMessage(summaryFailed, Utils.getResourceBundleString("formstate_reported"));
      } else {
        Utils.addContextMessage(summaryFailed, Utils.getResourceBundleString("formstate_signed"));
      }

      return null;
    }

    // Clean empty entries from repeatable data elements and records
    for (Entry<String, Object> formProps : form.getProperties().entrySet()) {
      if (formProps.getKey().startsWith("urn") && formProps.getValue() instanceof DatatableRows) {
        DatatableRows rows = (DatatableRows) formProps.getValue();
        List<DatatableRow> cleanedRows = new ArrayList<>(rows.getRows().size());
        for (DatatableRow rowToCheck : rows.getRows()) {
          // Keep all rows with at least one non-empty value
          if (rowToCheck.getColumns().values().stream()
              .anyMatch(
                  v ->
                      v != null
                          && (v instanceof String && !v.equals("")
                              || (v.getClass().isArray() && Array.getLength(v) > 0)))) {
            cleanedRows.add(rowToCheck);
          }
        }
        rows.setRows(cleanedRows);
      }
    }

    form.getDatabase().beginTransaction();
    form.saveOrUpdate();
    form.getDatabase().commit();

    // update the last Changed values (who and when)
    setLastChanged();

    /**
     * after "import from last visit" the form version might be conflicting, must be set to the
     * version of the currently displayed form.
     */
    Integer formVersionData =
        (Integer) Utils.getFormVersionData(getSessionBean().getCurrentFormName()).get("version");
    if (!form.getVersion().equals(formVersionData.toString())) {
      Utils.getLogger()
          .debug(
              "Formversion does not match: It was "
                  + form.getVersion()
                  + " it is supposed to be "
                  + formVersionData
                  + " setting it now!");
      form.setVersion(formVersionData);
    }

    // reload the form data, in case there were transformations or
    // conversions done
    form.load(getConfig());

    // update the form in the parent
    if (episode == null) {
      currentCase.replaceChild(OSSEVocabulary.Type.CaseForm, form, getConfig());
    } else {
      episode.replaceChild(OSSEVocabulary.Type.EpisodeForm, form, getConfig());
    }

    // say Yay!
    Utils.displayContextMessage("summary_savesuccess", "success_save");

    // logbook
    getSessionBean()
        .addLog(
            "saved form "
                + form.getFormID()
                + " (visit "
                + episode
                + ") of patient "
                + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

    // reload the formslist for navigation, in case status etc changed due
    // to the saveing
    ((SessionBean) getSessionBean()).loadNavigationPatientForms();

    // clear linearModel so it gets redrawn
    // TODO: This is not yet used, it's for future support of forms that may
    // have graphical charts based on form data (e.g. blood pressure chart
    // or similar)
    // linearModel = null;

    return null;
  }

  /**
   * Saves an upgraded form. The difference to {@link #save(Boolean)} is that a redirect is made to
   * the new form version - this is needed to upgrade the URL to the new form name.
   */
  public String saveUpgradedForm() {
    this.save(false);

    String indexEpisode = "index.xhtml";
    if (isEpisodeForm) {
      indexEpisode = "episode.xhtml";
    }

    String page = indexEpisode + "?form=" + form.getFormID();

    try {
      Utils.redirectToPage(page);
    } catch (IOException e) {
      Utils.getLogger().warn("Unable to redirect to form.", e);
    }
    return null;
  }

  /** Sets when and by who a form was changed. Also sets this in the patient-context. */
  private void setLastChanged() {
    form.storeLastChange();

    // TODO: This has a backend permission rights problem, if the user has
    // no right to write patient-data, and only may
    // write form data of a patient. So disabled this for now
    // getSessionBean().getCurrentPatient().storeLastChange();
  }

  /**
   * Tries to find a lower version of a form within the case (or episode).
   *
   * @param currentFormName The form we want to find
   * @param isCaseForm Is it a case form or an episode form?
   * @param myCase the my case
   * @param myEpisode the my episode
   * @return the form
   */
  private Form findLowerVersionForm(
      String currentFormName, Boolean isCaseForm, Case myCase, Episode myEpisode) {
    HashMap<String, Object> formVersionData = Utils.getFormVersionData(currentFormName);
    Form lowerVerForm;
    Integer formVersion = null;
    String formName = null;

    if (formVersionData != null
        && formVersionData.get("version") != null
        && formVersionData.get("name") != null) {
      formVersion = (Integer) formVersionData.get("version");
      formName = (String) formVersionData.get("name");
    }

    if (formVersion != null && formVersion > 1) {
      while (formVersion > 1) {
        formVersion--;
        String toFindForm = formName + "-" + formVersion;

        if (isCaseForm) {
          lowerVerForm = myCase.getForm(toFindForm, getConfig());
        } else {
          lowerVerForm = myEpisode.getForm(toFindForm, getConfig());
        }

        if (lowerVerForm != null) {
          return lowerVerForm;
        }
      }
    }

    return null;
  }

  /**
   * Find the latest version of the current form. The latest version is the one that is imported
   * into the registry.
   *
   * @return The latest version of the form or null if the form is no longer imported.
   */
  public Integer findLatestVersionOfCurrentForm() {
    FormName latestVersion = findNameOfLatestVersionOfCurrentForm();
    if (latestVersion != null) {
      return latestVersion.getVersion();
    } else {
      return null;
    }
  }

  /**
   * Find the full name of the latest version of the current form. The latest version is the one
   * that is imported into the registry.
   *
   * @return The name of latest version of the form or null if the form is no longer imported.
   */
  public FormName findNameOfLatestVersionOfCurrentForm() {
    Pattern formIdPattern = Pattern.compile("form_(\\d+)_ver-(\\d+)");
    String currentFormIdAndVersion = this.form.getFormID();
    Matcher formIdMatcher = formIdPattern.matcher(currentFormIdAndVersion);
    if (!formIdMatcher.find()) {
      return null;
    }
    int currentFormId = Integer.parseInt(formIdMatcher.group(1));
    int currentFormVersion = Integer.parseInt(formIdMatcher.group(2));
    Map<String, String> formList =
        isEpisodeForm
            ? ((ApplicationBean) Utils.getAB()).getVisitFormulars(getSessionBean().getLocale())
            : ((ApplicationBean) Utils.getAB()).getFormulars(getSessionBean().getLocale());
    for (String formId : formList.keySet()) {
      Matcher thisFormIdMatcher = formIdPattern.matcher(formId);
      if (thisFormIdMatcher.find()) {
        int thisFormId = Integer.parseInt(thisFormIdMatcher.group(1));
        int thisFormVersion = Integer.parseInt(thisFormIdMatcher.group(2));
        if (currentFormId == thisFormId) {
          // We found the same form
          return new FormName(thisFormId, thisFormVersion);
        }
      }
    }
    return null;
  }

  /**
   * Check whether the currently selected form instance is of the latest version of the form (i.e.
   * the one being imported into the registry).
   *
   * @return true|false
   */
  public boolean isFormLatestVersion() {
    return this.form == null
        || Integer.parseInt(this.form.getVersion()) == findLatestVersionOfCurrentForm();
  }

  /**
   * Initializes the necessary variables. Makes sure we got a case, an episode (if we handle an
   * episodeForm) and a form object. Without any of these, no operation can work.
   *
   * @param wasImported whether or not the form was imported
   */
  public void initializeForm(Boolean wasImported) {
    if (!wasImported) {
      // refresh case and visit (only if not already done)
      if (!reinitCaseAndEpisodeDone && !((SessionBean) getSessionBean()).reinitCaseAndEpisode()) {
        Utils.getLogger().error("FV.initForm: reinitCaseAndVisit returned with error, bailing out");
        episode = null;
        form = null;
        return;
      } else {
        // refresh done, remember that
        reinitCaseAndEpisodeDone = true;
      }
    }

    String currentFormName = getSessionBean().getCurrentFormName();
    if (currentFormName == null || currentFormName.equals("")) {
      return;
    }

    // Get our current case
    currentCase = ((SessionBean) getSessionBean()).getCurrentCase();
    episode = ((SessionBean) getSessionBean()).getCurrentEpisode();

    if (currentCase == null) {
      return;
    }

    Boolean mayWriteCurrentForm = ((SessionBean) Utils.getSB()).mayWriteCurrentForm();

    if (!isEpisodeForm || currentFormName.equals(Vocabulary.Development.testFormName)) {
      // caseform or our test form called "test-1", find it, and load it

      if (currentCase.hasChildren(OSSEVocabulary.Type.CaseForm, getConfig())) {
        form = currentCase.getForm(currentFormName, getConfig());

        if (form == null) {
          // We found no form. First check if we got an older version
          // of the form saved
          form = findLowerVersionForm(currentFormName, true, currentCase, episode);
          if (form != null && !isUpgradeRequested()) {
            // overload the current form setting
            redirectToForm(form.getFormID(), false, false);
          }
        }
      }

      if (form == null) {
        // the form is not yet in the data of the case, so we have to
        // create a new form (if we're allowed to do so)

        if (!mayWriteCurrentForm) {
          return;
        }

        form = new CaseForm(getSessionBean().getDatabase());
        form.setParent(currentCase);
        form.createForm(
            currentFormName, ((SessionBean) getSessionBean()).getFormVersion(), getConfig());
      } else {
        form.load(getConfig());
      }
    } else if (isEpisodeForm) {
      // episode form, find it and load it
      if (currentCase.hasChildren(OSSEVocabulary.Type.Episode, getConfig())) {
        form = episode.getForm(currentFormName, getConfig());

        if (form == null) {
          // We found no form. First check if we got an older version
          // of the form saved
          form = findLowerVersionForm(currentFormName, false, currentCase, episode);
          if (form != null && !isUpgradeRequested()) {
            // overload the current form setting
            redirectToForm(form.getFormID(), true, false);
          }
        }
      }

      if (form == null) {
        // this form yet does not exist, so we create a new one if we
        // are allowed to

        if (!mayWriteCurrentForm) {
          return;
        }

        form = new EpisodeForm(getSessionBean().getDatabase());
        form.setParent(episode);
        form.createForm(
            getSessionBean().getCurrentFormName(),
            ((SessionBean) getSessionBean()).getFormVersion(),
            getConfig());
      }
    }
  }

  /**
   * This redirects to a form in its older version, if it was saved as such. So if we got a form
   * "foobar-3", but we stored data in its version "foobar-1", we redirect the user to the
   * "foobar-1" form version instead
   *
   * @param newForm the new form we want the user to be redirected to
   */
  public void redirectToForm(String newForm) {
    redirectToForm(newForm, false, false);
  }

  /**
   * This redirects to a form in its older version, if it was saved as such. So if we got a form
   * "foobar-3", but we stored data in its version "foobar-1", we redirect the user to the
   * "foobar-1" form version instead
   *
   * @param newForm the new form we want the user to be redirected to
   * @param isEpisodeForm if the form is an episode form
   */
  public void redirectToForm(String newForm, Boolean isEpisodeForm, boolean isUpgrade) {
    String oldForm = getSessionBean().getCurrentFormName();
    getSessionBean().setCurrentFormName(newForm);

    String indexEpisode = "index.xhtml";
    if (isEpisodeForm) {
      indexEpisode = "episode.xhtml";
    }

    String page = indexEpisode + "?form=" + newForm;
    if (!newForm.equalsIgnoreCase(oldForm)) {
      if (isUpgrade) {
        page += "&upgradeFrom=" + oldForm;
      } else {
        page += "&oldform=" + oldForm;
      }
    }

    try {
      Utils.redirectToPage(page);
    } catch (IOException e) {
      Utils.getLogger().warn("Unable to redirect to form.", e);
    }
  }

  /**
   * Initiate upgrade of the current form to the latest version. The user is redirected to the
   * latest version of the form, filled out with the data elements form the currently stored
   * version.
   */
  public void upgradeForm() {
    FormName formNameToUpgradeTo = findNameOfLatestVersionOfCurrentForm();
    redirectToForm(formNameToUpgradeTo.toString(), this.isEpisodeForm, true);
  }

  /** Upgrade form data to the currently displayed form. */
  private void upgradeFormData() {
    FormName targetFormName = new FormName(getSessionBean().getCurrentFormName());
    Form targetForm;
    if (isEpisodeForm) {
      targetForm = new EpisodeForm(getSessionBean().getDatabase());
      targetForm.setParent(episode);
    } else {
      targetForm = new CaseForm(getSessionBean().getDatabase());
      targetForm.setParent(currentCase);
    }
    targetForm.createForm(
        targetFormName.toString(), ((SessionBean) getSessionBean()).getFormVersion(), getConfig());
    targetForm.importFrom(form, false);
    notMappedEntries = new ConcurrentHashMap<>(targetForm.getNotMappedProperties());
    // Convert representation of non-repeatable records so that they can be displayed more easily
    // Search for keys consisting of record and member id, separated by "/"
    Pattern recordPropPattern =
        Pattern.compile(
            "(urn__\\w*-\\d+__record__\\d+__\\d+)/(urn__\\w*-\\d+__dataelement__\\d+__\\d+)");
    LinkedList<String> keysToRemove = new LinkedList<>();
    for (Entry<String, Object> thisEntry : notMappedEntries.entrySet()) {
      Matcher recordPropMatcher = recordPropPattern.matcher(thisEntry.getKey());
      if (recordPropMatcher.find()) {
        String recordKey = recordPropMatcher.group(1);
        String memberKey = recordPropMatcher.group(2);
        // Create a DatatableRows entry when the record key is examined for the first time
        if (!notMappedEntries.containsKey(recordKey)) {
          DatatableRows rows = new DatatableRows();
          rows.add(new DatatableRow());
          notMappedEntries.put(recordKey, rows);
        }
        // Add the member dataelement to the DatatableRows object of the record it belongs to
        DatatableRows rows = (DatatableRows) notMappedEntries.get(recordKey);
        rows.getRows().get(0).getColumns().put(memberKey, thisEntry.getValue());
        // Remember to remove the old entry so that it does not create errors in the XHTML code
        keysToRemove.add(thisEntry.getKey());
      }
    }
    // Copy the mapped properties to the original object, otherwise a duplicate entry
    // would be created upon saving the form
    // First, remove the old data elements and records
    form.deleteFormMedicalData();
    // Then copy mapped data elements and records to the new form
    targetForm
        .getProperties()
        .entrySet()
        .forEach(
            e -> {
              if (e.getKey().startsWith("urn")) {
                form.setProperty(e.getKey(), e.getValue());
              }
            });
    // Not mapped non-repeatable records have to be converted so that they can be displayed
    // (the other alternative would be a far more complex XHTML page)
    Iterator<String> keyIt = notMappedEntries.keySet().iterator();
    while (keyIt.hasNext()) {
      String thisFormKey = keyIt.next();
      Matcher formKeyMatcher = FormUtils.getFormPropertyKeyPatternMatcher(thisFormKey);
      if (formKeyMatcher.find()) {
        if ("record".equals(formKeyMatcher.group(2))) {
          String namespace = formKeyMatcher.group(1);
          String recordId = formKeyMatcher.group(3);
          String recordVersion = formKeyMatcher.group(4);
          if (formKeyMatcher.group(6) != null) {
            // We found a key of a non-repeatable record
            String dataElementId = formKeyMatcher.group(8);
            String dataElementVersion = formKeyMatcher.group(9);
            String recordKey =
                FormUtils.constructMdrUrnRecord(namespace, recordId, recordVersion)
                    .replace(":", "__");
            if (!notMappedEntries.containsKey(recordKey)) {
              notMappedEntries.put(recordKey, new DatatableRows());
            }
            DatatableRows recordRows = (DatatableRows) notMappedEntries.get(recordKey);
            if (recordRows.getRows().size() == 0) {
              recordRows.add(new DatatableRow());
            }
            String dataElementKey =
                FormUtils.constructFormKey(namespace, dataElementId, dataElementVersion);
            recordRows
                .getRows()
                .get(0)
                .getColumns()
                .put(dataElementKey, notMappedEntries.get(thisFormKey));
            keyIt.remove();
          }
        }
      }
    }

    // Finally, update form version
    form.setProperty("version", Integer.parseInt(targetForm.getVersion()));
    form.setProperty("name", targetForm.getFormID());
    upgradeInProgress = true;
  }

  /** Check whether the current request initiates a form upgrade. */
  public boolean isUpgradeRequested() {
    HttpServletRequest request =
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    String upgradeParam = request.getParameter("upgradeFrom");
    return upgradeParam != null && !upgradeParam.equals("");
  }

  /**
   * Get whether there is currently a form upgrade in progress, i.e. the form has been mapped and
   * the user stil has to save the mapped form. This flag is needed to determine which save button
   * to show.
   */
  public boolean isUpgradeInProgress() {
    return upgradeInProgress;
  }

  /**
   * Faces method to load a form by given params. Prepared for sign form display quicklink, not yet
   * used in OSSE.
   *
   * @param patient PID of Patient
   * @param caseUri the case backend URI
   * @param episodeName Label of the visit
   * @param formName Name of the form
   * @return JSF outcome
   */
  public String showform(String patient, String caseUri, String episodeName, String formName) {
    Patient thePatient = new Patient(getSessionBean().getDatabase(), patient);
    thePatient.load(getConfig());
    thePatient.loadChildren(OSSEVocabulary.Type.Case, true, getConfig());

    Case theCase = new Case(getSessionBean().getDatabase(), caseUri);

    theCase.loadChildren(OSSEVocabulary.Type.Episode, false, getConfig());
    theCase.loadChildren(OSSEVocabulary.Type.CaseForm, false, getConfig());

    Episode theEpisode = null;
    Form theForm = null;

    if (episodeName == null || "".equals(episodeName)) {
      theForm = theCase.getForm(formName, getConfig());
    } else {
      theEpisode =
          (Episode) theCase.getChild(OSSEVocabulary.Type.Episode, episodeName, getConfig());

      if (theEpisode == null) {
        return "";
      }

      theEpisode.loadEpisodeFormsOnce(getConfig());
      theForm = theEpisode.getForm(formName, getConfig());
    }

    if (theForm == null) {
      return "";
    }

    ((SessionBean) getSessionBean()).setCurrentCase(theCase);
    ((SessionBean) getSessionBean()).setCurrentEpisode(theEpisode);
    ((SessionBean) getSessionBean()).setCurrentPatient(thePatient);
    getSessionBean().setCurrentFormName(formName);
    ((SessionBean) getSessionBean()).loadNavigationPatientForms();
    form = theForm;
    episode = theEpisode;

    loadValues();
    return "showform";
  }

  /**
   * Returns an Integer array from start year to the current year.
   *
   * @param start Integer start year
   * @return Array from start year to current year
   */
  public Integer[] selectItemYears(Integer start) {
    Calendar calendar = Calendar.getInstance();
    return Utils.createIntegerArray(start, calendar.get(Calendar.YEAR));
  }

  /**
   * Gets the last changed timestamp.
   *
   * @return the last changed timestamp
   */
  public String getLastChangedTimestamp() {
    if (form == null) {
      return "";
    }

    return form.getLastChangedDateAsString();
  }

  /**
   * Gets the LastChangedBy value (user real name).
   *
   * @return the last changed by
   */
  public String getLastChangedBy() {
    if (form == null || form.getLastChangedBy() == null) {
      return "null";
    }

    return ((User) form.getLastChangedBy()).getUserRealName();
  }

  /**
   * Is the visit validated?.
   *
   * @return boolean
   */
  @Deprecated
  public Boolean getVisitIsValidated() {
    return episodeIsValidated;
  }

  /**
   * Is the visit reported?.
   *
   * @return boolean
   */
  @Deprecated
  public Boolean getVisitIsReported() {
    return episodeIsReported;
  }

  /**
   * Is the episode validated?.
   *
   * @return boolean
   */
  public Boolean getEpisodeIsValidated() {
    return episodeIsValidated;
  }

  /**
   * Is the episode reported?.
   *
   * @return boolean
   */
  public Boolean getEpisodeIsReported() {
    return episodeIsReported;
  }

  /**
   * Gets the panel heading color, used in a css style class call.
   *
   * @return the panel heading color css class
   */
  @Deprecated
  public String getPanelHeadingColor() {
    if (form == null) {
      return "panel-default";
    }

    if (form.isOpen()) {
      return "panel-default";
    }

    // if (form.isReported() || form.isValidated())
    // return "panel-danger";

    return "panel-default";
  }

  /**
   * A goto method used by JSF action components.
   *
   * @param link the link
   * @return the string
   */
  public String goTo(String link) {
    if (link.equals("goNewVisitByButton")) {
      ((EpisodeBean) Utils.getBackingBean("episodeBean")).goAddEpisode();
    }

    if (link.equals("goEditVisit")) {
      ((EpisodeBean) Utils.getBackingBean("episodeBean")).goEditEpisode();
    }

    if (link.equals("deleteVisit")) {
      return ((EpisodeBean) Utils.getBackingBean("episodeBean")).deleteVisit();
    }

    if (link.equals("signWholeVisit")) {
      return signWholeVisit();
    }

    if (link.equals("reportWholeVisit")) {
      return reportWholeVisit();
    }

    if (link.equals("importLastYearData")) {
      return importLastYearData(false);
    }

    if (link.equals("importAllEpisodeForms")) {
      return importLastYearData(true);
    }

    return "";
  }

  /**
   * Method to add a row to a data table.
   *
   * @param name the name of the data table to add a row to
   */
  @Override
  public void addRow(String name) {
    DatatableRows stl = null;

    if (!entries.containsKey(name)) {
      stl = new DatatableRows();
    } else {
      stl = (DatatableRows) entries.get(name);
    }

    stl.add(new DatatableRow());
    entries.put(name, stl);
  }

  /**
   * Method to delete a row from a data table.
   *
   * @param name Name of the data table
   * @param weg Row to be removed
   */
  @Override
  public void deleteRow(String name, DatatableRow weg) {
    if (!entries.containsKey(name)) {
      return;
    }

    DatatableRows stl = (DatatableRows) entries.get(name);
    stl.remove(weg);

    entries.put(name, stl);
  }

  /**
   * Method to sort a datatable by row.
   *
   * @param datatable The datatable to sort
   * @param key the row key (mdr key)
   * @return the string
   */
  public String sortBy(String datatable, String key) {
    ((DatatableRows) entries.get(datatable)).sortyBy(key);
    return null;
  }

  /**
   * Makes a "linearModel" Chart Model (a 2-dimentional chart) out of a given Datatable and two of
   * it's column fields.
   *
   * @return the linear model
   */
  // public CartesianChartModel getLinearModel(String name, String xName,
  // String yName) {
  //
  // if (linearModel == null) {
  // linearModel = new CartesianChartModel();
  //
  // DatatableRows rows = (DatatableRows) entries.get(name);
  //
  // TreeMap<Object, Number> myRows = new TreeMap<Object, Number>();
  //
  // if (rows != null) {
  // LineChartSeries series1 = new LineChartSeries();
  // series1.setLabel(name);
  //
  // for (DatatableRow column : rows.getRows()) {
  // HashMap<String, Object> colentries = column.getColumns();
  //
  // Object xValue = colentries.get(xName);
  // Object yValue = colentries.get(yName);
  //
  // yValue = new Integer((String) yValue);
  //
  // myRows.put(xValue, (Number) yValue);
  // }
  //
  // for (Object myKey : myRows.keySet()) {
  // Number value = myRows.get(myKey);
  //
  // if (myKey instanceof Date) {
  // myKey = Utils.getDateString((Date) myKey, "dd.MM.yyyy");
  // } else
  // myKey = myKey.toString();
  //
  // series1.set(myKey, value);
  // }
  //
  // linearModel.addSeries(series1);
  // }
  // }
  //
  // return linearModel;
  // }

  /**
   * Gets the form state number.
   *
   * @return the form state number
   */
  public String getFormStateNumber() {
    if (form == null || episode == null) {
      return "1";
    }

    return form.getFormStateNumber();
  }

  /**
   * Gets the form state.
   *
   * @return the form state
   * @see AbstractViewBean#getFormState()
   */
  @Override
  public String getFormState() {
    if (form == null) {
      return "open";
    }

    return form.getFormState();
  }

  /**
   * Prepares the context menu of the current page
   *
   * <p>TODO: This is yet not finished, and needs rework for the future generic workflow setup.
   *
   * @see de.samply.edc.control.AbstractViewBean#makeContextMenu()
   */
  @Override
  public void makeContextMenu() {
    contextMenuList = new LinkedList<>();
    ContextMenuEntry ts = null;

    if (((SessionBean) getSessionBean()).hasCurrentPatient()) {

      if (((SessionBean) getSessionBean()).getMayCreateEpisodes()) {
        ts =
            new ContextMenuEntry(
                "goNewVisitByButton",
                "fa-plus",
                Utils.getResourceBundleString("link_newvisit"),
                false,
                null,
                null,
                null,
                null);
        contextMenuList.add(ts);
      }

      if (((SessionBean) getSessionBean()).hasCurrentEpisode()) {

        if (((SessionBean) getSessionBean()).mayWriteCurrentEpisode()) {
          ts =
              new ContextMenuEntry(
                  "goEditVisit",
                  "fa-edit",
                  Utils.getResourceBundleString("edit_visit_link"),
                  false,
                  null,
                  null,
                  null,
                  null);
          contextMenuList.add(ts);

          ts =
              new ContextMenuEntry(
                  "deleteVisit",
                  "fa-eraser",
                  Utils.getResourceBundleString("delete_visit_link"),
                  true,
                  Utils.getResourceBundleString("delete_visit_link"),
                  Utils.getResourceBundleString("delete_visit_text"),
                  Utils.getResourceBundleString("formbutton_delete_visit_yes"),
                  Utils.getResourceBundleString("formbutton_report_no"));
          contextMenuList.add(ts);
        }

        boolean isMedicalForm = ((SessionBean) getSessionBean()).isCurrentFormMedical();
        boolean mayWriteEpisodeForms =
            ((SessionBean) getSessionBean()).mayWriteCurrentEpisodeForm();
        boolean isCurrentFormInOpenState = getFormState().equals("open");
        boolean episodeHasAtleastOneOpenForm = !episodeIsReported && !episodeIsValidated;
        // is this necessary here? would just checking if an episode exists be enough instead of
        // reloading it?
        Boolean lastEpisodeExists = getTheLastEpisodeByName() != null;

        boolean isPatientUser = ((SessionBean) getSessionBean()).isPatientUser();
        if (lastEpisodeExists && !isPatientUser) {
          if (isMedicalForm && mayWriteEpisodeForms && isEpisodeForm && isCurrentFormInOpenState) {
            ts =
                new ContextMenuEntry(
                    "importLastYearData",
                    "fa-upload",
                    Utils.getResourceBundleString("formbutton_import"),
                    true,
                    Utils.getResourceBundleString("formbutton_import_title"),
                    Utils.getResourceBundleString("formbutton_import_areyousure"),
                    Utils.getResourceBundleString("formbutton_import_yes"),
                    Utils.getResourceBundleString("formbutton_import_no"));
            contextMenuList.add(ts);
          }

          if (mayWriteEpisodeForms && isEpisodeForm && episodeHasAtleastOneOpenForm) {
            ts =
                new ContextMenuEntry(
                    "importAllEpisodeForms",
                    "fa-upload",
                    Utils.getResourceBundleString("formbutton_import_all"),
                    true,
                    Utils.getResourceBundleString("formbutton_import_title_1"),
                    Utils.getResourceBundleString("formbutton_import_all_areyousure"),
                    Utils.getResourceBundleString("formbutton_import_yes"),
                    Utils.getResourceBundleString("formbutton_import_no"));
            contextMenuList.add(ts);
          }
        }
      }
    }
  }

  /**
   * May create patient account.
   *
   * @return the boolean
   */
  public Boolean mayCreatePatientAccount() {
    String locationName =
        ((SessionBean) sessionBean).getCurrentCase().getLocation(getConfig()).getName();
    return ((SessionBean) sessionBean).mayCreatePatientAccount(locationName);
  }

  /**
   * May change status of current form.
   *
   * @return the boolean
   */
  public Boolean mayChangeStatusOfCurrentForm() {
    if (((SessionBean) sessionBean).getCurrentCase() == null) {
      return false;
    }

    HashMap<String, Boolean> tempMap =
        ((SessionBean) sessionBean)
            .getMyChangeStatusPermissions()
            .get(((SessionBean) sessionBean).getCurrentCase().getLocation(getConfig()).getName());

    if (tempMap == null) {
      return false;
    }

    if (form == null) {
      return false;
    }

    if (tempMap.get(form.getFormStateNumber()) == null) {
      return false;
    }

    return tempMap.get(form.getFormStateNumber());
  }

  /**
   * Reports all forms of the current episode.
   *
   * @return JSF outcome
   */
  public String reportWholeVisit() {
    if (episode == null) {
      Utils.addContextMessage(
          Utils.getResourceBundleString("summary_reportfailed"),
          Utils.getResourceBundleString("error_general_noformorvisit"));
      throw new Error("formviewer.reportWholeVisit: Cancling reportform due to no currentvisit");
    }

    for (Entity form : episode.getChildren(OSSEVocabulary.Type.EpisodeForm, getConfig())) {
      if (!((Form) form).isValidated()) {
        try {
          ((Form) form).reportForm();
        } catch (FormException | DatabaseException e) {
          Utils.getLogger().warn("Unable to report a form:" + ((Form) form).getFormRealname(), e);
        }
      }
    }

    getSessionBean()
        .addLog(
            "reported whole visit "
                + episode.getName()
                + " of patient "
                + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

    ((SessionBean) getSessionBean()).reinitCaseAndEpisode();
    episode = ((SessionBean) getSessionBean()).getCurrentEpisode();
    ((SessionBean) getSessionBean()).loadNavigationPatientForms();
    makeContextMenu();

    episodeIsValidated = episode.allFormsAreValidated();
    episodeIsReported = episode.allFormsAreReported();

    // XXX: Why go to main page?
    getSessionBean().goMainPatientPage();

    Utils.addContextMessage(
        Utils.getResourceBundleString("summary_reportsuccess"),
        Utils.getResourceBundleString("success_report"));

    return "reported";
  }

  /**
   * Signs all forms of the current episode.
   *
   * @return JSF outcome
   */
  public String signWholeVisit() {
    if (episode == null) {
      Utils.addContextMessage(
          Utils.getResourceBundleString("summary_signfailed"),
          Utils.getResourceBundleString("error_general_noformorvisit"));
      throw new Error("formviewer.signformWholeVisit: Canceling reportform due to no currentvisit");
    }

    for (Entity form : episode.getChildren(OSSEVocabulary.Type.EpisodeForm, getConfig())) {
      if (!((Form) form).isValidated()) {
        try {
          ((Form) form).signForm();
        } catch (FormException | DatabaseException e) {
          Utils.getLogger().warn("Unable to sign a form:" + ((Form) form).getFormRealname(), e);
        }
      }
    }

    getSessionBean()
        .addLog(
            "validated whole visit "
                + episode.getName()
                + " of patient "
                + ((SessionBean) getSessionBean()).getCurrentPatient().getId());

    ((SessionBean) getSessionBean()).reinitCaseAndEpisode();
    episode = ((SessionBean) getSessionBean()).getCurrentEpisode();
    ((SessionBean) getSessionBean()).loadNavigationPatientForms();
    makeContextMenu();

    episodeIsValidated = episode.allFormsAreValidated();
    episodeIsReported = episode.allFormsAreReported();

    // XXX: why go to main page here?
    getSessionBean().goMainPatientPage();

    Utils.addContextMessage(
        Utils.getResourceBundleString("summary_signsuccess"),
        Utils.getResourceBundleString("success_sign"));
    return "sign";
  }

  /**
   * Gets the data entries of the form.
   *
   * @return the entries
   */
  public MyMap<String, Object> getEntries() {
    if (form == null) {
      return new MyMap<>(null);
    }
    if (entries == null) {
      entries = new MyMap<>(form);
    }
    return entries;
  }

  /**
   * Sets the data entries of the form.
   *
   * @param entries the entries
   */
  public void setEntries(MyMap<String, Object> entries) {
    this.entries = entries;
  }

  /** Gets the data entries that could not be mapped during a form upgrade. */
  public Map<String, Object> getNotMappedEntries() {
    return notMappedEntries;
  }

  /**
   * Gets the episode.
   *
   * @return Visit
   */
  public Episode getEpisode() {
    return episode;
  }

  /**
   * Gets the form.
   *
   * @return the form
   */
  public Form getForm() {
    return form;
  }

  /**
   * Gets the new status.
   *
   * @return the newStatus
   */
  public String getNewStatus() {
    return newStatus;
  }

  /**
   * Sets the new status.
   *
   * @param newStatus the newStatus to set
   */
  public void setNewStatus(String newStatus) {
    this.newStatus = newStatus;
  }

  /**
   * TODO: make this generic/config based.
   *
   * @return the possibleNewStatus
   */
  public List<SelectItem> getPossibleNewStatus() {
    return possibleNewStatus;
  }

  /**
   * Sets the possible new status.
   *
   * @param possibleNewStatus the possibleNewStatus to set
   */
  public void setPossibleNewStatus(List<SelectItem> possibleNewStatus) {
    this.possibleNewStatus = possibleNewStatus;
  }

  /**
   * Gets the form comments.
   *
   * @return the form comments
   */
  public TreeSet<FormComment> getFormComments() {
    return formComments;
  }

  /**
   * Checks for form comments.
   *
   * @return the boolean
   */
  public Boolean hasFormComments() {
    return formComments != null && !formComments.isEmpty();
  }

  /**
   * Returns the translated form name.
   *
   * @param locale ISO 639-1 code of the language to fetch.
   * @return The translated form name (or the default name if no translation available).
   */
  public String getFormTranslatedName(long formId, long formVersion, String locale) {
    String exactResult = null;
    FormDetails formDetails = FormUtils.loadFormFromXml(formId, formVersion);
    FormDetails.FormI18n formI18n = FormUtils.getFormI18n(formDetails, locale);

    if (formI18n instanceof FormDetails.FormI18n) {
      exactResult = formI18n.getName();
    }

    return (exactResult != null) ? exactResult : formDetails.getName();
  }

  /**
   * Returns the translated form description.
   *
   * @param locale ISO 639-1 code of the language to fetch.
   * @return The translated form description (or the default description if no translation
   *     available).
   */
  public String getFormTranslatedDescription(long formId, long formVersion, String locale) {
    String exactResult = null;
    FormDetails formDetails = FormUtils.loadFormFromXml(formId, formVersion);
    FormDetails.FormI18n formI18n = FormUtils.getFormI18n(formDetails, locale);

    if (formI18n instanceof FormDetails.FormI18n) {
      exactResult = formI18n.getDescription();
    }

    return (exactResult != null) ? exactResult : formDetails.getDescription();
  }

  /**
   * Returns the translated html content of a form item.
   *
   * @param formId The id of the form which contains the item requested.
   * @param formVersion The version of the form which contains the item requested.
   * @param itemPosition The numerical position of the item requested.
   * @param locale The ISO 639-1 code of the content's language to return.
   * @return The translated html content of the item specified (or the default content if no
   *     translation available).
   */
  public String getRichTextFormItemTranslated(
      long formId, long formVersion, int itemPosition, String locale) {
    String exactResult = null;
    FormDetails formDetails = FormUtils.loadFormFromXml(formId, formVersion);
    FormDetails.Item item = FormUtils.getFormItem(formDetails, itemPosition);
    FormDetails.Item.ItemI18n itemI18n =
        FormUtils.getFormItemI18n(formDetails, itemPosition, locale);

    if (itemI18n instanceof FormDetails.Item.ItemI18n) {
      exactResult = itemI18n.getHtml();
      exactResult =
          new String(
              exactResult.getBytes(Charset.forName("UTF-8")), Charset.forName("Windows-1252"));
      exactResult = exactResult.replace("<p><hr>", "<p /><hr />");
      exactResult = exactResult.replace("<p><hr/>", "<p /><hr />");
      exactResult = exactResult.replace("<br>", "<br />");
      exactResult = exactResult.replace("<hr>", "<hr />");
    }

    return (exactResult != null) ? exactResult : item.getHtml();
  }
}
