/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Definition;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.model.Location;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.osse.OSSEVocabulary.Case;
import de.samply.store.osse.OSSEVocabulary.CaseForm;
import de.samply.store.osse.OSSEVocabulary.Type;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import de.samply.web.mdrfaces.MdrContext;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/**
 * View scoped bean for the registry dashboard.
 */
@ManagedBean
@ViewScoped
public class DashboardBean extends AbstractViewBean {

  /**
   * The list of locations containing their names and patient numbers.
   */
  private List<Map<String, Object>> locations;

  /**
   * Postconstruct init.
   *
   * @see de.samply.edc.control.AbstractViewBean#init()
   */
  @Override
  @PostConstruct
  public void init() {
    setLocations(null);
    super.init();
  }

  /**
   * Loads all locations with names and patient count from the database.
   */
  private void loadLocations() {
    List<Map<String, Object>> tempLocations = new ArrayList<>();

    ResourceQuery locationQuery = new ResourceQuery(OSSEVocabulary.Type.Location);
    locationQuery.setFetchAdjacentResources(false);
    // ignore system role
    locationQuery.add(
        Criteria.NotEqual(
            OSSEVocabulary.Type.Location,
            OSSEVocabulary.Location.Name,
            Vocabulary.Generic.systemLocation));
    // perform query
    List<Resource> resultLocations = sessionBean.getDatabase().getResources(locationQuery);

    for (Resource locationResource : resultLocations) {
      Location location = new Location(getSessionBean().getDatabase(), locationResource);
      location.load(getConfig());

      dataObject = new HashMap<>();
      dataObject.put("name", location.getName());

      // fetch patients to this location
      // query will only get numbers for locations the user has the permission to read anyway,
      // but this allows us to set a custom string in the ui
      if (((SessionBean) sessionBean).mayRead(location)) {
        ResourceQuery patientsQuery = new ResourceQuery(Type.Patient);
        patientsQuery.setFetchAdjacentResources(false);
        patientsQuery.add(Criteria.Equal(Type.Case, Case.Location, location.getId()));
        patientsQuery.add(
            Criteria.Or(
                Criteria.IsNull(Type.Case, "isDeleted"),
                Criteria.Equal(Type.Case, "isDeleted", 0)));
        dataObject.put("amountPatients", sessionBean.getDatabase().countResources(patientsQuery));
      } else {
        dataObject.put("amountPatients", null);
      }

      tempLocations.add(dataObject);
    }

    setLocations(tempLocations);
  }

  /**
   * Returns a list of the locations.
   */
  public List<Map<String, Object>> getLocations() {
    if (locations == null) {
      loadLocations();
    }

    return locations;
  }

  /**
   * Set the list of locations.
   */
  protected void setLocations(List<Map<String, Object>> locations) {
    this.locations = locations;
  }

  /**
   * Returns the list of locations with the locations ordered by their number of patients.
   *
   * @return The list of locations ordered by number of patients.
   */
  public List<Map<String, Object>> getLocationsSortedByPatientCount() {
    List<Map<String, Object>> tempLocations = getLocations();

    tempLocations.sort(
        Comparator.comparing((Map<String, Object> o) -> {
          if (o.get("amountPatients") == null || o.get("amountPatients") == "") {
            return 0;
          } else {
            return (Integer) o.get("amountPatients");
          }
        }).reversed());

    return tempLocations;
  }

  /**
   * Returns a JavaScript array string representation of the location names ordered by the number of
   * patients in each location.
   *
   * @return JavaScript array of location names.
   */
  public String getLocationNamesAsArraySortedByPatientCount() {
    String result = "";
    List<Map<String, Object>> tempLocations = getLocationsSortedByPatientCount();

    for (Map<String, Object> location : tempLocations) {
      if (!result.equals("")) {
        result += ", ";
      }
      result += "'" + location.get("name") + "'";
    }

    return "[" + result + "]";
  }

  /**
   * Returns a JavaScript array string representation of the number of patients of each location.
   * The numbers appear in order.
   *
   * @return JavaScript array of patient numbers per lcoation.
   */
  public String getLocationPatientCountAsArraySortedByPatientCount() {
    String result = "";
    List<Map<String, Object>> tempLocations = getLocationsSortedByPatientCount();

    for (Map<String, Object> location : tempLocations) {
      if (!result.equals("")) {
        result += ", ";
      }
      result += location.get("amountPatients");
    }

    return "[" + result + "]";
  }

  /**
   * Counts patients with a certain property in their case form data. Does not include deleted
   * patients.
   *
   * @param formId ID of the case form.
   * @param urn The URN of the data element.
   * @param value Value, the data element has to have for patient to be counted.
   * @return Number of patients with that property.
   */
  public Integer fetchPatientCountByDataElement(int formId, String urn, String value) {
    ResourceQuery patientQuery = new ResourceQuery(Type.Patient);
    patientQuery.setFetchAdjacentResources(false);
    patientQuery.add(Criteria.And(
        Criteria.SimilarTo(Type.CaseForm, CaseForm.Name, "form_" + formId + "_ver-\\d+"),
        Criteria.Equal(Type.CaseForm, urn, value)
    ));
    patientQuery.add(Criteria.Or(
        Criteria.IsNull(Type.Case, "isDeleted"),
        Criteria.Equal(Type.Case, "isDeleted", 0)
    ));
    // perform count query
    int result = sessionBean.getDatabase().countResources(patientQuery);

    return result;
  }

  /**
   * Fetches the designation of a MDR data element.
   *
   * @param urn The URN of the data element.
   * @return The designation of the data element.
   */
  public String fetchDataElementDesignation(String urn) {
    String result = null;

    try {
      MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();
      Definition dataElementDefinition = mdrClient
          .getDataElementDefinition(urn, Utils.getLocale().getLanguage());
      result = dataElementDefinition.getDesignations().get(0).getDesignation();
    } catch (MdrConnectionException | ExecutionException | MdrInvalidResponseException e) {
      Utils.getLogger().error("Error fetching data element definition...", e);
    }

    return result;
  }

  /**
   * Get the total number of patients in the registry. Does not include deleted patients.
   *
   * @return Number of patients.
   */
  public int getPatientsTotal() {
    int result;
    ResourceQuery queryPatients = new ResourceQuery(OSSEVocabulary.Type.Patient);
    queryPatients.add(
        Criteria.Or(
            Criteria.IsNull(Type.Case, "isDeleted"),
            Criteria.Equal(Type.Case, "isDeleted", 0)));
    result = sessionBean.getDatabase().countResources(queryPatients);

    return result;
  }
}