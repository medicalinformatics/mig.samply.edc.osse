/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.FormsEmails;
import de.samply.edc.osse.model.Case;
import de.samply.edc.osse.model.Patient;
import de.samply.edc.osse.model.Role;
import de.samply.edc.osse.model.User;
import de.samply.edc.osse.model.UserContact;
import de.samply.edc.osse.utils.OsseUtils;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.apache.commons.lang.StringUtils;

/** View scoped bean to manage user data. */
@ManagedBean
@ViewScoped
public class UserBean extends AbstractViewBean {

  /** The users. */
  protected ArrayList<HashMap<String, Object>> users;

  /** SelectItem list of roles (used by JSF). */
  protected List<SelectItem> rolesSelectItems;

  /** SelectItem list of locations (used by JSF). */
  private List<SelectItem> locationsSelectItems;

  /**
   * Post-construct init.
   *
   * @see de.samply.edc.control.AbstractViewBean#init()
   */
  @Override
  @PostConstruct
  public void init() {
    users = null;
    rolesSelectItems = null;
    super.init();
  }

  /**
   * Changes the password of a user (forced by admin).
   *
   * @return the string
   */
  public String changePasswordByAdmin() {
    Resource userResource = (Resource) dataObject.get("userResource");
    String password = getFormParam("password");
    String passwordRepeat = getFormParam("password_repeat");

    if (password == null || "".equals(password)) {
      Utils.displayContextMessage("summary_edituserfailed", "error_neednewpassword");
      return "";
    }

    User user = new User(getSessionBean().getDatabase(), userResource);
    user.load(getConfig());

    String ok =
        Utils.validatePassword(
            "", password, passwordRepeat, user.getUsername(), user.getContact().getLastname());
    if (ok != null) {
      Utils.displayContextMessage("summary_edituserfailed", ok);
      return "";
    }

    if (userResource != null && !password.equals("")) {
      Utils.getLogger().debug("Changing password ");
      Resource moo =
          getSessionBean().getDatabase().changeUserPasswordByAdmin(userResource, password);

      getSessionBean().getDatabase().save(moo);
    }
    Utils.displayContextMessage("summary_editusersuccess", "success_edituser");
    goUserList();
    return "passwordChanged";
  }

  /**
   * JSF Button: edit user.
   *
   * @param userData the user data
   * @return the string
   */
  public String editUserAction(Object userData) {
    getSessionBean().setTempObject("dataObject", userData);
    Utils.goAdminForm("user_edit");
    return "editUser";
  }

  /**
   * JSF Button: add user.
   *
   * @return the string
   */
  public String goAddUser() {
    getSessionBean().clearTempObject("dataObject");
    Utils.goAdminForm("user_add");
    return "";
  }

  /**
   * JSF Button: delete user.
   *
   * @param toDeleteUser the user to be deleted
   */
  public void setToDeleteUser(Object toDeleteUser) {
    getSessionBean().setTempObject("dataObject", toDeleteUser);
  }

  /**
   * Deletes a user.
   *
   * @return the string
   */
  @SuppressWarnings("unchecked")
  public String deleteUser() {
    dataObject = (HashMap<String, Object>) Utils.getSB().clearTempObject("dataObject");
    if (dataObject == null) {
      return "";
    }

    ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.User);
    query.add(
        Criteria.Equal(
            OSSEVocabulary.Type.User,
            OSSEVocabulary.User.Username,
            (String) dataObject.get(OSSEVocabulary.User.Username)));
    ArrayList<Resource> found = getSessionBean().getDatabase().getResources(query);
    for (Resource userResource : found) {
      // Utils.getDatabase().deleteResource(moo);
      User user = new User(getSessionBean().getDatabase(), userResource);
      user.deleteUser(getConfig());
    }

    Utils.displayContextMessage("delete_successful_title", "delete_successful");

    loadUsers();
    return "deleteUser";
  }

  /** JSF Button: goUserList. */
  public void goUserList() {
    Utils.goAdminForm("userlist");
  }

  /**
   * Check if your own account setting belongs to you.
   *  If yes, the account setting is your own setting, not other user's setting.
   *  If no, other user uses your account setting.
   */
  public boolean isCurrentResourceSameAsCurrentUser() {
    Resource userResource = (Resource) dataObject.get("userResource");
    if (sessionBean == null || userResource == null) {
      return false;
    }
    String adminName = ((SessionBean)Utils.getSB()).getCurrentUser().getUsername();
    String userName = userResource.getProperty(OSSEVocabulary.User.Username).getValue();
    return adminName.equals(userName);
  }

  /** Check if the user is activated. */
  public boolean isUserActivated() {
    Resource userResource = (Resource) dataObject.get("userResource");
    if (userResource == null) {
      return false;
    }
    String isUserActivated = userResource.getProperty(OSSEVocabulary.User.IsActivated).getValue();
    return "1".equals(isUserActivated);
  }

  /** Check if the user is deactivated. */
  public boolean isUserDeactivated() {
    return !isUserActivated();
  }

  /** Deactivate the user. */
  public void deactivateUserByAdmin() {
    getSessionBean().getDatabase().beginTransaction();
    Resource userResource = (Resource) dataObject.get("userResource");
    if (userResource == null) {
      Utils.getLogger().error("Failed to deactivate the user.");
      return;
    }
    userResource.setProperty(OSSEVocabulary.User.IsActivated, 0);
    Utils.getLogger().debug("Deactivate this user ");
    getSessionBean().getDatabase().saveOrUpdateResource(userResource);
    getSessionBean().getDatabase().commit();
    Utils.displayContextMessage("summary_editusersuccess", "success_edituser");
    goUserList();
  }

  /** Activates a user. */
  public void activateUserByAdmin() {
    getSessionBean().getDatabase().beginTransaction();
    Resource userResource = (Resource) dataObject.get("userResource");
    if (userResource == null) {
      Utils.getLogger().error("Failed to activate the user.");
      return;
    }
    userResource.setProperty(OSSEVocabulary.User.IsActivated, 1);
    userResource.setProperty(OSSEVocabulary.User.FailCounter, 0);
    Utils.getLogger().debug("Activate this user ");
    getSessionBean().getDatabase().saveOrUpdateResource(userResource);
    getSessionBean().getDatabase().commit();
    Utils.displayContextMessage("summary_editusersuccess", "success_edituser");
    goUserList();
  }

  /** Activates a user. */
  public void activateUser() {
    String password = getFormParam("password");
    String passwordRepeat = getFormParam("password_repeat");
    String activationCode = getFormParam("activationcode");

    if (activationCode == null || activationCode.equals("")) {
      return;
    }

    // no password given
    if (password == null || password.equals("")) {
      Utils.displayContextMessage("summary_password_change_failed", "password_change_nopw");
      return;
    }

    String ok = Utils.validatePassword("", password, passwordRepeat, null, null);
    if (ok != null) {
      Utils.displayContextMessage("summary_password_change_failed", ok);
      return;
    }

    Resource userResource = getSessionBean().getDatabase().activateUser(password, activationCode);

    // TODO: Here we need a backend method to just return the userResource
    // without actually
    // activating the user, so we can check the password for
    // username/lastname equality first
    // before actually activating the user

    // User user = new User(getSessionBean().getDatabase(), userResource);
    // user.load();
    //
    // if(password.equalsIgnoreCase(user.getUsername())) {
    // Utils.addContextMessage(summary, Utils
    // .getResourceBundleString("error_newpassword_is_username"));
    // return;
    // }
    //
    // if(password.equalsIgnoreCase(user.getContact().getLastname())) {
    // Utils.addContextMessage(summary, Utils
    // .getResourceBundleString("error_newpassword_is_lastname"));
    // return;
    // }

    if (userResource != null) {
      Utils.displayContextMessage("summary_password_change_success", "success_edituser");
    } else {
      Utils.displayContextMessage("summary_password_change_failed", "password_change_failed");
    }
  }

  /**
   * Forgot password (yet unused).
   *
   * @return the boolean
   */
  public Boolean forgotPassword() {
    String username = getFormParam("username");

    if (username == null || username.equals("")) {
      return false;
    }

    Resource userResource = getSessionBean().getDatabase().forgotPassword(username);

    String activationCode = userResource.getProperty(OSSEVocabulary.User.ActivationCode).getValue();
    List<Resource> contact =
        userResource.getResources(
            OSSEVocabulary.User.ReadOnly.UserContacts,
            getSessionBean().getDatabase().getDatabaseModel());

    if (activationCode == null || contact == null || contact.size() < 1) {
      Utils.displayContextMessage("summary_enteremail", "doEnterEmail");
      return false;
    }

    String email = contact.get(0).getProperty(Vocabulary.Attributes.contactEMail).getValue();
    if (email == null || email.equalsIgnoreCase("")) {
      Utils.displayContextMessage("summary_enteremail", "doEnterEmail");
      return false;
    }

    FormsEmails emailer = new FormsEmails();
    emailer.sendForgotPassword(email, activationCode);

    Utils.displayContextMessage("summary_forgotSent", "forgotSent");

    return false;
  }

  /**
   * Unlocks a user, by resetting the fail counter.
   *
   * @param userData the user data
   * @return the string
   */
  public String unlockUser(HashMap<String, Object> userData) throws IOException {
    if (userData == null
        || userData.isEmpty()
        || !(userData.get("userResource") instanceof Resource)) {
      return null;
    }

    User user = new User(getSessionBean().getDatabase(), (Resource) userData.get("userResource"));

    user.resetFailCounter();
    user.activateUser();
    OsseUtils.displayContextMessage("success", "unlocked");
    getSessionBean().addLog("unlocked user " + user.getUsername());
    refreshPage();
    return "";
  }

  /**
   * Refresh or reload page.
   */
  public void refreshPage() throws IOException {
    ExternalContext ec = FacesContext.getCurrentInstance().getExternalContext();
    ec.redirect(((HttpServletRequest) ec.getRequest()).getRequestURI());
  }

  /**
   * JSF Button method to kill the session of a user from the userlist.
   *
   * @param userData the user data
   * @return the string
   */
  public String killUserSession(HashMap<String, Object> userData) {
    if (userData == null
        || userData.isEmpty()
        || !(userData.get("userResource") instanceof Resource)) {
      return null;
    }

    User user = new User(getSessionBean().getDatabase(), (Resource) userData.get("userResource"));

    user.logMeOut();
    users = null;
    OsseUtils.displayContextMessage("success","logged_out");
    getSessionBean().addLog("killed session of user " + user.getUsername());
    return "loggedout";
  }

  /** Displays a user edit fail message. */
  private void showDefaultUserEditFailedMessage() {
    Utils.displayContextMessage("summary_edituserfailed", "error_general_noformorvisit");
  }

  /**
   * Validates the user data in the data object.
   *
   * @return null in case of successful validation, otherwise the error message detail
   */
  private String validateDataObject() {
    Resource userResource = (Resource) dataObject.get("userResource");
    String username = (String) dataObject.get("username");
    String lastname = (String) dataObject.get("lastname");
    String password = getFormParam("password");
    String passwordRepeat = getFormParam("password_repeat");
    DatatableRows rows = (DatatableRows) dataObject.get("roles");

    if (rows == null) {
      return OsseUtils.contextMessage("please_choose_a_role");
    }

    if (userResource == null) {
      if (password == null || "".equals(password)) {
        return "error_neednewpassword";
      }
    }

    if (password != null) {
      String ok = Utils.validatePassword("", password, passwordRepeat, username, lastname);
      if (ok != null) {
        return ok;
      }
    }

    User user = new User(getSessionBean().getDatabase());

    // check if we've added a new user or changed our own name
    if (userResource != null
        && !userResource
            .getProperty(OSSEVocabulary.User.Username)
            .getValue()
            .equalsIgnoreCase(username)) {
      // nope, we changed our own name, so check if that new name already
      // exists

      if (user.entityExistsByProperty(OSSEVocabulary.User.Username, username) != null) {
        return "error_username_already_in_use";
      }
    } else if (userResource == null) {
      if (user.entityExistsByProperty(OSSEVocabulary.User.Username, username) != null) {
        return "error_username_already_in_use";
      }
    }
    return null;
  }

  /**
   * Saves the user data.
   *
   * @return the string
   * @see de.samply.edc.control.AbstractViewBean#save()
   */
  @Override
  public String save() {
    String validationMessage = validateDataObject();
    if (validationMessage != null) {
      Utils.displayContextMessage("summary_edituserfailed", validationMessage);
      return null;
    }

    saveUserInternal(false);

    Utils.displayContextMessage("summary_editusersuccess", "success_edituser");
    goUserList();
    return null;
  }

  /**
   * Sets the selected roles in the user object.
   *
   * @param user the user
   * @return false, if an error occurred when mapping role names to roles; true otherwise
   */
  private boolean setRoles(User user) {
    DatatableRows roles = (DatatableRows) dataObject.get("roles");

    user.removeProperty(OSSEVocabulary.User.Roles);
    user.getResource().removeProperties(OSSEVocabulary.User.Roles);

    // user roles
    if (roles != null) {
      List<Integer> alreadyPutIn = new ArrayList<>();

      for (DatatableRow role : roles.getRows()) {
        String roleName = (String) role.getColumns().get("role");
        List<Resource> found = ((Database) getSessionBean().getDatabase()).getRoles(roleName);

        if (found.size() < 1) {
          Utils.getLogger().error("The role selected was not found in the roles available");
          return false;
        }
        Resource roleResource = found.get(0);

        if (!alreadyPutIn.contains(roleResource.getId())) {
          user.addProperty(OSSEVocabulary.User.Roles, roleResource);
          alreadyPutIn.add(roleResource.getId());
        }
      }
    }
    return true;
  }

  /**
   * Determines whether the current user has the permission to save contact data of another user.
   * This is the case if the current user is local admin and at least one of the roles of the other
   * user is in the location of the local admin.
   *
   * @param user the user
   * @return true, if successful
   */
  private boolean allowedToSaveContactData(User user) {
    Role currentRole = ((SessionBean) getSessionBean()).getCurrentRole();

    if (currentRole.isLocalAdminRole()) {
      List<Role> allRoles = user.getRoles();

      for (Role role : allRoles) {
        if (role.getLocation().getName().equalsIgnoreCase(currentRole.getLocation().getName())) {
          return true;
        }
      }
    } else {
      return true;
    }
    return false;
  }

  /**
   * Saves the contact data of a user.
   *
   * @param user the user
   */
  private void saveContact(User user) {
    UserContact contact = user.getContact();
    if (contact == null) {
      contact = new UserContact(getSessionBean().getDatabase());
      contact.setUser(user);
    }

    String firstname = (String) dataObject.get("firstname");
    contact.setProperty(Vocabulary.Attributes.contactFirstName, firstname);

    String lastname = (String) dataObject.get("lastname");
    contact.setProperty(Vocabulary.Attributes.contactLastName, lastname);

    String title = (String) dataObject.get("title");
    contact.setProperty(Vocabulary.Attributes.contactTitle, title);

    String email = (String) dataObject.get("email");
    if (email != null) {
      email = email.toLowerCase();
    }
    contact.setProperty(Vocabulary.Attributes.contactEMail, email);
    contact.saveOrUpdate();
  }

  /**
   * Save user internal.
   *
   * @param activateUser Boolean to activate the user right away or not Saves the user data without
   *     validation and redirect.
   */
  private void saveUserInternal(Boolean activateUser) {
    String username = (String) dataObject.get("username");
    username = username.toLowerCase();

    getSessionBean().getDatabase().beginTransaction();

    User user = new User(getSessionBean().getDatabase());

    Resource userResource = (Resource) dataObject.get("userResource");

    // create new user if necessary
    if (userResource == null) {
      String password = getFormParam("password");
      userResource = getSessionBean().getDatabase().createUser(username, password);

      // rollback on fail
      if (userResource == null) {
        getSessionBean().getDatabase().rollback();
        showDefaultUserEditFailedMessage();
        Utils.getLogger().error("Create user seems to have failed.");
        return;
      }
    }

    user.setResource(userResource);
    user.load(getConfig());

    if (!((String) user.getProperty(OSSEVocabulary.User.Username)).equalsIgnoreCase(username)) {
      user.setProperty(OSSEVocabulary.User.Username, username);
    }

    if (activateUser) {
      user.setProperty(OSSEVocabulary.User.IsActivated, 1);
    }

    // get roles for user from db
    if (!setRoles(user)) {
      getSessionBean().getDatabase().rollback();
      showDefaultUserEditFailedMessage();
    }

    user.saveOrUpdate();
    // After saving: add resource to data object
    // (used in #createPatientAccount)
    // only if a user could be added
    if (user.getResource().getId() != 0) {
      dataObject.put("userResource", user.getResource());
    }

    getSessionBean().getDatabase().commit();
    //        user.load(getConfig());

    if (allowedToSaveContactData(user)) {
      getSessionBean().getDatabase().beginTransaction();
      saveContact(user);
      getSessionBean().getDatabase().commit();
    }
  }

  /** Loads all users. */
  protected void loadUsers() {
    loadUsers(false);
  }

  /**
   * Loads all users.
   *
   * @param deleted the deleted
   */
  protected void loadUsers(Boolean deleted) {
    users = new ArrayList<>();

    sessionBean.getDatabase().startCaching();

    /* Preload all locations */
    ResourceQuery locationCacheQuery = new ResourceQuery(OSSEVocabulary.Type.Location);
    locationCacheQuery.setFetchAdjacentResources(false);
    sessionBean.getDatabase().getResources(locationCacheQuery);

    ResourceQuery queryUsers = new ResourceQuery(OSSEVocabulary.Type.User);
    List<Resource> resultUsers = getSessionBean().getDatabase().getResources(queryUsers);
    for (Resource userResource : resultUsers) {
      User user = new User(getSessionBean().getDatabase(), userResource);
      if (!user.load(true, true, getConfig())) {
        continue;
      }

      if ("admin".equalsIgnoreCase(user.getUsername())) {
        if (!((SessionBean) sessionBean).getCurrentRole().isAdminRole()) {
          continue;
        }

        dataObject = new HashMap<>();
        dataObject.put("username", user.getUsername());
        dataObject.put("userResource", userResource);
        dataObject.put("failcounter", user.getFailcounter());
        dataObject.put("firstname", "Global Administrator");
        dataObject.put("lastname", "");
        dataObject.put("title", "");
        dataObject.put("roles", null);
        dataObject.put("isDeactivated", !user.isActivated());

        dataObject.put("isLoggedIn", User.isLoggedIn(user.getUsername()));

        dataObject.put(
            "isCurrentUser",
            ((SessionBean) getSessionBean())
                .getCurrentUser()
                .getUsername()
                .equalsIgnoreCase(user.getUsername()));

        users.add(dataObject);

        continue;
      }

      if ("export".equalsIgnoreCase(user.getUsername())) {
        continue;
      }

      if ("dev".equalsIgnoreCase(user.getUsername())) {
        continue;
      }

      // load only deleted users
      if (deleted && user.isActivated()) {
        continue;
      }

      if (!((SessionBean) getSessionBean()).getCurrentRole().isAdminRole()
          && user.getUsername()
              .equals(((SessionBean) getSessionBean()).getCurrentUser().getUsername())) {
        continue;
      }

      dataObject = new HashMap<>();
      dataObject.put("username", user.getUsername());
      dataObject.put("userResource", userResource);
      dataObject.put("failcounter", user.getFailcounter());
      dataObject.put("isDeactivated", !user.isActivated());
      ArrayList<String> locationList = new ArrayList<>();
      for (Role role : user.getRoles()) {
        String text = role.getName();
        if (role.isCustomRole()) {
          text += " (" + role.getLocation().getName() + ")";
        }
        locationList.add(text);
      }

      dataObject.put("location", StringUtils.join(locationList, ", "));

      if (user.getContact() != null) {
        dataObject.put(
            "firstname", user.getContact().getProperty(Vocabulary.Attributes.contactFirstName));
        dataObject.put(
            "lastname", user.getContact().getProperty(Vocabulary.Attributes.contactLastName));
        dataObject.put("title", user.getContact().getProperty(Vocabulary.Attributes.contactTitle));
        dataObject.put("email", user.getContact().getProperty(Vocabulary.Attributes.contactEMail));
      }

      boolean isPatientUser = false;
      boolean rolesAtLocalAdminLocation = false;

      DatatableRows roles = new DatatableRows();
      for (Role role : user.getRoles()) {
        if (role.isPatientRole()) {
          isPatientUser = true;
          break;
        }

        if (((SessionBean) getSessionBean()).getCurrentRole().isLocalAdminRole()) {
          if (role.isAdminRole() || role.isLocalAdminRole()) {
            continue;
          }

          if (!role.getLocation().getName().equals(
              ((SessionBean) getSessionBean()).getCurrentRole().getLocation().getName())) {
            continue;
          }
        }

        HashMap<String, Object> entries = new HashMap<>();
        entries.put("role", role.getProperty(OSSEVocabulary.Role.Name));
        DatatableRow roleColumn = new DatatableRow();

        roleColumn.setColumns(entries);
        roles.add(roleColumn);
        rolesAtLocalAdminLocation = true;
      }

      // admin is local and only roles from other locations
      if (((SessionBean) getSessionBean()).getCurrentRole().isLocalAdminRole()
          && !rolesAtLocalAdminLocation) {
        continue;
      }

      if (isPatientUser) {
        continue;
      }

      dataObject.put("roles", roles);

      dataObject.put("isLoggedIn", User.isLoggedIn(user.getUsername()));

      dataObject.put(
          "isCurrentUser",
          ((SessionBean) getSessionBean())
              .getCurrentUser()
              .getUsername()
              .equalsIgnoreCase(user.getUsername()));

      users.add(dataObject);
    }

    sessionBean.getDatabase().stopCaching();

    dataObject = null;
  }

  /**
   * Gets all users.
   *
   * @return the users
   */
  public ArrayList<HashMap<String, Object>> getUsers() {
    if (users == null) {
      loadUsers();
    }
    return users;
  }

  /**
   * Gets the deleted users.
   *
   * @return the deleted users
   */
  public ArrayList<HashMap<String, Object>> getDeletedUsers() {
    if (users == null) {
      loadUsers(true);
    }
    return users;
  }

  /**
   * Gets the role SelectItem list.
   *
   * @return the role list
   */
  public List<SelectItem> getRoleList() {
    if (rolesSelectItems == null) {
      rolesSelectItems = new ArrayList<>();

      Boolean isBridgehead = ((ApplicationBean) Utils.getAB()).isBridgehead();

      List<Resource> roleList = ((Database) getSessionBean().getDatabase()).getRoles(true);
      SelectItem item;

      for (Resource roleResource : roleList) {
        Role theRole = new Role(getSessionBean().getDatabase(), roleResource);

        if (theRole.isPatientRole()) {
          continue;
        }

        if (theRole.isSystemRole()) {
          continue;
        }

        if (isBridgehead && theRole.isLocalAdminRole()) {
          continue;
        }

        theRole.load(getConfig());

        if (((SessionBean) getSessionBean()).getCurrentRole().isLocalAdminRole()) {

          if (theRole.isAdminRole() || theRole.isLocalAdminRole()) {
            continue;
          }

          if (theRole.getLocation() != null
              && !theRole.getLocation().getName().equalsIgnoreCase(
                      ((SessionBean) getSessionBean()).getCurrentRole().getLocation().getName())) {
            continue;
          }
        }

        item = new SelectItem();

        item.setLabel((String) theRole.getProperty(OSSEVocabulary.Role.Name));
        item.setValue(theRole.getProperty(OSSEVocabulary.Role.Name));
        rolesSelectItems.add(item);
      }
    }

    return rolesSelectItems;
  }

  /**
   * Gets the location SelectItem list.
   *
   * @return the location list
   */
  public List<SelectItem> getLocationList() {
    if (locationsSelectItems == null) {
      locationsSelectItems =
          OsseUtils.getLocationsAsSelectItems((Database) getSessionBean().getDatabase());
    }
    return locationsSelectItems;
  }

  /** Creates a patient account TODO: Not yet finished, BE needs a real patient role type. */
  private void createPatientAccount() {

    // create new user name
    String username;
    do {
      username = Utils.generateRandomStringForUser(8, true);
    } while (getSessionBean().getDatabase().userExists(username));

    Patient patient = ((SessionBean) getSessionBean()).getCurrentPatient();
    if (patient == null) {
      throw new NullPointerException("Current patient is null.");
    }

    getSessionBean().getDatabase().beginTransaction();

    // save new user
    String password = Utils.generateRandomStringForUser(8, false);
    Case myCase = ((SessionBean) getSessionBean()).getCurrentCase();
    Resource userResource;
    try {
      userResource =
          ((Database) getSessionBean().getDatabase())
              .createPatientUser(myCase.getResource(), username, password);
      Utils.getLogger()
          .debug("Patuser " + userResource + " username: " + username + " password " + password);
    } catch (DatabaseException e) {
      e.printStackTrace();
      OsseUtils.displayContextMessage("account_activation_failed","account_could_not_ne_activated");
      getSessionBean().getDatabase().rollback();
      return;
    }
    patient.setProperty(Vocabulary.Patient.User, userResource);
    // workaround, needed for saving the patient
    // XXX: This should be fixed already
    patient.setProperty(OSSEVocabulary.Patient.Locations, patient.getLocation().getResource());
    patient.saveOrUpdate();
    patient.storeLastChange();
    getSessionBean().getDatabase().commit();
    patient.load(getConfig());
    ((SessionBean) getSessionBean()).setCurrentPatient(patient);
    OsseUtils.displayContextMessage("activate_patient_account","patient_account_was_activated");
    Utils.goForm("patientaccount");
  }

  /** Re-activates an existing user account for the current patient or creates a new one. */
  public void enablePatientAccount() {
    Patient patient = ((SessionBean) getSessionBean()).getCurrentPatient();
    User user = patient.getAsUser();
    if (user == null) {
      createPatientAccount();
    } else { // reactivate patient account
      reactivatePatientAccount();
    }
  }

  /**
   * Action to reset a patient password (i.e. create a new one)
   *
   * @return the new password
   */
  public String resetPatientPassword() {
    String password = Utils.generateRandomStringForUser(8, false);
    Patient patient = ((SessionBean) getSessionBean()).getCurrentPatient();
    User user = patient.getAsUser();

    getSessionBean().getDatabase().beginTransaction();
    if (user != null) {
      try {
        ((Database) getSessionBean().getDatabase())
            .changePatientUserPassword(user.getResource(), password);
      } catch (DatabaseException e) {
        e.printStackTrace();
        OsseUtils.displayContextMessage("changing_password_failed","password_could_not_be_set");
        getSessionBean().getDatabase().rollback();
        return "";
      }
      setFormParam("password", password);
    }
    getSessionBean().getDatabase().commit();
    return password;
  }

  /**
   * Deletes a patient account.
   *
   * @return jsf outcome
   */
  public String deletePatientAccount() {
    Patient patient = ((SessionBean) getSessionBean()).getCurrentPatient();
    User user = patient.getAsUser();

    getSessionBean().getDatabase().beginTransaction();
    try {
      ((Database) getSessionBean().getDatabase()).disablePatientUser(user.getResource());
    } catch (DatabaseException e) {
      e.printStackTrace();
      OsseUtils.displayContextMessage("disabling_failed", "patient_could_not_ne_deleted");
      getSessionBean().getDatabase().rollback();
      return "";
    }
    getSessionBean().getDatabase().commit();

    patient.load(getConfig());
    ((SessionBean) getSessionBean()).setCurrentPatient(patient);

    OsseUtils.displayContextMessage("disable_patient_account", "patient_account_was_disabled");
    Utils.goForm(getSessionBean().getCurrentFormName());
    return "";
  }

  /**
   * Reactivates a patient account.
   */
  public String reactivatePatientAccount() {
    Patient patient = ((SessionBean) getSessionBean()).getCurrentPatient();
    User user = patient.getAsUser();

    getSessionBean().getDatabase().beginTransaction();
    try {
      ((Database) getSessionBean().getDatabase()).enablePatientUser(user.getResource());
    } catch (DatabaseException e) {
      e.printStackTrace();
      OsseUtils.displayContextMessage("enabling_failed", "patient_could_not_be_enabled");
      getSessionBean().getDatabase().rollback();
      return "";
    }
    getSessionBean().getDatabase().commit();
    patient.load(getConfig());
    ((SessionBean) getSessionBean()).setCurrentPatient(patient);
    OsseUtils.displayContextMessage("enable_patient_account", "account_was_enabled");
    Utils.goForm(getSessionBean().getCurrentFormName());
    return "";
  }

  /**
   * Kills the sessions of all currently logged in users.
   *
   * @return the string
   */
  public String logoutAllUsers() {
    Map<User, HttpSession> foo = new HashMap<>(User.getLogins());
    for (User u : foo.keySet()) {
      Utils.getLogger().debug("Killing user session of user " + u.getUsername());
      u.logMeOut();
    }

    return "done";
  }

}
