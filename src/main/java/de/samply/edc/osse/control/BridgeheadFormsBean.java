/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.auth.rest.AccessTokenDto;
import de.samply.common.http.HttpConnector;
import de.samply.common.mdrclient.MdrClient;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.catalog.Vocabulary.Config.FormEditor;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.exceptions.OsseException;
import de.samply.edc.utils.Utils;
import de.samply.form.dto.FormOverview;
import de.samply.store.JSONResource;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.ws.rs.client.Client;
import org.apache.commons.configuration.Configuration;

/** Special version of RegisterFormsBean to import forms into OSSE Bridgehead. */
@ManagedBean
@ViewScoped
public class BridgeheadFormsBean extends RegisterFormsBean {
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2370623308758851751L;

  /**
   * Action to import OSSE Bridgehead forms from the form repository.
   *
   * @return failed|save
   * @throws OsseException the OSSE exception
   */
  public String importForms() throws OsseException {
    FormOverview caseForm = null;
    FormOverview episodeForm = null;
    FormOverview[] theForms = null;
    Configuration configuration = Utils.getAB().getConfig();

    HttpConnector httpConnector = null;
    AccessTokenDto accessToken = null;
    Client frJerseyClient = null;

    try {
      httpConnector = ApplicationBean.getHttpConnector();

      Client client = httpConnector.getJerseyClientForHTTPS(false);
      accessToken = Auth.getAccessToken(client, configuration);

      String urlFR = configuration.getProperty(FormEditor.formEditorRestUrl) + "";
      frJerseyClient = httpConnector.getJerseyClient(urlFR, false);
      theForms = getForms(frJerseyClient, accessToken);
    } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
      Utils.getLogger().error("Unable to get accessToken",e);
    }

    if (accessToken == null) {
      Utils.getLogger().error("No access token could be gained");
      Utils.addContextMessage("Failed", "A critical error occured (No access token was returned.");
      return "failed";
    }

    if (theForms == null) {
      Utils.addContextMessage("Error loading forms", "The forms list could not be loaded.");
      return "failed";
    }

    for (FormOverview formData : theForms) {
      if (formData.getName().equalsIgnoreCase("Longitudinal Data")) {
        if (episodeForm == null || episodeForm.getVersion() < formData.getVersion()) {
          episodeForm = formData;
        }
      } else if (formData.getName().equalsIgnoreCase("Basic Data")) {
        if (caseForm == null || caseForm.getVersion() < formData.getVersion()) {
          caseForm = formData;
        }
      } else {
        Utils.getLogger().warn(
            "Got form "
            + formData.getName()
            + " ID = "
            + formData.getId()
            + " from form repository which is neither case nor episode form");
      }
    }

    if (episodeForm == null || caseForm == null) {
      Utils.getLogger()
          .error(
              "Episodeform = "
                  + episodeForm
                  + " CaseForm = "
                  + caseForm
                  + " Neither should be null!");
      Utils.addContextMessage(
          "Failed", "Could not load the necessary forms from the form repository.");
      return "failed";
    }

    fih.clearData();

    JSONResource osseFormStorage = ((ApplicationBean) Utils.getAB()).getOsseFormStorage();

    JSONResource formStorage = null;
    if (osseFormStorage.getProperty(Vocabulary.Config.Form.storage) != null) {
      formStorage = osseFormStorage.getProperty(Vocabulary.Config.Form.storage).asJSONResource();
    } else {
      formStorage = new JSONResource();
    }

    JSONResource formNameMatrix = null;
    if (osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix) != null) {
      formNameMatrix =
          osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix).asJSONResource();
    } else {
      formNameMatrix = new JSONResource();
    }

    fih.setFormNameMatrix(formNameMatrix);

    // Get a Jersey client for the MDR
    String urlMdr = configuration.getProperty(Vocabulary.Config.Mdr.REST) + "";
    Client mdrJerseyClient = httpConnector.getJerseyClient(urlMdr, false);
    String mdrUrl = (String) configuration.getProperty(Vocabulary.Config.Mdr.REST);
    MdrClient mdrClient = new MdrClient(mdrUrl, mdrJerseyClient);

    formStorage =
        fih.doImport(
            frJerseyClient,
            mdrClient,
            accessToken,
            Integer.toString(caseForm.getId()),
            Integer.toString(caseForm.getVersion()),
            false,
            formStorage,
            true,
            false);

    formStorage =
        fih.doImport(
            frJerseyClient,
            mdrClient,
            accessToken,
            Integer.toString(episodeForm.getId()),
            Integer.toString(episodeForm.getVersion()),
            true,
            formStorage,
            true,
            false);

    // store the forms and names in the osse form storage for later recovery
    osseFormStorage.setProperty(Vocabulary.Config.Form.storage, formStorage);
    osseFormStorage.setProperty(Vocabulary.Config.Form.formNameMatrix, fih.getFormNameMatrix());
    getSessionBean().getDatabase().saveConfig("osse.form.storage", osseFormStorage);

    fih.fillMdrEntityHasValidation(mdrClient, accessToken);

    saveConfig(mdrJerseyClient, accessToken);

    Utils.addContextMessage(
        "Success", "Your selected forms have been imported into your registry.");

    httpConnector.closeClients();
    return "save";
  }

  /**
   * Returns the version of the case form.
   *
   * @return the integer
   */
  public Integer caseFormVersion() {
    String temp =
        (String) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.patientStandard);

    if (temp == null) {
      return 0;
    }

    return Utils.getFormVersion(temp);
  }

  /**
   * returns the version of the episode form.
   *
   * @return the integer
   */
  public Integer episodeFormVersion() {
    String temp =
        (String) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.visitStandard);

    if (temp == null) {
      return 0;
    }

    return Utils.getFormVersion(temp);
  }
}
