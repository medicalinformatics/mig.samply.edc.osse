/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.catalog.EpisodePatterns;
import de.samply.edc.osse.model.Case;
import de.samply.edc.osse.model.Episode;
import de.samply.edc.osse.model.EpisodePattern;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.TimestampLiteral;
import de.samply.store.osse.OSSEVocabulary;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.TimeZone;
import java.util.TreeMap;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;

/** The view scoped bean for episode data management. */
@ManagedBean
@ViewScoped
public class EpisodeBean extends AbstractViewBean {

  private Boolean takePrevEpisodeData = false;

  public Boolean getTakePrevEpisodeData() {
    return takePrevEpisodeData;
  }

  public void setTakePrevEpisodeData(Boolean takePrevEpisodeData) {
    this.takePrevEpisodeData = takePrevEpisodeData;
  }

  @PostConstruct
  public void init() {
    super.init();
  }

  /**
   * Checks if a name is already used in the episode's case.
   *
   * @param theCase the the case
   * @param name The name to check
   * @param oldName The name it had before (if none: null)
   * @return the boolean
   */
  private Boolean checkForEpisodeInCase(Case theCase, String name, String oldName) {
    EpisodePattern episodePattern = Episode.getEpisodePattern(name);

    // gets the date fitting to the pattern
    Date theDate = Episode.getDateByPattern(name, episodePattern.getName());

    List<String> representations = new ArrayList<>();

    for (String patternName : EpisodePatterns.patterns.keySet()) {
      String representation = Episode.getStringByPattern(theDate, patternName);

      // Never check the old name
      if (representation.equals(oldName)) {
        continue;
      }

      // Pattern is quarter? then only check for other quarters
      if ((episodePattern.getIsDatePattern()
              && !EpisodePatterns.patterns.get(patternName).getIsDatePattern())
          || (!episodePattern.getIsDatePattern()
              && EpisodePatterns.patterns.get(patternName).getIsDatePattern())) {
        continue;
      }

      // Pattern is monthyear, then only check for other monthyears
      if ((episodePattern.getName().equals("MONTHYEAR")
              && !EpisodePatterns.patterns.get(patternName).getName().equals("MONTHYEAR"))
          || (!episodePattern.getName().equals("MONTHYEAR")
              && EpisodePatterns.patterns.get(patternName).getName().equals("MONTHYEAR"))) {
        continue;
      }

      representations.add(representation);
    }

    return Entity.entityExistsByPropertyAsChildOfParentID(
            getSessionBean().getDatabase(),
            OSSEVocabulary.Type.Episode,
            OSSEVocabulary.Episode.Name,
            representations,
            OSSEVocabulary.Type.Case,
            theCase.getId())
        != null;
  }

  /**
   * Saves the episode data.
   *
   * @return the string
   * @see AbstractViewBean#save()
   */
  @Override
  public String save() {
    if (dataObject.get("name") == null
        || (dataObject.get("name") instanceof String && "".equals(dataObject.get("name")))) {
      Utils.displayContextMessage("summary_addvisitfailed", "addvisit_nolabel");
      return null;
    }

    Resource episodeResource = (Resource) dataObject.get("episodeResource");
    Episode episode = new Episode(getSessionBean().getDatabase());
    String name = null;

    // If the episode name was saved as a date instance, save it in the
    // yyyy-MM-dd date format
    if (dataObject.get("name") instanceof Date) {
      SimpleDateFormat format;
      format = new SimpleDateFormat("yyyy-MM-dd");
      format.setTimeZone(TimeZone.getDefault());
      Calendar cal = Calendar.getInstance();
      cal.setTime((Date) dataObject.get("name"));
      name = format.format(cal.getTime());
    } else {
      name = (String) dataObject.get("name");
    }

    if (episodeResource != null) {
      if (!episodeResource
          .getProperty(OSSEVocabulary.Episode.Name)
          .getValue()
          .equalsIgnoreCase(name)) {
        // if we renamed an episode, check if another episode of that
        // name
        // exists already in the same case
        if (checkForEpisodeInCase(
            ((SessionBean) getSessionBean()).getCurrentCase(),
            name,
            episodeResource.getProperty(OSSEVocabulary.Episode.Name).getValue())) {
          Utils.displayContextMessage("summary_addvisitfailed", "addvisit_labelexists");
          return null;
        }
      }
    } else {
      // if we added a new episode, check if another episode of that names
      // exists already in the same case
      if (checkForEpisodeInCase(((SessionBean) getSessionBean()).getCurrentCase(), name, null)) {
        Utils.displayContextMessage("summary_addvisitfailed", "addvisit_labelexists");
        return null;
      }
    }

    episode.setParent(((SessionBean) getSessionBean()).getCurrentCase());
    episode.setProperty(OSSEVocabulary.Episode.Name, name);
    String optionalText = (String) dataObject.get("optionalText");
    episode.setProperty(Vocabulary.Episode.optionalText, optionalText);

    Date date = Episode.getDateByPattern(name, null);
    episode.setProperty("timestamp", new TimestampLiteral(date.getTime()));

    episode.addProperty(OSSEVocabulary.Episode.Case, episode.getParent(getConfig()).getResource());

    getSessionBean().getDatabase().beginTransaction();

    String summary = Utils.getResourceBundleString("summary_editvisitsuccess");
    String report = Utils.getResourceBundleString("visit_edited");

    if (episodeResource != null) {
      episode.setResource(episodeResource);
    }
    episode.saveOrUpdate();
    if (episodeResource == null) {
      if (episode.getResource() == null || episode.getResource().getId() == 0) {
        Utils.addContextMessage(
            Utils.getResourceBundleString("summary_addvisitfailed"),
            "You were not authorized to add another episode");
        return null;
      }
      String[] replaceArray = {name};
      summary = Utils.getResourceBundleString("summary_addvisitsuccess");
      report = Utils.getResourceBundleStringWithPlaceholders("visit_added", replaceArray);

      episodeResource = episode.getResource();
      ((SessionBean) getSessionBean())
          .getCurrentCase()
          .addChild(OSSEVocabulary.Type.Episode, episode);
    }

    getSessionBean().getDatabase().commit();

    ((SessionBean) Utils.getSB()).getCurrentCase().forceReload();
    ((SessionBean) getSessionBean())
        .getCurrentCase()
        .loadChildren(OSSEVocabulary.Type.Episode, false, getConfig());

    ((SessionBean) Utils.getSB())
        .setCurrentEpisode(
            (Episode)
                ((SessionBean) Utils.getSB())
                    .getCurrentCase()
                    .getChild(OSSEVocabulary.Type.Episode, name, getConfig()));

    getSessionBean().setCurrentFormName(null);
    Utils.addContextMessage(summary, report);

    // new addition for issue#19: Transfer data from previous episode when creating new one
    if (takePrevEpisodeData == true) {
      Episode currentEpisode = ((SessionBean) Utils.getSB()).getCurrentEpisode();
      Episode lastEpisode = getPreviousEpisode(currentEpisode);
      if (lastEpisode != null) {
        importPrevEpisodeDataInNew(currentEpisode, lastEpisode);
      }
    }
    ((SessionBean) getSessionBean())
        .switchEpisode(((SessionBean) Utils.getSB()).getCurrentEpisode());

    return "";
  }

  /**
   * JSF action method when clicking on the button to edit a certain episode. Saves the episodeData
   * into the dataObject and moves the user to the episode_edit page.
   *
   * @param episodeData the episode data we want to edit
   * @return JSF outcome string "editEpisode"
   */
  public String editEpisode(HashMap<String, Object> episodeData) {
    getSessionBean().setTempObject("dataObject", episodeData);
    Utils.goForm("visit_edit");
    return "editEpisode";
  }

  /**
   * Moves the user to the episode_edit page (which is used for adding episodes, too).
   *
   * @return JSF outcome string "addEpisode"
   */
  public String goAddEpisode() {
    getSessionBean().clearTempObject("dataObject");
    Utils.goForm("visit_edit");
    return "addEpisode";
  }

  /**
   * JSF action method when clicking on the button to edit the current episode, saves the episode's
   * data into the dataObject and moves the user to the episode_edit page.
   *
   * @return JSF outcome string "editEpisode"
   */
  public String goEditEpisode() {
    dataObject = new HashMap<>();

    // first we transform the name to a datestring of our current setting
    String name = ((SessionBean) getSessionBean()).getCurrentEpisode().getName();
    String oldName = name;
    Date date = Episode.getDateByPattern(name, null);
    if (date == null) {
      dataObject.put(OSSEVocabulary.Episode.Name, name);
    } else {
      name =
          Episode.getStringByPattern(
              date, ((ApplicationBean) Utils.getAB()).getEpisodePatternName());
      dataObject.put(OSSEVocabulary.Episode.Name, name);
    }

    if (!oldName.equalsIgnoreCase(name)) {
      dataObject.put("showLabelWasConvertedWarning", true);
      dataObject.put("oldName", oldName);
    }

    dataObject.put(
        Vocabulary.Episode.optionalText,
        ((SessionBean) getSessionBean()).getCurrentEpisode().getOptionalText());
    dataObject.put(
        "episodeResource", ((SessionBean) getSessionBean()).getCurrentEpisode().getResource());
    getSessionBean().setTempObject("dataObject", dataObject);
    Utils.goForm("visit_edit");
    return "editEpisode";
  }

  /**
   * Stores the episode selected for deletion.
   *
   * @param toDeleteEpisode the episode to delete
   */
  public void setToDeleteEpisode(Object toDeleteEpisode) {
    getSessionBean().setTempObject("dataObject", toDeleteEpisode);
  }

  /**
   * Deletes the episode that previously was selected for deletion.
   *
   * @return JSF outcome "deleteEpisode"
   */
  @SuppressWarnings("unchecked")
  public String deleteEpisode() {
    dataObject = (HashMap<String, Object>) getSessionBean().clearTempObject("dataObject");
    if (dataObject == null) {
      return "";
    }
    String name = (String) dataObject.get(OSSEVocabulary.Episode.Name);

    Episode episode = new Episode(getSessionBean().getDatabase());
    Resource visitResource =
        Entity.entityExistsByPropertyAsChildOfParentID(
            getSessionBean().getDatabase(),
            OSSEVocabulary.Type.Episode,
            OSSEVocabulary.Episode.Name,
            name,
            OSSEVocabulary.Type.Case,
            ((SessionBean) getSessionBean()).getCurrentCase().getId());
    episode.setResource(visitResource);
    episode.delete();

    ((SessionBean) Utils.getSB()).setCurrentEpisode(null);
    Utils.getSB().setCurrentFormName(null);

    ((SessionBean) Utils.getSB()).getCurrentCase().reload(getConfig());
    ((SessionBean) Utils.getSB())
        .getCurrentCase()
        .loadChildren(OSSEVocabulary.Type.Episode, false, getConfig());

    Utils.displayContextMessage("summary_deletevisitsuccess", "visit_deleted");
    Utils.getSB().goMainPatientPage();

    return "deleteEpisode";
  }

  /**
   * Deletes the current episode.
   *
   * @return JSF outcome "deleteCurrentEpisode"
   */
  public String deleteVisit() {
    if (((SessionBean) Utils.getSB()).getCurrentCase() == null) {
      Utils.getLogger().debug("SB.addVisit: No myCase!");
      return "failed";
    }

    if (((SessionBean) Utils.getSB()).getCurrentEpisode() == null) {
      Utils.getLogger().debug("Edit episode called with no current episode");
      return "failed";
    }

    if (!((SessionBean) Utils.getSB()).getCurrentEpisode().delete()) {
      Utils.getLogger().debug("SB.deleteepisode: Something went wrong! No editEpisode!");
      return "failed";
    }
    Utils.getSB()
        .addLog(
            "deleted episode "
                + ((SessionBean) Utils.getSB()).getCurrentEpisode().getName()
                + " of patient "
                + ((SessionBean) Utils.getSB()).getCurrentPatient().getId());

    Utils.getSB().clearValues();

    ((SessionBean) Utils.getSB()).setCurrentEpisode(null);
    Utils.getSB().setCurrentFormName(null);

    ((SessionBean) Utils.getSB()).getCurrentCase().reload(getConfig());
    ((SessionBean) Utils.getSB())
        .getCurrentCase()
        .loadChildren(OSSEVocabulary.Type.Episode, false, getConfig());

    Utils.addContextMessage(
        Utils.getResourceBundleString("summary_deletevisitsuccess"),
        Utils.getResourceBundleString("visit_deleted"));
    Utils.getSB().goMainPatientPage();

    return "deleteCurrentEpisode";
  }

  /**
   * TODO: add javadoc.
   */
  public Episode getPreviousEpisode(Episode currentEpisode) {
    Case currentCase = (Case) currentEpisode.getParent(getConfig());
    TreeMap<Long, Episode> myEpisodes = new TreeMap<>(Collections.reverseOrder());
    // fill the tree
    for (Entity theEpisode : currentCase.getChildren(OSSEVocabulary.Type.Episode, getConfig())) {

      Date theDate = Episode.getDateByPattern(((Episode) theEpisode).getName(), null);
      if (theDate != null) {
        Long compareKey = theDate.getTime();
        myEpisodes.put(compareKey, (Episode) theEpisode);
      }
    }

    Map.Entry<Long, Episode> theLast = null;
    Date myDate =
        Episode.getDateByPattern(((SessionBean) Utils.getSB()).getCurrentEpisode().getName(), null);
    if (myDate != null) {
      theLast = myEpisodes.higherEntry(myDate.getTime());
    }

    Episode theLastEpisode = null;

    // If there are no data to be imported go away
    if (theLast == null) {
      return null;
    } else {
      return theLastEpisode = theLast.getValue();
    }
  }

  /**
   * TODO: add javadoc.
   */
  public void importPrevEpisodeDataInNew(Episode newEpisodeToFill, Episode theLastEpisode) {

    // messages
    String successImport = Utils.getResourceBundleString("success_import");
    String summarySucceeded = Utils.getResourceBundleString("summary_importsuccess");
    String infoPartialData =
        Utils.getResourceBundleString(
            "de.samply.edc.osse.messages.osse_messages", "info_formImportPartialData");
    String summaryFailed = Utils.getResourceBundleString("summary_importfailed");
    String errorNoMatchingData =
        Utils.getResourceBundleString(
            "de.samply.edc.osse.messages.osse_messages", "error_formImportNoMatchingData");

    // importing previous data
    List<Entity> lastEpisodeForms = theLastEpisode.getForms(getConfig());
    Boolean status = newEpisodeToFill.importAllPreviousEpisodeForms2(lastEpisodeForms, getConfig());
    if (!status) {
      Utils.addContextMessage(summaryFailed, errorNoMatchingData);
    } else {
      // inform the user that the import was successful
      Utils.addContextMessage(summarySucceeded, successImport);
      ((SessionBean) Utils.getSB())
          .addLog(
              "imported last versions of open forms of the "
                  + " (visit "
                  + newEpisodeToFill.getName()
                  + ") from last "
                  + " (visit "
                  + theLastEpisode.getName()
                  + ") of patient "
                  + ((SessionBean) Utils.getSB()).getCurrentPatient().getId());
    }
  }
}
