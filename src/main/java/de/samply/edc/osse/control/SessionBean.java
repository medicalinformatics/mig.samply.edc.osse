/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.control;

import de.pseudonymisierung.mainzelliste.client.InvalidSessionException;
import de.pseudonymisierung.mainzelliste.client.MainzellisteConnection;
import de.pseudonymisierung.mainzelliste.client.MainzellisteNetworkException;
import de.pseudonymisierung.mainzelliste.client.Session;
import de.samply.common.http.HttpConnector;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.control.AbstractSessionBean;
import de.samply.edc.model.Entity;
import de.samply.edc.model.MenuItem;
import de.samply.edc.osse.model.Case;
import de.samply.edc.osse.model.Episode;
import de.samply.edc.osse.model.Form;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.Patient;
import de.samply.edc.osse.model.Permission;
import de.samply.edc.osse.model.Role;
import de.samply.edc.osse.model.User;
import de.samply.edc.osse.searchviewer.SigninTokenExchangeStorage;
import de.samply.edc.osse.utils.CookieHelper;
import de.samply.edc.osse.utils.MultiFactorAuthBean;
import de.samply.edc.osse.utils.OsseUtils;
import de.samply.edc.utils.Utils;
import de.samply.store.BasicDB;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.exceptions.FailCounterException;
import de.samply.store.exceptions.LoginFailedException;
import de.samply.store.exceptions.UserNotActivatedException;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import org.omnifaces.util.Faces;

/** The session scoped bean. */
@ManagedBean(name = "sessionBean")
@SessionScoped
public class SessionBean extends AbstractSessionBean {

  /** The Constant SESSION_USERNAME. */
  protected static final String SESSION_USERNAME = "username";
  /** The Constant serviceFailureCauseAUTH. */
  private static final String serviceFailureCauseAUTH = "auth";
  /** The Constant serviceFailureCauseMainzelliste. */
  private static final String serviceFailureCauseMainzelliste = "mainzelliste";
  /** The list of patients with reported forms. */
  protected List<Patient> patientsWithReportedForms = new ArrayList<>();
  /** The list of patients with episodes that have reported forms. */
  protected HashMap<String, List<Episode>> patientsWithReportedFormsVisitList = new HashMap<>();
  /** The list of patients with cases that have reported forms. */
  protected HashMap<String, List<Case>> patientsWithReportedFormsCaseList = new HashMap<>();
  /**
   * Used for printing out old revisions of forms to store where we came from e.g. you load
   * "basics-2" but in an old visit there is data for "basics-1"
   */
  protected String isOldForm = null;
  /** List of MenuItems for NavigationBar/Form-tabs. */
  protected ArrayList<MenuItem> navigationPatientForms = new ArrayList<>();
  /** Token used to signin for the query result viewer. */
  private String signinToken;
  /** The Query Result Id to display, if coming from the search viewer. */
  private Integer queryResultId;
  /** The mainzelliste session. */
  private transient Session mainzellisteSession = null;
  /** The mainzelliste session id. */
  private String mainzellisteSessionId = null;
  /** write permissions of the current user. */
  private HashMap<String, Permission> myWritePermissions;
  /** create permissions of the current user. */
  private HashMap<String, Permission> myCreatePermissions;
  /** permissions to change status of the current user. */
  private HashMap<String, HashMap<String, Boolean>> myChangeStatusPermissions;
  /** may the current user create patients?. */
  private Boolean mayCreatePatients = false;
  /** may the current user create a patient user account?. */
  private HashMap<String, Boolean> mayCreatePatientAccounts;
  /** may the current user create episode?. */
  private Boolean mayCreateEpisodes = false;
  /** may the current user export medical data?. */
  private Boolean mayExport = false;
  /** may always see IDAT no matter the location. */
  private Boolean mayAlwaysSeeIdat = false;
  /** may see IDAT of own location. */
  private Boolean maySeeIdat = false;
  /** may read all location's patients. */
  private Boolean mayReadAllMdat = false;
  /** In case there is a critical Sampy.AUTH service error, set this to true. */
  private Boolean criticalAuthError = false;
  /** In case there is a critical Mainzelliste service error, set this to true. */
  private Boolean criticalMainzellisteError = false;
  /** In case there is a critical Mainzelliste service error, set this to true. */
  private Boolean criticalFormEditorError = false;
  /** List of roles of the current user. */
  private List<Role> myRoles;

  /** List of SelectItems of roles (used by JSF). */
  private List<SelectItem> rolesSelectItems;

  /** The selected role of the current user. */
  private String selectedRole;

  /** The language selected by user. */
  private String language;

  private String currLogin;
  int loginFailsBeforLogin;

  // Experimental
  private Map<Integer, String> locations;

  /**
   * TODO: add javadoc.
   */
  public String getLocationName(int id) {
    if (locations == null) {
      locations = new HashMap<>();
    }
    if (!locations.containsKey(id)) {
      Resource locationResource = getDatabase().getResource(OSSEVocabulary.Type.Location, id);
      if (locationResource != null) {
        locations.put(id, locationResource.getProperty(OSSEVocabulary.Location.Name).getValue());
      }
    }
    return locations.get(id);
  }

  /**
   * TODO: add javadoc.
   */
  public String getLocationName(String locationUri) {
    try {
      return getLocationName(Integer.valueOf(locationUri.substring(locationUri.indexOf(":") + 1)));
    } catch (NullPointerException | NumberFormatException e) {
      return "unknown";
    }
  }

  private void cacheLocationNames() {
    ResourceQuery locationQuery = new ResourceQuery(OSSEVocabulary.Type.Location);
    locationQuery.setFetchAdjacentResources(false);
    ArrayList<Resource> locations = getDatabase().getResources(locationQuery);
    for (Resource location : locations) {
      this.locations.put(
          location.getId(), location.getProperty(OSSEVocabulary.Location.Name).getValue());
    }
  }

  /**
   * Gets the.
   *
   * @return the abstract database
   * @see de.samply.edc.control.AbstractSessionBean#get()
   */
  @Override
  public AbstractDatabase<?> get() {
    return new Database(this);
  }

  /**
   * Inits the.
   *
   * @see de.samply.edc.control.AbstractSessionBean#init()
   */
  @Override
  @PostConstruct
  public void init() {
    super.init();
    cacheLocationNames();
  }

  /**
   * Logout.
   *
   * @return the string
   * @see de.samply.edc.control.AbstractSessionBean#logout()
   */
  @Override
  public String logout() {
    // remove current user from the session map
    // FacesContext.getCurrentInstance().getExternalContext().getSessionMap().remove("user");
    clearVariables();
    return super.logout();
  }

  /** Checks the Samply.AUTH service. */
  public void checkAuthService() {
    try {
      if (OsseUtils.mdrFacesClientStuff()) {
        criticalAuthError = false;
        Utils.addContextMessage(
            "Service recovered",
            "Connection to the authentication service has been recovered. Normal "
                + "operations are reinitiated.");
        return;
      }
    } catch (InvalidKeyException | NoSuchAlgorithmException | SignatureException e) {
      e.printStackTrace();
    }
    Utils.addContextMessage(
        "Authentication service error",
        "The authentication service is not responding. Only registry administrators can "
            + "log in at this time.",
        FacesMessage.SEVERITY_FATAL);
  }

  /** Check the mainzelliste service. */
  public void checkMainzellisteService() {
    if (((ApplicationBean) Utils.getAB()).isBridgehead()) {
      return;
    }

    try {
      mainzellisteSession = getMainzellisteSession(true);
      mainzellisteSessionId = mainzellisteSession.getId();
    } catch (MainzellisteNetworkException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }

    if (mainzellisteSession == null || mainzellisteSessionId == null) {
      mainzellisteSession = null;
      mainzellisteSessionId = null;
      Utils.addContextMessage(
          "Mainzelliste service error",
          "The Mainzelliste service is not responding. Only registry administrators can "
              + "log in at this time.",
          FacesMessage.SEVERITY_FATAL);
    } else {
      criticalMainzellisteError = false;
      Utils.addContextMessage(
          "Service recovered",
          "Connection to the Mainzelliste service has been recovered. Normal operations "
              + "are reinitiated.");
    }
  }

  /**
   * Checks if the current user has multiple roles.
   *
   * @return true|false
   */
  public Boolean hasMultipleRoles() {
    return myRoles != null && myRoles.size() > 1;
  }

  /**
   * Switches the role of the current user to the give one.
   *
   * @param role the new role
   */
  private void switchRole(Role role) {
    clearCurrentObject(OSSEVocabulary.Type.Patient);
    clearCurrentObject(OSSEVocabulary.Type.Case);
    clearCurrentObject(OSSEVocabulary.Type.Episode);

    // renew Mainzelliste session to clear any cached tokens
    createNewMainzellisteSession();

    setCurrentRole(role);
    ((Database) getDatabase()).selectRole(getCurrentRole().getResource());
    getCurrentRole().load(getConfig());
    selectedRole = getCurrentRole().getName();

    currentFormName = null;
    clearNavigationPatientForms();

    defineMyRights();

    goStartPage();
  }

  /**
   * Action event called by JSF component to switch/change the role of the current user.
   *
   * @param event the action event
   */
  public void changeRoleAction(AjaxBehaviorEvent event) {
    changeRole();
  }

  /**
   * Action event called by JSF component to switch/change the role of the current user.
   *
   * @param event the action event
   */
  public void changeRoleAction(ValueChangeEvent event) {
    Object newValue = event.getNewValue();

    selectedRole = (String) newValue;
    changeRole();
  }

  /**
   * JSF action to change role, checks for the name and calls the switch role method.
   *
   * @return JSF outcome
   */
  public String changeRole() {
    Boolean weSwitchedRole = false;
    for (Role role : myRoles) {
      if (role.getName() == null) {
        role.load(getConfig());
      }

      if (role.getName().equalsIgnoreCase(selectedRole)) {
        weSwitchedRole = true;
        switchRole(role);
        break;
      }
    }

    if (!weSwitchedRole) {
      selectedRole = getCurrentRole().getName();
    }
    return "";
  }

  /**
   * Method to log in the user called by login form.
   *
   * @param login the login name
   * @param password the password
   * @return the boolean
   */
  public Boolean loginUser(String login, String password) {
    criticalAuthError = false;

    // no username given
    if (login == null || login.equals("")) {
      Utils.addContextMessage(
          "login",
          Utils.getResourceBundleString("summary_login"),
          Utils.getResourceBundleString("missingusername"));
      return false;
    }
    // no password given
    if (password == null || password.equals("")) {
      Utils.addContextMessage(
          "login",
          Utils.getResourceBundleString("summary_login"),
          Utils.getResourceBundleString("missingpassword"));
      return false;
    }

    login = login.toLowerCase();
    String language = getCurrentLanguage();

    try {
      getDatabase().login(login, password);

      clearVariables();

      setCurrentUser(new User(getDatabase()));

      this.myRoles = new ArrayList<>();
      List<Resource> myRoles = ((Database) getDatabase()).getRoles(true);
      for (Resource roleRes : myRoles) {
        Role myRole = new Role(getDatabase(), roleRes);

        // A user should never have a system role, if he has it push out
        // an error message to the log for now
        if (myRole.isSystemRole()) {
          if (!"admin".equalsIgnoreCase(login)) {
            Utils.getLogger().error("User with login " + login + " has systemrole!");
          }
          continue;
        }

        if (getCurrentRole() == null) {
          setCurrentRole(myRole);
          ((Database) getDatabase()).selectRole(getCurrentRole().getResource());
          getCurrentRole().load(getConfig());
          selectedRole = getCurrentRole().getName();
        } else {
          myRole.load(getConfig());
        }

        this.myRoles.add(myRole);
      }

      Resource userResource = getDatabase().findUser(login);
      getCurrentUser().setResource(userResource);
      getCurrentUser().load(getConfig());

      // put current user into the session map
      FacesContext.getCurrentInstance()
          .getExternalContext()
          .getSessionMap()
          .put("user", getCurrentUser());

      if (getCurrentUser().hasDefaultRole()) {
        if (!getCurrentUser().getDefaultRole().equals(selectedRole)) {
          for (Role role : this.myRoles) {
            if (role.getName().equals(getCurrentUser().getDefaultRole())) {
              setCurrentRole(role);
              ((Database) getDatabase()).selectRole(getCurrentRole().getResource());
              selectedRole = getCurrentRole().getName();
              break;
            }
          }
        }
      }

      // Defining my write rights
      defineMyRights();
      return true;

    } catch (FailCounterException e) {
      logout();
      Utils.changeLanguage(language);
      Utils.addContextMessage(
          "login",
          Utils.getResourceBundleString("summary_login"),
          Utils.getResourceBundleString("accountlocked"));
      Utils.getLogger().error("User was denied access. Account locked.");
      return false;
    } catch (LoginFailedException e) {
      Utils.getLogger().debug("Login failed.");
      logout();
      Utils.changeLanguage(language);
      Utils.addContextMessage(
          "login",
          Utils.getResourceBundleString("summary_login"),
          Utils.getResourceBundleString("loginfailure"));
      return false;
    } catch (UserNotActivatedException e) {
      Utils.getLogger().debug("Login failed. User account not active.");
      logout();
      Utils.changeLanguage(language);
      Utils.addContextMessage(
          "login",
          Utils.getResourceBundleString("summary_login"),
          Utils.getResourceBundleString("loginnotactivated"));
      return false;
    } catch (DatabaseException e) {
      logout();
      Utils.changeLanguage(language);
      Utils.getLogger().debug("Login failed.", e);
    }

    Utils.addContextMessage(
        "login",
        Utils.getResourceBundleString("summary_login"),
        Utils.getResourceBundleString("loginfailure"));
    return false;
  }

  /**
   * Refuse login. Called in case of service errors (AUTH, Mainzelliste). Checks if the user has an
   * admin role and, if yes, logs him in into that role. Otherwise, the login is rejected.
   *
   * @param cause the cause for refusing login (serviceFailureCauseAUTH,
   *     serviceFailureCauseMainzelliste)
   * @return the boolean
   */
  private Boolean refuseLogin(String cause) {
    // check if user has a global admin role, then select that one, and
    // remove all other roles for this session
    // otherwise logout the user with an error
    Role theAdminRole = null;

    for (Role role : this.myRoles) {
      if (role.isAdminRole()) {
        theAdminRole = role;
      }
    }

    Boolean userIsAlsoAdmin = false;

    if (theAdminRole != null) {
      setCurrentRole(theAdminRole);
      ((Database) getDatabase()).selectRole(getCurrentRole().getResource());
      selectedRole = getCurrentRole().getName();
      this.myRoles.clear();
      this.myRoles.add(theAdminRole);

      if (cause.equalsIgnoreCase(serviceFailureCauseAUTH)) {
        String subject = "Authentication service error";
        String message = "The authentication service is not responding. "
            + "Only registry administrators can log in at this time.";
        Utils.addContextMessage(subject, message, FacesMessage.SEVERITY_FATAL);
        Utils.getLogger().error(subject + ": " + message);
        criticalAuthError = true;
      } else if (cause.equalsIgnoreCase(serviceFailureCauseMainzelliste)) {
        String subject = "Mainzelliste service error";
        String message = "The Mainzelliste service is not responding. "
            + "Only registry administrators can log in at this time.";
        Utils.addContextMessage(subject, message, FacesMessage.SEVERITY_FATAL);
        Utils.getLogger().error(subject + ": " + message);
        criticalMainzellisteError = true;
      }
      userIsAlsoAdmin = true;
    } else {
      // no admin, no cry
      if (cause.equalsIgnoreCase(serviceFailureCauseAUTH)) {
        String subject = "Mainzelliste service error";
        String message = "The authentication service is not responding. "
            + "Please contact your registry administrator.";
        Utils.addContextMessage(subject, message, FacesMessage.SEVERITY_FATAL);
        Utils.getLogger().error(subject + ": " + message);
      } else if (cause.equalsIgnoreCase(serviceFailureCauseMainzelliste)) {
        String subject = "Mainzelliste service error";
        String message = "The Mainzelliste service is not responding. "
            + "Please contact your registry administrator.";
        Utils.addContextMessage(subject, message, FacesMessage.SEVERITY_FATAL);
        Utils.getLogger().error(subject + ": " + message);
      }

      logout();
    }

    return userIsAlsoAdmin;
  }

  /**
   * Show identifying data (IDAT) of current patient to the current user?.
   *
   * @return the boolean
   */
  public Boolean showIdatOfCurrentPatient() {
    if (mayAlwaysSeeIdat) {
      return true;
    }

    if (getCurrentCase() != null) {
      if (getCurrentCase()
          .getLocation(getConfig())
          .getName()
          .equals(getCurrentRole().getLocation().getName())) {
        return maySeeIdat;
      } else {
        return false;
      }
    }

    return false;
  }

  /**
   * May the current user write current episode?.
   *
   * @return the boolean
   */
  public Boolean mayWriteCurrentEpisode() {
    return mayWrite(
        getCurrentCase().getLocation(getConfig()).getName(), OSSEVocabulary.Type.Episode);
  }

  /**
   * May the current user write current form?.
   *
   * @return the boolean
   */
  public Boolean mayWriteCurrentForm() {
    return mayWrite(
        getCurrentCase().getLocation(getConfig()).getName(), OSSEVocabulary.Type.CaseForm);
  }

  /**
   * May the current user write current medical form?.
   *
   * @return the boolean
   */
  public Boolean mayWriteCurrentEpisodeForm() {
    return mayWrite(
        getCurrentCase().getLocation(getConfig()).getName(), OSSEVocabulary.Type.EpisodeForm);
  }

  /**
   * Returns whether the current user is allowed to read patient's data from a specific location.
   *
   * @param location The location to check read permissions.
   * @return True if the user is allowed to read that location's patient's data.
   */
  public Boolean mayRead(Location location) {
    return getMayReadAllMdat() || location.equals(getCurrentRole().getLocation());
  }

  /**
   * May the current user write a certain entity-type in a given location.
   *
   * @param location the location
   * @param type the OSSEVocabulary.Type.* type
   * @return the boolean
   */
  public Boolean mayWrite(String location, String type) {
    // On a query result display, nothing may be written
    if (showQueryResult()) {
      return false;
    }

    if (myWritePermissions.containsKey(location)) {
      if (OSSEVocabulary.Type.Patient.equals(type)) {
        return myWritePermissions.get(location).getEntityPatient();
      }
      if (OSSEVocabulary.Type.Case.equals(type)) {
        return myWritePermissions.get(location).getEntityCase();
      }
      if (OSSEVocabulary.Type.Episode.equals(type)) {
        return myWritePermissions.get(location).getEntityEpisode();
      }
      if (OSSEVocabulary.Type.CaseForm.equals(type)) {
        return myWritePermissions.get(location).getEntityForm();
      }
      if (OSSEVocabulary.Type.EpisodeForm.equals(type)) {
        return myWritePermissions.get(location).getEntityForm();
      }
    }

    return false;
  }

  /**
   * May the current user create a certain entity-type in a given location.
   *
   * @param location the location
   * @param type the type
   * @return the boolean
   */
  public Boolean mayCreate(String location, String type) {
    // On a query result display, nothing may be created
    if (showQueryResult()) {
      return false;
    }

    if (myCreatePermissions.containsKey(location)) {
      if (OSSEVocabulary.Type.Patient.equals(type)) {
        return myCreatePermissions.get(location).getEntityPatient();
      }
      if (OSSEVocabulary.Type.Case.equals(type)) {
        return myCreatePermissions.get(location).getEntityCase();
      }
      if (OSSEVocabulary.Type.Episode.equals(type)) {
        return myCreatePermissions.get(location).getEntityEpisode();
      }
      if (OSSEVocabulary.Type.CaseForm.equals(type)) {
        return myCreatePermissions.get(location).getEntityForm();
      }
      if (OSSEVocabulary.Type.EpisodeForm.equals(type)) {
        return myCreatePermissions.get(location).getEntityForm();
      }
    }

    return false;
  }

  /**
   * May the current user create a patient account for the given location?.
   *
   * @param location the location
   * @return the boolean
   */
  public Boolean mayCreatePatientAccount(String location) {
    if (mayCreatePatientAccounts.get(location) == null) {
      return false;
    }

    return mayCreatePatientAccounts.get(location);
  }

  /** Loads the rights of the current user and prepares certain may* variables. */
  private void defineMyRights() {
    if (myWritePermissions == null) {
      myWritePermissions = new HashMap<>();
    } else {
      myWritePermissions.clear();
    }

    if (myCreatePermissions == null) {
      myCreatePermissions = new HashMap<>();
    } else {
      myCreatePermissions.clear();
    }

    if (myChangeStatusPermissions == null) {
      myChangeStatusPermissions = new HashMap<>();
    } else {
      myChangeStatusPermissions.clear();
    }

    mayCreatePatients = false;
    mayCreatePatientAccounts = new HashMap<>();
    mayCreateEpisodes = false;
    mayExport = false;
    mayReadAllMdat = false;

    maySeeIdat = false;
    mayAlwaysSeeIdat = false;

    for (Entity permission : getCurrentRole().getPermissions(getConfig())) {

      if ("export".equalsIgnoreCase(((Permission) permission).getAccess())) {
        mayExport = true;
      }

      if ("idatReader".equalsIgnoreCase(((Permission) permission).getAccess())) {
        mayAlwaysSeeIdat = true;
      }

      if ("myIdatReader".equalsIgnoreCase(((Permission) permission).getAccess())) {
        maySeeIdat = true;
      }

      if ("write".equalsIgnoreCase(((Permission) permission).getAccess())) {
        if (((Permission) permission).getLocation() != null) {
          String locName = ((Permission) permission).getLocation().getName();
          myWritePermissions.put(locName, (Permission) permission);
        }
      }

      if ("create".equalsIgnoreCase(((Permission) permission).getAccess())) {
        if (((Permission) permission).getLocation() != null) {
          String locName = ((Permission) permission).getLocation().getName();
          myCreatePermissions.put(locName, (Permission) permission);

          if (((Permission) permission).getEntityPatient()) {
            mayCreatePatients = true;
          }
        }
      }

      if ("executeAction".equalsIgnoreCase(((Permission) permission).getAccess())) {
        String permissionName = ((Permission) permission).getName();
        if (permissionName != null) {
          String locName = ((Permission) permission).getLocation().getName();

          if (permissionName.equalsIgnoreCase("changeStatus")) {
            HashMap<String, Boolean> tempMap = new HashMap<>();
            if (myChangeStatusPermissions.get(locName) != null) {
              tempMap = myChangeStatusPermissions.get(locName);
            }
            tempMap.put("" + ((Permission) permission).getStatusID(), true);

            myChangeStatusPermissions.put(locName, tempMap);
          } else if (permissionName.equalsIgnoreCase("createPatientUser")) {
            mayCreatePatientAccounts.put(locName, true);
          }
        }
      }

      // a "read" permission without a location equals the right to read all locations patients
      if ("read".equalsIgnoreCase(((Permission) permission).getAccess())) {
        if (((Permission) permission).getEntityPatient()
            && ((Permission) permission).getEntityCase()
            && ((Permission) permission).getEntityEpisode()
            && ((Permission) permission).getLocation() == null) {
          mayReadAllMdat = true;
        }
      }
    }
  }


  /**
   * Login form action. Checks for username and password and starts a Mainzelliste session on login
   *
   * @return String navigation string for JSF (see faces-config) @
   */
  @Override
  public String login() {
    super.login();
    Utils.getLogger().info(
        (String) formvars.getEntry("username") + "(" + FacesContext.getCurrentInstance()
            .getExternalContext().getSessionId(false) + ")");


    this.currLogin = (String) formvars.getEntry("username");
    loginFailsBeforLogin = OsseUtils.getFailCount(currLogin);


    if (!loginUser(
        (String) formvars.getEntry("username"), (String) formvars.getEntry("password"))) {
      addLog("login failed " + formvars.getEntry("username"));
      clearCurrentUser();
      formvars.clearEntries();
      return "failed";
    } else {
      addLog("login of user " + formvars.getEntry("username"));
      formvars.clearEntries();

      // set the session
      HttpServletRequest request =
          (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
      request.getSession().setAttribute(SESSION_USERNAME, getCurrentUser().getUsername());

      addLog("login by " + getCurrentUser().getUsername() + " from " + Utils.getClientIP());

      currentFormName = "";

      // Bridgehead has no ML
      if (!((ApplicationBean) Utils.getAB()).isBridgehead()) {
        try {
          mainzellisteSession = getMainzellisteSession(true);
          mainzellisteSessionId = mainzellisteSession.getId();
        } catch (MainzellisteNetworkException e) {
          e.printStackTrace();
        }

        if (mainzellisteSession == null || mainzellisteSessionId == null) {
          mainzellisteSession = null;
          mainzellisteSessionId = null;
          if (!refuseLogin(serviceFailureCauseMainzelliste)) {
            clearCurrentUser();
            return "failed";
          }
        }
      }

      //check if multi factor auth is activated
      if (MultiFactorAuthBean.isToTpActived()) {
        //user level
        Database mfaDb = new Database(true);
        Resource userResourceMultiFactorAuth = mfaDb.findUser(currLogin);
        mfaDb = null;
        Value mfaValue = userResourceMultiFactorAuth.getProperty(
            MultiFactorAuthBean.authMfaAuthenticator);
        if (mfaValue != null && mfaValue.asBoolean()) {
          //multifactor auth needed, restore failCount as login is not yet complete
          OsseUtils.setFailCount(currLogin, loginFailsBeforLogin);
          //check fail count before redirect
          if (!OsseUtils.hasLoginTrysLeft(currLogin)) {
            logout();
          }
          Utils.getLogger()
              .debug("multi factor auth for user " + currLogin + " required, redirect...");
          return "login_mfa";
        }
      }

      return finishLogin();
    }
  }

  /**
   * Handle navigation after login.
   * @return the navigation case.
   */
  public String finishLogin() {
    Role myRole = getCurrentRole();
    if (myRole != null && myRole.isAdminRole()) {
      goPage("admin.xhtml");
      //return "adminlogin";
      return "";
    }

    if (myRole != null && myRole.isLocalAdminRole()) {
      return "userlist";
    }

    if (getCurrentRole().isPatientRole()) {
      return goPatientuserStartpage();

    }

    if (isUseDashboard()) {
      goPage("dashboard.xhtml");
      return "";
    }

    return "patientlist";
  }

  /**
   * Use dashboard when dashboard is enable in admin page.
   * @return use dashboard or not.
   */
  public boolean isUseDashboard() {
    String confString =
        Utils.getStringOfJsonResource(
            ((ApplicationBean) Utils.getAB()).getDesignConfig(), DesignBean.useDashboard);

    return Boolean.parseBoolean(confString);
  }


  /**
   * Go patientuser startpage.
   *
   * @return the string
   */
  public String goPatientuserStartpage() {
    // load patient, set current patient etc
    ResourceQuery queryPatients = new ResourceQuery(OSSEVocabulary.Type.Patient);
    List<Resource> resultPatients = getDatabase().getResources(queryPatients);
    if (resultPatients == null || resultPatients.isEmpty()) {
      Utils.getLogger().debug("Patient login without any patients!");
      clearCurrentUser();
      return "failed";
    }

    Patient patient = new Patient(getDatabase(), resultPatients.get(0));
    patient.load(getConfig());
    patient.loadChildren(OSSEVocabulary.Type.Case, true, getConfig());
    Location myLocation = getCurrentRole().getLocation();
    Case theCase = null;
    for (Entity patientCase : patient.getCases(getConfig())) {
      if (patientCase.isDeleted()) {
        continue;
      }

      if (((Case) patientCase).getLocation(getConfig()).getName().equals(myLocation.getName())) {
        theCase = (Case) patientCase;
      }
    }

    if (theCase == null) {
      Utils.getLogger().debug("Patient login without any cases!");
      clearCurrentUser();
      return "failed";
    }

    mayAlwaysSeeIdat = true;

    switchPatient(patient, theCase.getResource());
    return "";
  }

  /**
   * Gets the mainzelliste connection.
   *
   * @return the mainzelliste connection
   */
  public MainzellisteConnection getMainzellisteConnection() {
    try {
      HttpConnector hc = ApplicationBean.getHttpConnector();
      String apiKey = Utils.getAB().getConfig().getString(Vocabulary.Config.Mainzelliste.apikey);

      // TODO: This may cause problems only if the Mainzelliste is not
      // accessed via https and we got a https-proxy defined
      // that is different from the http client
      return new MainzellisteConnection(
          OsseUtils.getMainzellisteInternalRestUrl(), apiKey, hc.getHttpClientForHTTPS());
    } catch (URISyntaxException e) {
      e.printStackTrace();
    }

    return null;
  }

  /**
   * Gets the mainzelliste session.
   *
   * @param validate check if the session is still active (not recommended for bulk operations)
   * @return the mainzelliste session
   * @throws MainzellisteNetworkException if a network error occurs
   */
  public Session getMainzellisteSession(boolean validate) throws MainzellisteNetworkException {

    if (mainzellisteSession == null) {
      MainzellisteConnection mlc = getMainzellisteConnection();
      if (mainzellisteSessionId != null) {
        try {
          mainzellisteSession = mlc.readSession(mainzellisteSessionId);
        } catch (InvalidSessionException e) {
          mainzellisteSession = mlc.createSession();
          mainzellisteSessionId = mainzellisteSession.getId();
        }
      } else {
        mainzellisteSession = mlc.createSession();
        mainzellisteSessionId = mainzellisteSession.getId();
      }
    }
    if (validate) {
      checkMainzellisteSession();
    }
    return mainzellisteSession;
  }

  /**
   * Creates a new MainzellisteSession.
   */
  public void createNewMainzellisteSession() {
    MainzellisteConnection mlc = getMainzellisteConnection();
    try {
      mainzellisteSession = mlc.createSession();
      mainzellisteSessionId = mainzellisteSession.getId();
    } catch (MainzellisteNetworkException e) {
      Utils.getLogger().warn("Unable to create a new mainzelliste session!",e);
    }
  }

  /**
   * Check if MainzellisteSession is valid and if not create a new.
   */
  public void checkMainzellisteSession() {
    if (mainzellisteSession != null) {
      try {
        boolean valid = mainzellisteSession.isValid();
        if (valid) {
          return;
        } else {
          createNewMainzellisteSession();
        }
      } catch (MainzellisteNetworkException e) {
        e.printStackTrace();
      }
    }
    createNewMainzellisteSession();
  }

  /**
   * Try to retrieve language setting from cookie.
   */
  public String getLanguage() {
    CookieHelper cookieHelper = new CookieHelper();
    Cookie languageCookie = cookieHelper.getCookie("lang");
    String cookieLanguage = null;
    String appLanguage = Utils.getLanguage();

    if (languageCookie != null) {
      cookieLanguage = languageCookie.getValue();
      if (!appLanguage.equals(cookieLanguage)) {
        changeLanguage(cookieLanguage);
        return cookieLanguage;
      }
    }

    return appLanguage;
  }

  /**
   * Get the the current language setting.
   */
  public String getCurrentLanguage() {
    return Utils.getLanguage();
  }

  /**
   * Get the language which is disabled.
   *
   * @return language which is disabled
   */
  public String getDisabledLanguage() {

    String language = getCurrentLanguage();

    if (getCurrentLanguage() == "en") {
      language = "de";

    } else if (getCurrentLanguage() == "de") {
      language = "en";
    }

    return language;
  }

  /**
   * Automatically switching between English and German by fetching the current language with
   * getLanguage(), reset language cookie.
   */
  public void switchLanguage() {
    if (getCurrentLanguage() == "en") {
      changeLanguage("de");
    } else if (getCurrentLanguage() == "de") {
      changeLanguage("en");
    }
  }

  /**
   * Change language.
   *
   * @param languageCode the language code
   * @see de.samply.edc.control.AbstractSessionBean#changeLanguage(java.lang.String)
   */
  @Override
  public void changeLanguage(String languageCode) {
    userChangedLocale = true;
    Utils.changeLanguage(languageCode);
    locale = new Locale(languageCode);
    ((ApplicationBean) Utils.getAB()).initFormLists();
    language = languageCode;

    CookieHelper cookieHelper = new CookieHelper();
    cookieHelper.setCookie("lang", language);
  }

  /**
   * Change language and store browser history.
   * It is important for history-back-button in any browser.
   * @param languageCode the language code
   * @see de.samply.edc.control.AbstractSessionBean#changeLanguage(java.lang.String)
   */
  public String changeLanguageAndStoreBrowserHistory(String languageCode) {
    changeLanguage(languageCode);
    HttpServletRequest request =
        (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
    String uri = request.getRequestURI();
    if (uri != null) {
      return uri + "?faces-redirect=true";
    } else {
      return "";
    }
  }

  /**
   * Checks if is current form is a medical form. A form with the name "test-1" is always treated as
   * medical.
   *
   * @return the boolean
   */
  public Boolean isCurrentFormMedical() {
    // In case of form "test-1" always say it's a medical form, so it can be
    // displayed
    if (Vocabulary.Development.testFormName.equals(currentFormName)) {
      return true;
    }

    return ((ApplicationBean) Utils.getAB()).isMedicalForm(getCurrentFormName());
  }

  /**
   * Checks if current form is the form named "noform".
   *
   * @return the boolean
   */
  public Boolean isCurrentFormNoForm() {
    return "noform".equals(currentFormName);
  }

  /**
   * Checks if is current form is unique (aka a case form).
   *
   * @return the boolean
   */
  public Boolean isCurrentFormUnique() {
    return ((ApplicationBean) Utils.getAB()).isUniqueForm(getCurrentFormName());
  }

  /**
   * Loads the navigation menu for a patient.
   *
   * @param chosenForm the chosen form
   */
  public void loadNavigationPatientForms(String chosenForm) {
    if (chosenForm == null) {
      initNavigationPatientForms(Utils.getFallbackPage());
    } else {
      initNavigationPatientForms(chosenForm);
    }
  }

  /** Loads the form list with state icons for navigation/breadcrumbs. */
  public void loadNavigationPatientForms() {
    if (currentFormName == null || currentFormName.equals("")) {
      currentFormName = Utils.getFallbackPage();
    }

    initNavigationPatientForms(currentFormName);
  }

  /**
   * Inits the navigation patient forms.
   *
   * @param chosenForm the chosen form
   */
  protected void initNavigationPatientForms(String chosenForm) {
    Episode currentEpisode = getCurrentEpisode();
    Case currentCase = getCurrentCase();

    // make sure the episodeforms are freshly loaded
    if (currentEpisode != null) {
      currentEpisode.load(getConfig());
      currentEpisode.loadEpisodeForms(getConfig());
      setCurrentEpisode(currentEpisode);
    }

    // make sure the caseforms are freshly loaded
    if (currentCase != null) {
      currentCase.load(getConfig());
      currentCase.loadChildren(OSSEVocabulary.Type.CaseForm, false, getConfig());
      setCurrentCase(currentCase);
    }

    LinkedHashMap<String, String> caseForms =
        ((ApplicationBean) Utils.getAB()).getFormulars(getLocale());

    if (caseForms == null) {
      Utils.getLogger().error("MenutItems.init: Formulars is null! (There are probably no forms "
          + "in configuration yet");
      return;
    }

    navigationPatientForms = new ArrayList<>();

    Set<String> keys = caseForms.keySet();

    for (String key : keys) {
      String kkeyreplace = null;

      String formStatusText = "unused";

      if (getCurrentRole().isPatientRole()) {
        if (!((ApplicationBean) Utils.getAB()).isPatientForm(key)) {
          continue;
        }
      }

      if (currentCase != null) {
        Form form = currentCase.getForm(key, getConfig());

        if (form != null) {
          // We got our form

          formStatusText = form.getFormState();
        } else {
          // In case we're displaying an older version of the form, we
          // need to find which version first

          HashMap<String, Object> formVersionData = Utils.getFormVersionData(key);
          Integer formVersion = (Integer) formVersionData.get("version");
          String formName = (String) formVersionData.get("name");

          if (formVersion != null && formVersion > 1) {
            while (formVersion > 1) {
              formVersion--;
              String toFindForm = formName + "-" + formVersion;

              form = currentCase.getForm(toFindForm, getConfig());
              if (form != null) {
                formStatusText = form.getFormState();

                kkeyreplace = toFindForm;
                break;
              }
            }
          }
        }
      }

      String isChosen = kkeyreplace == null ? key : kkeyreplace;

      if (isChosen.equals(chosenForm)) {
        navigationPatientForms.add(
            new MenuItem(
                kkeyreplace == null ? key : kkeyreplace,
                caseForms.get(key),
                "selected",
                "",
                "",
                formStatusText));
      } else {
        navigationPatientForms.add(
            new MenuItem(
                kkeyreplace == null ? key : kkeyreplace,
                caseForms.get(key),
                "",
                "",
                "",
                formStatusText));
      }
    }

    LinkedHashMap<String, String> episodeForms =
        ((ApplicationBean) Utils.getAB()).getVisitFormulars(getLocale());

    if (episodeForms == null) {
      Utils.getLogger().error("MenutItems.init: visitFormulars is null! (There are probably no "
          + "episode forms in configuration yet");
      return;
    }

    for (String key : episodeForms.keySet()) {
      String kkeyreplace = null;
      String formStatusText = "unused";

      if (getCurrentRole().isPatientRole()) {
        if (!((ApplicationBean) Utils.getAB()).isPatientForm(key)) {
          continue;
        }
      }

      if (currentEpisode != null) {
        Form form = currentEpisode.getForm(key, getConfig());

        if (form != null) {
          // We got our form

          formStatusText = form.getFormState();
        } else {
          // In case we're displaying an older version of the form, we
          // need to find which version first

          HashMap<String, Object> formVersionData = Utils.getFormVersionData(key);
          Integer formVersion = (Integer) formVersionData.get("version");
          String formName = (String) formVersionData.get("name");

          if (formVersion != null && formVersion > 1) {
            while (formVersion > 1) {
              formVersion--;
              String toFindForm = formName + "-" + formVersion;

              form = currentEpisode.getForm(toFindForm, getConfig());
              if (form != null) {
                formStatusText = form.getFormState();
                kkeyreplace = toFindForm;
                break;
              }
            }
          }
        }
      }

      String isChosen = kkeyreplace == null ? key : kkeyreplace;

      if (isChosen.equals(chosenForm)) {
        navigationPatientForms.add(
            new MenuItem(
                kkeyreplace == null ? key : kkeyreplace,
                episodeForms.get(key),
                "selected",
                "subitem",
                "",
                formStatusText));
      } else {
        navigationPatientForms.add(
            new MenuItem(
                kkeyreplace == null ? key : kkeyreplace,
                episodeForms.get(key),
                "",
                "subitem",
                "",
                formStatusText));
      }
    }
  }

  /**
   * Switch episode to the given one.
   *
   * @param toGoTo the episode to switch to
   * @return JSF outcome
   */
  public String switchEpisode(Episode toGoTo) {
    Boolean isEpisodeForm = ((ApplicationBean) Utils.getAB()).isEpisodeForm(currentFormName);

    if (getCurrentEpisode() == null
        || currentFormName == null
        || (getCurrentEpisode() != null && !isEpisodeForm)) {
      // no current Episode or had no form set, or the current form is no
      // episode form,
      // so we go initially to the standard form
      if (isPatientUser()) {
        currentFormName = ((ApplicationBean) Utils.getAB()).getPatientUserEpisodeMainPage();
      } else {
        currentFormName =
            (String) Utils.getAB().getConfig().getProperty(Vocabulary.Config.Form.visitStandard);
      }
    }

    setCurrentEpisode(toGoTo);

    getCurrentEpisode().setParent(getCurrentCase());
    getCurrentEpisode().load(getConfig());
    getCurrentEpisode().loadEpisodeFormsOnce(getConfig());

    if (currentFormName != null) {
      // revisioning compatibility
      // we came from befunde-2 and now are in a visit where there is only
      // a befunde-1 (or vice versa)
      // so we need to redirect to the right form

      // 1) get the number of the form in the old tree
      Integer index = getIndexOfCurrentForm(currentFormName);

      loadNavigationPatientForms(currentFormName);
      String realFormName = getNameOfIndex(index);

      if (!currentFormName.equals(realFormName) && realFormName != null) {
        currentFormName = realFormName;
      }
    }
    goEpisodeForm(currentFormName);
    return "";
  }

  /**
   * redirects the user to an episode form.
   *
   * @param form the form
   */
  public void goEpisodeForm(String form) {
    if (form == null || "".equals(form)) {
      goMainPatientPage();
    }

    currentFormName = form;
    String page = "episode.xhtml?form=" + form;

    try {
      Utils.redirectToPage(page);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  /**
   * Sets the currently displayed form and prepares the navigation list.
   *
   * @param form the new form
   * @see de.samply.edc.control.AbstractSessionBean#setForm(java.lang.String)
   */
  @Override
  public void setForm(String form) {
    this.currentFormName = form;
    loadNavigationPatientForms();
  }

  /**
   * Gets the index of current form in the navigation list.
   *
   * @param currentForm the current form
   * @return the index of current form
   */
  public Integer getIndexOfCurrentForm(String currentForm) {
    if (currentForm == null) {
      return null;
    }

    if (navigationPatientForms == null || navigationPatientForms.size() < 1) {
      return null;
    }

    Integer index = 0;

    for (MenuItem entry : navigationPatientForms) {
      if (entry.getUrl().equals(currentForm)) {
        return index;
      }

      index++;
    }

    return null;
  }

  /**
   * Gets the name of a form at a given index in the navigation list.
   *
   * @param index the index of the form in the navigation list
   * @return the name of the requested form
   */
  public String getNameOfIndex(Integer index) {
    if (index == null) {
      return null;
    }

    if (navigationPatientForms == null
        || navigationPatientForms.size() < 1
        || navigationPatientForms.size() < index) {
      return null;
    }

    return navigationPatientForms.get(index).getUrl();
  }

  /**
   * Gets the navigation list of patient forms.
   *
   * @return the navigation patient forms
   */
  public ArrayList<MenuItem> getNavigationPatientForms() {
    return navigationPatientForms;
  }

  /** Clears the navigation list of patient forms. */
  public void clearNavigationPatientForms() {
    if (navigationPatientForms == null) {
      return;
    }
    navigationPatientForms.clear();
  }

  /**
   * Re-init case and episode. Reloads the data of the case, the episodes and the forms under
   * either.
   *
   * @return the boolean
   */
  public Boolean reinitCaseAndEpisode() {
    if (getCurrentCase() == null) {
      return true;
    }

    getCurrentCase().reload(getConfig());
    getCurrentCase().loadChildren(OSSEVocabulary.Type.Episode, false, getConfig(), true);
    getCurrentCase().loadChildren(OSSEVocabulary.Type.CaseForm, false, getConfig(), true);
    for (Entity episode : getCurrentCase().getChildren(OSSEVocabulary.Type.Episode, getConfig())) {
      ((Episode) episode).loadEpisodeForms(getConfig());
    }

    if (getCurrentEpisode() == null) {
      return true;
    }

    // dies kann null werden falls jemand anders in der Zwischenzeit den
    // Label umbenannt hat oder den Visit gelöscht hat!
    setCurrentEpisode(
        (Episode)
            getCurrentCase()
                .getChild(OSSEVocabulary.Type.Episode, getCurrentEpisode().getName(), getConfig()));
    if (getCurrentEpisode() == null) {
      // this may happen, in case someone changed the label of our chosen
      // visit meanwhile (or even deleted it)!
      // in this case throw the user back to the start page as we cannot
      // recover from this
      // well we could recover, if we find the visit in the new list by
      // the timestamp (creation date)
      // but unfortunately it is not 100% certain that timestamp is unique
      // (due to import)

      Utils.getLogger()
          .error("SessionBean.reinitCaseAndVisit: new currentVisit is null while old was not!");
      currentFormName = null;
      goMainPatientPage();
      return false;
    }

    return true;
  }

  /**
   * Action for pressing on the show forms to sign link. TODO: this has yet to be activated in OSSE,
   * signer.xhtml needs styling.
   */
  public void goSignForms() {
    setCurrentEpisode(null);
    setCurrentPatient(null);
    loadSignerPatients();
    Utils.goAdminForm("signer");
  }

  /**
   * Switch patient by list.
   *
   * @param patientToSwitchTo the patient to switch to
   * @return the string
   */
  public String switchPatientByList(HashMap<String, Object> patientToSwitchTo) {
    Boolean isDeleted = (Boolean) patientToSwitchTo.get("isDeleted");
    // do not switch to deleted patients/cases
    if (isDeleted) {
      return null;
    }

    getDatabase().startCaching();
    ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Location);
    query.setFetchAdjacentResources(false);
    getDatabase().getResources(query);

    Resource patRes = (Resource) patientToSwitchTo.get("patientResource");
    Resource caseRes = (Resource) patientToSwitchTo.get("caseResource");
    Patient patient = new Patient(getDatabase(), patRes);
    String patientResult = switchPatient(patient, caseRes);
    getDatabase().stopCaching();

    return patientResult;
  }

  /**
   * Switches to a given patient.
   *
   * @param patientToSwitchTo the patient to switch to
   * @return the string
   */
  public String switchPatient(Entity patientToSwitchTo) {
    return switchPatient((Patient) patientToSwitchTo, null);
  }

  /**
   * Switches to a given patient.
   *
   * @param patientToSwitchTo the patient to switch to
   * @param caseRes the case resource
   * @return the string
   */
  public String switchPatient(Patient patientToSwitchTo, Resource caseRes) {
    patientToSwitchTo.load(getConfig());
    patientToSwitchTo.loadChildren(OSSEVocabulary.Type.Case, false, getConfig());

    currentFormName = "";
    setCurrentPatient(patientToSwitchTo);
    setCurrentEpisode(null);
    setCurrentCase(null);

    if (!patientToSwitchTo.hasCases(getConfig())) {
      Utils.getLogger().error("Switch patient CaseList = null");
    } else {
      if (caseRes != null) {
        for (Entity theCase : patientToSwitchTo.getCases(getConfig())) {
          if (caseRes.getId() == theCase.getId()) {
            setCurrentCase((Case) theCase);
          }
        }

        if (getCurrentCase() == null) {
          setCurrentCase((Case) patientToSwitchTo.getCases(getConfig()).get(0));
        }
      } else {
        setCurrentCase((Case) patientToSwitchTo.getCases(getConfig()).get(0));
      }

      getCurrentCase().load(getConfig());
      getCurrentCase().setParent(getCurrentPatient());
      getCurrentCase().loadChildren(OSSEVocabulary.Type.Episode, false, getConfig());
    }

    if (getCurrentPatient() == null) {
      goStartPage();
    }

    if (isPatientUser()) {
      currentFormName = ((ApplicationBean) Utils.getAB()).getPatientUserPatientMainPage();
    } else {
      currentFormName = ((ApplicationBean) Utils.getAB()).getPatientMainPage();
    }

    if (currentFormName.equals("")) {
      try {
        Utils.addContextMessage("Error",
            Utils.getResourceBundleString("error_missing_form"));
      } catch (Exception e) {
        Utils.getLogger().error("No patient form available.", e);
      }
      goStartPage();
    } else {
      loadNavigationPatientForms(currentFormName);

      // define mayCreateEpisode
      if (getCurrentCase() == null) {
        mayCreateEpisodes = false;
      } else {
        mayCreateEpisodes =
            mayCreate(
                getCurrentCase().getLocation(getConfig()).getName(), OSSEVocabulary.Type.Episode);
      }

      Utils.goForm(currentFormName);
      return "";
    }
    return "";
  }

  /**
   * TODO: add javadoc.
   */
  public String switchPatientFromTable() {
    String patientResourceUri = Faces.getRequestParameter("patientURI");
    if (!Utils.isNullOrEmpty(patientResourceUri)) {
      return switchPatientByUri(patientResourceUri);
    } else {
      return "";
    }
  }

  /**
   * TODO: add javadoc.
   */
  public String switchPatientByUri(String patientResourceUri) {
    Resource patientResource = getDatabase().getResourceByIdentifier(patientResourceUri);
    Patient patient = new Patient(getDatabase(), patientResource);
    return switchPatient(patient, null);
  }

  /**
   * Deletes all variables important to a user session.
   *
   * @see de.samply.edc.control.AbstractSessionBean#clearVariables()
   */
  @Override
  protected void clearVariables() {
    myWritePermissions = null;
    myCreatePermissions = null;
    myChangeStatusPermissions = null;

    myRoles = null;

    mayCreateEpisodes = false;
    mayCreatePatients = false;
    mayExport = false;
    mayAlwaysSeeIdat = false;

    patientsWithReportedFormsCaseList = null;
    patientsWithReportedFormsVisitList = null;
    navigationPatientForms = null;

    if (mainzellisteSession != null) {
      try {
        mainzellisteSession.destroy();
        mainzellisteSessionId = null;
      } catch (MainzellisteNetworkException e) {
        // TODO Auto-generated catch block
        e.printStackTrace();
      }
    }

    super.clearVariables();
  }

  /**
   * Checks if we have selected a current location.
   *
   * @return the boolean
   */
  public Boolean hasCurrentLocation() {
    return (getCurrentLocation() != null);
  }

  /**
   * Checks for current visit.
   *
   * @return the boolean
   */
  @Deprecated
  public Boolean hasCurrentVisit() {
    return (getCurrentVisit() != null);
  }

  /**
   * Checks for current episode.
   *
   * @return the boolean
   */
  public Boolean hasCurrentEpisode() {
    return (getCurrentEpisode() != null);
  }

  /** Redirects user to the main patient page or to the start page if no patient is selected. */
  @Override
  public void goMainPatientPage() {
    setCurrentEpisode(null);
    if (getCurrentPatient() == null) {
      goStartPage();
    } else {
      if (isPatientUser()) {
        currentFormName = ((ApplicationBean) Utils.getAB()).getPatientUserPatientMainPage();
      } else {
        currentFormName = ((ApplicationBean) Utils.getAB()).getPatientMainPage();
      }
    }

    loadNavigationPatientForms(currentFormName);
    Utils.goForm(currentFormName);
  }

  /** Redirects user to the start page. */
  @Override
  public void goStartPage() {
    // no current role set, bail him out
    if (getCurrentRole() == null) {
      logout();
    }

    setCurrentEpisode(null);
    setCurrentPatient(null);
    setCurrentCase(null);
    currentFormName = null;

    if (getCurrentRole().isLocalAdminRole()) {
      Utils.goAdminForm("userlist");
      return;
    } else if (getCurrentRole().isAdminRole()) {
      // Load Register Design if necessary pre-cofigurations found

      if (((ApplicationBean) Utils.getAB()).isInstallationEmailDone()) {
        if (((ApplicationBean) Utils.getAB()).isInstallationAuthDone()) {
          if (!((ApplicationBean) Utils.getAB()).isBridgehead()) {
            Utils.goAdminForm("forms");
          }
        }
      } else {
        Utils.goAdminForm("register");
      }

      return;
    } else if (getCurrentRole().isPatientRole()) {
      goPatientuserStartpage();
      return;
    }

    String page = "patientlist.xhtml";

    String confString =
        Utils.getStringOfJsonResource(
            ((ApplicationBean) Utils.getAB()).getDesignConfig(), DesignBean.useDashboard);
    if (Boolean.parseBoolean(confString)) {
      page = "dashboard.xhtml";
    }


    if (showQueryResult()) {
      page = "patientlistSearchView.xhtml";
    }

    goPage(page);
  }

  /** Loads all patients that have a form to be validated. */
  public void loadSignerPatients() {
    patientsWithReportedForms = new ArrayList<>();
    patientsWithReportedFormsVisitList = new HashMap<>();

    String language = Utils.getLanguage();
    if (!(language.equals("en") || language.equals("de"))) {
      language = "en";
    }

    List<Resource> patientlistUri = getDatabase().getResources(OSSEVocabulary.Type.Patient);
    for (Resource patient : patientlistUri) {
      // only accept patients with a patientID
      ArrayList<Episode> visitsWithReports = new ArrayList<>();
      ArrayList<Case> casesWithReports = new ArrayList<>();

      Patient thePatient = new Patient(getDatabase(), patient);
      thePatient.load(getConfig());
      thePatient.loadChildren(OSSEVocabulary.Type.Case, false, getConfig());

      if (!thePatient.hasCases(getConfig())) {
        continue;
      }

      for (Entity theCase : thePatient.getCases(getConfig())) {
        if (theCase == null) {
          continue;
        }

        theCase.loadChildren(OSSEVocabulary.Type.Episode, false, getConfig());
        theCase.loadChildren(OSSEVocabulary.Type.CaseForm, false, getConfig());

        ArrayList<Entity> caseFormsWithReports = new ArrayList<>();
        Boolean done = false;
        for (Entity theForm : theCase.getChildren(OSSEVocabulary.Type.CaseForm, getConfig())) {

          if (((Form) theForm).isReported()) {
            if (!done) {
              casesWithReports.add((Case) theCase);

              done = true;
            }

            caseFormsWithReports.add(theForm);
          }
        }

        theCase.setSubListChildren(OSSEVocabulary.Type.CaseForm, caseFormsWithReports);

        for (Entity theEpisode : theCase.getChildren(OSSEVocabulary.Type.Episode, getConfig())) {
          done = false;
          ArrayList<Entity> formsWithReports = new ArrayList<>();

          ((Episode) theEpisode).loadEpisodeForms(getConfig());
          for (Entity theForm :
              theEpisode.getChildren(OSSEVocabulary.Type.EpisodeForm, getConfig())) {

            if (((Form) theForm).isReported()) {
              if (!done) {
                visitsWithReports.add((Episode) theEpisode);

                done = true;
              }

              formsWithReports.add(theForm);
            }
          }

          theEpisode.setSubListChildren(OSSEVocabulary.Type.EpisodeForm, formsWithReports);
        }
      }

      patientsWithReportedFormsVisitList.put(thePatient.getPseudonym(), visitsWithReports);
      patientsWithReportedFormsCaseList.put(thePatient.getPseudonym(), casesWithReports);

      if (visitsWithReports.size() > 0 || casesWithReports.size() > 0) {
        patientsWithReportedForms.add(thePatient);
      }
    }
  }

  /**
   * Generates a pid (random 8-long string with capital letters only).
   *
   * @return the string
   */
  protected String generatePid() {
    return Utils.generateRandomString(8, true);
  }

  /**
   * Gets the form version.
   *
   * @return the form version
   */
  public Integer getFormVersion() {
    return Utils.getFormVersion(currentFormName);
  }

  /**
   * Gets the current location.
   *
   * @return the current location
   */
  public Location getCurrentLocation() {
    if (getCurrentObject("location") != null) {
      return (Location) getCurrentObject("location");
    } else {
      return null;
    }
  }

  /**
   * Gets the title of the form.
   *
   * @return the formtitle
   * @see de.samply.edc.control.AbstractSessionBean#getFormtitle()
   */
  @Override
  public String getFormtitle() {
    if (currentFormName == null) {
      return "";
    }

    String formTitle = ((ApplicationBean) Utils.getAB()).getFormTitle(currentFormName);
    if (formTitle != null && !formTitle.equals("")) {
      return formTitle;
    }

    return Utils.getAB().getPageTitleName(currentFormName, Utils.getLocale());
  }

  /**
   * Returns the position number of an episode in a case. Used for the Episode-Carousel.
   *
   * @return String Position
   */
  public String getCurrentPositionCurrentEpisodeInCurrentCase() {
    if (hasCurrentPatient() && hasCurrentEpisode()) {
      List<Entity> list = getCurrentCase().getEpisodes(getConfig());
      Integer pos = list.indexOf(getCurrentEpisode());
      if (pos != -1) {
        return pos.toString();
      }
    }

    return "0";
  }

  /**
   * Checks if the given form is the current form. Used for css style class calls.
   *
   * @param formName the form name
   * @return "active" if the form is the current one, empty String otherwise.
   */
  public String isActiveForm(String formName) {
    if (currentFormName != null && currentFormName.equalsIgnoreCase(formName)) {
      return "active";
    }

    return "";
  }

  /**
   * Checks if is patient form.
   *
   * @param formName the form name
   * @return the string
   */
  public String isPatientForm(String formName) {
    if (currentFormName != null && ((ApplicationBean) Utils.getAB()).isPatientForm(formName)) {
      return "patientform";
    }

    return "";
  }

  /**
   * Gets the selected role.
   *
   * @return the selected role
   */
  public String getSelectedRole() {
    return selectedRole;
  }

  /**
   * Sets the selected role.
   *
   * @param selectedRole the new selected role
   */
  public void setSelectedRole(String selectedRole) {
    this.selectedRole = selectedRole;
  }

  /**
   * Gets the write permissions of the current user.
   *
   * @return the my permissions
   */
  public HashMap<String, Permission> getMyPermissions() {
    return myWritePermissions;
  }

  /**
   * Whether the current user may export medical data.
   *
   * @return true|false
   */
  public Boolean getMayExport() {
    return mayExport;
  }

  /**
   * Whether the current user may see IDAT.
   *
   * @return true|false
   */
  public Boolean getMayAlwaysSeeIdat() {
    return mayAlwaysSeeIdat;
  }

  /**
   * Whether the current user may see IDAT of own location.
   *
   * @return true|false
   */
  public Boolean getMaySeeIdat() {
    return maySeeIdat;
  }

  /**
   * Whether the current user may see MDAT of all locations.
   *
   * @return true|false
   */
  public Boolean getMayReadAllMdat() {
    return mayReadAllMdat;
  }

  /**
   * Whether the current user may create patients.
   *
   * @return the may create patients
   */
  public Boolean getMayCreatePatients() {
    return mayCreatePatients;
  }

  /**
   * Set whether the current user may create patients.
   *
   * @param mayCreatePatients the new may create patients
   */
  public void setMayCreatePatients(Boolean mayCreatePatients) {
    this.mayCreatePatients = mayCreatePatients;
  }

  /**
   * Sets whether the current user may create episodes.
   *
   * @return the may create episodes
   */
  public Boolean getMayCreateEpisodes() {
    return mayCreateEpisodes;
  }

  /**
   * Sets whether the current user may create episodes.
   *
   * @param mayCreateEpisodes the new may create episodes
   */
  public void setMayCreateEpisodes(Boolean mayCreateEpisodes) {
    this.mayCreateEpisodes = mayCreateEpisodes;
  }

  /**
   * Wether a critical auth error has occured.
   *
   * @return the critical auth error
   */
  public Boolean getCriticalAuthError() {
    return criticalAuthError;
  }

  /**
   * Sets whether a critical auth error has occured.
   *
   * @param criticalAuthError the new critical auth error
   */
  public void setCriticalAuthError(Boolean criticalAuthError) {
    this.criticalAuthError = criticalAuthError;
  }

  /**
   * Whether a critical mainzelliste error has occured.
   *
   * @return the critical mainzelliste error
   */
  public Boolean getCriticalMainzellisteError() {
    return criticalMainzellisteError;
  }

  /**
   * Sets whether a critical mainzelliste error has occured.
   *
   * @param criticalMainzellisteError the new critical mainzelliste error
   */
  public void setCriticalMainzellisteError(Boolean criticalMainzellisteError) {
    this.criticalMainzellisteError = criticalMainzellisteError;
  }

  /**
   * Whether a critical FormEditor error has occured.
   *
   * @return the critical FormEditor error
   */
  public Boolean getCriticalFormEditorError() {
    return criticalFormEditorError;
  }

  /**
   * Sets whether a critical FormEditor error has occured.
   *
   * @param criticalFormEditorError the new critical FormEditor error
   */
  public void setCriticalFormEditorError(Boolean criticalFormEditorError) {
    this.criticalFormEditorError = criticalFormEditorError;
  }

  /**
   * Checks whether a current patient is set.
   *
   * @return the boolean
   */
  public Boolean hasCurrentPatient() {
    return (getCurrentPatient() != null);
  }

  /**
   * Checks whether a current case is set.
   *
   * @return the boolean
   */
  public Boolean hasCurrentCase() {
    return (getCurrentCase() != null);
  }

  /*
   * (non-Javadoc)
   *
   * @see de.samply.edc.control.AbstractSessionBean#cancelPage()
   */
  @Override
  public void cancelPage() {
    if (hasCurrentEpisode()) {
      goEpisodeForm(currentFormName);
    } else {
      Utils.goForm(currentFormName);
    }
    Utils.addContextMessage(
        "Changes reverted", "All your changes have been reverted to the current state.");
  }

  /**
   * Gets the role SelectItem list.
   *
   * @return the role list
   */
  public List<SelectItem> getRoleList() {
    if (rolesSelectItems == null) {
      rolesSelectItems = new ArrayList<>();

      SelectItem item;

      for (Role role : myRoles) {
        item = new SelectItem();

        item.setLabel(role.getName());
        item.setValue(role.getName());
        rolesSelectItems.add(item);
      }
    }
    return rolesSelectItems;
  }

  /**
   * Gets the current episode.
   *
   * @return the current episode
   */
  public Episode getCurrentEpisode() {
    return (Episode) getCurrentObject(OSSEVocabulary.Type.Episode);
  }

  /**
   * Sets the current episode.
   *
   * @param currentEpisode the new current episode
   */
  public void setCurrentEpisode(Episode currentEpisode) {
    setCurrentObject(OSSEVocabulary.Type.Episode, currentEpisode);
  }

  /**
   * Gets the current visit.
   *
   * @return the current visit
   */
  @Deprecated
  public Episode getCurrentVisit() {
    return (Episode) getCurrentObject(OSSEVocabulary.Type.Episode);
  }

  /**
   * Sets the current visit.
   *
   * @param currentVisit the new current visit
   */
  @Deprecated
  public void setCurrentVisit(Episode currentVisit) {
    setCurrentObject(OSSEVocabulary.Type.Episode, currentVisit);
  }

  /**
   * Gets the current user.
   *
   * @return the current user
   */
  public User getCurrentUser() {
    return (User) getCurrentObject(OSSEVocabulary.Type.User);
  }

  /**
   * Sets the current user.
   *
   * @param user the new current user
   */
  public void setCurrentUser(User user) {
    setCurrentObject(OSSEVocabulary.Type.User, user);
  }

  /** Clear current user. */
  public void clearCurrentUser() {
    clearCurrentObject(OSSEVocabulary.Type.User);
  }

  /**
   * Gets the current role.
   *
   * @return the current role
   */
  public Role getCurrentRole() {
    return (Role) getCurrentObject(OSSEVocabulary.Type.Role);
  }

  /**
   * Sets the current role.
   *
   * @param role the new current role
   */
  public void setCurrentRole(Role role) {
    setCurrentObject(OSSEVocabulary.Type.Role, role);
  }

  /**
   * Gets the current group.
   *
   * @return the current group
   */
  @Deprecated
  public Entity getCurrentGroup() {
    return null;
  }

  /**
   * Gets the group list.
   *
   * @return the group list
   */
  @Deprecated
  public List<Entity> getGroupList() {
    return null;
  }

  /**
   * Gets the patients of current group.
   *
   * @return the patients of current group
   */
  @Deprecated
  public List<Entity> getPatientsOfCurrentGroup() {
    return null;
  }

  /**
   * Gets the users of current group.
   *
   * @return the users of current group
   */
  @Deprecated
  public List<Entity> getUsersOfCurrentGroup() {
    return null;
  }

  /**
   * Gets the current patient.
   *
   * @return the current patient
   */
  public Patient getCurrentPatient() {
    return (Patient) getCurrentObject(OSSEVocabulary.Type.Patient);
  }

  /**
   * Sets the current patient.
   *
   * @param currentPatient the new current patient
   */
  public void setCurrentPatient(Patient currentPatient) {
    setCurrentObject(OSSEVocabulary.Type.Patient, currentPatient);
  }

  /**
   * Gets the current case.
   *
   * @return the current case
   */
  public Case getCurrentCase() {
    return (Case) getCurrentObject(OSSEVocabulary.Type.Case);
  }

  /**
   * Sets the current case.
   *
   * @param currentCase the new current case
   */
  public void setCurrentCase(Case currentCase) {
    setCurrentObject(OSSEVocabulary.Type.Case, currentCase);
  }

  /**
   * Gets the patients with reported forms.
   *
   * @return the patients with reported forms
   */
  public List<Patient> getPatientsWithReportedForms() {
    return patientsWithReportedForms;
  }

  /**
   * Gets the patients with reported forms visit list.
   *
   * @return the patients with reported forms visit list
   */
  public HashMap<String, List<Episode>> getPatientsWithReportedFormsVisitList() {
    return patientsWithReportedFormsVisitList;
  }

  /**
   * Gets the patients with reported forms case list.
   *
   * @return the patients with reported forms case list
   */
  public HashMap<String, List<Case>> getPatientsWithReportedFormsCaseList() {
    return patientsWithReportedFormsCaseList;
  }

  /**
   * Checks whether the current form is an old form.
   *
   * @return the checks if is old form
   */
  public String getIsOldForm() {
    return isOldForm;
  }

  /**
   * Sets whether the current form is an old form.
   *
   * @param isOldForm the new checks if is old form
   */
  public void setIsOldForm(String isOldForm) {
    this.isOldForm = isOldForm;
  }

  /**
   * Gets the my change status permissions.
   *
   * @return the myChangeStatusPermissions
   */
  public HashMap<String, HashMap<String, Boolean>> getMyChangeStatusPermissions() {
    return myChangeStatusPermissions;
  }

  /**
   * Gets the URL to the Mainzelliste REST interface.
   *
   * @return the mainzelliste resturl
   */
  public String getMainzellisteRestUrl() {
    return OsseUtils.getMainzellisteRestUrl();
  }

  /**
   * Gets the signin token.
   *
   * @return the signin token
   */
  public String getSigninToken() {
    return signinToken;
  }

  /**
   * Sets a signin token. This is used in the search viewer to "login" a session for displaying the
   * search results. If no queryResultId was found linked to this signin token, the login will fail
   *
   * @param signinToken the new signin token
   */
  public void setSigninToken(String signinToken) {
    if (this.signinToken != null) {
      return;
    }

    this.signinToken = signinToken;

    queryResultId = SigninTokenExchangeStorage.instance.getQueryResultId(signinToken);

    // no queryResultId, bail out
    if (queryResultId == null) {
      this.signinToken = null;
      return;
    }

    // remove the token from the singleton so it cannot be used again
    SigninTokenExchangeStorage.instance.removeSigninToken(signinToken);

    // This won't be a regular user, so we'll force in certain rights here
    // Start DB with Systemrole
    Database db = new Database(true);
    setDatabase(db);

    // Set current to systemrole
    ResourceQuery query = new ResourceQuery(BasicDB.Type.Role);
    query.add(Criteria.Equal(BasicDB.Type.Role, BasicDB.Role.RoleType, "SYSTEM"));
    Resource theRole = getDatabase().getResources(query).get(0);
    Role theSystemRole = new Role(getDatabase(), theRole);
    theSystemRole.load(getConfig());
    setCurrentRole(theSystemRole);
    myRoles = new ArrayList<>();
    myRoles.add(theSystemRole);

    // Define currentuser to admin
    query = new ResourceQuery(BasicDB.Type.User);
    query.add(Criteria.Equal(BasicDB.Type.User, BasicDB.ID, 1));
    Resource theUserResource = getDatabase().getResources(query).get(0);
    User theUser = new User(getDatabase(), theUserResource);
    theUser.load(false, false, getConfig());
    setCurrentUser(theUser);

    // define rights
    defineMyRights();
  }

  /**
   * Tells us if we are showing a query result in this session.
   *
   * @return true|false
   */
  public Boolean showQueryResult() {
    return (queryResultId != null && queryResultId != 0);
  }

  /**
   * Gets the query result id.
   *
   * @return the query result id
   */
  public Integer getQueryResultId() {
    return queryResultId;
  }

  /**
   * Says if currently used role is a patient user role.
   *
   * @return the boolean
   */
  public Boolean isPatientUser() {
    return getCurrentRole().isPatientRole();
  }

  /**
   * If or not to display the episode carousel.
   *
   * @return the boolean
   */
  public Boolean showEpisodeCarousel() {
    if (hasCurrentPatient() && (isCurrentFormMedical() || isCurrentFormNoForm())) {
      if (isPatientUser()) {
        return ((ApplicationBean) Utils.getAB()).hasPatientUserEpisodeForm();
      } else {
        return true;
      }
    }

    return false;
  }

  /**
   * Adds a logbook entry. Additional logging to default log file.
   *
   * @param action Log message.
   */
  @Override
  public void addLog(String action) {
    super.addLog(action);
    Utils.getLogger().debug(action);
  }
}
