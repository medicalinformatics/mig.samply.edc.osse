/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import javax.faces.context.FacesContext;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/** Created by Linh on 28.04.2017. */
public class CookieHelper {

  /**
   * TODO: add javadoc.
   */
  public void setCookie(String name, String value) {

    FacesContext facesContext = FacesContext.getCurrentInstance();

    HttpServletRequest request =
        (HttpServletRequest) facesContext.getExternalContext().getRequest();
    Cookie cookie = null;

    Cookie[] userCookies = request.getCookies();
    if (userCookies != null && userCookies.length > 0) {
      for (Cookie userCooky : userCookies) {
        if (userCooky.getName().equals(name)) {
          cookie = userCooky;
          break;
        }
      }
    }

    if (cookie != null) {
      cookie.setValue(value);
    } else {
      cookie = new Cookie(name, value);
      cookie.setPath(request.getContextPath());
    }

    cookie.setMaxAge(60 * 60 * 24 * 365);

    HttpServletResponse response =
        (HttpServletResponse) facesContext.getExternalContext().getResponse();
    response.addCookie(cookie);
  }

  /**
   * TODO: add javadoc.
   */
  public Cookie getCookie(String name) {

    FacesContext facesContext = FacesContext.getCurrentInstance();

    HttpServletRequest request =
        (HttpServletRequest) facesContext.getExternalContext().getRequest();
    Cookie cookie;

    Cookie[] userCookies = request.getCookies();
    if (userCookies != null && userCookies.length > 0) {
      for (Cookie userCooky : userCookies) {
        if (userCooky.getName().equals(name)) {
          cookie = userCooky;
          return cookie;
        }
      }
    }
    return null;
  }
}
