/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import de.samply.auth.client.GrantType;
import de.samply.auth.rest.AccessTokenDto;
import de.samply.common.http.HttpConnector;
import de.samply.common.mdrclient.MdrClient;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.exceptions.OsseException;
import de.samply.edc.osse.model.MdrKeyUsageData;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.web.mdrfaces.MdrContext;
import java.io.File;
import java.io.FileNotFoundException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.client.Client;

/** Helper for a complete forms recreation. */
public class FormRecreationHelper {

  /** The application bean. */
  private ApplicationBean applicationBean;

  /** Helper for imports. */
  private FormImportHelper fih;

  /** The access token to access the MDR. */
  private AccessTokenDto accessToken;

  /** jersey clients for form repository and mdr communication. */
  private Client frJerseyClient;
  private Client mdrJerseyClient;

  /** The form storage. */
  private JSONResource formStorage = new JSONResource();

  /**
   * Instantiates a new form recreation helper.
   *
   * @param applicationBean the application bean
   */
  public FormRecreationHelper(ApplicationBean applicationBean) {
    this.applicationBean = applicationBean;
    fih = new FormImportHelper(applicationBean);
  }

  /**
   * Recreates a certain form, old versions will be deleted and replaced.
   *
   * @param formName the name of the form
   * @param episodeForm the episode form
   * @param isArchivedForm the is archived form
   * @return true, if successful
   * @throws FileNotFoundException the file not found exception
   */
  private boolean doRecreateForm(String formName, Boolean episodeForm, Boolean isArchivedForm,
      String formRepoToken)
      throws FileNotFoundException {
    String saveFileName = "/forms/" + formName + ".xhtml";
    String saveFileNamePure = saveFileName;
    saveFileName = Utils.getRealPath(saveFileName);
    File file = new File(saveFileName);

    if (formStorage.getProperty(saveFileNamePure) != null) {
      Utils.getLogger().debug("This form was stored, removing it.");
      formStorage.removeProperties(saveFileNamePure);
    }

    if (file.exists()) {
      Utils.getLogger()
          .debug("Form " + formName + " exists as form in " + saveFileName + ". Deleting it.");
      file.delete();
    }

    Pattern pattern = Pattern.compile("^form_([^-]+)_ver-([^-]+)$");
    Matcher matcher = pattern.matcher(formName);
    if (!matcher.find()) {
      Utils.getLogger().debug("Formname " + formName + " does not match pattern.");
      return false;
    }

    String mdrUrl = (String) fih.getConfiguration().getProperty(Vocabulary.Config.Mdr.REST);
    MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();
    if (mdrClient == null) {
      mdrClient = new MdrClient(mdrUrl, mdrJerseyClient);
    }

    formStorage =
        fih.doImport(
            frJerseyClient,
            mdrClient,
            accessToken,
            matcher.group(1),
            matcher.group(2),
            episodeForm,
            formStorage,
            true,
            isArchivedForm,
            formRepoToken);

    return true;
  }

  /**
   * Reworks all forms and updates already saved data to the new non-repeatable saving strategy.
   *
   * @return boolean
   * @throws DatabaseException the database exception
   */
  public Boolean recreateForms() {
    Database database = new Database(true);
    try {
      // We call this to verify the system role was selectable and is valid.
      // If not, a null pointer exception is thrown
      database.getDatabaseModel();
    } catch (Exception e) {
      Utils.getLogger()
          .warn(
              "Unable to get database model."
                  + "Maybe the system role was not selected or is not valid",
              e);
      return false;
    }
    JSONResource osseFormStorage = applicationBean.getOsseFormStorage();

    if (osseFormStorage.getProperty(Vocabulary.Config.Form.storage) != null) {
      formStorage = osseFormStorage.getProperty(Vocabulary.Config.Form.storage).asJSONResource();
    }

    fih.clearData();

    if (osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix) != null) {
      fih.setFormNameMatrix(
          osseFormStorage.getProperty(Vocabulary.Config.Form.formNameMatrix).asJSONResource());
    }

    HttpConnector httpConnector = ApplicationBean.getHttpConnector();
    try {
      // Get a HTTPS jersey client (AUTH is always https)
      Client client = httpConnector.getJerseyClientForHTTPS(false);
      accessToken = Auth.getAccessToken(client, applicationBean.getConfig());
    } catch (InvalidKeyException | SignatureException | NoSuchAlgorithmException e) {
      Utils.getLogger().warn("Unable to get access toke.",e);
    }

    if (accessToken == null) {
      Utils.getLogger().error("No access token could be gained");
      return false;
    }

    // Get a Jersey client for the FormRepository
    String urlFR = applicationBean.getConfig()
        .getProperty(Vocabulary.Config.FormEditor.formEditorRestUrl).toString();
    frJerseyClient = httpConnector.getJerseyClient(urlFR, false);

    // Get a Jersey client for the MDR
    String urlMdr = applicationBean.getConfig().getProperty(Vocabulary.Config.Mdr.REST) + "";
    mdrJerseyClient = httpConnector.getJerseyClient(urlMdr, false);

    if (applicationBean.getArchivedFormulars().get("en") == null
        && applicationBean.getFormulars().get("en") == null
        && applicationBean.getArchivedVisitFormulars().get("en") == null
        && applicationBean.getVisitFormulars().get("en") == null) {
      Utils.getLogger().info("Registry has no forms so skipping rework.");
      return false;
    }

    String token = "";
    AccessTokenDto accessToken =
        null;
    try {
      accessToken = Auth.getOAuth2Client(fih.getConfiguration()).isUseSamplyAuth()
          ? Auth.getAccessToken(frJerseyClient, fih.getConfiguration())
          : Auth.getAccessToken(frJerseyClient, fih.getConfiguration(), GrantType.PASSWORD);
      token = "Bearer " + accessToken.getAccessToken();
    } catch (Exception e) {
      Utils.getLogger().error("Unable to get access token! Unable to rework all forms.", e);
      return false;
    }

    if (token == null) {
      Utils.getLogger().error("AccessToken is null! Unable to rework all forms.");
      return false;
    }


    // 1) rework all forms
    try {
      LinkedHashMap<String, String> archivedFormularsEn =
          applicationBean.getArchivedFormulars().get("en");
      if (archivedFormularsEn != null) {
        for (String formName : archivedFormularsEn.keySet()) {
          if (!doRecreateForm(formName, false, true, token)) {
            return false;
          }
        }
      }

      LinkedHashMap<String, String> formularsEn =
          applicationBean.getFormulars().get("en");
      if (formularsEn != null) {
        for (String formName : formularsEn.keySet()) {
          if (!doRecreateForm(formName, false, false, token)) {
            return false;
          }
        }
      }

      LinkedHashMap<String, String> archivedVisitFormularsEn =
          applicationBean.getArchivedVisitFormulars().get("en");
      if (archivedVisitFormularsEn != null) {
        for (String formName : archivedVisitFormularsEn.keySet()) {
          if (!doRecreateForm(formName, true, true, token)) {
            return false;
          }
        }
      }

      LinkedHashMap<String, String> visitFormularsEn =
          applicationBean.getVisitFormulars().get("en");
      if (visitFormularsEn != null) {
        for (String formName : visitFormularsEn.keySet()) {
          if (!doRecreateForm(formName, true, false, token)) {
            return false;
          }
        }
      }
    } catch (Exception e) {
      Utils.getLogger().warn("Unable to rework all forms.",e);
      return false;
    }

    osseFormStorage.setProperty(Vocabulary.Config.Form.storage, formStorage);

    database.saveConfig("osse.form.storage", osseFormStorage);

    JSONResource osseConfig = applicationBean.getOsseConfig();

    osseConfig.setProperty(
        Vocabulary.Config.mdrEntityIsInForm, fih.getMdrKeyUsageStore().asJsonResource());

    JSONResource recordHasMdrEntities = new JSONResource();
    for (String key : fih.getRecordHasMdrEntities().keySet()) {
      LinkedHashSet<String> moo = fih.getRecordHasMdrEntities().get(key);
      for (String me : moo) {
        recordHasMdrEntities.addProperty(key, me);
      }
    }
    osseConfig.setProperty(Vocabulary.Config.recordHasMdrEntities, recordHasMdrEntities);

    JSONResource mdrs = new JSONResource();
    for (String key : fih.getWarnDoubleUsage().keySet()) {
      Set<MdrKeyUsageData> moo = fih.getWarnDoubleUsage().get(key);
      for (MdrKeyUsageData me : moo) {
        mdrs.addProperty(key, me.asJsonResource());
      }
    }
    osseConfig.setProperty(Vocabulary.Config.warnDoubleMDRKeyUsage, mdrs);

    MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();
    if (mdrClient == null) {
      mdrClient = new MdrClient(urlMdr, mdrJerseyClient);
    }

    try {
      fih.fillMdrEntityHasValidation(mdrClient, accessToken);
    } catch (OsseException e) {
      Utils.getLogger().warn("Unable to fill if entity has validation",e);
    }

    osseConfig.setProperty(
        Vocabulary.Config.mdrEntityHasValidation, fih.getMdrEntityHasValidation());

    database.saveConfig("osse", osseConfig);

    JSONResource osseXsdStorage = applicationBean.getOsseXsdStorage();
    if (osseXsdStorage == null) {
      osseXsdStorage = new JSONResource();
    }
    try {
      String specificXsd = fih.makeV2Xsd(mdrClient, accessToken);
      osseXsdStorage.setProperty(Vocabulary.Config.specificXSD, specificXsd);
      // create csv file
      fih.createDataElementListCsv(mdrClient, accessToken);
    } catch (Exception e) {
      Utils.getLogger().warn("Unable to create data element csv list",e);
    }
    database.saveConfig("osse.xsd.storage", osseXsdStorage);

    Utils.getLogger().debug("Forms redone.");

    return true;
  }
}
