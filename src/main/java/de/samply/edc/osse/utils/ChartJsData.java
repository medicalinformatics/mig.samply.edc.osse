/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Helper class for chartjs data json in java.
 */
public class ChartJsData {

  private List<String> labels = new ArrayList<>();
  private List<ChartJsDataset> datasets = new ArrayList<>();

  /**
   * Add a lable.
   */
  public void addLabel(String label) {
    labels.add(label);
  }

  /**
   * Add a dataset to the data.
   */
  public void addValue(ChartJsDataset dataset) {
    datasets.add(dataset);
  }

  /**
   * Get the json string representation of this chart data.
   * @return json string.
   */
  public String getDataString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{\n");
    List<String> escapedLabels = labels.stream().map(s -> "\"" + s + "\"")
        .collect(Collectors.toList());
    stringBuilder.append("\"labels\": [" + String.join(",", escapedLabels) + "],\n");
    stringBuilder.append("\"datasets\": [\n");
    for (Iterator<ChartJsDataset> it = datasets.iterator(); it.hasNext(); ) {
      stringBuilder.append(it.next().getDatasetString());
      if (it.hasNext()) {
        stringBuilder.append(",\n");
      }
    }
    stringBuilder.append("]\n");
    stringBuilder.append("}\n");
    return stringBuilder.toString();
  }

  //  example
  //  data: {
  //    labels: ["Africa", "Asia", "Europe", "Latin America", "North America"],
  //    datasets: [
  //    {
  //      label: "Population (millions)",
  //          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
  //      data: [2478,5267,734,784,433]
  //    }
  //      ]
  //  }

}
