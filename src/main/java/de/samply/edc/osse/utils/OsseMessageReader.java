/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.StandardCharsets;
import java.util.Enumeration;
import java.util.Locale;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import javax.faces.context.FacesContext;

/**
 * Reads the UTF-8 encoded message properties. See
 * http://jdevelopment.nl/internationalization-jsf-utf8-encoded-properties-files/ for more
 * information on why this is necessary while handling with UTF-8 encoded properties files.
 */
public class OsseMessageReader extends ResourceBundle {

  protected static final String BUNDLE_NAME = "de.samply.edc.messages.messages";
  protected static final String BUNDLE_EXTENSION = "properties";
  protected static final Control UTF8_CONTROL = new Utf8Control();

  /**
   * TODO: add javadoc.
   */
  public OsseMessageReader() {
    setParent(
        ResourceBundle.getBundle(
            BUNDLE_NAME,
            FacesContext.getCurrentInstance().getViewRoot().getLocale(),
            UTF8_CONTROL));
  }

  @Override
  protected Object handleGetObject(String key) {
    return parent.getObject(key);
  }

  @Override
  public Enumeration getKeys() {
    return parent.getKeys();
  }

  protected static class Utf8Control extends Control {
    public ResourceBundle newBundle(
        String baseName, Locale locale, String format, ClassLoader loader, boolean reload)
        throws IOException {
      // The below code is copied from default Control#newBundle() implementation.
      // Only the PropertyResourceBundle line is changed to read the file as UTF-8.
      String bundleName = toBundleName(baseName, locale);
      String resourceName = toResourceName(bundleName, BUNDLE_EXTENSION);
      ResourceBundle bundle = null;
      InputStream stream = null;
      if (reload) {
        URL url = loader.getResource(resourceName);
        if (url != null) {
          URLConnection connection = url.openConnection();
          if (connection != null) {
            connection.setUseCaches(false);
            stream = connection.getInputStream();
          }
        }
      } else {
        stream = loader.getResourceAsStream(resourceName);
      }
      if (stream != null) {
        try {
          bundle =
              new PropertyResourceBundle(new InputStreamReader(stream, StandardCharsets.UTF_8));
        } finally {
          stream.close();
        }
      }
      return bundle;
    }
  }
}
