/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import de.samply.edc.utils.Utils;
import de.samply.form.dto.FormDetails;
import de.samply.store.osse.OSSEVocabulary;
import java.io.File;
import java.io.Serializable;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/** Various supporting methods for handling forms in OSSE. */
public class FormUtils implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7687229240479353431L;

  /** FormDetails jaxb context. */
  private static JAXBContext formDetailsJaxbContext = null;

  /**
   * Construct a full form xml file path for a specific form.
   *
   * @param formId The id of the form to load.
   * @param formVersion The version number of the form to load.
   * @return Full path of the form's xml file.
   */
  public static String getFormXmlFilePath(long formId, long formVersion) {
    return Utils.getRealPath("/forms/xml_" + formId + "_ver-" + formVersion + ".xml");
  }

  /**
   * Creates a formDetailsJaxbContext that should be use through out the project.
   *
   * @return The formDetailsJaxbContext.
   */
  public static JAXBContext getFormDetailsJaxbContext() {
    if (formDetailsJaxbContext != null) {
      return formDetailsJaxbContext;
    }
    try {
      formDetailsJaxbContext = JAXBContext.newInstance(FormDetails.class);
      return formDetailsJaxbContext;
    } catch (JAXBException e) {
      Utils.getLogger()
          .error("Unable to get JaxbContext for FormDetails.class, unable to proceed.", e);
      return null;
    }
  }

  /**
   * Loads the xml file of a specific form and returns the form's definition.
   *
   * @param formId The id of the form to load.
   * @param formVersion The version number of the form to load.
   * @return The form's full definition stored in the xml file.
   */
  public static FormDetails loadFormFromXml(long formId, long formVersion) {
    FormDetails formDetails = null;

    String fileName = FormUtils.getFormXmlFilePath(formId, formVersion);
    File file = new File(fileName);
    if (file.exists()) {
      try {
        JAXBContext jaxbContext = getFormDetailsJaxbContext();
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        formDetails = (FormDetails) unmarshaller.unmarshal(file);
      } catch (JAXBException e) {
        Utils.getLogger()
            .error(
                "Loading form XML failed. Form ID: " + formId + " Form version: " + formVersion, e);
      }
    } else {
      Utils.getLogger()
          .error(
              "Loading form XML failed, file does not exist. Form ID: "
                  + formId
                  + " Form version: "
                  + formVersion);
    }

    return formDetails;
  }

  /**
   * Retrieves a specific translation from a form object.
   *
   * @param formDetails The form definition.
   * @param locale The ISO 639-1 code of the requested language.
   * @return The form translation.
   */
  public static FormDetails.FormI18n getFormI18n(FormDetails formDetails, String locale) {
    FormDetails.FormI18n result = null;

    for (FormDetails.FormI18n formI18n : formDetails.getI18ns()) {
      if (formI18n.getLanguage().equals(locale)) {
        result = formI18n;
        break;
      }
    }

    return result;
  }

  /**
   * Retrieves a specific item from a form definition.
   *
   * @param formDetails The form definition.
   * @param itemPosition The numeric position of the item within the form to fetch.
   * @return The form item definition.
   */
  public static FormDetails.Item getFormItem(FormDetails formDetails, int itemPosition) {
    FormDetails.Item result = null;

    for (FormDetails.Item item : formDetails.getItems()) {
      if (item.getPosition() == itemPosition) {
        result = item;
        break;
      }
    }

    return result;
  }

  /**
   * TODO: add javadoc.
   * @param formDetails The form definition.
   * @param itemPosition The numeric position of the item within the form to fetch.
   * @param locale The ISO 639-1 code of the requested language.
   * @return The form item translation object.
   */
  public static FormDetails.Item.ItemI18n getFormItemI18n(
      FormDetails formDetails, int itemPosition, String locale) {
    FormDetails.Item.ItemI18n result = null;

    for (FormDetails.Item.ItemI18n itemI18n : getFormItem(formDetails, itemPosition).getI18ns()) {
      if (itemI18n.getLanguage().equals(locale)) {
        result = itemI18n;
        break;
      }
    }

    return result;
  }

  /**
   * Returns the id number from a form name string.
   *
   * @param formName The form name string ("form_x_ver-y").
   * @return The ID of the form. NULL if formName doesn't match pattern.
   */
  public static Long getFormIdFromName(String formName) {
    Long result = null;

    Matcher matcher = getFormNamePatternMatcher(formName);

    if (matcher.find()) {
      result = Long.valueOf(matcher.group(1));
    }

    return result;
  }

  /**
   * Returns the version number from a form name string.
   *
   * @param formName The form name string ("form_x_ver-y").
   * @return The version of the form. NULL if formName doesn't match pattern.
   */
  public static Long getFormVersionFromName(String formName) {
    Long result = null;

    Matcher matcher = getFormNamePatternMatcher(formName);

    if (matcher.find()) {
      result = Long.valueOf(matcher.group(2));
    }

    return result;
  }

  /**
   * Returns a pattern matcher to extract form id and version from a form name string.
   *
   * @param formName The form name string ("form_x_ver-y").
   * @return A pattern matcher. Groups: 1 = form ID, 2 = form version.
   */
  public static Matcher getFormNamePatternMatcher(String formName) {
    Pattern pattern = Pattern.compile("form_(\\d+)_ver-(\\d+)");

    return pattern.matcher(formName);
  }

  /**
   * Returns a pattern matcher to extract namespace, type, id and version from a MDR URN.
   *
   * @param urn The MDR URN string ("urn:%namespace%:%type%:%id%:%version%").
   * @return A pattern matcher. Groups: 1 = namespace, 2 = type, 3 = ID, 4 = version.
   */
  public static Matcher getMdrUrnPatternMatcher(String urn) {
    Pattern pattern = Pattern.compile("urn:(.*-\\d+):(record|dataelement):(\\d+):(\\d+)");

    return pattern.matcher(urn);
  }

  /**
   * Returns a pattern matcher to extract namespace, type, id and version from a form property key.
   *
   * @param urn The form property key string representing an URN (either single data element or
   *     record following data element).
   * @return A pattern matcher. Groups: 1 = namespace, 2 = type, 3 = ID, 4 = version. Optional
   *     groups: 5 = namespace, 6 = type, 7 = ID, 8 = version.
   */
  public static Matcher getFormPropertyKeyPatternMatcher(String urn) {
    Pattern pattern = Pattern.compile("urn__(\\w*-\\d+)__(record|dataelement)__(\\d+)__(\\d+)"
        + "($|/urn__(\\w*-\\d+)__(dataelement)__(\\d+)__(\\d+))");

    return pattern.matcher(urn);
  }

  /**
   * Creates a URN string for a data element.
   *
   * @param namespace MDR namespace of the data element.
   * @param dataElementId Data element's ID.
   * @param dataElementVersion Data element's version.
   * @return The urn of the data element.
   */
  public static String constructMdrUrnDataElement(
      String namespace, String dataElementId, String dataElementVersion) {
    return "urn:" + namespace + ":dataelement:" + dataElementId + ":" + dataElementVersion;
  }

  /**
   * Creates a URN string for a record.
   *
   * @param namespace MDR namespace of the data element.
   * @param recordId The record's ID.
   * @param recordVersion The record's version.
   * @return The URN of the record.
   */
  public static String constructMdrUrnRecord(
      String namespace, String recordId, String recordVersion) {
    return "urn:" + namespace + ":record:" + recordId + ":" + recordVersion;
  }

  /**
   * Creates a form property key for a data element.
   *
   * @param namespace MDR namespace of the data element.
   * @param dataElementId Data element's ID.
   * @param dataElementVersion Data element's version.
   * @return The form property key.
   */
  public static String constructFormKey(
      String namespace, String dataElementId, String dataElementVersion) {
    return constructMdrUrnDataElement(namespace, dataElementId, dataElementVersion)
        .replace(":", "__");
  }

  /**
   * Creates a form property key for a data element within a record.
   *
   * @param namespace MDR namespace of the data element.
   * @param recordId The record's ID.
   * @param recordVersion The record's version.
   * @param dataElementId Data element's ID.
   * @param dataElementVersion Data element's version.
   * @return The form property key.
   */
  public static String constructFormKey(
      String namespace,
      String recordId,
      String recordVersion,
      String dataElementId,
      String dataElementVersion) {
    String result;
    if (dataElementId == null && dataElementVersion == null) {
      result = constructMdrUrnRecord(namespace, recordId, recordVersion).replace(":", "__");
    } else {
      result =
          constructMdrUrnRecord(namespace, recordId, recordVersion).replace(":", "__")
              + "/"
              + constructMdrUrnDataElement(namespace, dataElementId, dataElementVersion)
                  .replace(":", "__");
    }

    return result;
  }

  /**
   * Returns a Map containing episode form keys that should or should not be transferred to a new
   * episode form.
   *
   * @return The map containing form keys as keys with a value of true if the key/value should not
   *     be transferred.
   */
  public static HashMap<String, Boolean> getNotCopyableDataKeys() {
    HashMap<String, Boolean> notCopyableDataKeys = new HashMap<>();
    notCopyableDataKeys.put(OSSEVocabulary.ID, true);
    notCopyableDataKeys.put("lastChangedDate", true);
    notCopyableDataKeys.put("lastChangedBy", true);
    notCopyableDataKeys.put("transaction_id", true);
    notCopyableDataKeys.put("activePermissions", true);
    notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Name, true);
    notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Status, true);
    notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Version, true);
    notCopyableDataKeys.put(OSSEVocabulary.EpisodeForm.Episode, true);

    return notCopyableDataKeys;
  }

}
