/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import de.samply.edc.osse.handler.XmlErrorHandler;
import de.samply.edc.osse.model.OsseMessage;
import de.samply.edc.utils.Utils;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Source;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.SchemaFactory;
import org.apache.commons.io.IOUtils;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/** Helper class to validate XMLs. */
public class XmlHelper implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5857260950582713132L;

  /** error message store. */
  private OsseMessage errorMessages;

  /** Instantiates a new XML helper. */
  public XmlHelper() {
    errorMessages = new OsseMessage();
  }

  /**
   * Instantiates a new XML helper.
   *
   * @param errorMessages the error messages
   */
  public XmlHelper(OsseMessage errorMessages) {
    this.errorMessages = errorMessages;
  }

  /**
   * Validates an XML.
   *
   * @param xsd The XSD
   * @param xml The XML
   * @return org.w3c.dom.Document
   */
  public Document validateXml(String xsd, String xml) {
    // Prepare the DOM builder
    Document dom;
    DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
    dbf.setValidating(false);
    dbf.setNamespaceAware(true);

    // prepare the errorhandler
    XmlErrorHandler errorHandler = new XmlErrorHandler();

    try {
      InputStream xsdStream = IOUtils.toInputStream(xsd, "UTF-8");
      InputStream xmlStream = IOUtils.toInputStream(xml, "UTF-8");

      SchemaFactory schemaFactory = SchemaFactory.newInstance("http://www.w3.org/2001/XMLSchema");
      dbf.setSchema(schemaFactory.newSchema(new Source[] {new StreamSource(xsdStream)}));

      DocumentBuilder db = dbf.newDocumentBuilder();
      db.setErrorHandler(errorHandler);

      // parse using builder to get DOM representation of the XML file
      dom = db.parse(xmlStream);
    } catch (ParserConfigurationException | IOException e) {
      Utils.getLogger().error("Error while parsing.",e);
      errorMessages.setFinalMessage(
          "Error while parsing input data. Please provide a legal XML object.");
      errorMessages.addErrorMessage(null, null, e.getMessage());
      Utils.getLogger()
          .error("Error while parsing input data. Please provide a legal XML object.", e);
      return null;
    } catch (SAXException e) {
      for (String message : errorHandler.getErrorMessages()) {
        errorMessages.addErrorMessage(null, null, message);
        Utils.getLogger().debug(message);
      }
      Utils.getLogger().error("The XML you provided is not valid.", e);
      errorMessages.setFinalMessage("The XML you provided is not valid.");
      return null;
    }

    // If the XML is invalid, respond with an error
    if (!errorHandler.isValid()) {
      for (String message : errorHandler.getErrorMessages()) {
        errorMessages.addErrorMessage(null, null, message);
        Utils.getLogger().debug(message);
      }
      Utils.getLogger().error("The XML you provided is not valid.");
      errorMessages.setFinalMessage("The XML you provided is not valid.");
      return null;
    }

    // Something else went wrong when building the DOM
    if (dom == null) {
      errorMessages.setFinalMessage(
          "Error while parsing input data. Please provide a legal XML object.");
      return null;
    }

    return dom;
  }

  /**
   * Gets the error messages.
   *
   * @return the errorMessages
   */
  public OsseMessage getErrorMessages() {
    return errorMessages;
  }

  /**
   * Sets the error messages.
   *
   * @param errorMessages the errorMessages to set
   */
  public void setErrorMessages(OsseMessage errorMessages) {
    this.errorMessages = errorMessages;
  }
}
