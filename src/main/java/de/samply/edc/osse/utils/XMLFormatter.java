/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import de.samply.edc.utils.Utils;
import java.io.IOException;
import java.io.StringReader;
import java.io.StringWriter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import org.w3c.dom.DOMConfiguration;
import org.w3c.dom.DOMImplementation;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.bootstrap.DOMImplementationRegistry;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSOutput;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/** Class to beautify a XML. Yet neither of the two used methods are working satisfactory :( */
public class XmlFormatter {

  /**
   * Pretty print with do m3 ls.
   *
   * @param input the input
   * @return the string
   * @throws ParserConfigurationException the parser configuration exception
   * @throws SAXException the SAX exception
   * @throws IOException Signals that an I/O exception has occurred.
   */
  static String prettyPrintWithDom3LS(String input)
      throws ParserConfigurationException, SAXException, IOException {
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = factory.newDocumentBuilder();
    Document document = builder.parse(new InputSource(new StringReader(input)));

    // Pretty-prints a DOM document to XML using DOM Load and Save's
    // LSSerializer.
    // Note that the "format-pretty-print" DOM configuration parameter can
    // only be set in JDK 1.6+.
    DOMImplementation domImplementation = document.getImplementation();
    if (domImplementation.hasFeature("LS", "3.0") && domImplementation.hasFeature("Core", "2.0")) {
      DOMImplementationLS domImplementationLS =
          (DOMImplementationLS) domImplementation.getFeature("LS", "3.0");
      LSSerializer lsSerializer = domImplementationLS.createLSSerializer();
      DOMConfiguration domConfiguration = lsSerializer.getDomConfig();
      if (domConfiguration.canSetParameter("format-pretty-print", Boolean.TRUE)) {
        lsSerializer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE);
        LSOutput lsOutput = domImplementationLS.createLSOutput();
        lsOutput.setEncoding("UTF-8");
        StringWriter stringWriter = new StringWriter();
        lsOutput.setCharacterStream(stringWriter);
        lsSerializer.write(document, lsOutput);
        return stringWriter.toString();
      } else {
        throw new RuntimeException(
            "DOMConfiguration 'format-pretty-print' parameter isn't settable.");
      }
    } else {
      throw new RuntimeException("DOM 3.0 LS and/or DOM 2.0 Core not supported.");
    }
  }

  /**
   * beautify attempt one.
   *
   * @param input the xml to beautify
   * @param indent the number of spaces for indentation
   * @return the string
   */
  public static String prettyFormat(String input, int indent) {
    try {
      Source xmlInput = new StreamSource(new StringReader(input));
      StringWriter stringWriter = new StringWriter();
      StreamResult xmlOutput = new StreamResult(stringWriter);
      TransformerFactory transformerFactory = TransformerFactory.newInstance();
      transformerFactory.setAttribute("indent-number", indent);
      Transformer transformer = transformerFactory.newTransformer();
      transformer.setOutputProperty(OutputKeys.INDENT, "yes");
      transformer.transform(xmlInput, xmlOutput);
      return xmlOutput.getWriter().toString();
    } catch (Exception e) {
      Utils.getLogger().error("Error while beautifying.",e);
      throw new RuntimeException(e); // simple exception handling, please
      // review it
    }
  }

  /**
   * beautify attempt two.
   *
   * @param xml the xml to beautify
   * @return the string
   */
  public static String format(String xml) {

    try {
      final InputSource src = new InputSource(new StringReader(xml));
      final Node document =
          DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(src).getDocumentElement();
      final Boolean keepDeclaration = xml.startsWith("<?xml");

      System.setProperty(
          DOMImplementationRegistry.PROPERTY,
          "com.sun.org.apache.xerces.internal.dom.DOMImplementationSourceImpl");

      final DOMImplementationRegistry registry = DOMImplementationRegistry.newInstance();
      final DOMImplementationLS impl = (DOMImplementationLS) registry.getDOMImplementation("LS");
      final LSSerializer writer = impl.createLSSerializer();

      writer.getDomConfig().setParameter("format-pretty-print", Boolean.TRUE); // Set
      // this
      // to
      // true
      // if
      // the
      // output
      // needs
      // to
      // be
      // beautified.
      writer.getDomConfig().setParameter("xml-declaration", keepDeclaration); // Set
      // this
      // to
      // true
      // if
      // the
      // declaration
      // is
      // needed
      // to
      // be
      // outputted.

      return writer.writeToString(document);
    } catch (Exception e) {
      Utils.getLogger().error("Error while beautifying.",e);
      throw new RuntimeException(e);
    }
  }
}
