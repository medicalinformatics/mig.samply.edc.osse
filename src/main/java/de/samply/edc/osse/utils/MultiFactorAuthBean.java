package de.samply.edc.osse.utils;

import static dev.samstevens.totp.util.Utils.getDataUriForImage;

import de.samply.edc.catalog.Vocabulary.Config.Authentication;
import de.samply.edc.control.AbstractViewBean;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.SessionBean;
import de.samply.edc.utils.Utils;
import de.samply.store.Resource;
import de.samply.store.Value;
import dev.samstevens.totp.code.CodeGenerator;
import dev.samstevens.totp.code.CodeVerifier;
import dev.samstevens.totp.code.DefaultCodeGenerator;
import dev.samstevens.totp.code.DefaultCodeVerifier;
import dev.samstevens.totp.code.HashingAlgorithm;
import dev.samstevens.totp.exceptions.QrGenerationException;
import dev.samstevens.totp.qr.QrData;
import dev.samstevens.totp.qr.QrGenerator;
import dev.samstevens.totp.qr.ZxingPngQrGenerator;
import dev.samstevens.totp.secret.DefaultSecretGenerator;
import dev.samstevens.totp.secret.SecretGenerator;
import dev.samstevens.totp.time.SystemTimeProvider;
import dev.samstevens.totp.time.TimeProvider;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import org.apache.commons.lang.RandomStringUtils;
import org.bouncycastle.crypto.generators.Argon2BytesGenerator;
import org.bouncycastle.crypto.generators.OpenBSDBCrypt;

@ManagedBean
@ViewScoped
public class MultiFactorAuthBean extends AbstractViewBean {

  public static String authMfaAuthenticator = "authentication.mfa.authenticator";
  public static String authMfaAuthenticatorSecret = "authentication.mfa.authenticator.secret";
  public static String authMfaRecovery = "authentication.mfa.recovery";

  SessionBean sessionBean;

  @Override
  @PostConstruct
  public void init() {
    sessionBean = (SessionBean) getSessionBean();
  }

  /**
   * Check if ToTp - MFA is system-wide active.
   *
   * @return true if system-wide active.
   */
  public boolean isToTpActive() {
    return isToTpActived();
  }

  /**
   * Check if ToTp - MFA is system-wide active.
   *
   * @return true if system-wide active.
   */
  public static boolean isToTpActived() {
    return ((ApplicationBean) Utils.getAB()).getOsseConfig()
        .getProperty(Authentication.systemTotpActive) != null && Boolean.parseBoolean(
        ((ApplicationBean) Utils.getAB()).getOsseConfig()
            .getProperty(Authentication.systemTotpActive).getValue());
  }

  /**
   * Check the current TotP Token.
   *
   * @return navigation path.
   */
  public String checkToken() {
    if (evaltotp((String) sessionBean.getFormvars().getEntry("current_totp_token"))) {
      sessionBean.setCurrentObject("mfa_successful", true);
      return sessionBean.finishLogin();
    } else {
      mfaFailed();
      Utils.addContextMessage(null, Utils.getResourceBundleString("summary_login"),
          Utils.getResourceBundleString("loginfailure"));
      if (OsseUtils.hasLoginTrysLeft(sessionBean.getCurrentUser().getUsername())) {
        sessionBean.goPage("login_mfa.xhtml");
        return "login_mfa";
      }
      return "login";
    }
  }

  /**
   * Handle failed mfa attempt by increase fail counter.
   */
  private void mfaFailed() {
    int failCount = OsseUtils.getFailCount(sessionBean.getCurrentUser().getUsername());
    OsseUtils.setFailCount(sessionBean.getCurrentUser().getUsername(), failCount + 1);
  }

  /**
   * Check the current ToTp.
   *
   * @param token the ToTp.
   * @return true if toke is valid.
   */
  public boolean evaltotp(String token) {
    TimeProvider timeProvider = new SystemTimeProvider();
    CodeGenerator codeGenerator = new DefaultCodeGenerator();
    CodeVerifier verifier = new DefaultCodeVerifier(codeGenerator, timeProvider);
    if (sessionBean.getCurrentUser().getResource()
        .getProperty(MultiFactorAuthBean.authMfaAuthenticatorSecret)
        == null) {
      return false;
    }
    if (verifier.isValidCode(
        sessionBean.getCurrentUser().getResource()
            .getProperty(MultiFactorAuthBean.authMfaAuthenticatorSecret)
            .getValue(), token)) {
      return true;
    } else if (evalRecoveryPassword(token)) {
      return true;
    }
    return false;
  }

  /**
   * Check the current RecoveryPassword.
   *
   * @param password the password.
   * @return true if password is valid.
   */
  public boolean evalRecoveryPassword(String password) {
    List<Value> recoveryCodes = sessionBean.getCurrentUser().getResource()
        .getProperties(MultiFactorAuthBean.authMfaRecovery);

    for (Iterator<Value> it = recoveryCodes.iterator(); it.hasNext(); ) {
      Value currPasswordHash = it.next();
      if (OpenBSDBCrypt.checkPassword(currPasswordHash.getValue(), password.toCharArray())) {
        recoveryCodes.remove(currPasswordHash);
        sessionBean.getDatabase().save(sessionBean.getCurrentUser().getResource());
        return true;
      }
    }
    return false;
  }

  /**
   * Generates ToTp Secret and stores it in user json.
   *
   * @return QR-Code data uri.
   * @throws QrGenerationException qr code generation failed.
   */
  public String generate2faSecret() throws QrGenerationException {
    //mfa already active nothing should be done;
    if (sessionBean.getCurrentUser().getResource()
        .getProperty(MultiFactorAuthBean.authMfaAuthenticator) != null && Boolean.parseBoolean(
        sessionBean.getCurrentUser().getResource()
            .getProperty(MultiFactorAuthBean.authMfaAuthenticator).getValue())) {
      return "";
    }
    SecretGenerator secretGenerator = new DefaultSecretGenerator();
    String secret = secretGenerator.generate();
    sessionBean.getCurrentUser().getResource()
        .setProperty(MultiFactorAuthBean.authMfaAuthenticatorSecret, secret);
    sessionBean.getDatabase().save(sessionBean.getCurrentUser().getResource());
    String username = sessionBean.getCurrentUser().getUsername() == null ? "osse"
        : sessionBean.getCurrentUser().getUsername();
    String registryName = ((ApplicationBean) Utils.getAB()).getOsseConfig()
        .getProperty("registryName").getValue();
    QrData data = new QrData.Builder().label(username).secret(secret).issuer("OSSE-" + registryName)
        .algorithm(HashingAlgorithm.SHA1).digits(6).period(30).build();
    QrGenerator generator = new ZxingPngQrGenerator();
    byte[] imageData = generator.generate(data);
    String mimeType = generator.getImageMimeType();
    String dataUri = getDataUriForImage(imageData, mimeType);
    return dataUri;
  }

  /**
   * Get the current secret for totp.
   *
   * @return the current secret for totp.
   */
  public String getCurrentSecret() {
    if (sessionBean.getCurrentUser().getResource()
        .getProperty(MultiFactorAuthBean.authMfaAuthenticatorSecret) != null) {
      return sessionBean.getCurrentUser().getResource()
          .getProperty(MultiFactorAuthBean.authMfaAuthenticatorSecret).getValue();
    }
    return "";
  }

  /**
   * Generates recovery codes and stores it in user json.
   *
   * @return
   */
  public List<String> generateRecoveryCodes() {
    //mfa already active nothing should be done;
    if (sessionBean.getCurrentUser().getResource()
        .getProperty(MultiFactorAuthBean.authMfaAuthenticator) != null && Boolean.parseBoolean(
        sessionBean.getCurrentUser().getResource()
            .getProperty(MultiFactorAuthBean.authMfaAuthenticator).getValue())) {
      return null;
    }
    sessionBean.getCurrentUser().getResource()
        .removeProperties(MultiFactorAuthBean.authMfaRecovery);
    List<String> passwords = new ArrayList<>();
    for (int i = 0; i < 3; i++) {
      UUID uuid = UUID.randomUUID();
      String[] pwArray = {RandomStringUtils.randomNumeric(4), RandomStringUtils.randomNumeric(4),
          RandomStringUtils.randomNumeric(4)};
      String currPw = String.join("-", pwArray);
      passwords.add(currPw);
      String currHash = OpenBSDBCrypt.generate(currPw.toCharArray(),
          RandomStringUtils.randomAlphanumeric(16).getBytes(StandardCharsets.UTF_8), 10);
      sessionBean.getCurrentUser().getResource()
          .addProperty(MultiFactorAuthBean.authMfaRecovery, currHash);
      sessionBean.getDatabase().save(sessionBean.getCurrentUser().getResource());
    }
    return passwords;
  }

  /**
   * Deativate mfa for userData.
   *
   * @param userData the user data.
   */
  public void deactivateMfa(HashMap<String, Object> userData) {
    if (userData == null || userData.isEmpty() || !(userData.get(
        "userResource") instanceof Resource)) {
      return;
    }
    Resource user = (Resource) userData.get("userResource");
    user.setProperty(MultiFactorAuthBean.authMfaAuthenticator, false);
    getSessionBean().getDatabase().save(user);
  }

  /**
   * Check if mfa is active for user.
   *
   * @param userData the user data.
   * @return true if mfa ist active.
   */
  public boolean isMfaActiveForUser(HashMap<String, Object> userData) {
    if (userData == null || userData.isEmpty() || !(userData.get(
        "userResource") instanceof Resource)) {
      return false;
    }
    Resource user = (Resource) userData.get("userResource");
    return user.getProperty(MultiFactorAuthBean.authMfaAuthenticator) != null && user.getProperty(
        MultiFactorAuthBean.authMfaAuthenticator).asBoolean();
  }

}

