/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * POJO of localized dashboard design config that holds the mapping between language and localized
 * infos.
 */
public class DashboardDesign {

  Map<String, DashboardDesignLocal> localizedDesign = new HashMap<>();

  public Map<String, DashboardDesignLocal> getLocalizedDesign() {
    return localizedDesign;
  }

  public void setLocalizedDesign(
      Map<String, DashboardDesignLocal> localizedDesign) {
    this.localizedDesign = localizedDesign;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) {
      return true;
    }
    if (o == null || getClass() != o.getClass()) {
      return false;
    }
    DashboardDesign that = (DashboardDesign) o;
    return Objects.equals(getLocalizedDesign(), that.getLocalizedDesign());
  }

  @Override
  public int hashCode() {
    return Objects.hash(getLocalizedDesign());
  }

  public String marshalToJson() throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.writeValueAsString(this);
  }

  public static DashboardDesign marshalToInstance(String json) throws JsonProcessingException {
    ObjectMapper mapper = new ObjectMapper();
    return mapper.readValue(json, DashboardDesign.class);
  }

}
