/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import com.google.common.base.Charsets;
import de.samply.auth.rest.AccessTokenDto;
import de.samply.common.http.HttpConnector;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.domain.Result;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.utils.Utils;
import de.samply.jsf.JsfUtils;
import de.samply.jsf.LoggedUser;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.web.mdrfaces.MdrContext;
import java.io.File;
import java.io.IOException;
import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;
import javax.faces.context.FacesContext;
import javax.faces.model.SelectItem;
import javax.rmi.CORBA.Util;
import javax.ws.rs.client.Client;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.w3c.dom.Document;
import org.xml.sax.SAXException;

/** General Utils class with OSSE helper methods. */
public abstract class OsseUtils implements Serializable {

  public static final String PATH_OSSE_SHARE = "/osse-share";
  public static final String PATH_MAINZELLISTE = "/mainzelliste";
  /** The path of osse_message. */
  private static final String MESSAGE_PATH = "de.samply.edc.osse.messages.osse_messages";

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -1914341370062594247L;

  /**
   * OSSE uses context message.
   * @param message as String
   * @return message as String
   */
  public static String contextMessage(String message) {
    return Utils.getResourceBundleString(MESSAGE_PATH, message);
  }

  /**
   * OSSE displays subject and message.
   * @param subject as String
   * @param message as String
   */
  public static void displayContextMessage(String subject, String message) {
    Utils.addContextMessage(
        Utils.getResourceBundleString(MESSAGE_PATH, subject),
        Utils.getResourceBundleString(MESSAGE_PATH, message)
    );
  }

  /**
   * Gets the callback URL to the registry, used by Mainzelliste callbacks. In case the Mainzelliste
   * has to use an localhost port that is different from the way a user accesses the registry
   *
   * @return the callback url to registry
   */
  public static String getCallbackUrlToRegistry() {
    String webPath = Utils.generateUrlToWebservice();
    String port = ((ApplicationBean) Utils.getAB()).getInternalMainzellistePort();
    String path;

    if (port == null || "".equals(port)) {
      path = webPath;
    } else {
      path = "http://localhost:"
          + ((ApplicationBean) Utils.getAB()).getInternalMainzellistePort()
          + FacesContext.getCurrentInstance().getExternalContext().getApplicationContextPath();
    }

    return path;
  }

  /**
   * Gets a list of location as select items.
   *
   * @param database the database
   * @return the locations as select items
   */
  public static List<SelectItem> getLocationsAsSelectItems(Database database) {
    List<SelectItem> locationsSelectItems = new ArrayList<>();

    List<Resource> groupList = database.getLocations(null);

    SelectItem item;

    for (Resource group : groupList) {
      item = new SelectItem();

      item.setLabel(group.getProperty(OSSEVocabulary.Location.Name).getValue());
      item.setValue(group.getProperty(OSSEVocabulary.Location.Name).getValue());
      locationsSelectItems.add(item);
    }

    return locationsSelectItems;
  }

  /**
   * Prepares data in the session used by the MDRFaces components.
   *
   * @return true: all ok, false: could not get an accessToken
   * @throws InvalidKeyException the invalid key exception
   * @throws NoSuchAlgorithmException the no such algorithm exception
   * @throws SignatureException the signature exception
   */
  public static Boolean mdrFacesClientStuff()
      throws InvalidKeyException, NoSuchAlgorithmException, SignatureException {
    HttpConnector httpConnector = ApplicationBean.getHttpConnector();

    LoggedUser loggedUser = new LoggedUser();

    if (httpConnector == null) {
      Utils.setSessionValue(JsfUtils.SESSION_USER, loggedUser);
      return false;
    }

    Client client = httpConnector.getJerseyClientForHTTPS(false);
    AccessTokenDto accessToken;
    accessToken = Auth.getAccessToken(client, Utils.getAB().getConfig());
    if (accessToken != null) {
      loggedUser.setAccessToken(accessToken.getAccessToken());
      Utils.setSessionValue(JsfUtils.SESSION_USER, loggedUser);
      return true;
    } else {
      Utils.setSessionValue(JsfUtils.SESSION_USER, loggedUser);
      return false;
    }
  }

  /**
   * Gets the URL to the Mainzelliste REST interface.
   *
   * @return the mainzelliste resturl
   */
  public static String getMainzellisteRestUrl() {
    // If a mainzelliste is configured, we use that URL
    String mainzellisteRestUrl =
        Utils.getAB().getConfig().getString(Vocabulary.Config.Mainzelliste.restUrlPublic);

    if (mainzellisteRestUrl == null || "".equals(mainzellisteRestUrl)) {
      // if the URL is empty, something is wrong
      Utils.getLogger().error("Empty config parameter: mainzelliste.rest");
    }

    return mainzellisteRestUrl;
  }

  /**
   * Gets the URL to the Mainzelliste REST interface via localhost-HTTP connection If no REST URL
   * for the Mainzelliste is set, then the mainzelliste runs on the same machine. In that case, the
   * connection from registry to mainzelliste can be done by a simple HTTP request to localhost, if
   * the port to it is set
   *
   * @return the mainzelliste internal resturl
   */
  public static String getMainzellisteInternalRestUrl() {
    String mainzellisteEdcRestUrl =
            Utils.getAB().getConfig().getString(Vocabulary.Config.Mainzelliste.restUrlEdc);
    if (mainzellisteEdcRestUrl == null || "".equals(mainzellisteEdcRestUrl)) {
      String mainzellistePublicRestUrl =
              Utils.getAB().getConfig().getString(Vocabulary.Config.Mainzelliste.restUrlPublic);
      return mainzellistePublicRestUrl;
    } else {
      return mainzellisteEdcRestUrl;
    }
  }

  /**
   * Gets the URL to the teiler REST interface.
   *
   * @return the teiler resturl
   */
  public static String getTeilerRestUrl() {
    // If a mainzelliste is configured, we use that URL
    String teilerRestUrl = Utils.getAB().getConfig().getString(Vocabulary.Config.Teiler.REST);

    if (teilerRestUrl == null || "".equals(teilerRestUrl)) {
      // else we grab what the user has typed into his browser to
      // access the registry and use that as URL-base
      String currentHostUrl = Utils.generateUrlToWebservice();
      teilerRestUrl = currentHostUrl + PATH_OSSE_SHARE;
    }

    return teilerRestUrl;
  }

  /**
   * TODO: add javadoc.
   */
  public static List<String> getMembersOfRecord(String recordUrn) {
    List<String> memberUrns = new ArrayList<>();
    try {
      MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();
      // The language Code doesn't matter here, since only the urns are needed, so just get the
      // english ones
      List<Result> recordMembers = mdrClient.getRecordMembers(recordUrn, "en");

      for (Result member : recordMembers) {
        memberUrns.add(member.getId());
      }

    } catch (MdrConnectionException | ExecutionException e) {
      Utils.getLogger().warn("Unable to get members of record:" + recordUrn, e);
    }

    return memberUrns;
  }

  /**
   * Get a map with query parameters from an URL.
   *
   * @param queryString e.g. http://www.foo.bar?p1=foo&p2=bar
   * @return a map with the query parameters. e.g. "p1"->"foo", "p2"->"bar"
   */
  public static Map<String, String> getQueryParameters(String queryString) {
    String decodedQueryString;
    try {
      decodedQueryString = URLDecoder.decode(queryString, Charsets.UTF_8.name());
    } catch (UnsupportedEncodingException e) {
      Utils.getLogger().warn("Unable to decode with url decoder:" + queryString, e);
      decodedQueryString = queryString;
    }

    Map<String, String> queryParameterMap = new HashMap<>();

    String[] tokens = decodedQueryString.split("[\\?&]+");

    for (String token : tokens) {
      String[] keyValueSplit = token.split("=");
      if (keyValueSplit != null && keyValueSplit.length == 2) {
        queryParameterMap.put(keyValueSplit[0], keyValueSplit[1]);
      }
    }

    return queryParameterMap;
  }

  /**
   * Get the number of failed login attempts by username.
   * @param username the username.
   * @return  number of failed login attempts. 0 if the username doesn't exist.
   */
  public static int getFailCount(String username) {
    Database mfaDb = new Database(true);
    Resource modUserRes = mfaDb.findUser(username);
    return modUserRes != null ? modUserRes.getProperty("failCounter").asInteger() : 0;
  }

  /**
   * Set the number of failed login attempts by username.
   * @param username the username.
   * @param count number of failed login attempts.
   */
  public static void setFailCount(String username, int count) {
    Database mfaDb = new Database(true);
    Resource modUserRes = mfaDb.findUser(username);
    modUserRes.setProperty("failCounter",count);
    mfaDb.save(modUserRes);
  }

  /**
   * Check if a user has login attempts left.
   * @param username the username.
   * @return true if the user has login attempts left.
   */
  public static boolean hasLoginTrysLeft(String username) {
    Database mfaDb = new Database(true);
    Resource modUserRes = mfaDb.findUser(username);
    int fails = modUserRes.getProperty("failCounter").asInteger();
    //getting the maxFailCounter, this is a stupid way to get the value but it works.
    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
    DocumentBuilder builder = null;
    try {
      builder = factory.newDocumentBuilder();
      Document document = builder.parse(new File(Utils.findConfigurationFile("backend.xml")));
      XPath xpath = XPathFactory.newInstance().newXPath();
      Integer maxFailCounter = Integer.valueOf(
          (String) xpath.evaluate("/descendant-or-self::security/maxFailCounter", document,
              XPathConstants.STRING));
      if (fails < maxFailCounter) {
        return true;
      }
    } catch (ParserConfigurationException | SAXException
        | IOException | XPathExpressionException e) {
      Utils.getLogger().error("Unable to check if user has login trys left, denied access.", e);
    }

    return false;
  }



}
