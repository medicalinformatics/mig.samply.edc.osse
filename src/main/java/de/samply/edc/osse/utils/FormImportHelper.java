/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import com.google.template.soy.SoyFileSet;
import com.google.template.soy.data.SanitizedContent.ContentKind;
import com.google.template.soy.data.SoyMapData;
import com.google.template.soy.data.UnsafeSanitizedContentOrdainer;
import com.google.template.soy.tofu.SoyTofu;
import com.opencsv.CSVWriter;
import de.samply.auth.client.GrantType;
import de.samply.auth.client.jwt.JwtException;
import de.samply.auth.rest.AccessTokenDto;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.Definition;
import de.samply.common.mdrclient.domain.EnumValidationType;
import de.samply.common.mdrclient.domain.Label;
import de.samply.common.mdrclient.domain.PermissibleValue;
import de.samply.common.mdrclient.domain.Result;
import de.samply.common.mdrclient.domain.Slot;
import de.samply.common.mdrclient.domain.Validations;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.catalog.Vocabulary.Config.FormEditor;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.bridgehead.ImportValidator;
import de.samply.edc.osse.catalog.Patterns;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.exceptions.OsseException;
import de.samply.edc.osse.model.MdrKey;
import de.samply.edc.osse.model.MdrKeyUsageData;
import de.samply.edc.osse.model.MdrKeyUsageData.FieldType;
import de.samply.edc.osse.model.MdrKeyUsageStore;
import de.samply.edc.utils.Utils;
import de.samply.form.dto.FormDetails;
import de.samply.form.dto.FormDetails.Item;
import de.samply.store.JSONResource;
import de.samply.web.enums.EnumDateFormat;
import de.samply.web.enums.EnumTimeFormat;
import de.samply.web.mdrfaces.validators.DateTimeValidator;
import de.samply.web.mdrfaces.validators.DateTimeValidator.DateTimeFormats;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.Serializable;
import java.io.StringWriter;
import java.nio.charset.Charset;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SignatureException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map.Entry;
import java.util.Objects;
import java.util.Set;
import java.util.TreeMap;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.ws.rs.client.Client;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import org.apache.commons.collections.ListUtils;
import org.apache.commons.configuration.Configuration;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringEscapeUtils;

/** The Class FormImportHelper. */
public class FormImportHelper implements Serializable {
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 557436206742046618L;

  /** The form name matrix. */
  private JSONResource formNameMatrix = null;

  /** The mdr key usage store. */
  private MdrKeyUsageStore mdrKeyUsageStore;

  /** List of MDR entities in a record. */
  private HashMap<String, LinkedHashSet<String>> recordHasMdrEntities = new HashMap<>();

  /** List of double MDR entity entries. */
  private HashMap<String, Set<MdrKeyUsageData>> warnDoubleUsage = new HashMap<>();

  /** The form names. */
  private JSONResource formNames = new JSONResource();

  /** The case forms. */
  private LinkedList<String> caseForms = new LinkedList<>();

  /** The episode forms. */
  private LinkedList<String> episodeForms = new LinkedList<>();

  /** The osse configuration. */
  private transient Configuration configuration;

  /** The validation storage of all mdr keys. */
  private JSONResource mdrEntityHasValidation;

  /** The application bean. */
  private transient ApplicationBean applicationBean;

  /**
   * Instantiates a new form import helper.
   * @param applicationBean the application bean.
   */
  public FormImportHelper(ApplicationBean applicationBean) {
    this.configuration = applicationBean.getConfig();
    this.applicationBean = applicationBean;

    mdrKeyUsageStore = new MdrKeyUsageStore();
  }

  /**
   * Stores the submitted FormDetails from the Formrepository as xml file.
   *
   * @param theForm the the form
   * @param id the id
   * @param version the version
   * @throws JAXBException the JAXB exception
   */
  private void storeXml(FormDetails theForm, String id, String version) throws JAXBException {
    JAXBContext jaxbContext = FormUtils.getFormDetailsJaxbContext();
    Marshaller jaxbMarshaller = jaxbContext.createMarshaller();

    jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

    String saveFileName = FormUtils.getFormXmlFilePath(Long.valueOf(id), Long.valueOf(version));
    File file = new File(saveFileName);
    if (file.exists()) {
      file.delete();
    }

    jaxbMarshaller.marshal(theForm, file);
  }

  /**
   * Method that converts a form from its XML representation to an XHTML file.
   *
   * @param frJerseyClient JerseyClient to access FormRepository
   * @param mdrClient the mdr client
   * @param accessToken Samply.AUTH AccessTokenDTO
   * @param id ID of the form
   * @param version Version of the form
   * @param episodeForm Is it an episodeForm or a caseForm?
   * @param formStorage The formStorage (fallback and recovery form storage in DB)
   * @param doWriteFiles the do write files
   * @param isArchivedForm the is archived form
   * @return JSONResource formStorage (the updated version)
   * @throws FileNotFoundException the file not found exception
   */
  public JSONResource doImport(
      Client frJerseyClient,
      MdrClient mdrClient,
      AccessTokenDto accessToken,
      String id,
      String version,
      Boolean episodeForm,
      JSONResource formStorage,
      Boolean doWriteFiles,
      Boolean isArchivedForm) {
    return doImport(frJerseyClient, mdrClient, accessToken, id, version, episodeForm, formStorage,
        doWriteFiles, isArchivedForm, "");
  }

  /**
   * Method that converts a form from its XML representation to an XHTML file.
   *
   * @param frJerseyClient JerseyClient to access FormRepository
   * @param mdrClient the mdr client
   * @param accessToken Samply.AUTH AccessTokenDTO
   * @param id ID of the form
   * @param version Version of the form
   * @param episodeForm Is it an episodeForm or a caseForm?
   * @param formStorage The formStorage (fallback and recovery form storage in DB)
   * @param doWriteFiles the do write files
   * @param isArchivedForm the is archived form
   * @param formRepoToken toke to access form repo
   * @return JSONResource formStorage (the updated version)
   * @throws FileNotFoundException the file not found exception
   */
  public JSONResource doImport(
      Client frJerseyClient,
      MdrClient mdrClient,
      AccessTokenDto accessToken,
      String id,
      String version,
      Boolean episodeForm,
      JSONResource formStorage,
      Boolean doWriteFiles,
      Boolean isArchivedForm,
      String formRepoToken) {
    if ("".equalsIgnoreCase(id) || id == null) {
      return formStorage;
    }

    String requiredTrueText = "#{empty param['disableValidation']}";

    try {
      FormDetails theForm = getForm(id, version, frJerseyClient, formRepoToken);

      // Store the XML file
      if (doWriteFiles) {
        storeXml(theForm, id, version);
      }

      String formNameAndVersion = makeFormVersionString(id, "" + theForm.getVersion());

      // collect form titles in all supported languages (+ fallback) for storing in database
      if (!isArchivedForm) {
        formNameMatrix.setProperty(formNameAndVersion, theForm.getName());
        formNames.setProperty(formNameAndVersion, theForm.getName());
        for (String locale : applicationBean.getSupportedLocales()) {
          FormDetails.FormI18n formi18 = FormUtils.getFormI18n(theForm, locale);

          if (formi18 != null) {
            String formNameTranslated = FormUtils.getFormI18n(theForm, locale).getName();
            formNameMatrix.setProperty(formNameAndVersion + "_" + locale, formNameTranslated);
            formNames.setProperty(formNameAndVersion + "_" + locale, formNameTranslated);
          }
        }
      }

      TreeMap<Integer, Item> formItemsAndPositions = new TreeMap<>();

      // get the items in the form and order them in the predefined
      // positions
      // we use the magic of a treemap for this
      for (Item item : theForm.getItems()) {
        formItemsAndPositions.put(item.getPosition(), item);
      }

      StringBuilder pageContentBuilder = new StringBuilder();

      // preparing closure
      SoyFileSet sfs =
          SoyFileSet.builder().add(new File(Utils.getRealPath("/formtemplate/form.soy"))).build();
      SoyTofu tofu = sfs.compileToTofu();
      SoyTofu simpleTofu = tofu.forNamespace("samply.forms");
      SoyMapData soyMapData;

      // run through the items of our form
      for (Entry<Integer, Item> formitemAndPosition : formItemsAndPositions.entrySet()) {
        Item formitem = formitemAndPosition.getValue();
        String mdrID = formitem.getMdrId();

        // Unfortunately, mdrID can be the text "richText", too, so we
        // catch that right away
        if ("richText".equalsIgnoreCase(mdrID)) {
          // Special case, a html text will be provided, we just copy
          // that into our XHTML

          String htmlText = formitem.getHtml();

          // fix for possible problem in form editor data
          if (htmlText == null) {
            htmlText = "";
          }

          // Fix for wrong charset
          htmlText =
              new String(
                  htmlText.getBytes(Charset.forName("UTF-8")), Charset.forName("Windows-1252"));

          Utils.getLogger().debug("RichText in form found:\n" + htmlText);

          soyMapData = new SoyMapData("formId", id, "formVersion", version, "itemPosition",
              formitem.getPosition());
          pageContentBuilder.append(simpleTofu.newRenderer(".patternRichTextField")
              .setData(soyMapData).renderHtml().getContent());

          continue;
        }

        MdrKey mdrKey = new MdrKey();

        // If it's not an MDRKey then it's something we don't want to
        // use here
        if (!mdrKey.parseString(mdrID)) {
          continue;
        }

        // initiate the mdr client as we will need labels and members of
        // this record
        String authUserId = (String) configuration.getProperty(Vocabulary.Config.Auth.UserId);

        mdrKey.initMdrClient(mdrClient, accessToken, authUserId);

        if (mdrKey.isRecord()) {
          // we want to store which record has which members
          LinkedHashSet<String> fieldsOfRecord = new LinkedHashSet<>();

          String recordLabel = "";
          String recordDefinition = "";

          if (!mdrKey.getLabel().isEmpty()) {
            recordLabel = mdrKey.getMdrLabel();
            recordDefinition = mdrKey.getMdrDefinition();
          }

          // A record is always presented in its own panelgroup
          soyMapData = new SoyMapData("mdrKey",mdrKey.toString());
          pageContentBuilder.append(simpleTofu.newRenderer(".patternRecordPanelBegin")
              .setData(soyMapData)
              .renderHtml().getContent());

          // Is this record a repeatable record?
          if (formitem.isRepeatable()) {

            // Determine whether to display the record in a datatable or in tabs
            Slot renderTypeSlot = mdrKey.getSlot("renderType");
            boolean renderAsTabs = renderTypeSlot != null
                && renderTypeSlot.getSlotValue().equals("TABS");

            // Add the repeatable record to the list of where in
            // which form
            mdrKeyUsageStore.addField(
                mdrKey.toString(),
                formNameAndVersion,
                episodeForm ? MdrKeyUsageData.FormType.EPISODE : MdrKeyUsageData.FormType.CASE,
                MdrKeyUsageData.FieldType.REPEATABLERECORD,
                formitem.isMandatory());

            StringBuilder pageContentColumnsBuilder = new StringBuilder();

            // each member of the record is a column in a
            // datatable or a row in a tab
            for (Result member : mdrKey.getMembers()) {
              MdrKey memberKey = new MdrKey(member.getId());
              memberKey.initMdrClient(mdrClient, accessToken, authUserId);

              String validationType = "";
              Validations validations = memberKey.getValidations();
              if (hasPermissibleValues(validations)) {
                validationType = "permissibleValues";
                Utils.getLogger().debug("MDR " + memberKey + " has values " + validationType);
              } else if (validations != null) {
                validationType = validations.getValidationType();
                Utils.getLogger().debug("MDR " + memberKey + " is type " + validationType);
              }

              soyMapData = new SoyMapData(
                  "mdrKey", memberKey.toString(), "mdrKeyPurified", memberKey.purify(),
                  "foo", "doo",
                  "mdrKeyRecordPurified", mdrKey.purify(),
                  "requiredText", (formitem.isMandatory() ? requiredTrueText : "false"));

              if (!renderAsTabs) {
                soyMapData.put("mdrKeyDataType",
                    (validationType == null) ? "" : validationType);
              }
              pageContentColumnsBuilder.append(simpleTofu.newRenderer(
                  renderAsTabs ? ".patternTabRow" : ".patternDatatableColumn")
                  .setData(soyMapData).renderHtml().getContent());

              // add the mdrkey of the member in the
              // fieldsOfRecord list
              fieldsOfRecord.add(memberKey.toString());
            }

            // build the datatable / tabs
            soyMapData = new SoyMapData("mdrKeyPurified", mdrKey.purify());
            pageContentBuilder.append(simpleTofu.newRenderer(
                renderAsTabs ? ".patternTabViewBegin" : ".patternDatatableBegin")
                .setData(soyMapData)
                .renderHtml().getContent());

            pageContentBuilder.append(pageContentColumnsBuilder.toString());

            pageContentBuilder.append(simpleTofu.newRenderer(
                renderAsTabs ? ".patternTabViewEnd" : ".patternDatatableEnd")
                .setData(soyMapData)
                .renderHtml().getContent());
          } else {
            // it is a non-repeatable record, which is represented
            // by a panelgroup with simple fields in it

            // Add the record to the list of where in which form
            mdrKeyUsageStore.addField(
                mdrKey.toString(),
                formNameAndVersion,
                episodeForm ? MdrKeyUsageData.FormType.EPISODE : MdrKeyUsageData.FormType.CASE,
                MdrKeyUsageData.FieldType.RECORD,
                formitem.isMandatory());

            for (int position = 0; position < mdrKey.getMembers().size(); position++) {
              MdrKey memberKey = new MdrKey(mdrKey.getMembers().get(position).getId());

              // as we need to save these fields in a special way,
              // the key of the field has to be
              // a combined mdrkey of the record and the member
              String combinedMdrId = mdrKey.purify() + "/" + memberKey.purify();

              soyMapData = new SoyMapData("mdrKey", memberKey.toString(), "combinedMdrKey",
                      combinedMdrId, "requiredText",
                      (formitem.isMandatory() ? requiredTrueText : "false"),
                      "option", option(formitem.getOption(), position));
              pageContentBuilder.append(simpleTofu.newRenderer(".patternRecord")
                  .setData(soyMapData)
                  .renderHtml().getContent());

              // add the mdrkey of the member in the
              // fieldsOfRecord list
              fieldsOfRecord.add(memberKey.toString());
            }
          }

          // close the record's panelgroups
          pageContentBuilder.append(simpleTofu.newRenderer(".patternRecordPanelEnd")
              .renderHtml().getContent());

          // Add this record in the list of records and fields
          if (!recordHasMdrEntities.containsKey(mdrKey.toString())) {
            recordHasMdrEntities.put(mdrKey.toString(), fieldsOfRecord);
          }

          continue;
        } // record end

        if (formitem.isRepeatable()) {
          // a repeatable formitem is represented as a datatable with
          // only one column

          mdrKeyUsageStore.addField(
              mdrKey.toString(),
              formNameAndVersion,
              episodeForm ? MdrKeyUsageData.FormType.EPISODE : MdrKeyUsageData.FormType.CASE,
              MdrKeyUsageData.FieldType.REPEATABLEFIELD,
              formitem.isMandatory());

          String validationType = "";
          Validations validations = mdrKey.getValidations();
          if (hasPermissibleValues(validations)) {
            validationType = "permissibleValues";
          } else if (validations != null) {
            validationType = validations.getValidationType();
          }

          soyMapData = new SoyMapData(
              "mdrKeyDataType", (validationType == null) ? "" : validationType,
              "mdrKey", mdrKey.toString(),
              "mdrKeyPurified", mdrKey.purify(),
              "requiredText", (formitem.isMandatory() ? requiredTrueText : "false"));
          pageContentBuilder.append(simpleTofu.newRenderer(".patternRepeatableField")
              .setData(soyMapData)
              .renderHtml().getContent());
        } else {
          mdrKeyUsageStore.addField(mdrKey.toString(), formNameAndVersion,
              episodeForm ? MdrKeyUsageData.FormType.EPISODE : MdrKeyUsageData.FormType.CASE,
              MdrKeyUsageData.FieldType.FIELD, formitem.isMandatory());

          // an ordinary field item

          soyMapData = new SoyMapData("mdrKey", mdrKey.toString(), "mdrKeyPurified",
              mdrKey.purify(), "option", (formitem.getOption()),
              "requiredText", (formitem.isMandatory() ? requiredTrueText : "false"));
          pageContentBuilder.append(simpleTofu.newRenderer(".patternField")
              .setData(soyMapData).renderHtml().getContent());
        }
      }

      // mdrkeys shall not be used twice in a registry, so we gather those
      // which are in a warning list
      for (Entry<String, Set<MdrKeyUsageData>> entry :
          mdrKeyUsageStore.getMdrKeyUsageStore().entrySet()) {
        if (entry.getValue().size() > 1) {
          warnDoubleUsage.put(entry.getKey(), entry.getValue());
        }
      }

      if (!isArchivedForm) {
        // store the form into the list of episodeforms, caseforms, and the
        // formnames
        if (episodeForm) {
          episodeForms.add(formNameAndVersion);
        } else {
          caseForms.add(formNameAndVersion);
        }
      }

      // Now we create the XHTML file of this form
      String saveFileName = formNameAndVersion + ".xhtml";
      soyMapData = new SoyMapData("pagedescription",
          theForm.getDescription(), "pageversion", theForm.getVersion(), "pagecontent",
          UnsafeSanitizedContentOrdainer.ordainAsSafe(pageContentBuilder.toString(),
          ContentKind.HTML),
          "pagename", theForm.getName(), "pageid", theForm.getId());
      String fileContent = simpleTofu.newRenderer(".newform")
          .setData(soyMapData).renderHtml().getContent();

      if (doWriteFiles) {
        Utils.writeFile("/forms/", saveFileName, fileContent);
      }

      // Also store form into DB, so in case of a redeploy we can recover
      // the forms
      formStorage.setProperty("/forms/" + saveFileName, fileContent);

    } catch (Exception ex) {
      Utils.getLogger().warn("Unable to generate xhtml form from xml.", ex);
    }

    return formStorage;
  }

  /**
   * Set Options for example: order of permitted values.
   * @param options as simple String format
   * @param position of permitted values array.
   */
  private String option(String options, int position) {
    if (options != null && !options.isEmpty()) {
      String[] optionsArray = options.split("%");
      if (position < optionsArray.length) {
        return optionsArray[position];
      }
    }
    return null;
  }

  /**
   * Loads a form from the form repository REST interface.
   *
   * @param formID the form id
   * @param version the version
   * @param client Jersey Client
   * @return FormDetails Form details as provided by XML
   * @throws InvalidKeyException the invalid key exception
   * @throws NoSuchAlgorithmException the no such algorithm exception
   * @throws SignatureException the signature exception
   * @throws JWTException the JWT exception
   */
  private FormDetails getForm(String formID, String version, Client client, String formRepoToken)
      throws InvalidKeyException, NoSuchAlgorithmException, SignatureException, JwtException {
    String restUrl =
        configuration.getProperty(FormEditor.formEditorRestUrl)
            + "/form/"
            + formID
            + "/"
            + version;

    if (formRepoToken.isEmpty()) {
      AccessTokenDto accessToken =
          Auth.getOAuth2Client(configuration).isUseSamplyAuth()
              ? Auth.getAccessToken(client, configuration)
              : Auth.getAccessToken(client, configuration, GrantType.PASSWORD);
      formRepoToken = "Bearer " + accessToken.getAccessToken();
    }


    // XXX: For debugging purposes
    // client.addFilter(new LoggingFilter(System.out));

    Response response =
        client
            .target(restUrl)
            .request()
            .header(HttpHeaders.AUTHORIZATION, formRepoToken)
            .accept(MediaType.APPLICATION_JSON)
            .get(Response.class);
    return response.readEntity(FormDetails.class);
  }

  /**
   * Make form version string.
   *
   * @param id the form id
   * @param version the form version
   * @return the form version string in format "form_{id}_ver-{version}"
   */
  private String makeFormVersionString(String id, String version) {
    return "form_" + id + "_ver-" + version;
  }

  /** Clear data. */
  public void clearData() {
    mdrKeyUsageStore = new MdrKeyUsageStore();

    recordHasMdrEntities = new HashMap<>();
    warnDoubleUsage = new HashMap<>();

    formNames = new JSONResource();
    caseForms = new LinkedList<>();
    episodeForms = new LinkedList<>();

    formNameMatrix = new JSONResource();
    mdrEntityHasValidation = new JSONResource();
  }

  /**
   * Check if validations include permissible values.
   *
   * @param dataElementValidations the data element validations
   * @return true if {@link Validations} include permissible values, false otherwise
   */
  private boolean hasPermissibleValues(final Validations dataElementValidations) {
    return dataElementValidations == null
        || dataElementValidations.getPermissibleValues() != null
            && dataElementValidations.getPermissibleValues().size() > 0;
  }

  /**
   * gets the validation info as JSONResource for a mdrkey.
   *
   * @param mdrKey the mdr key
   * @return the validation json
   * @throws OsseException the OSSE exception
   * @throws MdrConnectionException the mdr connection exception
   * @throws MdrInvalidResponseException the mdr invalid response exception
   * @throws ExecutionException the execution exception
   */
  private JSONResource getValidationJson(MdrKey mdrKey)
      throws OsseException, MdrConnectionException, MdrInvalidResponseException,
          ExecutionException {
    JSONResource temp = new JSONResource();

    Validations dataElementValidations = mdrKey.getValidations();
    String validationType = dataElementValidations.getValidationType();
    temp.addProperty("type", validationType);

    if (dataElementValidations.getErrorMessages() != null
        && !dataElementValidations.getErrorMessages().isEmpty()) {
      temp.addProperty("error", dataElementValidations.getErrorMessages().get(0)
          .getDefinition());
    } else {
      temp.addProperty("error", "Some error");
    }

    if (hasPermissibleValues(dataElementValidations)) {
      for (PermissibleValue permissibleValue : dataElementValidations.getPermissibleValues()) {
        temp.addProperty("permissibleValue", permissibleValue.getValue());
      }
      temp.setProperty("type", "permissibleValues");
    } else if (EnumValidationType.INTEGERRANGE.name().equalsIgnoreCase(validationType)) {
      Pattern pattern = Pattern.compile(ImportValidator.FLOAT_RANGE_REGEX);
      Matcher matcher = pattern.matcher(dataElementValidations.getValidationData());

      if (matcher.find()) {
        String min = matcher.group(1);
        String max = matcher.group(2);

        temp.addProperty("min", min == null ? "" : min);
        temp.addProperty("max", max == null ? "" : max);
      }
    } else if (EnumValidationType.FLOATRANGE.name().equalsIgnoreCase(validationType)) {
      Pattern pattern = Pattern.compile(ImportValidator.FLOAT_RANGE_REGEX);
      Matcher matcher = pattern.matcher(dataElementValidations.getValidationData());

      if (matcher.find()) {
        String min = matcher.group(1);
        String max = matcher.group(2);

        temp.addProperty("min", min == null ? "" : min);
        temp.addProperty("max", max == null ? "" : max);
      }
    } else if (EnumValidationType.REGEX.name().equalsIgnoreCase(validationType)) {
      String regex = dataElementValidations.getValidationData();
      temp.addProperty("regexp", regex);
    } else if (EnumValidationType.DATE.name().equalsIgnoreCase(validationType)) {
      EnumDateFormat enumDateFormat =
          EnumDateFormat.valueOf(dataElementValidations.getValidationData());
      temp.addProperty("enumDateFormat", enumDateFormat.name());
    } else if (EnumValidationType.TIME.name().equalsIgnoreCase(validationType)) {
      EnumTimeFormat enumTimeFormat =
          EnumTimeFormat.valueOf(dataElementValidations.getValidationData());
      temp.addProperty("enumTimeFormat", enumTimeFormat.name());
    } else if (EnumValidationType.DATETIME.name().equalsIgnoreCase(validationType)) {
      DateTimeFormats dateTimeFormats =
          DateTimeValidator.getDateTimeFormats(dataElementValidations.getValidationData());

      temp.addProperty("enumDateFormat", dateTimeFormats.getDateFormat().name());
      temp.addProperty("enumTimeFormat", dateTimeFormats.getTimeFormat().name());
    }
    return temp;
  }

  /**
   * Fills out a JSONResourced list which fields (mdrs) that contains the info of their validation.
   *
   * @param mdrClient the mdr client
   * @param accessToken An accesstoken to contact the MDR
   * @throws OsseException the OSSE exception
   */
  public void fillMdrEntityHasValidation(MdrClient mdrClient, AccessTokenDto accessToken)
      throws OsseException {
    String authUserId = (String) configuration.getProperty(Vocabulary.Config.Auth.UserId);

    for (String key : mdrKeyUsageStore.getMdrKeyUsageStore().keySet()) {
      MdrKey mdrKey = new MdrKey(key);
      mdrKey.initMdrClient(mdrClient, accessToken, authUserId);

      if (mdrKey.isRecord()) {
        try {
          for (MdrKey memberKey : mdrKey.getMembersSet()) {
            JSONResource temp = getValidationJson(memberKey);

            /**
             * If the value domain type is invalid, the type is null and we just ignore the MDR key
             */
            if (temp.getString("type") != null) {
              mdrEntityHasValidation.setProperty(memberKey.toString(), temp);
            }
          }
        } catch (MdrConnectionException | ExecutionException | MdrInvalidResponseException e) {
          Utils.getLogger().debug("ERROR for a key in record " + mdrKey.toString(), e);
        }
      } else {
        try {
          JSONResource temp = getValidationJson(mdrKey);

          /**
           * If the value domain type is invalid, the type is null and we just ignore the MDR key
           */
          if (temp.getString("type") != null) {
            mdrEntityHasValidation.setProperty(mdrKey.toString(), temp);
          }
        } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
          Utils.getLogger().debug("ERROR for key " + mdrKey.toString(), e);
        }
      }
    }
  }

  /**
   * Creates a datafield XML fragment.
   *
   * @param key the mdrkey
   * @param fieldType the field type
   * @return the datafield xml
   * @throws MdrConnectionException the mdr connection exception
   * @throws MdrInvalidResponseException the mdr invalid response exception
   * @throws ExecutionException the execution exception
   * @throws OsseException the OSSE exception
   */
  private String getDatafieldXml(MdrKey key, FieldType fieldType)
      throws MdrConnectionException, MdrInvalidResponseException, ExecutionException,
      OsseException {
    Definition dataElementDefinition = key.getDefinition();

    String fieldLabel = "";
    String fieldDefinition = "";

    if (!dataElementDefinition.getDesignations().isEmpty()) {
      fieldLabel = dataElementDefinition.getDesignations().get(0).getDesignation();
      fieldDefinition = dataElementDefinition.getDesignations().get(0).getDefinition();
    }

    return String.format(
        fieldType == FieldType.FIELD
            ? Patterns.patternXSDField
            : Patterns.patternXSDRepeatableField,
        key.forXml(),
        StringEscapeUtils.escapeXml(fieldDefinition),
        StringEscapeUtils.escapeXml(fieldLabel));
  }

  /**
   * Creates a record XML fragment.
   *
   * @param key the key
   * @param fieldType the field type
   * @return the record xml
   * @throws MdrConnectionException the mdr connection exception
   * @throws ExecutionException the execution exception
   * @throws MdrInvalidResponseException the mdr invalid response exception
   * @throws OsseException the OSSE exception
   */
  private String getRecordXml(MdrKey key, FieldType fieldType)
      throws MdrConnectionException, ExecutionException, MdrInvalidResponseException,
      OsseException {
    List<Label> label = key.getLabel();
    List<Result> members = key.getMembers();

    StringBuilder recordMemberXml = new StringBuilder();

    for (Result member : members) {
      String memberMdrKey = member.getId();
      MdrKey memberKey = new MdrKey(memberMdrKey);
      memberKey.setMdrClient(key.getMdrClient());
      memberKey.setAccessToken(key.getAccessToken());
      memberKey.setAuthUserId(key.getAuthUserId());
      recordMemberXml.append(getDatafieldXml(memberKey, FieldType.FIELD));
    }

    String fieldLabel = "";
    String fieldDefinition = "";

    if (!label.isEmpty()) {
      fieldLabel = label.get(0).getDesignation();
      fieldDefinition = label.get(0).getDefinition();
    }

    return String.format(
        fieldType == FieldType.RECORD
            ? Patterns.patternXSDRecord
            : Patterns.patternXSDRepeatableRecord,
        key.forXml(),
        StringEscapeUtils.escapeXml(fieldDefinition),
        recordMemberXml.toString(),
        StringEscapeUtils.escapeXml(fieldLabel));
  }

  /**
   * Creates the XSD for the imported forms.
   *
   * @param mdrClient the mdr client
   * @param accessToken the access token
   * @return the string
   * @throws MdrConnectionException the mdr connection exception
   * @throws ExecutionException the execution exception
   * @throws MdrInvalidResponseException the mdr invalid response exception
   * @throws OsseException the OSSE exception
   */
  public String makeV2Xsd(MdrClient mdrClient, AccessTokenDto accessToken)
      throws MdrConnectionException, ExecutionException, MdrInvalidResponseException,
      OsseException {
    Utils.getLogger().debug("Creating Import XSD");

    String authUserId = (String) configuration.getProperty(Vocabulary.Config.Auth.UserId);

    // TODO: create a real xml namespace based on forms imported and own
    // registry/bridgehead
    // TODO: needs versionizing
    final String xmlNamespace = "http://registry.samply.de/schemata/import_v1";

    String mdrNamespace = null;

    HashMap<String, StringBuilder> caseFormFields = new LinkedHashMap<>();
    HashMap<String, StringBuilder> episodeFormFields = new LinkedHashMap<>();

    for (String formKey : (List<String>) ListUtils.union(caseForms, episodeForms)) {
      Pattern pattern = Pattern.compile("form_(\\d+)_ver-(\\d+)");
      Matcher matcher = pattern.matcher(formKey);
      if (matcher.find()) {
        FormDetails formDetails =
            FormUtils.loadFormFromXml(
                Long.valueOf(matcher.group(1)), Long.valueOf(matcher.group(2)));
        if (formDetails instanceof FormDetails) {
          for (Item item : formDetails.getItems()) {
            // skip html items
            if (item.getMdrId().equals("richText")) {
              continue;
            }

            MdrKey key = new MdrKey(item.getMdrId());
            key.initMdrClient(mdrClient, accessToken, authUserId);

            if (mdrNamespace == null) {
              mdrNamespace = key.namespaceForXml();
            }

            String tempNewField;

            if (key.getKeyType() == MdrKey.Type.DATAELEMENT) {
              // field is a data element
              FieldType fieldType;
              if (item.isRepeatable()) {
                fieldType = FieldType.REPEATABLEFIELD;
              } else {
                fieldType = FieldType.FIELD;
              }
              tempNewField = getDatafieldXml(key, fieldType);
            } else {
              // field is a record (or anything else)
              FieldType fieldType;
              if (item.isRepeatable()) {
                fieldType = FieldType.REPEATABLERECORD;
              } else {
                fieldType = FieldType.RECORD;
              }
              tempNewField = getRecordXml(key, fieldType);
            }

            String formName = formKey;

            if (caseForms.contains(formKey)) {
              StringBuilder caseFields;
              if (Utils.isBridgehead(configuration)) {
                formName = "Basic Data";
              }

              if (caseFormFields.containsKey(formName)) {
                caseFields = caseFormFields.get(formName);
              } else {
                caseFields = new StringBuilder();
              }

              caseFields.append(tempNewField);
              caseFormFields.put(formName, caseFields);
            } else if (episodeForms.contains(formKey)) {
              StringBuilder episodeFields;
              if (Utils.isBridgehead(configuration)) {
                formName = "Longitudinal Data";
              }

              if (episodeFormFields.containsKey(formName)) {
                episodeFields = episodeFormFields.get(formName);
              } else {
                episodeFields = new StringBuilder();
              }

              episodeFields.append(tempNewField);
              episodeFormFields.put(formName, episodeFields);
            }
          }
        }
      }
    }

    String soyFile = Utils.getRealPath("/formtemplate/specificxsd2.soy");
    SoyFileSet sfs = SoyFileSet.builder().add(new File(soyFile)).build();
    SoyTofu tofu = sfs.compileToTofu();
    SoyTofu simpleTofu = tofu.forNamespace("samply.xsd");

    StringBuilder caseForms = new StringBuilder();
    int counter = -1;
    for (String caseForm : caseFormFields.keySet()) {
      String counterText = "";
      counter++;
      if (counter > 0) {
        counterText = String.valueOf(counter);
      }
      SoyMapData data = new SoyMapData("caseFields", UnsafeSanitizedContentOrdainer
          .ordainAsSafe(caseFormFields.get(caseForm).toString(), ContentKind.HTML), "formName",
          caseForm, "formDefinition", "", "formCounter", counterText);
      String fileContent = simpleTofu.newRenderer(".caseform")
          .setData(data).renderHtml().getContent();
      caseForms.append(fileContent);
    }

    StringBuilder episodeForms = new StringBuilder();
    counter = -1;
    for (String episodeForm : episodeFormFields.keySet()) {
      String counterText = "";
      counter++;
      if (counter > 0) {
        counterText = counter + "";
      }
      SoyMapData data = new SoyMapData("episodeFields", UnsafeSanitizedContentOrdainer
          .ordainAsSafe(episodeFormFields.get(episodeForm).toString(), ContentKind.HTML),
          "formName", episodeForm, "formDefinition", "", "formCounter", counterText);
      String fileContent = simpleTofu.newRenderer(".episodeform")
          .setData(data).renderHtml().getContent();
      episodeForms.append(fileContent);
    }

    String mdrUrl = (String) configuration.getProperty(Vocabulary.Config.Mdr.REST);
    SoyMapData data = new SoyMapData("mdrurl", mdrUrl, "mdrnamespace",
        (mdrNamespace == null) ? "" : mdrNamespace, "mynamespace", xmlNamespace, "caseForms",
        UnsafeSanitizedContentOrdainer.ordainAsSafe(caseForms.toString(), ContentKind.HTML),
        "episodeForms",
        UnsafeSanitizedContentOrdainer.ordainAsSafe(episodeForms.toString(), ContentKind.HTML));
    String fileContent = simpleTofu.newRenderer(".newxsd")
        .setData(data).renderHtml().getContent();

    Utils.writeFile("/schemata/", "import_v1.xsd", fileContent);

    Utils.getLogger().info("Creation of Import XSD done.");

    return fileContent;
  }

  /**
   * Gets the form name matrix.
   *
   * @return the formNameMatrix
   */
  public JSONResource getFormNameMatrix() {
    return formNameMatrix;
  }

  /**
   * Sets the form name matrix.
   *
   * @param formNameMatrix the formNameMatrix to set
   */
  public void setFormNameMatrix(JSONResource formNameMatrix) {
    this.formNameMatrix = formNameMatrix;
  }

  /**
   * Gets the record has mdr entities.
   *
   * @return the recordHasMdrEntities
   */
  public HashMap<String, LinkedHashSet<String>> getRecordHasMdrEntities() {
    return recordHasMdrEntities;
  }

  /**
   * Sets the record has mdr entities.
   *
   * @param recordHasMdrEntities the recordHasMdrEntities to set
   */
  public void setRecordHasMdrEntities(HashMap<String, LinkedHashSet<String>> recordHasMdrEntities) {
    this.recordHasMdrEntities = recordHasMdrEntities;
  }

  /**
   * Gets the warn double usage.
   *
   * @return the warnDoubleUsage
   */
  public HashMap<String, Set<MdrKeyUsageData>> getWarnDoubleUsage() {
    return warnDoubleUsage;
  }

  /**
   * Gets the form names.
   *
   * @return the formNames
   */
  public JSONResource getFormNames() {
    return formNames;
  }

  /**
   * Sets the form names.
   *
   * @param formNames the formNames to set
   */
  public void setFormNames(JSONResource formNames) {
    this.formNames = formNames;
  }

  /**
   * Gets the case forms.
   *
   * @return the caseForms
   */
  public LinkedList<String> getCaseForms() {
    return caseForms;
  }

  /**
   * Sets the case forms.
   *
   * @param caseForms the caseForms to set
   */
  public void setCaseForms(LinkedList<String> caseForms) {
    this.caseForms = caseForms;
  }

  /**
   * Gets the episode forms.
   *
   * @return the episodeForms
   */
  public LinkedList<String> getEpisodeForms() {
    return episodeForms;
  }

  /**
   * Sets the episode forms.
   *
   * @param episodeForms the episodeForms to set
   */
  public void setEpisodeForms(LinkedList<String> episodeForms) {
    this.episodeForms = episodeForms;
  }

  /**
   * Gets the mdr entity has validation.
   *
   * @return the mdrEntityHasValidation
   */
  public JSONResource getMdrEntityHasValidation() {
    return mdrEntityHasValidation;
  }

  /**
   * Sets the mdr entity has validation.
   *
   * @param mdrEntityHasValidation the mdrEntityHasValidation to set
   */
  public void setMdrEntityHasValidation(JSONResource mdrEntityHasValidation) {
    this.mdrEntityHasValidation = mdrEntityHasValidation;
  }

  /**
   * Gets the configuration.
   *
   * @return the configuration
   */
  public Configuration getConfiguration() {
    return configuration;
  }

  /**
   * Sets the configuration.
   *
   * @param configuration the configuration to set
   */
  public void setConfiguration(Configuration configuration) {
    this.configuration = configuration;
  }

  /**
   * Gets the mdr key usage store.
   *
   * @return the mdrKeyUsageStore
   */
  public MdrKeyUsageStore getMdrKeyUsageStore() {
    return mdrKeyUsageStore;
  }

  /**
   * Creates a list in csv format of all forms and data elements, currently used in the registry.
   *
   * @return The data element list as csv string.
   */
  public String createDataElementListCsv(MdrClient mdrClient, AccessTokenDto accessToken)
      throws MdrConnectionException, ExecutionException, MdrInvalidResponseException,
      OsseException {
    String result = null;
    StringWriter resultWriter = new StringWriter();
    Utils.getLogger().debug("Create data element list csv - starting...");

    // csv file
    CSVWriter csvWriter = new CSVWriter(resultWriter);
    List<String[]> csv = new ArrayList<>();
    // write header
    csv.add(("Form#Form ID:Version#Form Type#Record URN#Record Designation#Record Mandatory#Record "
        + "Repeatable#Data Element - URN#Data Element - Designation#Data Element - Definition#Data "
        + "Element Mandatory#Data Element Repeatable#Data Type#Data Format#Unit of "
        + "Measure#Permissible Values ...")
            .split("#"));

    String authUserId = (String) configuration.getProperty(Vocabulary.Config.Auth.UserId);

    String mdrNamespace = null;

    for (String formKey : (List<String>) ListUtils.union(caseForms, episodeForms)) {
      Pattern pattern = Pattern.compile("form_(\\d+)_ver-(\\d+)");
      Matcher matcher = pattern.matcher(formKey);
      if (matcher.find()) {
        FormDetails formDetails =
            FormUtils.loadFormFromXml(
                Long.valueOf(matcher.group(1)), Long.valueOf(matcher.group(2)));
        if (formDetails instanceof FormDetails) {
          for (Item item : formDetails.getItems()) {
            // skip html items
            if (item.getMdrId().equals("richText")) {
              continue;
            }

            MdrKey key = new MdrKey(item.getMdrId());
            key.initMdrClient(mdrClient, accessToken, authUserId);

            if (mdrNamespace == null) {
              mdrNamespace = key.namespaceForXml();
            }

            // currently necessary to explicitly set form type
            String formType = "";
            if (caseForms.contains(formKey)) {
              formType = "Basic";
            } else if (episodeForms.contains(formKey)) {
              formType = "Longitudinal";
            }

            String formName = formDetails.getName();

            String[] tempNewField;

            if (key.getKeyType() == MdrKey.Type.DATAELEMENT) {
              // field is a data element
              Definition dataElementDefinition = key.getDefinition();

              tempNewField =
                  (String[])
                      ArrayUtils.addAll(
                          new String[] {
                            formName,
                            String.valueOf(formDetails.getId())
                              + ":"
                              + String.valueOf(formDetails.getVersion()),
                            formType,
                            "",
                            "",
                            "",
                            "",
                            key.toString()
                          },
                          getDataElementInformationForCsv(dataElementDefinition, item));
              csv.add(tempNewField);
            } else {
              // field is a record (or anything else)
              List<Label> label = key.getLabel();
              List<Result> members = key.getMembers();

              String recordDesignation = "";

              if (!label.isEmpty()) {
                recordDesignation = label.get(0).getDesignation();
              }

              for (Result member : members) {
                String memberMdrKey = member.getId();
                MdrKey memberKey = new MdrKey(memberMdrKey);
                memberKey.setMdrClient(key.getMdrClient());
                memberKey.setAccessToken(key.getAccessToken());
                memberKey.setAuthUserId(key.getAuthUserId());

                Definition dataElementDefinition = memberKey.getDefinition();

                tempNewField =
                    (String[])
                        ArrayUtils.addAll(
                            new String[] {
                              formName,
                              String.valueOf(formDetails.getId())
                                + ":"
                                + String.valueOf(formDetails.getVersion()),
                              formType,
                              key.toString(),
                              recordDesignation,
                              String.valueOf(item.isMandatory()),
                              String.valueOf(item.isRepeatable()),
                              memberKey.toString()
                            },
                            getDataElementInformationForCsv(dataElementDefinition, null));
                csv.add(tempNewField);
              }
            }
          }
        }
      }
    }

    try {
      csvWriter.writeAll(csv);
      csvWriter.close();
      result = resultWriter.toString();
      Utils.writeFile("/schemata/", "data_element_list.csv", result);
    } catch (IOException e) {
      Utils.getLogger().error("Create data element list csv - error while closing CSVWriter", e);
    }

    Utils.getLogger().info("Create data element list csv - finished");

    return result;
  }

  /**
   * Collects all the information needed for a textual description of the data element and returns
   * it as a string array. Useful for csv export.
   *
   * @param dataElementDefinition The data element to extract the information from.
   * @param item The form item definition to extract further information from.
   * @return A string array containing the information. May contain empty fields if parts of
   *     dataElementDefinition are missing.
   */
  String[] getDataElementInformationForCsv(Definition dataElementDefinition, Item item) {
    String fieldLabel = "";
    String fieldDefinition = "";
    String fieldDatatype = "";
    String fieldFormat = "";
    String fieldUnit = "";
    String[] permissibleValueArray = new String[] {};

    if (dataElementDefinition instanceof Definition) {
      if (!dataElementDefinition.getDesignations().isEmpty()) {
        fieldLabel = dataElementDefinition.getDesignations().get(0).getDesignation();
        fieldDefinition = dataElementDefinition.getDesignations().get(0).getDefinition();
      }

      Validations validations = dataElementDefinition.getValidation();
      if (validations != null) {
        fieldDatatype = Objects.toString(validations.getDatatype(), "");
        fieldFormat = Objects.toString(validations.getFormat(), "");
        fieldUnit = Objects.toString(validations.getUnitOfMeasure(), "");
      }

      // insert all permissible values, if there are any
      List<PermissibleValue> permissibleValues = validations.getPermissibleValues();
      if (permissibleValues != null && permissibleValues.size() > 0) {
        for (PermissibleValue permissibleValue : permissibleValues) {
          permissibleValueArray =
              (String[])
                  ArrayUtils.addAll(
                      permissibleValueArray,
                      new String[] {permissibleValue.getMeanings().get(0).getDesignation()});
        }
      }
    }

    String[] tempString;
    if (item != null) {
      tempString =
          (String[])
              ArrayUtils.addAll(
                  new String[] {
                    fieldLabel,
                    fieldDefinition,
                    String.valueOf(item.isMandatory()),
                    String.valueOf(item.isRepeatable()),
                    fieldDatatype,
                    fieldFormat,
                    fieldUnit
                  },
                  permissibleValueArray);
    } else {
      tempString =
          (String[])
              ArrayUtils.addAll(
                  new String[] {
                    fieldLabel, fieldDefinition, "", "", fieldDatatype, fieldFormat, fieldUnit
                  },
                  permissibleValueArray);
    }

    return tempString;
  }
}
