/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Helper class for chartjs dataset json in java.
 */
public class ChartJsDataset {

  private List<String> data = new ArrayList<>();
  private Map<String, String> options = new HashMap<>();

  /**
   * Add a value to the dataset.
   */
  public void addValue(String value) {
    data.add(value);
  }

  /**
   * Get the json string representation of this chart dataset.
   * @return json string.
   */
  public String getDatasetString() {
    StringBuilder stringBuilder = new StringBuilder();
    stringBuilder.append("{");
    stringBuilder.append("\"data\": [" + String.join(",", data) + "]");
    if (!options.isEmpty()) {
      options.entrySet().stream().forEach(entry -> {
        stringBuilder.append(", " + entry.getKey() + " : " + entry.getValue());
      });
    }
    stringBuilder.append("}");
    return stringBuilder.toString();
  }
  //  example
  //    {
  //      label: "Population (millions)",
  //          backgroundColor: ["#3e95cd", "#8e5ea2","#3cba9f","#e8c3b9","#c45850"],
  //      data: [2478,5267,734,784,433]
  //    }

}
