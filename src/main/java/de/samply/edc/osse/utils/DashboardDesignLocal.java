/*
 * Copyright (C) 2021 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * POJO of localized dashboard design.
 */
public class DashboardDesignLocal {
  String registryName = "";
  String greetingsText = "";
  List<Map<String,String>> documentList = new ArrayList<>();
  List<Map<String,String>> linkList = new ArrayList<>();

  public String getRegistryName() {
    return registryName;
  }

  public void setRegistryName(String registryName) {
    this.registryName = registryName;
  }

  public String getGreetingsText() {
    return greetingsText;
  }

  public void setGreetingsText(String greetingsText) {
    this.greetingsText = greetingsText;
  }

  public List<Map<String, String>> getDocumentList() {
    return documentList;
  }

  public void setDocumentList(
      List<Map<String, String>> documentList) {
    this.documentList = documentList;
  }

  public List<Map<String, String>> getLinkList() {
    return linkList;
  }

  public void setLinkList(List<Map<String, String>> linkList) {
    this.linkList = linkList;
  }

  /**
   * JSF hook to add new links.
   */
  public void onAddNewLink() {
    Map<String,String> entry = new HashMap<>();
    entry.put("name","");
    entry.put("url","");
    linkList.add(entry);
  }

}
