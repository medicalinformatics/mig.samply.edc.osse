/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.phaselistener;

import de.samply.edc.osse.model.User;
import de.samply.edc.utils.Utils;
import java.util.HashMap;
import java.util.Map;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

/**
 * This listener is used to ensure that all current users are logged out if the singleton
 * SessionsKillerSingleton.doKillAllSessions is set to true
 *
 * <p>Used for example after a fresh import of data via the REST interface
 */
public class AllSessionsKiller implements PhaseListener {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 554203307473472015L;

  /**
   * Checks if the killer singleton is set and logs out all users.
   *
   * @param event the event
   * @see javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
   */
  @Override
  public void afterPhase(PhaseEvent event) {

    if (de.samply.edc.osse.model.SessionsKillerSingleton.instance.getDoKillAllSessions()) {
      // reset the killer
      de.samply.edc.osse.model.SessionsKillerSingleton.instance.setDoKillAllSessions(false);

      Map<User, HttpSession> foo = new HashMap<>(User.getLogins());
      for (User u : foo.keySet()) {
        Utils.getLogger().debug("Killing user session of user " + u.getUsername());
        u.logMeOut();
      }
    }
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
   */
  @Override
  public void beforePhase(PhaseEvent event) {}

  /*
   * (non-Javadoc)
   *
   * @see javax.faces.event.PhaseListener#getPhaseId()
   */
  @Override
  public PhaseId getPhaseId() {
    return PhaseId.RESTORE_VIEW;
  }
}
