/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.validator;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.catalog.EpisodePatterns;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.validator.Validator;
import javax.faces.validator.ValidatorException;

/** Validator for the configurable episode label. */
public class EpisodeValidator implements Validator {

  /*
   * (non-Javadoc)
   *
   * @see
   * javax.faces.validator.Validator#validate(javax.faces.context.FacesContext
   * , javax.faces.component.UIComponent, java.lang.Object)
   */
  @Override
  public void validate(FacesContext context, UIComponent component, Object value)
      throws ValidatorException {
    String entry = (String) value;

    String summary = Utils.getResourceBundleString("validation_error_header");

    ApplicationBean ab = Utils.findBean("applicationBean", context);
    JSONResource config = ab.getOsseConfig();

    String episodeFormatName =
        Utils.getStringOfJsonResource(config, Vocabulary.Config.Episode.pattern);
    String episodeRegExp;
    String episodeFormatLabel;

    if (episodeFormatName == null) {
      // use yyyy-mm-dd as standard format
      episodeRegExp = EpisodePatterns.patterns.get("ISODATE").getRegexp();
      episodeFormatLabel = EpisodePatterns.patterns.get("ISODATE").getPattern();
    } else {
      episodeRegExp = EpisodePatterns.patterns.get(episodeFormatName).getRegexp();
      episodeFormatLabel = EpisodePatterns.patterns.get(episodeFormatName).getPattern();
    }

    String[] replaceArray = {episodeFormatLabel};
    String message =
        Utils.getResourceBundleStringWithPlaceholders("validate_episode_error", replaceArray);

    Pattern pattern = Pattern.compile(episodeRegExp);
    Matcher matcher = pattern.matcher(entry);
    if (!matcher.find()) {
      FacesMessage msg = new FacesMessage(summary, message);
      msg.setSeverity(FacesMessage.SEVERITY_ERROR);
      throw new ValidatorException(msg);
    }
  }
}
