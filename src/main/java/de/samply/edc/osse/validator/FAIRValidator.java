/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.validator;

import de.samply.store.JSONResource;

/** Created by Jannik on 18.05.2017. */
public class FairValidator {

  /**
   * TODO: add javadoc.
   */
  public boolean checkFdpMetaData(JSONResource osseConfig) {

    boolean validate = true;

    if (osseConfig.getProperty("fair.fdpmetadata.version").getValue().equals("")) {
      validate = false;
    }

    if (osseConfig.getProperty("fair.fdpmetadata.url").getValue().equals("")) {
      validate = false;
    }

    return validate;
  }

  /**
   * TODO: add javadoc.
   */
  public boolean checkCatalogMetaData(JSONResource osseConfig) {
    // Version
    boolean validate = true;

    if (osseConfig.getProperty("fair.catalog.version").getValue().equals("")) {
      validate = false;
    }

    return validate;
  }

  /**
   * TODO: add javadoc.
   */
  public boolean checkDataSetMetaData(JSONResource osseConfig) {

    // version
    // theme

    boolean validate = true;

    if (osseConfig.getProperty("fair.dataset.version").getValue().equals("")) {
      validate = false;
    }

    if (osseConfig.getProperty("fair.dataset.theme").getValue().equals("")) {
      validate = false;
    }

    return validate;
  }

  /**
   * TODO: add javadoc.
   */
  public boolean checkDistributionMetaData(JSONResource osseConfig) {
    boolean validate = true;

    if (osseConfig.getProperty("fair.distribution.version").getValue().equals("")) {
      validate = false;
    }

    return validate;
  }
}
