/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation,
// v2.2.8-b130911.1802 generiert
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren.
// Generiert: 2015.11.18 um 11:16:49 AM CET
//

package de.samply.edc.osse.upgrade.dto;

import javax.xml.bind.annotation.XmlRegistry;

/**
 * This object contains factory methods for each Java content interface and Java element interface
 * generated in the de.samply.edc.osse.upgrade.dto package.
 *
 * <p>An ObjectFactory allows you to programatically construct new instances of the Java
 * representation for XML content. The Java representation of XML content can consist of schema
 * derived interfaces and classes representing the binding of schema type definitions, element
 * declarations and model groups. Factory methods for each of these are provided in this class.
 */
@XmlRegistry
public class ObjectFactory {

  /**
   * Create a new ObjectFactory that can be used to create new instances of schema derived classes
   * for package: de.samply.edc.osse.upgrade.dto
   */
  public ObjectFactory() {}

  /**
   * Create an instance of {@link Upgrades }.
   *
   * @return the upgrades
   */
  public Upgrades createUpgrades() {
    return new Upgrades();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade }
   *
   * @return the upgrade
   */
  public Upgrades.Upgrade createUpgradesUpgrade() {
    return new Upgrades.Upgrade();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Proxy }
   *
   * @return the proxy
   */
  public Upgrades.Upgrade.Proxy createUpgradesUpgradeProxy() {
    return new Upgrades.Upgrade.Proxy();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Version }
   *
   * @return the version
   */
  public Upgrades.Upgrade.Version createUpgradesUpgradeVersion() {
    return new Upgrades.Upgrade.Version();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Formeditor }
   *
   * @return the formeditor
   */
  public Upgrades.Upgrade.Formeditor createUpgradesUpgradeFormeditor() {
    return new Upgrades.Upgrade.Formeditor();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Auth }
   *
   * @return the auth
   */
  public Upgrades.Upgrade.Auth createUpgradesUpgradeAuth() {
    return new Upgrades.Upgrade.Auth();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Mainzelliste }
   *
   * @return the mainzelliste
   */
  public Upgrades.Upgrade.Mainzelliste createUpgradesUpgradeMainzelliste() {
    return new Upgrades.Upgrade.Mainzelliste();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Mdr }
   *
   * @return the mdr
   */
  public Upgrades.Upgrade.Mdr createUpgradesUpgradeMdr() {
    return new Upgrades.Upgrade.Mdr();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Teiler }
   *
   * @return the teiler
   */
  public Upgrades.Upgrade.Teiler createUpgradesUpgradeTeiler() {
    return new Upgrades.Upgrade.Teiler();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Proxy.Http }
   *
   * @return the http
   */
  public Upgrades.Upgrade.Proxy.Http createUpgradesUpgradeProxyHttp() {
    return new Upgrades.Upgrade.Proxy.Http();
  }

  /**
   * Create an instance of {@link Upgrades.Upgrade.Proxy.Https }
   *
   * @return the https
   */
  public Upgrades.Upgrade.Proxy.Https createUpgradesUpgradeProxyHttps() {
    return new Upgrades.Upgrade.Proxy.Https();
  }
}
