/*
 * Copyright (C) 2022 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.model.UpgradeDbModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Performs upgrade steps for a new version. 3.1.0 -> 3.2.0
 *
 * <p>See CHANGELOG.md for details.
 */
public class UpgradeExecution20 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution20(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("3.1.0");
    toVersion = new VersionNumber("3.2.0");
  }

  /**
   * Instantiates a new upgrade execution.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution20(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("3.1.0");
    toVersion = new VersionNumber("3.2.0");
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }

  /**
   * Do post upgrade.
   *
   * @see UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    try {
      if (checkForDuplicateCaseForms()) {
        removeEmptyDuplicateCaseForms();
        moveNonEmptyDuplicateCaseFormsToHistory();
        copyNonEmptyDuplicateCaseFormsInHistory();
        removeNonEmptyDuplicatedCaseForms();
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Error fixing duplicated case forms.", e);
    }
    try {
      if (checkForDuplicateEpisodeForms()) {
        removeEmptyDuplicateEpisodeForms();
        moveNonEmptyDuplicateEpisodeFormsToHistory();
        copyNonEmptyDuplicateEpisodeFormsInHistory();
        removeNonEmptyDuplicatedEpisodeForms();
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Error fixing duplicated episode forms.", e);
    }

    setCaseFormConstraint();
    setEpisodeFormConstraint();

  }

  /**
   * Runs SQL query to check for duplicate case forms. Logs info about duplicates when found.
   *
   * @return TRUE if duplicate case forms found. FALSE otherwise.
   */
  private boolean checkForDuplicateCaseForms() {
    String sql = "select case_id, split_part(name, '_', 2) as form_id, count(*) "
        + "from \"caseForms\" "
        + "group by case_id, form_id "
        + "having count(*) > 1 "
        + "order by case_id, form_id";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      ResultSet result = statement.executeQuery();
      dbModel.getConnection().commit();
      if (result == null) {
        return false;
      } else {
        Utils.getLogger().warn("Duplicate case forms present.");
        int duplicateCount = 0;
        int singleCount = 0;
        while (result.next() == true) {
          Utils.getLogger().warn(
              result.getInt(3) + " total entries found for case id " + result.getInt(1)
                  + " form id " + result.getInt(2) + ".");
          duplicateCount = duplicateCount + (result.getInt(3) - 1);
          singleCount++;
        }
        Utils.getLogger().warn("Found a total of " + duplicateCount
            + " duplicate case forms (of " + singleCount + " individual case forms).");
        return true;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Error checking for duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Runs SQL query to check for duplicate episode forms. Logs info about duplicates when found.
   *
   * @return TRUE if duplicate episode forms found. FALSE otherwise.
   */
  private boolean checkForDuplicateEpisodeForms() {
    String sql = "select episode_id, split_part(name, '_', 2) as form_id, count(*) "
        + "from \"episodeForms\" "
        + "group by episode_id, form_id "
        + "having count(*) > 1 "
        + "order by episode_id, form_id;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      ResultSet result = statement.executeQuery();
      dbModel.getConnection().commit();
      if (result == null) {
        return false;
      } else {
        Utils.getLogger().warn("Duplicate episode forms present.");
        int duplicateCount = 0;
        int singleCount = 0;
        while (result.next() == true) {
          Utils.getLogger().warn(
              result.getInt(3) + " total entries found for episode id " + result.getInt(1)
                  + " form id " + result.getInt(2) + ".");
          duplicateCount = duplicateCount + (result.getInt(3) - 1);
          singleCount++;
        }
        Utils.getLogger().warn("Found a total of " + duplicateCount
            + " duplicate episode forms (of " + singleCount + " individual episode forms).");
        return true;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Error checking for duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Deletes all empty duplicates of a case form, keeping the latest one.
   *
   * @return TRUE if the query worked without error. FALSE otherwise.
   */
  private boolean removeEmptyDuplicateCaseForms() {
    Utils.getLogger().warn("Removing all duplicate case forms that are empty, "
        + "keeping the latest one of each case/form combination...");
    String sql = "with empty_dubs as (select *, "
        + "                           row_number() "
        + "                           over (partition by case_id, substring(\"caseForms\".name, 0, "
        + "strpos(\"caseForms\".name, '-')) order by transaction_id desc ) as duplicate_id "
        + "                    from \"caseForms\" "
        + "                    where array_upper(ARRAY(Select json_object_keys(data)), 1) = 1 "
        + "                      and (data -> 'formType') IS NOT NULL) "
        + "DELETE "
        + "FROM \"caseForms\" c "
        + "    using empty_dubs "
        + "WHERE empty_dubs.id = c.id "
        + "  and empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger().warn("Removed " + affectedRows + " empty duplicate case forms.");
        return true;
      } else {
        Utils.getLogger().error("Failed to remove empty duplicate case forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to remove empty duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Deletes all empty duplicates of an episode form, keeping the latest one.
   *
   * @return TRUE if the query worked without error. FALSE otherwise.
   */
  private boolean removeEmptyDuplicateEpisodeForms() {
    Utils.getLogger().warn("Removing all duplicate episode forms that are empty, "
        + "keeping the latest one of each episode/form combination...");
    String sql = "with empty_dubs as (select *,\n"
        + "           row_number()\n"
        + "           over (partition by episode_id, substring(name, 0, strpos(name, '-')) "
        + "           order by transaction_id desc ) as duplicate_id\n"
        + "           from \"episodeForms\"\n"
        + "           where array_upper(ARRAY(Select json_object_keys(data)), 1) = 1\n"
        + "               and (data -> 'formType') IS NOT NULL)\n"
        + "DELETE\n"
        + "FROM \"episodeForms\" e\n"
        + "    using empty_dubs\n"
        + "WHERE empty_dubs.id = e.id\n"
        + "  and empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger().warn("Removed " + affectedRows + " empty duplicate episode forms.");
        return true;
      } else {
        Utils.getLogger().error("Failed to remove empty duplicate episode forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to remove empty duplicate episode forms.", e);
      return false;
    }
  }

  /**
   * Move all non-empty duplicate case forms to history table except the one with the latest
   * transaction ID, keeping their original transaction.
   *
   * @return TRUE if moving was successful, FALSE otherwise.
   */
  private boolean moveNonEmptyDuplicateCaseFormsToHistory() {
    Utils.getLogger().warn(
        "Inserting all non-empty duplicate case forms unchanged into case form history table, "
            + "except for the latest one of each case/form combination...");
    String sql = "with non_empty_dubs as (select *,\n"
        + "                               row_number()\n"
        + "                               over (partition by case_id, substring(\"caseForms\".name,"
        + " 0, strpos(\"caseForms\".name, '-')) order by transaction_id desc ) as duplicate_id\n"
        + "                        from \"caseForms\"\n"
        + "                        where array_upper(ARRAY(Select json_object_keys(data)), 1) > 1)"
        + "insert\n"
        + "into \"caseForms_history\" (oid, data, name, version, status_id, case_id,"
        + " transaction_id)"
        + "\n"
        + "select non_empty_dubs.id as oid, data, name, version, status_id, case_id, "
        + "transaction_id\n"
        + "from non_empty_dubs\n"
        + "WHERE non_empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger()
            .warn(
                "Inserted " + affectedRows + " non-empty duplicate case forms into history table.");
        return true;
      } else {
        Utils.getLogger().error("Failed to insert non-empty duplicate case forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to insert non-empty duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Move all non-empty duplicate episode forms to history table except the one with the latest
   * transaction ID, keeping their original transaction.
   *
   * @return TRUE if moving was successful, FALSE otherwise.
   */
  private boolean moveNonEmptyDuplicateEpisodeFormsToHistory() {
    Utils.getLogger().warn("Inserting all non-empty duplicate episode forms unchanged into episode "
        + "form history table, except for the latest one of each episode/form combination...");
    String sql = "with non_empty_dubs as (select *,\n"
        + "           row_number()\n"
        + "           over (partition by episode_id, substring(name, 0, strpos(name, '-')) "
        + "           order by transaction_id desc ) as duplicate_id\n"
        + "           from \"episodeForms\"\n"
        + "           where array_upper(ARRAY(Select json_object_keys(data)), 1) > 1)\n"
        + "insert\n"
        + "into \"episodeForms_history\" "
        + "    (oid, data, name, version, status_id, episode_id, transaction_id)\n"
        + "select non_empty_dubs.id as oid, data, name, version, status_id, episode_id, "
        + "    transaction_id\n"
        + "from non_empty_dubs\n"
        + "WHERE non_empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger().warn(
            "Inserted " + affectedRows + " non-empty duplicate episode forms into history table.");
        return true;
      } else {
        Utils.getLogger().error("Failed to insert non-empty duplicate episode forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to insert non-empty duplicate episode forms.", e);
      return false;
    }
  }

  /**
   * Copy all non-empty duplicate case forms to history table except the one with the latest
   * transaction ID, using a new transaction.
   *
   * @return TRUE if transfer was successful, FALSE otherwise.
   */
  private boolean copyNonEmptyDuplicateCaseFormsInHistory() {
    Utils.getLogger().warn(
        "Copying all non-empty duplicate case forms with new transaction into case form history "
            + "table, except for the latest one of each case/form combination...");
    String sql = "with non_empty_dubs as (select *,\n"
        + "                               row_number()\n"
        + "                               over (partition by case_id, substring(\"caseForms\".name,"
        + " 0, strpos(\"caseForms\".name, '-')) order by transaction_id desc ) as duplicate_id\n"
        + "                        from \"caseForms\"\n"
        + "                        where array_upper(ARRAY(Select json_object_keys(data)), 1) > 1),"
        + "\n"
        + "     current_transaction as\n"
        + "         (insert into \"transactions\" (timestamp, user_id) VALUES"
        + " (current_timestamp, 1)"
        + " returning id)\n"
        + "insert\n"
        + "into \"caseForms_history\" (oid, data, name, version, status_id, case_id,"
        + " transaction_id)"
        + "\n"
        + "select non_empty_dubs.id as oid, data, name, version, status_id, case_id, "
        + "current_transaction.id as transaction_id\n"
        + "from non_empty_dubs,\n"
        + "     current_transaction\n"
        + "WHERE non_empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger()
            .warn("Copied " + affectedRows + " non-empty duplicate case forms in history table.");
        return true;
      } else {
        Utils.getLogger().error("Failed to copy non-empty duplicate case forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to copy non-empty duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Copy all non-empty duplicate episode forms to history table except the one with the latest
   * transaction ID, using a new transaction.
   *
   * @return TRUE if transfer was successful, FALSE otherwise.
   */
  private boolean copyNonEmptyDuplicateEpisodeFormsInHistory() {
    Utils.getLogger().warn(
        "Inserting all non-empty duplicate episode forms with new transaction into episode form "
            + "history table, except for the latest one of each episode/form combination...");
    String sql = "with non_empty_dubs as (select *,\n"
        + "           row_number()\n"
        + "           over (partition by episode_id, substring(name, 0, strpos(name, '-')) "
        + "           order by transaction_id desc ) as duplicate_id\n"
        + "           from \"episodeForms\"\n"
        + "           where array_upper(ARRAY(Select json_object_keys(data)), 1) > 1),\n"
        + "     current_transaction as\n"
        + "         (insert into \"transactions\" (timestamp, user_id) "
        + "             VALUES (current_timestamp, 1) returning id)\n"
        + "insert\n"
        + "into \"episodeForms_history\" "
        + "    (oid, data, name, version, status_id, episode_id, transaction_id)\n"
        + "select non_empty_dubs.id as oid, data, name, version, status_id, episode_id, "
        + "    current_transaction.id as transaction_id\n"
        + "from non_empty_dubs,\n"
        + "     current_transaction\n"
        + "WHERE non_empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger().warn(
            "Copied " + affectedRows + " non-empty duplicate episode forms in history table.");
        return true;
      } else {
        Utils.getLogger().error("Failed to copy non-empty duplicate episode forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to copy non-empty duplicate episode forms.", e);
      return false;
    }
  }

  /**
   * Remove all non-empty duplicate case forms except the one with the latest transaction ID. If
   * there are any empty duplicate case forms left, they will also be deleted.
   */
  private boolean removeNonEmptyDuplicatedCaseForms() throws DatabaseException, SQLException {
    Utils.getLogger().warn("Removing all non-empty duplicate case forms, keeping the latest one "
        + "of each case/form combination only if it isn't empty...");
    String sql = "with non_empty_dubs as (select *,\n"
        + "                               row_number()\n"
        + "                               over (partition by case_id, substring(\"caseForms\".name,"
        + " 0, strpos(\"caseForms\".name, '-')) order by array_upper(ARRAY(Select"
        + " json_object_keys(data)), 1) > 1 desc, transaction_id desc) as duplicate_id\n"
        + "                        from \"caseForms\")\n"
        + "DELETE\n"
        + "FROM \"caseForms\" c\n"
        + "    using non_empty_dubs\n"
        + "where non_empty_dubs.id = c.id\n"
        + "  and non_empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger().warn("Removed " + affectedRows + " non-empty duplicate case forms "
            + "(including empty ones if they were the last one remaining of that case).");
        return true;
      } else {
        Utils.getLogger().error("Failed to remove non-empty duplicate case forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to remove non-empty duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Remove all non-empty duplicate episode forms except the one with the latest transaction ID. If
   * there are any empty duplicate episode forms left, they will also be deleted.
   */
  private boolean removeNonEmptyDuplicatedEpisodeForms() throws DatabaseException, SQLException {
    Utils.getLogger().warn("Removing all non-empty duplicate episode forms, keeping the latest one "
        + "of each episode/form combination only if it isn't empty...");
    String sql = "with non_empty_dubs as (select *,\n"
        + "           row_number()\n"
        + "           over (partition by episode_id, substring(name, 0, strpos(name, '-')) "
        + "           order by array_upper(ARRAY(Select json_object_keys(data)), 1) > 1 desc, "
        + "               transaction_id desc) as duplicate_id\n"
        + "           from \"episodeForms\")\n"
        + "DELETE\n"
        + "FROM \"episodeForms\" e\n"
        + "    using non_empty_dubs\n"
        + "where non_empty_dubs.id = e.id\n"
        + "  and non_empty_dubs.duplicate_id > 1;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows > 0) {
        Utils.getLogger().warn("Removed " + affectedRows + " non-empty duplicate episode forms "
            + "(including empty ones if they were the last one remaining of that episode).");
        return true;
      } else {
        Utils.getLogger().error("Failed to remove non-empty duplicate episode forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to remove non-empty duplicate episode forms.", e);
      return false;
    }
  }

  /**
   * Sets a unique index on the caseForms table.
   *
   * @return TRUE if unique index was set. FALSE otherwise.
   */
  private boolean setCaseFormConstraint() {
    String sql = "create unique index if not exists caseForms_duplicate_exclusion "
        + "on \"caseForms\" (split_part(name, '_', 2), case_id);";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows == 0) {
        Utils.getLogger().warn("Set constraint to prevent duplicate case forms.");
        return true;
      } else {
        Utils.getLogger().error("Failed to set constraint to prevent duplicate case forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to set constraint to prevent duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Sets a unique index on the episodeForms table.
   *
   * @return TRUE if unique index was set. FALSE otherwise.
   */
  private boolean setEpisodeFormConstraint() {
    String sql = "create unique index if not exists episodeForms_duplicate_exclusion "
        + "on \"episodeForms\" (split_part(name, '_', 2), episode_id);";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows == 0) {
        Utils.getLogger().warn("Set constraint to prevent duplicate episode forms.");
        return true;
      } else {
        Utils.getLogger().error("Failed to set constraint to prevent duplicate episode forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to set constraint to prevent duplicate episode forms.", e);
      return false;
    }
  }
}
