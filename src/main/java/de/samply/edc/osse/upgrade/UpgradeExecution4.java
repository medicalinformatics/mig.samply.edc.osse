/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.model.UpgradeDbModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/** This just fixes status from signed to validated. No version update */
public class UpgradeExecution4 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution4(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.0");
    toVersion = new VersionNumber("1.1.1");
  }

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution4(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.0");
    toVersion = new VersionNumber("1.1.1");
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {}

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    try {
      changeStatusFromSignedToValidated();
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().warn("Unable to change status from changed to validated.", e);
      return false;
    }
    return true;
  }

  /**
   * Change status from signed to validated.
   *
   * @return the boolean
   * @throws DatabaseException the database exception
   * @throws SQLException the SQL exception
   */
  private Boolean changeStatusFromSignedToValidated() throws DatabaseException, SQLException {
    UpgradeDbModel tmodel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend());

    String sql = "UPDATE \"statuses\" set data='{\"name\":\"validated\"}' WHERE id=3;";
    PreparedStatement stmt = tmodel.getConnection().prepareStatement(sql);
    stmt.execute();
    stmt.close();
    sql = "UPDATE \"statusChanges\" set data='{\"name\":\"Validate form\"}' WHERE id=2;";
    stmt = tmodel.getConnection().prepareStatement(sql);
    stmt.execute();
    stmt.close();

    tmodel.getConnection().commit();
    tmodel.logout();
    tmodel.close();

    return true;
  }
}
