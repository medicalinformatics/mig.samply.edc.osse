/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.Permission;
import de.samply.edc.osse.model.Role;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.osse.utils.FormRecreationHelper;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;

/** 1.1.2 -> 1.1.3 Adds the Bridgehead Data Reader Role Recreates all forms in the registry */
public class UpgradeExecution6 extends UpgradeExecution {

  /** The Constant bridgeheadDataViewerRoleName. */
  private static final String bridgeheadDataViewerRoleName = "Data Viewer";

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution6(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.2");
    toVersion = new VersionNumber("1.1.3");
  }

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution6(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.2");
    toVersion = new VersionNumber("1.1.3");
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    Database database = new Database(true);
    if (Utils.isBridgehead(currentOsseConfig)) {
      // check if bridgehead role is added already
      Role role = new Role(database);
      if (role.entityExistsByProperty(OSSEVocabulary.Role.Name, bridgeheadDataViewerRoleName)
          == null) {
        Utils.getLogger().debug("Creating Data Reader role");

        // create it!
        database.beginTransaction();

        role.setProperty(OSSEVocabulary.Role.Name, bridgeheadDataViewerRoleName);
        Location roleLocation = new Location(database);
        Resource roleLocationResource =
            roleLocation.entityExistsByProperty(
                OSSEVocabulary.Location.Name, Vocabulary.Generic.systemLocation);
        role.setProperty(OSSEVocabulary.Role.Location, roleLocationResource);

        role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.CUSTOM);
        role.setProperty(OSSEOntology.Role.IsUserRole, false);
        role.saveOrUpdate();

        Permission permission = new Permission(database);
        permission.setAccess("read");
        permission.setEntityPatient(true);
        permission.setEntityCase(true);
        permission.setEntityEpisode(true);
        permission.setEntityForm(true);
        permission.setShowIdat(true);
        permission.setParent(role);

        permission.saveOrUpdate();

        database.commit();

      } else {
        Utils.getLogger().debug("Data Reader role already existed.");
      }
    }

    // check if the registry was already registered in AUTH
    if (!applicationBean.doWeHaveAuthUserId()) {
      Utils.getLogger().error("AUTH User ID yet not created. This does not need to continue.");
      return;
    }

    // completely recreate the forms
    FormRecreationHelper rch = new FormRecreationHelper(applicationBean);
    rch.recreateForms();
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }
}
