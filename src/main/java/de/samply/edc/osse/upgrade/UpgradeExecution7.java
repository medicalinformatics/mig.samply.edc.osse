/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.Value;

/** 1.1.3 -> 1.1.4 Adds UKDATE as standard pattern if none was set yet */
public class UpgradeExecution7 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution7(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.3");
    toVersion = new VersionNumber("1.1.4");
  }

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution7(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.3");
    toVersion = new VersionNumber("1.1.4");
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    // add standard pattern UKDATE
    Value oldValue = currentOsseConfig.getProperty(Vocabulary.Config.Episode.pattern);
    if (oldValue == null) {
      Utils.getLogger().debug("Setting episode pattern to UKDATE");
      currentOsseConfig.setProperty(Vocabulary.Config.Episode.pattern, "UKDATE");
    } else {
      Utils.getLogger().debug("Keeping old episode pattern setting: " + oldValue.getValue());
    }
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }
}
