/*
 * Copyright (C) 2023 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.model.UpgradeDbModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 * Performs upgrade steps for a new version. 3.2.0 -> 3.3.0
 *
 * <p>See CHANGELOG.md for details.
 */
public class UpgradeExecution21 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution21(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("3.2.0");
    toVersion = new VersionNumber("3.3.0");
  }

  /**
   * Instantiates a new upgrade execution.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean   the application bean
   */
  public UpgradeExecution21(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("3.2.0");
    toVersion = new VersionNumber("3.3.0");
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }

  /**
   * Do post upgrade.
   *
   * @see UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    dropCurrentCaseFormsIndex();
    dropCurrentEpisodeFormsIndex();
    setCaseFormConstraint();
    setEpisodeFormConstraint();
    updateMainzellisteSettings();
    splitFormEditorUrl();
  }

  /**
   * Remove current case form duplication prevention index.
   *
   * @return TRUE if index was removed. FALSE otherwise.
   */
  private boolean dropCurrentCaseFormsIndex() {
    String sql = "drop index caseforms_duplicate_exclusion;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows == 0) {
        Utils.getLogger().warn("Removed current index prevent duplicate episode forms.");
        return true;
      } else {
        Utils.getLogger()
            .error("Failed to remove current index: episodeforms_duplicate_exclusion.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger()
          .error("Failed to remove current index: episodeforms_duplicate_exclusion.", e);
      return false;
    }
  }

  /**
   * Remove current episode form duplication prevention index.
   *
   * @return TRUE if index was removed. FALSE otherwise.
   */
  private boolean dropCurrentEpisodeFormsIndex() {
    String sql = "drop index episodeforms_duplicate_exclusion;";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows == 0) {
        Utils.getLogger().warn("Removed current index prevent duplicate episode forms.");
        return true;
      } else {
        Utils.getLogger()
            .error("Failed to remove current index: episodeforms_duplicate_exclusion.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger()
          .error("Failed to remove current index: episodeforms_duplicate_exclusion.", e);
      return false;
    }
  }


  /**
   * Sets a unique index on the caseForms table.
   *
   * @return TRUE if unique index was set. FALSE otherwise.
   */
  private boolean setCaseFormConstraint() {
    String sql = "create unique index if not exists caseForms_duplicate_exclusion "
        + "on \"caseForms\" (split_part(name, '_', 2), case_id, (data ->> 'formType'));";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows == 0) {
        Utils.getLogger().warn("Set constraint to prevent duplicate case forms.");
        return true;
      } else {
        Utils.getLogger().error("Failed to set constraint to prevent duplicate case forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to set constraint to prevent duplicate case forms.", e);
      return false;
    }
  }

  /**
   * Sets a unique index on the episodeForms table.
   *
   * @return TRUE if unique index was set. FALSE otherwise.
   */
  private boolean setEpisodeFormConstraint() {
    String sql = "create unique index if not exists episodeForms_duplicate_exclusion "
        + "on \"episodeForms\" (split_part(name, '_', 2), episode_id, (data ->> 'formType'));";
    try (UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend())) {
      PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
      int affectedRows = statement.executeUpdate();
      dbModel.getConnection().commit();
      if (affectedRows == 0) {
        Utils.getLogger().warn("Set constraint to prevent duplicate episode forms.");
        return true;
      } else {
        Utils.getLogger().error("Failed to set constraint to prevent duplicate episode forms.");
        return false;
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Failed to set constraint to prevent duplicate episode forms.", e);
      return false;
    }
  }

  /**
   * Updates current Mainzelliste settings to the new URL config.
   */
  private void updateMainzellisteSettings() {
    String mzlUrl = currentOsseConfig.getString(Vocabulary.Config.Mainzelliste.restUrlPublic);
    String internalMzlPort = currentOsseConfig.getString(
            Vocabulary.Config.Mainzelliste.RESTInternalMainzellistePort
    );
    String mzlEdcUrl = currentOsseConfig.getString(Vocabulary.Config.Mainzelliste.restUrlEdc);

    if (mzlEdcUrl == null || "".equals(mzlEdcUrl)) {
      if (mzlUrl != null && !"".equals(mzlUrl)) {
        // if the Mainzelliste URL is already set, keep it
        Utils.getLogger().info("Mainzelliste URL set to: '" + mzlUrl);
      } else {
        // if the Mainzelliste URL is empty, use the registry's URL with static /mainzelliste path
        mzlUrl = Utils.generateUrlToWebservice() + "/mainzelliste";
        Utils.getLogger().info("Mainzelliste URL empty, using path in registry's URL: " + mzlUrl);
        currentOsseConfig.setProperty(Vocabulary.Config.Mainzelliste.restUrlPublic, mzlUrl);
      }

      if (internalMzlPort != null && !"".equals(internalMzlPort)) {
        // if an internal port is set, use localhost + port number
        Utils.getLogger().info("Internal Mainzelliste port set to: '" + internalMzlPort);
        // protocol? path?
        mzlEdcUrl = "http://localhost:" + internalMzlPort + "/mainzelliste";
        currentOsseConfig.setProperty(Vocabulary.Config.Mainzelliste.restUrlEdc, mzlEdcUrl);
        currentOsseConfig.setProperty(
            Vocabulary.Config.Mainzelliste.RESTInternalMainzellistePort, "");
      } else {
        Utils.getLogger().info("Single Mainzelliste URL used, no changes made.");
        currentOsseConfig.setProperty(Vocabulary.Config.Mainzelliste.restUrlEdc, "");
      }
    }
  }

  /**
   * Splits the form editor URL into one URL for the frontend and one for the REST interface. This
   * method assumes that the default samply form editor is being used.
   */
  private void splitFormEditorUrl() {
    // only update if the formeditor base url is set and the formeditor rest url isn't
    // if no REST URL is set for the form editor, copy the base URL to the REST URL setting
    if (currentOsseConfig.getProperty(Vocabulary.Config.FormEditor.formEditorBASE) != null
        && !"".equals(
        currentOsseConfig.getProperty(Vocabulary.Config.FormEditor.formEditorBASE).getValue()) && (
        currentOsseConfig.getProperty(Vocabulary.Config.FormEditor.formEditorRestUrl) == null
            || "".equals(
            currentOsseConfig.getString(Vocabulary.Config.FormEditor.formEditorRestUrl)))) {
      String oldBaseUrl = currentOsseConfig.getProperty(Vocabulary.Config.FormEditor.formEditorBASE)
          .getValue();
      Utils.getLogger().info("Splitting old form editor URL '" + oldBaseUrl + "' into ...");
      if (currentOsseConfig.getProperty(Vocabulary.Config.FormEditor.formEditorRestUrl) == null
          || "".equals(currentOsseConfig.getProperty(Vocabulary.Config.FormEditor.formEditorRestUrl)
          .getValue())) {
        currentOsseConfig.setProperty(Vocabulary.Config.FormEditor.formEditorRestUrl,
            oldBaseUrl + "/rest");
        Utils.getLogger().info("... '" + oldBaseUrl + "/rest' as REST URL ...");
      } else {
        Utils.getLogger().warn("Form editor REST URL not empty, value kept.");
      }
      currentOsseConfig.setProperty(Vocabulary.Config.FormEditor.formEditorBASE,
          oldBaseUrl + "/samplyLogin.xhtml");
      Utils.getLogger().info("... '" + oldBaseUrl + "/samplyLogin.xhtml' as frontend URL.");
    } else {
      Utils.getLogger().warn("Can't update form editor URL, no changes made.");
    }
  }
}
