/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.model.UpgradeDbModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;

/** Base Class for UpgradeExecutions. */
public abstract class   UpgradeExecution {
  /**
   * Defines the version number after which the system role is available. Config saving then was
   * changed and only the new system shall be used from then forward.
   */
  protected final VersionNumber systemRoleAvailableSince = new VersionNumber("1.1.2");

  /** The upgrade data. */
  protected Upgrade upgradeData = null;

  /** The current osse config. */
  protected JSONResource currentOsseConfig;

  /** The factory settings. */
  protected JSONResource factorySettings = null;

  /** The current version of the registry . */
  protected VersionNumber currentVersion;

  /** The version this upgrade wants to upgrade to. */
  protected VersionNumber toVersion = null;

  /** The version this upgrade wants to upgrade from. */
  protected VersionNumber fromVersion = null;

  /** The appBean to access app-stored values. */
  protected ApplicationBean applicationBean;

  /**
   * Instantiates a new upgrade execution.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    this.applicationBean = applicationBean;
    this.currentOsseConfig = currentOsseConfig;
    this.currentVersion = new VersionNumber(getOsseVersion());
  }

  /**
   * Instantiates a new upgrade execution.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    this.applicationBean = applicationBean;
    this.currentOsseConfig = currentOsseConfig;
    this.currentVersion = new VersionNumber(getOsseVersion());
    this.upgradeData = upgradeData;
  }

  /**
   * Sets the version of the registry to the newly upgraded version number and saves the
   * configuration.
   *
   * @return true on success
   */
  private Boolean upgradeVersionNumber() {
    if (currentVersion.compareTo(systemRoleAvailableSince) < 0) {
      try {
        Utils.getLogger().debug("Upgrading DB version the old way to " + toVersion);
        currentOsseConfig.setProperty(Vocabulary.Config.version, toVersion.toString());

        UpgradeDbModel tmodel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend());
        tmodel.saveOsseConfig("osse", currentOsseConfig);
        tmodel.logout();
        tmodel.close();
      } catch (DatabaseException e) {
        Utils.getLogger().warn("Unable to upgrade version number of database", e);
        return false;
      }
    } else {
      Utils.getLogger().debug("Upgrading DB version the new way to " + toVersion);
      currentOsseConfig.setProperty(Vocabulary.Config.version, toVersion.toString());

      Utils.getLogger().debug("Saving OSSE Config");
      Database database = new Database(true);
      database.saveConfig("osse", currentOsseConfig);
    }

    return true;
  }

  /**
   * Gets the version number the registry currently has.
   *
   * @return the OSSE version
   */
  private String getOsseVersion() {
    if (currentOsseConfig.getProperty(Vocabulary.Config.version) == null) {
      return "1.0.0";
    }

    return currentOsseConfig.getProperty(Vocabulary.Config.version).getValue();
  }

  /**
   * Overload this for own upgrade functionality that is called after the DB-configuration was
   * updated with the XML file data. Also this upgrade will not cause a cancellation of the current
   * upgrade execution if it goes wrong
   */
  public abstract void doPostUpgrade();

  /**
   * Overload this for own upgrade functionality that is called before the DB-configuration is
   * updated with the XML file data. It must return true for the upgrade to continue successfully,
   * or the current upgrade execution will be terminated
   *
   * @return the boolean
   */
  public abstract Boolean doPreUpgrade();

  /**
   * Changes settings in the OSSE configuration and in the factory settings.
   *
   * @param key the key
   * @param value the value
   */
  private void changeSettings(String key, String value) {
    if (value == null
        || value.equalsIgnoreCase("nochange")
        || value.equalsIgnoreCase("PUT_INTERNAL_PORT_HTTP_MAINZELISTE_HERE")) {
      return;
    }
    String oldFactorySetting =
        factorySettings.getProperty(key) != null ? factorySettings.getProperty(key).getValue() : "";
    String currentConfigSetting =
        currentOsseConfig.getProperty(key) != null
            ? currentOsseConfig.getProperty(key).getValue()
            : "";
    factorySettings.setProperty(key, value);
    if (oldFactorySetting.equalsIgnoreCase(currentConfigSetting)) {
      currentOsseConfig.setProperty(key, value);
    }
  }

  /**
   * Method than runs through the theOneUpgrade.xml XML and inserts its values into the
   * DB-configuration.
   *
   * @return the boolean
   * @throws UpgradeExecutionException the upgrade execution exception
   */
  public Boolean doUpgrade() throws UpgradeExecutionException {
    if (currentVersion == null || fromVersion == null || toVersion == null) {
      Utils.getLogger()
          .debug(
              "Bailing out as any was null: cur:"
                  + currentVersion
                  + " from:"
                  + fromVersion
                  + " to:"
                  + toVersion);
      return false;
    }

    Boolean doReconfigure = false;

    if (fromVersion.toString().equalsIgnoreCase("0.0.0")
        && toVersion.toString().equalsIgnoreCase("0.0.0")) {
      // reconfigure was called
      Utils.getLogger().debug("A reconfigure was called.");
      doReconfigure = true;
    } else {
      // Check if version is newer
      if (currentVersion.compareTo(toVersion) >= 0) {
        Utils.getLogger()
            .debug(
                "OSSE version is current or newer, so we don't do this "
                    + this.getClass()
                    + " update");
        return false;
      }

      if (!doPreUpgrade()) {
        Utils.getLogger().debug("PreUpgrade returned false, bailing out");
        throw new UpgradeExecutionException("PreUpgrade did not finish");
      }

      if (upgradeData == null) {
        // no xml entry to deal with
        Utils.getLogger().debug("There was no upgradeData available. Nothing to do here.");

        doPostUpgrade();

        return upgradeVersionNumber();
      }
    }

    if (!doReconfigure) {
      if (!upgradeData.getVersion().getFrom().equalsIgnoreCase(fromVersion.toString())
          || !upgradeData.getVersion().getTo().equalsIgnoreCase(toVersion.toString())) {
        Utils.getLogger().error("The from-to values in the xml are not consistent with the "
            + "from-to data provided in this upgradejava.");
        return false;
      }

      if (currentVersion.compareTo(toVersion) >= 0) {
        Utils.getLogger().debug("OSSE version is current or newer, so we don't do any update");
        return false;
      } else {
        Utils.getLogger()
            .debug(
                "Update running! current="
                    + currentVersion
                    + " update="
                    + toVersion
                    + " not before "
                    + fromVersion);
      }

      if (fromVersion.compareTo(currentVersion) != 0) {
        Utils.getLogger()
            .debug(
                "I expect DB in version "
                    + fromVersion
                    + ", but it currently is in version "
                    + currentVersion
                    + ". Bailing out!");
        return false;
      }
    }

    factorySettings = null;

    UpgradeDbModel tmodel = null;
    Database database = null;
    if (currentVersion.compareTo(systemRoleAvailableSince) < 0) {
      // use old dbmodel, because there is no system role in the current
      // edc version yet
      try {
        tmodel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend());
        factorySettings = tmodel.getConfig("osse.factory.settings");
      } catch (DatabaseException e) {
        Utils.getLogger().debug("Update aborted!", e);
        return false;
      }
    } else {
      // use new database model, because we got a system role by now
      database = new Database(true);
      factorySettings = database.getConfig("osse.factory.settings");
    }

    // Here we need to run through the upgrades structure

    if (upgradeData.getMainzelliste() != null) {
      Utils.getLogger()
          .debug(
              "* MZ stuff set with REST = "
                  + upgradeData.getMainzelliste().getRest()
                  + " DOM = "
                  + upgradeData.getMainzelliste().getDomain()
                  + " noIDAT (cannot be changed, so will be ignored) = "
                  + upgradeData.getMainzelliste().getNoIdat()
                  + " PORT = "
                  + upgradeData.getMainzelliste().getRestInternalPort()
                  + " INTERNALPORT = "
                  + upgradeData.getMainzelliste().getInternalMainzellistePort());
      changeSettings(
          Vocabulary.Config.Mainzelliste.RESTInternalPort,
          upgradeData.getMainzelliste().getRestInternalPort());
      changeSettings(
          Vocabulary.Config.Mainzelliste.RESTInternalMainzellistePort,
          upgradeData.getMainzelliste().getInternalMainzellistePort());
      changeSettings(Vocabulary.Config.Mainzelliste.restUrlPublic,
              upgradeData.getMainzelliste().getRest());
      changeSettings(
          Vocabulary.Config.Mainzelliste.apikey, upgradeData.getMainzelliste().getDomain());
    }

    if (upgradeData.getFormeditor() != null) {
      Utils.getLogger()
          .debug(
              "* FormEditor stuff set with BASE = "
                  + upgradeData.getFormeditor().getBase()
                  + " CI = "
                  + upgradeData.getFormeditor().getClientId());
      changeSettings(
          Vocabulary.Config.FormEditor.formEditorBASE, upgradeData.getFormeditor().getBase());
      changeSettings(
          Vocabulary.Config.FormEditor.formEditorClientId,
          upgradeData.getFormeditor().getClientId());
    }

    if (upgradeData.getAuth() != null) {
      Utils.getLogger()
          .debug(
              "* Auth stuff set with BASE = "
                  + upgradeData.getAuth().getRest()
                  + " REALM = "
                  + upgradeData.getAuth().getRealm()
                  + " useSamplyAuth = "
                  + upgradeData.getAuth().isUseSamplyAuth()
                  + " pubkey = "
                  + upgradeData.getAuth().getPubkey()
                  + " clientId = "
                  + upgradeData.getAuth().getClientId()
                  + " clientSecret = "
                  + upgradeData.getAuth().getClientSecret()
                  + " registryPassword = "
                  + upgradeData.getAuth().getRegistryPassword());

      Utils.getLogger().error("AUTH data may not be changed!");
    }

    if (upgradeData.getMdr() != null) {
      Utils.getLogger().debug("* MDR stuff set with BASE = " + upgradeData.getMdr().getRest());
      changeSettings(Vocabulary.Config.Mdr.REST, upgradeData.getMdr().getRest());
    }

    if (upgradeData.getTeiler() != null) {
      Utils.getLogger()
          .debug(
              "* Teiler stuff set with BASE = "
                  + upgradeData.getTeiler().getApikey()
                  + " CI = "
                  + upgradeData.getTeiler().getApikeyExport()
                  + " REST = "
                  + upgradeData.getTeiler().getRest()
                  + " PORT = "
                  + upgradeData.getTeiler().getInternalTeilerPort());
      changeSettings(Vocabulary.Config.Teiler.apiKey, upgradeData.getTeiler().getApikey());
      changeSettings(
          Vocabulary.Config.Teiler.apiKeyExport, upgradeData.getTeiler().getApikeyExport());
      changeSettings(Vocabulary.Config.Teiler.REST, upgradeData.getTeiler().getRest());
      changeSettings(
          Vocabulary.Config.Teiler.internalTeilerPort,
          upgradeData.getTeiler().getInternalTeilerPort());
    }

    if (currentVersion.compareTo(systemRoleAvailableSince) < 0) {
      try {
        Utils.getLogger().debug("Saving them");
        tmodel.saveOsseConfig("osse.factory.settings", factorySettings);
        tmodel.saveOsseConfig("osse", currentOsseConfig);
        tmodel.logout();
        tmodel.close();
      } catch (DatabaseException e) {
        Utils.getLogger().warn("Unable to save factorySettings and or currentOsseConfig.", e);
        return false;
      }
    } else {
      Utils.getLogger().debug("Saving them");
      database.saveConfig("osse.factory.settings", factorySettings);
      database.saveConfig("osse", currentOsseConfig);
    }

    doPostUpgrade();

    if (!doReconfigure) {
      return upgradeVersionNumber();
    } else {
      return doReconfigure;
    }
  }
}
