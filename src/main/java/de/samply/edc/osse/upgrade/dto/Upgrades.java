/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

//
// Diese Datei wurde mit der JavaTM Architecture for XML Binding(JAXB) Reference Implementation,
// v2.2.8-b130911.1802 generiert
// Siehe <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a>
// Änderungen an dieser Datei gehen bei einer Neukompilierung des Quellschemas verloren.
// Generiert: 2015.11.18 um 11:16:49 AM CET
//

package de.samply.edc.osse.upgrade.dto;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/**
 * Java-Klasse für anonymous complex type.
 *
 * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
 * ist.
 *
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="updateUUID" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="upgrade" maxOccurs="unbounded" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="version">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="internalRevision" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="from" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="to" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="formeditor" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="base" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="auth" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="mainzelliste">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="noIdat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="restInternalPort" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="internalMainzellistePort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                             &lt;element name="domain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="mdr" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="teiler" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="internalTeilerPort" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="apikey" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="apikeyExport" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="proxy" minOccurs="0">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="realm" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="http">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                             &lt;element name="https">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                       &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "",
    propOrder = {"updateUUID", "upgrade"})
@XmlRootElement(name = "upgrades")
public class Upgrades {

  /** The update uuid. */
  @XmlElement(required = true)
  protected String updateUUID;

  /** The upgrade. */
  protected List<Upgrades.Upgrade> upgrade;

  /**
   * Ruft den Wert der updateUUID-Eigenschaft ab.
   *
   * @return possible object is {@link String }
   */
  public String getUpdateUuid() {
    return updateUUID;
  }

  /**
   * Legt den Wert der updateUUID-Eigenschaft fest.
   *
   * @param value allowed object is {@link String }
   */
  public void setUpdateUUID(String value) {
    this.updateUUID = value;
  }

  /**
   * Gets the value of the upgrade property.
   *
   * <p>This accessor method returns a reference to the live list, not a snapshot. Therefore any
   * modification you make to the returned list will be present inside the JAXB object. This is why
   * there is not a <CODE>set</CODE> method for the upgrade property.
   *
   * <p>For example, to add a new item, do as follows:
   *
   * <pre>
   *    getUpgrade().add(newItem);
   * </pre>
   *
   * <p>Objects of the following type(s) are allowed in the list {@link Upgrades.Upgrade }
   *
   * @return the upgrade
   */
  public List<Upgrades.Upgrade> getUpgrade() {
    if (upgrade == null) {
      upgrade = new ArrayList<>();
    }
    return this.upgrade;
  }

  /**
   * Java-Klasse für anonymous complex type.
   *
   * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
   * ist.
   *
   * <pre>
   * &lt;complexType>
   *   &lt;complexContent>
   *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *       &lt;sequence>
   *         &lt;element name="version">
   *           &lt;complexType>
   *             &lt;complexContent>
   *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                 &lt;sequence>
   *                   &lt;element name="internalRevision" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="from" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="to" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                 &lt;/sequence>
   *               &lt;/restriction>
   *             &lt;/complexContent>
   *           &lt;/complexType>
   *         &lt;/element>
   *         &lt;element name="formeditor" minOccurs="0">
   *           &lt;complexType>
   *             &lt;complexContent>
   *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                 &lt;sequence>
   *                   &lt;element name="base" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                 &lt;/sequence>
   *               &lt;/restriction>
   *             &lt;/complexContent>
   *           &lt;/complexType>
   *         &lt;/element>
   *         &lt;element name="auth" minOccurs="0">
   *           &lt;complexType>
   *             &lt;complexContent>
   *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                 &lt;sequence>
   *                   &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                 &lt;/sequence>
   *               &lt;/restriction>
   *             &lt;/complexContent>
   *           &lt;/complexType>
   *         &lt;/element>
   *         &lt;element name="mainzelliste">
   *           &lt;complexType>
   *             &lt;complexContent>
   *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                 &lt;sequence>
   *                   &lt;element name="noIdat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
   *                   &lt;element name="restInternalPort" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="internalMainzellistePort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
   *                   &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
   *                   &lt;element name="domain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
   *                 &lt;/sequence>
   *               &lt;/restriction>
   *             &lt;/complexContent>
   *           &lt;/complexType>
   *         &lt;/element>
   *         &lt;element name="mdr" minOccurs="0">
   *           &lt;complexType>
   *             &lt;complexContent>
   *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                 &lt;sequence>
   *                   &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                 &lt;/sequence>
   *               &lt;/restriction>
   *             &lt;/complexContent>
   *           &lt;/complexType>
   *         &lt;/element>
   *         &lt;element name="teiler" minOccurs="0">
   *           &lt;complexType>
   *             &lt;complexContent>
   *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                 &lt;sequence>
   *                   &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="internalTeilerPort" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="apikey" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="apikeyExport" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                 &lt;/sequence>
   *               &lt;/restriction>
   *             &lt;/complexContent>
   *           &lt;/complexType>
   *         &lt;/element>
   *         &lt;element name="proxy" minOccurs="0">
   *           &lt;complexType>
   *             &lt;complexContent>
   *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                 &lt;sequence>
   *                   &lt;element name="realm" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                   &lt;element name="http">
   *                     &lt;complexType>
   *                       &lt;complexContent>
   *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                           &lt;sequence>
   *                             &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                             &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                             &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                             &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                           &lt;/sequence>
   *                         &lt;/restriction>
   *                       &lt;/complexContent>
   *                     &lt;/complexType>
   *                   &lt;/element>
   *                   &lt;element name="https">
   *                     &lt;complexType>
   *                       &lt;complexContent>
   *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
   *                           &lt;sequence>
   *                             &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                             &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                             &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                             &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
   *                           &lt;/sequence>
   *                         &lt;/restriction>
   *                       &lt;/complexContent>
   *                     &lt;/complexType>
   *                   &lt;/element>
   *                 &lt;/sequence>
   *               &lt;/restriction>
   *             &lt;/complexContent>
   *           &lt;/complexType>
   *         &lt;/element>
   *       &lt;/sequence>
   *     &lt;/restriction>
   *   &lt;/complexContent>
   * &lt;/complexType>
   * </pre>
   */
  @XmlAccessorType(XmlAccessType.FIELD)
  @XmlType(
      name = "",
      propOrder = {"version", "formeditor", "auth", "mainzelliste", "mdr", "teiler", "proxy"})
  public static class Upgrade {

    /** The version. */
    @XmlElement(required = true)
    protected Upgrades.Upgrade.Version version;

    /** The formeditor. */
    protected Upgrades.Upgrade.Formeditor formeditor;

    /** The auth. */
    protected Upgrades.Upgrade.Auth auth;

    /** The mainzelliste. */
    @XmlElement(required = true)
    protected Upgrades.Upgrade.Mainzelliste mainzelliste;

    /** The mdr. */
    protected Upgrades.Upgrade.Mdr mdr;

    /** The teiler. */
    protected Upgrades.Upgrade.Teiler teiler;

    /** The proxy. */
    protected Upgrades.Upgrade.Proxy proxy;

    /**
     * Ruft den Wert der version-Eigenschaft ab.
     *
     * @return possible object is {@link Upgrades.Upgrade.Version }
     */
    public Upgrades.Upgrade.Version getVersion() {
      return version;
    }

    /**
     * Legt den Wert der version-Eigenschaft fest.
     *
     * @param value allowed object is {@link Upgrades.Upgrade.Version }
     */
    public void setVersion(Upgrades.Upgrade.Version value) {
      this.version = value;
    }

    /**
     * Ruft den Wert der formeditor-Eigenschaft ab.
     *
     * @return possible object is {@link Upgrades.Upgrade.Formeditor }
     */
    public Upgrades.Upgrade.Formeditor getFormeditor() {
      return formeditor;
    }

    /**
     * Legt den Wert der formeditor-Eigenschaft fest.
     *
     * @param value allowed object is {@link Upgrades.Upgrade.Formeditor }
     */
    public void setFormeditor(Upgrades.Upgrade.Formeditor value) {
      this.formeditor = value;
    }

    /**
     * Ruft den Wert der auth-Eigenschaft ab.
     *
     * @return possible object is {@link Upgrades.Upgrade.Auth }
     */
    public Upgrades.Upgrade.Auth getAuth() {
      return auth;
    }

    /**
     * Legt den Wert der auth-Eigenschaft fest.
     *
     * @param value allowed object is {@link Upgrades.Upgrade.Auth }
     */
    public void setAuth(Upgrades.Upgrade.Auth value) {
      this.auth = value;
    }

    /**
     * Ruft den Wert der mainzelliste-Eigenschaft ab.
     *
     * @return possible object is {@link Upgrades.Upgrade.Mainzelliste }
     */
    public Upgrades.Upgrade.Mainzelliste getMainzelliste() {
      return mainzelliste;
    }

    /**
     * Legt den Wert der mainzelliste-Eigenschaft fest.
     *
     * @param value allowed object is {@link Upgrades.Upgrade.Mainzelliste }
     */
    public void setMainzelliste(Upgrades.Upgrade.Mainzelliste value) {
      this.mainzelliste = value;
    }

    /**
     * Ruft den Wert der mdr-Eigenschaft ab.
     *
     * @return possible object is {@link Upgrades.Upgrade.Mdr }
     */
    public Upgrades.Upgrade.Mdr getMdr() {
      return mdr;
    }

    /**
     * Legt den Wert der mdr-Eigenschaft fest.
     *
     * @param value allowed object is {@link Upgrades.Upgrade.Mdr }
     */
    public void setMdr(Upgrades.Upgrade.Mdr value) {
      this.mdr = value;
    }

    /**
     * Ruft den Wert der teiler-Eigenschaft ab.
     *
     * @return possible object is {@link Upgrades.Upgrade.Teiler }
     */
    public Upgrades.Upgrade.Teiler getTeiler() {
      return teiler;
    }

    /**
     * Legt den Wert der teiler-Eigenschaft fest.
     *
     * @param value allowed object is {@link Upgrades.Upgrade.Teiler }
     */
    public void setTeiler(Upgrades.Upgrade.Teiler value) {
      this.teiler = value;
    }

    /**
     * Ruft den Wert der proxy-Eigenschaft ab.
     *
     * @return possible object is {@link Upgrades.Upgrade.Proxy }
     */
    public Upgrades.Upgrade.Proxy getProxy() {
      return proxy;
    }

    /**
     * Legt den Wert der proxy-Eigenschaft fest.
     *
     * @param value allowed object is {@link Upgrades.Upgrade.Proxy }
     */
    public void setProxy(Upgrades.Upgrade.Proxy value) {
      this.proxy = value;
    }

    /**
     * Java-Klasse für anonymous complex type.
     *
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
     * ist.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {
          "rest",
          "useSamplyAuth",
          "realm",
          "pubkey",
          "clientId",
          "clientSecret",
          "registryPassword"
        })
    public static class Auth {

      /** The rest. */
      @XmlElement(required = true)
      protected String rest;

      @XmlElement(required = true)
      protected boolean useSamplyAuth;

      protected String realm;

      @XmlElement(required = true)
      protected String pubkey;

      @XmlElement(required = true)
      protected String clientId;

      @XmlElement(required = true)
      protected String clientSecret;

      protected String registryPassword;

      /**
       * Ruft den Wert der rest-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getRest() {
        return rest;
      }

      /**
       * Legt den Wert der rest-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setRest(String value) {
        this.rest = value;
      }

      public boolean isUseSamplyAuth() {
        return useSamplyAuth;
      }

      public void setUseSamplyAuth(boolean useSamplyAuth) {
        this.useSamplyAuth = useSamplyAuth;
      }

      public String getRealm() {
        return realm;
      }

      public void setRealm(String realm) {
        this.realm = realm;
      }

      public String getPubkey() {
        return pubkey;
      }

      public void setPubkey(String pubkey) {
        this.pubkey = pubkey;
      }

      public String getClientId() {
        return clientId;
      }

      public void setClientId(String clientId) {
        this.clientId = clientId;
      }

      public String getClientSecret() {
        return clientSecret;
      }

      public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
      }

      public String getRegistryPassword() {
        return registryPassword;
      }

      public void setRegistryPassword(String registryPassword) {
        this.registryPassword = registryPassword;
      }
    }

    /**
     * Java-Klasse für anonymous complex type.
     *
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
     * ist.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="base" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="clientId" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {"base", "clientId"})
    public static class Formeditor {

      /** The base. */
      @XmlElement(required = true)
      protected String base;

      /** The client id. */
      @XmlElement(required = true)
      protected String clientId;

      /**
       * Ruft den Wert der base-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getBase() {
        return base;
      }

      /**
       * Legt den Wert der base-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setBase(String value) {
        this.base = value;
      }

      /**
       * Ruft den Wert der clientId-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getClientId() {
        return clientId;
      }

      /**
       * Legt den Wert der clientId-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setClientId(String value) {
        this.clientId = value;
      }
    }

    /**
     * Java-Klasse für anonymous complex type.
     *
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
     * ist.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="noIdat" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="restInternalPort" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="internalMainzellistePort" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *         &lt;element name="domain" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {"noIdat", "restInternalPort", "internalMainzellistePort", "rest", "domain"})
    public static class Mainzelliste {

      /** The no idat. */
      protected String noIdat;

      /** The rest internal port. */
      @XmlElement(required = true)
      protected String restInternalPort;

      /** The internal mainzelliste port. */
      protected String internalMainzellistePort;

      /** The rest. */
      protected String rest;

      /** The domain. */
      protected String domain;

      /**
       * Ruft den Wert der noIdat-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getNoIdat() {
        return noIdat;
      }

      /**
       * Legt den Wert der noIdat-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setNoIdat(String value) {
        this.noIdat = value;
      }

      /**
       * Ruft den Wert der restInternalPort-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getRestInternalPort() {
        return restInternalPort;
      }

      /**
       * Legt den Wert der restInternalPort-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setRestInternalPort(String value) {
        this.restInternalPort = value;
      }

      /**
       * Ruft den Wert der internalMainzellistePort-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getInternalMainzellistePort() {
        return internalMainzellistePort;
      }

      /**
       * Legt den Wert der internalMainzellistePort-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setInternalMainzellistePort(String value) {
        this.internalMainzellistePort = value;
      }

      /**
       * Ruft den Wert der rest-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getRest() {
        return rest;
      }

      /**
       * Legt den Wert der rest-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setRest(String value) {
        this.rest = value;
      }

      /**
       * Ruft den Wert der domain-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getDomain() {
        return domain;
      }

      /**
       * Legt den Wert der domain-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setDomain(String value) {
        this.domain = value;
      }
    }

    /**
     * Java-Klasse für anonymous complex type.
     *
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
     * ist.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {"rest"})
    public static class Mdr {

      /** The rest. */
      @XmlElement(required = true)
      protected String rest;

      /**
       * Ruft den Wert der rest-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getRest() {
        return rest;
      }

      /**
       * Legt den Wert der rest-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setRest(String value) {
        this.rest = value;
      }
    }

    /**
     * Java-Klasse für anonymous complex type.
     *
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
     * ist.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="realm" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="http">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="https">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {"realm", "http", "https"})
    public static class Proxy {

      /** The realm. */
      @XmlElement(required = true)
      protected String realm;

      /** The http. */
      @XmlElement(required = true)
      protected Upgrades.Upgrade.Proxy.Http http;

      /** The https. */
      @XmlElement(required = true)
      protected Upgrades.Upgrade.Proxy.Https https;

      /**
       * Ruft den Wert der realm-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getRealm() {
        return realm;
      }

      /**
       * Legt den Wert der realm-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setRealm(String value) {
        this.realm = value;
      }

      /**
       * Ruft den Wert der http-Eigenschaft ab.
       *
       * @return possible object is {@link Upgrades.Upgrade.Proxy.Http }
       */
      public Upgrades.Upgrade.Proxy.Http getHttp() {
        return http;
      }

      /**
       * Legt den Wert der http-Eigenschaft fest.
       *
       * @param value allowed object is {@link Upgrades.Upgrade.Proxy.Http }
       */
      public void setHttp(Upgrades.Upgrade.Proxy.Http value) {
        this.http = value;
      }

      /**
       * Ruft den Wert der https-Eigenschaft ab.
       *
       * @return possible object is {@link Upgrades.Upgrade.Proxy.Https }
       */
      public Upgrades.Upgrade.Proxy.Https getHttps() {
        return https;
      }

      /**
       * Legt den Wert der https-Eigenschaft fest.
       *
       * @param value allowed object is {@link Upgrades.Upgrade.Proxy.Https }
       */
      public void setHttps(Upgrades.Upgrade.Proxy.Https value) {
        this.https = value;
      }

      /**
       * Java-Klasse für anonymous complex type.
       *
       * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse
       * enthalten ist.
       *
       * <pre>
       * &lt;complexType>
       *   &lt;complexContent>
       *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
       *       &lt;sequence>
       *         &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *         &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *       &lt;/sequence>
       *     &lt;/restriction>
       *   &lt;/complexContent>
       * &lt;/complexType>
       * </pre>
       */
      @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(
          name = "",
          propOrder = {"host", "port", "username", "password"})
      public static class Http {

        /** The host. */
        @XmlElement(required = true)
        protected String host;

        /** The port. */
        @XmlElement(required = true)
        protected String port;

        /** The username. */
        @XmlElement(required = true)
        protected String username;

        /** The password. */
        @XmlElement(required = true)
        protected String password;

        /**
         * Ruft den Wert der host-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getHost() {
          return host;
        }

        /**
         * Legt den Wert der host-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setHost(String value) {
          this.host = value;
        }

        /**
         * Ruft den Wert der port-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getPort() {
          return port;
        }

        /**
         * Legt den Wert der port-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setPort(String value) {
          this.port = value;
        }

        /**
         * Ruft den Wert der username-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getUsername() {
          return username;
        }

        /**
         * Legt den Wert der username-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setUsername(String value) {
          this.username = value;
        }

        /**
         * Ruft den Wert der password-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getPassword() {
          return password;
        }

        /**
         * Legt den Wert der password-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setPassword(String value) {
          this.password = value;
        }
      }

      /**
       * Java-Klasse für anonymous complex type.
       *
       * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse
       * enthalten ist.
       *
       * <pre>
       * &lt;complexType>
       *   &lt;complexContent>
       *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
       *       &lt;sequence>
       *         &lt;element name="host" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *         &lt;element name="port" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *         &lt;element name="username" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string"/>
       *       &lt;/sequence>
       *     &lt;/restriction>
       *   &lt;/complexContent>
       * &lt;/complexType>
       * </pre>
       */
      @XmlAccessorType(XmlAccessType.FIELD)
      @XmlType(
          name = "",
          propOrder = {"host", "port", "username", "password"})
      public static class Https {

        /** The host. */
        @XmlElement(required = true)
        protected String host;

        /** The port. */
        @XmlElement(required = true)
        protected String port;

        /** The username. */
        @XmlElement(required = true)
        protected String username;

        /** The password. */
        @XmlElement(required = true)
        protected String password;

        /**
         * Ruft den Wert der host-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getHost() {
          return host;
        }

        /**
         * Legt den Wert der host-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setHost(String value) {
          this.host = value;
        }

        /**
         * Ruft den Wert der port-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getPort() {
          return port;
        }

        /**
         * Legt den Wert der port-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setPort(String value) {
          this.port = value;
        }

        /**
         * Ruft den Wert der username-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getUsername() {
          return username;
        }

        /**
         * Legt den Wert der username-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setUsername(String value) {
          this.username = value;
        }

        /**
         * Ruft den Wert der password-Eigenschaft ab.
         *
         * @return possible object is {@link String }
         */
        public String getPassword() {
          return password;
        }

        /**
         * Legt den Wert der password-Eigenschaft fest.
         *
         * @param value allowed object is {@link String }
         */
        public void setPassword(String value) {
          this.password = value;
        }
      }
    }

    /**
     * Java-Klasse für anonymous complex type.
     *
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
     * ist.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="rest" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="internalTeilerPort" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="apikey" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="apikeyExport" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {"rest", "internalTeilerPort", "apikey", "apikeyExport"})
    public static class Teiler {

      /** The rest. */
      @XmlElement(required = true)
      protected String rest;

      /** The internal teiler port. */
      @XmlElement(required = true)
      protected String internalTeilerPort;

      /** The apikey. */
      @XmlElement(required = true)
      protected String apikey;

      /** The apikey export. */
      @XmlElement(required = true)
      protected String apikeyExport;

      /**
       * Ruft den Wert der rest-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getRest() {
        return rest;
      }

      /**
       * Legt den Wert der rest-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setRest(String value) {
        this.rest = value;
      }

      /**
       * Ruft den Wert der internalTeilerPort-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getInternalTeilerPort() {
        return internalTeilerPort;
      }

      /**
       * Legt den Wert der internalTeilerPort-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setInternalTeilerPort(String value) {
        this.internalTeilerPort = value;
      }

      /**
       * Ruft den Wert der apikey-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getApikey() {
        return apikey;
      }

      /**
       * Legt den Wert der apikey-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setApikey(String value) {
        this.apikey = value;
      }

      /**
       * Ruft den Wert der apikeyExport-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getApikeyExport() {
        return apikeyExport;
      }

      /**
       * Legt den Wert der apikeyExport-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setApikeyExport(String value) {
        this.apikeyExport = value;
      }
    }

    /**
     * Java-Klasse für anonymous complex type.
     *
     * <p>Das folgende Schemafragment gibt den erwarteten Content an, der in dieser Klasse enthalten
     * ist.
     *
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="internalRevision" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="from" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="to" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {"internalRevision", "from", "to"})
    public static class Version {

      /** The internal revision. */
      @XmlElement(required = true)
      protected String internalRevision;

      /** The from. */
      @XmlElement(required = true)
      protected String from;

      /** The to. */
      @XmlElement(required = true)
      protected String to;

      /**
       * Ruft den Wert der internalRevision-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getInternalRevision() {
        return internalRevision;
      }

      /**
       * Legt den Wert der internalRevision-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setInternalRevision(String value) {
        this.internalRevision = value;
      }

      /**
       * Ruft den Wert der from-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getFrom() {
        return from;
      }

      /**
       * Legt den Wert der from-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setFrom(String value) {
        this.from = value;
      }

      /**
       * Ruft den Wert der to-Eigenschaft ab.
       *
       * @return possible object is {@link String }
       */
      public String getTo() {
        return to;
      }

      /**
       * Legt den Wert der to-Eigenschaft fest.
       *
       * @param value allowed object is {@link String }
       */
      public void setTo(String value) {
        this.to = value;
      }
    }
  }
}
