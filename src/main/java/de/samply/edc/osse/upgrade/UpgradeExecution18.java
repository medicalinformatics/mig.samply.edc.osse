/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.catalog.Vocabulary.Config;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;

/**
 * Performs upgrade steps for a new version.
 * 2.0.2 -> 3.0.0
 *
 * <p>See CHANGELOG.md for details.
 */
public class UpgradeExecution18 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution18(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("2.0.2");
    toVersion = new VersionNumber("3.0.0");
  }

  /**
   * Instantiates a new upgrade execution.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution18(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("2.0.2");
    toVersion = new VersionNumber("3.0.0");
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }

  /**
   * Do post upgrade.
   *
   * @see UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    // set api default off
    if (currentOsseConfig.getProperty("importer.apiActive") == null) {
      currentOsseConfig.setProperty("importer.apiActive", false);
    }
    // set dashboard default off
    if (currentOsseConfig.getProperty(Config.useDashboard) == null) {
      currentOsseConfig.setProperty(Config.useDashboard, false);
    }
  }
}
