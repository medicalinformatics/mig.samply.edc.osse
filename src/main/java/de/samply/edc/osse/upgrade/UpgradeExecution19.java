/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.model.UpgradeDbModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Performs upgrade steps for a new version.
 * 3.0.0 -> 3.1.0
 *
 * <p>See CHANGELOG.md for details.
 */
public class UpgradeExecution19 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution19(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("3.0.0");
    toVersion = new VersionNumber("3.1.0");
  }

  /**
   * Instantiates a new upgrade execution.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution19(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("3.0.0");
    toVersion = new VersionNumber("3.1.0");
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    // fix inconsistent episode date patterns
    try {
      int wrongTimestampCount = checkForWrongTimestamps();
      if (wrongTimestampCount > 0) {
        fixWrongTimeStampsInEpisodes();
      }
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().warn("Error fixing timestamps of episodes.", e);
    }

  }

  /**
   * Counts the number of wrong timestamp values in the episodes table.
   *
   * @return The number of episode entries with wrong timestamp.
   */
  private int checkForWrongTimestamps() throws DatabaseException, SQLException {
    UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend());
    String countSql = "select count(*) from episodes where (data->> 'timestamp') not like '%-%';";
    PreparedStatement statement = dbModel.getConnection().prepareStatement(countSql);
    ResultSet resultSet = statement.executeQuery();
    resultSet.next();
    int wrongTimestampCount = resultSet.getInt(1);
    Utils.getLogger().info(wrongTimestampCount + " episode timestamps need to be fixed...");
    statement.close();
    dbModel.getConnection().commit();
    return wrongTimestampCount;
  }

  /**
   * Fixes wrong timestamp values of entries in the episodes table.
   */
  private void fixWrongTimeStampsInEpisodes() throws DatabaseException, SQLException {
    UpgradeDbModel dbModel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend());
    String sql = "update episodes set data = to_json(jsonb_set(to_jsonb(data), '{timestamp}', "
        + "to_jsonb(to_char(to_timestamp(cast(left(data->> 'timestamp',-3) as double precision)),"
        + "'YYYY-MM-DD HH24:MI:SS.002')) )) where (data->> 'timestamp') not like '%-%';";

    PreparedStatement statement = dbModel.getConnection().prepareStatement(sql);
    int modifiedCount = statement.executeUpdate();
    Utils.getLogger().info(modifiedCount + " episode timestamps fixed.");
    statement.close();
    dbModel.getConnection().commit();
    dbModel.logout();
    dbModel.close();
  }
}
