/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.model.Episode;
import de.samply.edc.osse.model.UpgradeDbModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.osse.utils.FormRecreationHelper;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.TimestampLiteral;
import de.samply.store.Value;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * 1.2.0 -> 1.3.0 adds mainzelliste.noIdat setting in osseconfig and setting it to false, if it's
 * not there or not a boolean Also recreates all forms due to changes in form-data-storage Creates
 * several postgres functions for samply.store Changes the MDR Rest URL to v3 version Changes the
 * timestamps in all episodes to the timestamp version of the name
 */
public class UpgradeExecution9 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution9(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.2.0");
    toVersion = new VersionNumber("1.3.0");
  }

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution9(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.2.0");
    toVersion = new VersionNumber("1.3.0");
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    // add mainzelliste.noIdat into config if it's missing
    Value noIdatSetting = currentOsseConfig.getProperty(Vocabulary.Config.Mainzelliste.noIdat);

    if (noIdatSetting == null) {
      Utils.getLogger()
          .debug("Setting mainzelliste.noIdat was not found. Adding it, and setting to false.");
      currentOsseConfig.setProperty(Vocabulary.Config.Mainzelliste.noIdat, false);
    } else {
      if (noIdatSetting.asBoolean() == null) {
        Utils.getLogger()
            .debug(
                "Setting mainzelliste.noIdat was found, but was not a boolean. Setting to false.");
        currentOsseConfig.setProperty(Vocabulary.Config.Mainzelliste.noIdat, false);

      } else {
        Utils.getLogger()
            .debug(
                "Setting mainzelliste.noIdat was found and is set to " + noIdatSetting.asBoolean());
      }
    }

    // add patientformlist to config
    currentOsseConfig.setProperty(Vocabulary.Config.PatientForm.list, "");

    // completely recreate the forms
    FormRecreationHelper rch = new FormRecreationHelper(applicationBean);
    rch.recreateForms();
  }

  /**
   * Creates the getJsonArrayFunction for samply.store
   *
   * @throws DatabaseException the database exception
   * @throws SQLException the SQL exception
   */
  private void createGetJsonArrayFunction() throws DatabaseException, SQLException {
    UpgradeDbModel tmodel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend());

    String sql =
        "CREATE OR REPLACE FUNCTION getjsonarray(value json)\n"
            + "  RETURNS json AS\n"
            + "$BODY$\n"
            + "BEGIN\n"
            + "\n"
            + "IF substring(trim(leading from (value::text)) from 1 for 1) = '[' THEN\n"
            + "    RETURN value;\n"
            + "ELSE\n"
            + "    RETURN array_to_json(array_agg(value));\n"
            + "END IF;\n"
            + "\n"
            + "END;\n"
            + "$BODY$\n"
            + "  LANGUAGE plpgsql;\n"
            + "\n"
            + "\n"
            + "CREATE OR REPLACE FUNCTION asbigint(value text)\n"
            + "   RETURNS bigint AS\n"
            + "$BODY$\n"
            + "BEGIN\n"
            + "     RETURN value::BIGINT;\n"
            + "EXCEPTION WHEN others then\n"
            + "     RETURN NULL;\n"
            + "END;\n"
            + "\n"
            + "$BODY$\n"
            + "   LANGUAGE plpgsql;\n"
            + "\n"
            + "\n"
            + "CREATE OR REPLACE FUNCTION asboolean(value text)\n"
            + "   RETURNS boolean AS\n"
            + "$BODY$\n"
            + "BEGIN\n"
            + "     RETURN value::BOOLEAN;\n"
            + "EXCEPTION WHEN others then\n"
            + "     RETURN NULL;\n"
            + "END;\n"
            + "\n"
            + "$BODY$\n"
            + "   LANGUAGE plpgsql;\n"
            + "\n"
            + "\n"
            + "CREATE OR REPLACE FUNCTION asdecimal(value text)\n"
            + "   RETURNS numeric AS\n"
            + "$BODY$\n"
            + "BEGIN\n"
            + "     RETURN value::DECIMAL;\n"
            + "EXCEPTION WHEN others then\n"
            + "     RETURN NULL;\n"
            + "END;\n"
            + "\n"
            + "$BODY$\n"
            + "   LANGUAGE plpgsql;\n"
            + "\n"
            + "\n"
            + "CREATE OR REPLACE FUNCTION astimestamp(value text)\n"
            + "   RETURNS timestamp without time zone AS\n"
            + "$BODY$\n"
            + "BEGIN\n"
            + "     RETURN value::timestamp;\n"
            + "EXCEPTION WHEN others then\n"
            + "     RETURN NULL;\n"
            + "END;\n"
            + "\n"
            + "$BODY$\n"
            + "   LANGUAGE plpgsql;\n"
            + "\n"
            + "\n"
            + "CREATE OR REPLACE FUNCTION astime(value text)\n"
            + "  RETURNS time AS\n"
            + "$BODY$\n"
            + "BEGIN\n"
            + "    RETURN value::time;\n"
            + "EXCEPTION WHEN others then\n"
            + "    RETURN NULL;\n"
            + "END;\n"
            + "\n"
            + "$BODY$\n"
            + "  LANGUAGE plpgsql; ";

    Statement stmt = tmodel.getConnection().createStatement();
    stmt.execute(sql);
    stmt.close();

    tmodel.getConnection().commit();
    tmodel.logout();
    tmodel.close();
  }

  /** Changes the MDR Rest URL to v3 version. */
  public void updateMdrRestUrl() {
    String[] moo =
        new String[] {
          "https://mdr.demo.osse-register.de/v2/api/mdr",
          "https://mdr.osse-register.de/v2/api/mdr",
          "http://dev05.imbei.uni-mainz.de:8080/mdr-rest/api/mdr"
        };

    String savedMdrUrl = currentOsseConfig.getString(Vocabulary.Config.Mdr.REST);
    if (savedMdrUrl == null || "".equals(savedMdrUrl)) {
      return;
    }

    if (Arrays.asList(moo).contains(savedMdrUrl)) {
      savedMdrUrl = savedMdrUrl.replace("mdr-rest", "v3");
      savedMdrUrl = savedMdrUrl.replace("v2", "v3");

      currentOsseConfig.setProperty(Vocabulary.Config.Mdr.REST, savedMdrUrl);
    }
  }

  /**
   * Changes the timestamps in all episodes to the timestamp version of the name.
   *
   * @return true, if successful
   */
  public boolean changeEpisodeTimestamps() {
    // change all episodes set timestamp
    Database db = new Database(true);
    List<Resource> allEpisodes = db.getResources(OSSEVocabulary.Type.Episode);
    for (Resource episodeRes : allEpisodes) {
      String episodeName = episodeRes.getString(OSSEVocabulary.Episode.Name);
      if (episodeName == null) {
        continue;
      }

      if (episodeName.contains("_deleted_")) {
        episodeRes.removeProperties("timestamp");
      } else {
        Date date = Episode.getDateByPattern(episodeName, null);
        if (date == null) {
          Utils.getLogger().debug("Date is null for name = " + episodeName);
          return false;
        }
        TimestampLiteral timestampLiteral = new TimestampLiteral(date.getTime());
        episodeRes.setProperty("timestamp", timestampLiteral);
        Utils.getLogger()
            .debug("Saving timestamp for episode " + episodeName + " to " + timestampLiteral);
      }

      db.save(episodeRes);
    }

    return true;
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    try {
      Utils.getLogger().debug("Adding or Replacing Postgres Functions: getJsonArrayFunction, "
          + "asbigint, asboolean, asdecimal, astimestamp");
      createGetJsonArrayFunction();

      if (!changeEpisodeTimestamps()) {
        return false;
      }

      updateMdrRestUrl();
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().warn("Unable to do pre upgrade.", e);
      return false;
    }
    return true;
  }
}
