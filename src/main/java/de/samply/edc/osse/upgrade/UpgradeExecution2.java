/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.model.UpgradeDbModel;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

/** This Upgrade changes the American episode dates into UK episode dates. */
public class UpgradeExecution2 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution2.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution2(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.0.2");
    toVersion = new VersionNumber("1.0.3");
  }

  /**
   * Instantiates a new upgrade execution2.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution2(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.0.2");
    toVersion = new VersionNumber("1.0.3");
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    try {
      changeEpisodeDateToUK();
    } catch (DatabaseException | SQLException e) {
      Utils.getLogger().error("Error while changing episode date.",e);
    }
  }

  /**
   * Change episode date to uk.
   *
   * @return the boolean
   * @throws DatabaseException the database exception
   * @throws SQLException the SQL exception
   */
  private Boolean changeEpisodeDateToUK() throws DatabaseException, SQLException {
    UpgradeDbModel tmodel = new UpgradeDbModel(ApplicationBean.getConfigFilenameBackend());

    String sql = "SELECT id, name FROM episodes";
    PreparedStatement stmt = tmodel.getConnection().prepareStatement(sql);
    ResultSet set = stmt.executeQuery();

    String number = "\\d+\\/\\d+\\/\\d+";

    HashMap<Integer, String> newDateMap = new HashMap<>();

    while (set.next()) {
      Integer id = set.getInt("id");
      String name = set.getString("name");

      // Parse for american format
      if (name.matches(number)) {
        String[] split = name.split("\\/");
        String month = split[0];
        String day = split[1];
        String year = split[2];
        Utils.getLogger().debug("D = " + day + " M = " + month + " Y = " + year + " for ID " + id);
        name = day + "/" + month + "/" + year;
        newDateMap.put(id, name);
      } else {
        Utils.getLogger().debug("Pattern not recognized in name " + name + " for ID " + id);
      }
    }
    set.close();
    stmt.close();

    sql = "UPDATE episodes SET name=? WHERE id=?";
    stmt = tmodel.getConnection().prepareStatement(sql);
    for (Integer id : newDateMap.keySet()) {
      Utils.getLogger().debug("Setting id " + id + " to " + newDateMap.get(id));
      stmt.setInt(2, id);
      stmt.setString(1, newDateMap.get(id));
      stmt.execute();
    }
    stmt.close();
    tmodel.getConnection().commit();

    tmodel.logout();
    tmodel.close();

    return true;
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }
}
