/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.Utils;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;

/**
 * 1.1.4 -> 1.2.0 adds RESTLocalhostToMainzelliste configuration value for local HTTP connection to
 * Mainzelliste and fixes TeilerURL and port settings
 */
public class UpgradeExecution8 extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution8(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.4");
    toVersion = new VersionNumber("1.2.0");
  }

  /**
   * Instantiates a new upgrade execution3.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecution8(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    fromVersion = new VersionNumber("1.1.4");
    toVersion = new VersionNumber("1.2.0");
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {
    // put mainzelliste URL in and remove PORT as that was done wrong in
    // older releases
    String mzUrl = Utils.getStringOfJsonResource(currentOsseConfig,
            Vocabulary.Config.Mainzelliste.restUrlPublic);
    String port = Utils.getStringOfJsonResource(currentOsseConfig,
            Vocabulary.Config.Mainzelliste.RESTInternalPort);
    String mzInternalPort = Utils.getStringOfJsonResource(currentOsseConfig,
            Vocabulary.Config.Mainzelliste.RESTInternalMainzellistePort);
    if (mzUrl == null || "".equals(mzUrl)) {
      if (port != null && !"".equals(port)) {
        if (mzInternalPort != null && !"".equals(mzInternalPort)) {
          // MZ Internal port is not empty, so was already set to
          // something, don't overwrite that!
          Utils.getLogger().debug("Not overwriting Mainzelliste RESTInternalPort");
        } else {
          Utils.getLogger().debug("Copying Mainzelliste REST value to RESTInternalPort");
          // if MZ URL is empty but port is not, older installations
          // incorrectly used these info to calculate URLs
          currentOsseConfig.setProperty(
              Vocabulary.Config.Mainzelliste.RESTInternalMainzellistePort, port);
        }
      }
    }

    // Fix Teiler URL data

    String teilerUrl =
        Utils.getStringOfJsonResource(currentOsseConfig, Vocabulary.Config.Teiler.REST);

    if (teilerUrl == null || "".equals(teilerUrl)) {
      // check if port is set, if no teilerURL is set
      String teilerPort =
          Utils.getStringOfJsonResource(
              currentOsseConfig, Vocabulary.Config.Teiler.internalTeilerPort);
      if (teilerPort == null || "".equals(teilerPort)) {
        // set port to the same as above
        currentOsseConfig.setProperty(Vocabulary.Config.Teiler.internalTeilerPort, port);
        Utils.getLogger().debug("TeilerURL is empty, so setting internal Teiler PORT to " + port);
      }
    } else {
      // check if teilerURL is set to localhost, then remove that and make
      // sure port is set
      if (teilerUrl.contains("localhost")) {
        Utils.getLogger().debug("TeilerURL contains localhost, so deleting it");
        currentOsseConfig.setProperty(Vocabulary.Config.Teiler.internalTeilerPort, port);
        String teilerPort =
            Utils.getStringOfJsonResource(
                currentOsseConfig, Vocabulary.Config.Teiler.internalTeilerPort);
        if (teilerPort == null) {
          Utils.getLogger().debug("...and setting internal Teiler PORT to " + port);
          currentOsseConfig.setProperty(Vocabulary.Config.Teiler.internalTeilerPort, port);
        }
      }
    }
  }

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }
}
