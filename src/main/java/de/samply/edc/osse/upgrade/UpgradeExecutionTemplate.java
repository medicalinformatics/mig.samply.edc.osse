/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.upgrade;

import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.upgrade.dto.Upgrades.Upgrade;
import de.samply.edc.utils.VersionNumber;
import de.samply.store.JSONResource;

/**
 * Template for UpgradeExecutionX classes. Use this to make a new file and replace the X with a
 * number, defining in which order the upgrades are executed in. This will be done in the
 * application bean through a for loop to a number defined in the field
 * "applicationBean.requiredVersion"
 *
 * <p>So if you add a new upgrade here, remember to increase the number in the applicationBean.
 *
 * <p>If necessary for an upgrade, you also can make an own (fitting) section in the
 * theOneUpgrade.xml file.
 *
 * <p>No matter if or not you have a new section in theOneUpgrade.xml, you must change the
 * UpgradeUUID in that file, or else no upgrades will be done. Normally that should by the Debian
 * package management (or the installer package used).
 *
 * <p>Upgrades work incremental, so the software can upgrade itself no matter at which version it is
 * reinstalled smoothly.
 */
public class UpgradeExecutionTemplate extends UpgradeExecution {

  /**
   * Instantiates a new upgrade execution.
   *
   * @param upgradeData the upgrade data
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecutionTemplate(
      Upgrade upgradeData, JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(upgradeData, currentOsseConfig, applicationBean);

    // define from which version to which version this upgrade is valid for
    fromVersion = new VersionNumber("1.0.0");
    toVersion = new VersionNumber("1.0.0");
  }

  /**
   * Instantiates a new upgrade execution.
   *
   * @param currentOsseConfig the current osse config
   * @param applicationBean the application bean
   */
  public UpgradeExecutionTemplate(JSONResource currentOsseConfig, ApplicationBean applicationBean) {
    super(currentOsseConfig, applicationBean);

    // define from which version to which version this upgrade is valid for
    fromVersion = new VersionNumber("1.0.0");
    toVersion = new VersionNumber("1.0.0");
  }

  /**
   * Do post upgrade.
   *
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPostUpgrade()
   */
  @Override
  public void doPostUpgrade() {}

  /**
   * Do pre upgrade.
   *
   * @return the boolean
   * @see de.samply.edc.osse.upgrade.UpgradeExecution#doPreUpgrade()
   */
  @Override
  public Boolean doPreUpgrade() {
    return true;
  }
}
