/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.searchviewer;

import java.util.HashMap;

/** Singleton for signintokens created by the search viewer interface to be used in edc. */
public enum SigninTokenExchangeStorage {

  /** The instance. */
  instance;

  /** The token storage. */
  private HashMap<String, Integer> tokenStorage;

  /**
   * Adds data to the map.
   *
   * @param signinToken the signin token
   * @param queryResultId the query result id
   */
  public synchronized void addSigninToken(String signinToken, Integer queryResultId) {
    if (tokenStorage == null) {
      tokenStorage = new HashMap<>();
    }

    tokenStorage.put(signinToken, queryResultId);
  }

  /**
   * Gets the queryResultId linked to a signinToken.
   *
   * @param signinToken the signin token
   * @return queryResultId
   */
  public synchronized Integer getQueryResultId(String signinToken) {
    if (tokenStorage == null) {
      return null;
    }

    return tokenStorage.get(signinToken);
  }

  /**
   * Removes a signinToken from the map.
   *
   * @param signinToken the signin token
   */
  public synchronized void removeSigninToken(String signinToken) {
    if (tokenStorage == null) {
      return;
    }

    tokenStorage.remove(signinToken);
  }
}
