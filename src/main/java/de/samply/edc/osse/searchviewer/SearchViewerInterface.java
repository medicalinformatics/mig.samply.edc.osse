/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.searchviewer;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.Database;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.util.HashMap;
import java.util.Map;
import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import org.codehaus.jettison.json.JSONObject;

/** The SearchViewerInterface. */
@Path("")
public class SearchViewerInterface {

  /**
   * Creates a token to access the viewer of a query result.
   *
   * @param apiKey must be identical with the Teiler-Export-APIkey
   * @param queryResultIdString the query result id string
   * @return json/xml with ["token": token]
   */
  @GET
  @Path("/getToken")
  @Produces(MediaType.APPLICATION_JSON)
  public Response createSearchViewToken(
      @HeaderParam("apiKey") String apiKey,
      @QueryParam("queryResultId") String queryResultIdString) {

    if (queryResultIdString == null || "".equals(queryResultIdString)) {
      Map<String, String> entity = new HashMap<>();
      entity.put("error", "Provide a queryResultId");
      return Response.status(Status.BAD_REQUEST).entity(new JSONObject(entity)).build();
    }

    Integer queryResultId;

    try {
      queryResultId = Integer.parseInt(queryResultIdString);
    } catch (NumberFormatException e) {
      Map<String, String> entity = new HashMap<>();
      entity.put("error", "An Integer is expected as queryResultId");
      return Response.status(Status.BAD_REQUEST).entity(new JSONObject(entity)).build();
    }

    Database database = new Database(true);
    JSONResource config = database.getConfig("osse");
    String exporterApiKey =
        Utils.getStringOfJsonResource(config, Vocabulary.Config.Teiler.apiKeyExport);

    if (!exporterApiKey.equalsIgnoreCase(apiKey)) {
      return Response.status(Status.UNAUTHORIZED).build();
    }

    String signinToken = null;
    while (signinToken == null) {
      signinToken = Utils.generateRandomString(32, false);
      if (SigninTokenExchangeStorage.instance.getQueryResultId(signinToken) != null) {
        signinToken = null;
      }
    }
    SigninTokenExchangeStorage.instance.addSigninToken(signinToken, queryResultId);

    Map<String, String> out = new HashMap<>();
    out.put("signinToken", signinToken);
    JSONObject answer = new JSONObject(out);

    return Response.status(Status.OK).entity(answer).build();
  }
}
