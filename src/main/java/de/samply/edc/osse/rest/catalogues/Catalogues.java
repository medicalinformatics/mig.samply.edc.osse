/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.rest.catalogues;

import de.samply.web.mdrfaces.CatalogueBean;
import java.util.prefs.Preferences;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/** Offer a REST endpoint to get catalogues from. */
@Path("")
public class Catalogues {

  public static void storePreference(String key, String value) {
    Preferences prefs = Preferences.userNodeForPackage(Catalogues.class);
    prefs.put(key, value);
  }

  public static String readPreference(String key) {
    Preferences prefs = Preferences.userNodeForPackage(Catalogues.class);
    return prefs.get(key, "n/a");
  }

  /**
   * TODO: add javadoc.
   */
  @GET
  @Path("/{urn}/languages/{lang}")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getCatalogData(@PathParam("urn") String urn, @PathParam("lang") String language) {
    if (!CatalogueBean.getCatalogues().containsKey(urn)) {
      CatalogueBean.preloadCatalogue(urn, language, null, null);
    }
    return Response.ok(CatalogueBean.getCatalogues().get(urn)).build();
  }
}
