/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.rest;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.Database;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;

public abstract class AbstractRestInterface {

  /**
   * Checks if access should be granted. To succeed the api has to be active and the received api
   * key must be correct.
   *
   * @param headers received headers
   * @return
   *     <ul>
   *       <li>Status.FORBIDDEN if the api is not active
   *       <li>Status.UNAUTHORIZED if the apikey was incorrect
   *       <li>Status.OK if access should be granted
   *     </ul>
   */
  protected Status accessAllowed(HttpHeaders headers) {
    if (!allowRest()) {
      return Status.FORBIDDEN;
    }
    // check if key is correct
    Database database = new Database(true);
    JSONResource config = database.getConfig("osse");
    String apikey = Utils.getStringOfJsonResource(config, Vocabulary.Config.Importer.apikey);
    if (apikey.equals("")) {
      return Status.OK; // empty key means no key required
    } else {
      String givenApiKey =
          headers.getRequestHeader("apikey") != null
              ? headers.getRequestHeader("apikey").get(0)
              : null;
      if (apikey.equals(givenApiKey)) {
        return Status.OK; // key is correct
      } else {
        return Status.UNAUTHORIZED; // key is not correct
      }
    }
  }

  /**
   * Checks if rest api is active.
   * @return importer.apiActive flag
   */
  protected Boolean allowRest() {
    Database database = new Database(true);
    JSONResource config = database.getConfig("osse");
    boolean apiState = Boolean
        .parseBoolean(Utils.getStringOfJsonResource(config, "importer.apiActive"));
    return apiState;
  }

}
