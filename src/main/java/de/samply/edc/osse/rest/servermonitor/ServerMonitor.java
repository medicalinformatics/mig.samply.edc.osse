/*
 * Copyright (C) 2020 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.rest.servermonitor;

import de.samply.edc.osse.model.User;
import de.samply.edc.osse.rest.AbstractRestInterface;
import java.util.HashMap;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

/** Offer a REST endpoint to get infos about the current state. */
@Path("")
public class ServerMonitor extends AbstractRestInterface {

  /** Gives a plain text response if users are currently logged in. */
  @GET
  @Path("/activeUsers")
  @Produces(MediaType.TEXT_PLAIN)
  public Response usersLoggedIn(
      @Context HttpHeaders headers, @Context HttpServletRequest httpServletRequest) {
    Response.Status accessStatus;
    String lastAddr = httpServletRequest.getRemoteAddr(); // client ip or last proxy
    String localAddr = httpServletRequest.getLocalAddr();
    if (lastAddr.equals(localAddr)) { // from localhost?
      accessStatus = Status.OK;
    } else {
      accessStatus = accessAllowed(headers);
      if (!accessStatus.equals(Status.OK)) {
        return Response.status(accessStatus).build();
      }
    }

    Map<User, HttpSession> foo = new HashMap<>(User.getLogins());
    if (foo.isEmpty()) {
      return Response.status(accessStatus).entity("false").build();
    }
    return Response.status(accessStatus).entity("true").build();
  }
}
