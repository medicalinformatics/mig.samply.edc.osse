/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.rest.internal;

import com.google.gson.Gson;
import de.pseudonymisierung.mainzelliste.client.ID;
import de.pseudonymisierung.mainzelliste.client.InvalidSessionException;
import de.pseudonymisierung.mainzelliste.client.MainzellisteNetworkException;
import de.pseudonymisierung.mainzelliste.client.Session;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.control.SessionBean;
import de.samply.edc.osse.model.patientlist.rest.Patientlist;
import de.samply.edc.osse.model.patientlist.rest.PatientlistLine;
import de.samply.edc.osse.utils.OsseUtils;
import de.samply.edc.utils.Utils;
import de.samply.store.DatabaseModel;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import javax.faces.FacesException;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.RequestScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/** A pseudo REST resource that can access managed beans. */
@ManagedBean
@RequestScoped
public class PatientlistDatasource {

  public static final String QUERY_PARAM_START = "start";
  public static final String QUERY_PARAM_LENGTH = "length";
  public static final String QUERY_PARAM_DRAW = "draw";
  private static final Logger logger = LogManager.getLogger(PatientlistDatasource.class);
  private static final String CONTENT_TYPE_JSON_UTF8 = "application/json; charset=utf-8";
  private static final Gson gson = new Gson();

  @ManagedProperty(value = "#{sessionBean}")
  private SessionBean sessionBean;

  public SessionBean getSessionBean() {
    return sessionBean;
  }

  public void setSessionBean(SessionBean sessionBean) {
    this.sessionBean = sessionBean;
  }

  /** The datasource to be queried via AJAX to fill the unsortable patientlist. */
  public void getPatientlist() {
    FacesContext context = FacesContext.getCurrentInstance();
    ExternalContext externalContext = context.getExternalContext();
    HttpServletRequest request = (HttpServletRequest) externalContext.getRequest();
    HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();

    Map<String, String> queryParameterMap = OsseUtils.getQueryParameters(request.getQueryString());

    response.setContentType(CONTENT_TYPE_JSON_UTF8);
    try {
      PrintWriter printWriter = response.getWriter();
      String rs = createResponseString(queryParameterMap);
      logger.info(rs);
      printWriter.print(rs);
    } catch (IOException ex) {
      throw new FacesException(ex);
    }
    context.responseComplete();
  }

  /**
   * Create the JSON response to be sent back to the calling script.
   *
   * @param queryParameterMap the map of parameters extracted from the servlet request
   * @return json formatted patientlist page
   */
  private String createResponseString(Map<String, String> queryParameterMap) {
    int draw;
    int start;
    int length;

    try {
      draw = Integer.valueOf(queryParameterMap.get(QUERY_PARAM_DRAW));
    } catch (NumberFormatException e) {
      draw = 0;
    }

    try {
      start = Integer.valueOf(queryParameterMap.get(QUERY_PARAM_START));
    } catch (NumberFormatException e) {
      start = 0;
    }

    try {
      length = Integer.valueOf(queryParameterMap.get(QUERY_PARAM_LENGTH));
    } catch (NumberFormatException e) {
      length = 10;
    }

    Patientlist patientlist = new Patientlist();
    patientlist.setDraw(draw);
    patientlist.setShowIdat(
        !Utils.getBooleanOfJsonResource(
            sessionBean.getConfig(), Vocabulary.Config.Mainzelliste.noIdat));
    patientlist.setBridgehead(ApplicationBean.isBridgehead());

    List<Resource> resultPatients = getPatientResources(start, length, patientlist);

    patientlist.setData(
        createPatientlist(resultPatients, sessionBean.getDatabase().getDatabaseModel()));
    return gson.toJson(patientlist);
  }

  /**
   * Get a list of patient resources from the database.
   *
   * @param start pagination related parameter, startindex
   * @param length pagination related parameter, pagelength
   * @param patientlist the Patientlist object that will be used to build the reply to the ajax call
   *     from datatables
   * @return a list of up to length patients
   */
  private List<Resource> getPatientResources(int start, int length, Patientlist patientlist) {
    Integer resourceCountTotal;

    // First get the total amount of patients and add the information to the patientlist
    ResourceQuery queryPatientsTotal = new ResourceQuery(OSSEVocabulary.Type.Patient);
    queryPatientsTotal.add(
        Criteria.Or(
            Criteria.IsNull(OSSEVocabulary.Type.Case, "isDeleted"),
            Criteria.Equal(OSSEVocabulary.Type.Case, "isDeleted", 0)));
    resourceCountTotal = sessionBean.getDatabase().countResources(queryPatientsTotal);

    patientlist.setRecordsTotal(resourceCountTotal);
    patientlist.setRecordsFiltered(resourceCountTotal);

    // Then get the amount of patients of the own location
    ResourceQuery queryPatientsOwnLocation = new ResourceQuery(OSSEVocabulary.Type.Patient);
    queryPatientsOwnLocation.add(
        Criteria.And(
            Criteria.Equal(
                OSSEVocabulary.Type.Case,
                OSSEVocabulary.Case.Location,
                sessionBean.getCurrentRole().getLocation().getId()),
            Criteria.Or(
                Criteria.IsNull(OSSEVocabulary.Type.Case, "isDeleted"),
                Criteria.Equal(OSSEVocabulary.Type.Case, "isDeleted", 0))));
    Integer resourceCountOwnLocation =
        sessionBean.getDatabase().countResources(queryPatientsOwnLocation);

    List<Resource> patientPage;

    if ((start + length) <= resourceCountOwnLocation) {
      // If all patients needed to display the page can be filled with patients from the own
      // location, only get them
      queryPatientsOwnLocation.setLimit(length);
      queryPatientsOwnLocation.setOffset(start);
      patientPage = sessionBean.getDatabase().getResources(queryPatientsOwnLocation);
    } else {
      // In other cases, a second query is needed to get patients explicitly from other locations
      ResourceQuery queryPatientsOtherLocations = new ResourceQuery(OSSEVocabulary.Type.Patient);
      queryPatientsOtherLocations.add(
          Criteria.And(
              Criteria.NotEqual(
                  OSSEVocabulary.Type.Case,
                  OSSEVocabulary.Case.Location,
                  sessionBean.getCurrentRole().getLocation().getId()),
              Criteria.Or(
                  Criteria.IsNull(OSSEVocabulary.Type.Case, "isDeleted"),
                  Criteria.Equal(OSSEVocabulary.Type.Case, "isDeleted", 0))));
      if (start > resourceCountOwnLocation) {
        // If the startindex already exceeds the number of patients of the own location, just get
        // those from other locations
        queryPatientsOtherLocations.setLimit(length);
        queryPatientsOtherLocations.setOffset(start - resourceCountOwnLocation);
        patientPage = sessionBean.getDatabase().getResources(queryPatientsOtherLocations);
      } else {
        // Two queries have to be executed and the result has to be merged in this case
        queryPatientsOwnLocation.setOffset(start);
        patientPage = sessionBean.getDatabase().getResources(queryPatientsOwnLocation);
        queryPatientsOtherLocations.setLimit(length - patientPage.size());
        patientPage.addAll(sessionBean.getDatabase().getResources(queryPatientsOtherLocations));
      }
    }

    return patientPage;
  }

  /**
   * Create the patientlist, based on the given list of resources.
   *
   * @param resources the list of patient resources from the backend
   * @param model the database model to use for further queries
   * @return a list of patients in the format to be sent back to the datatable plugin
   */
  private List<PatientlistLine> createPatientlist(
      List<Resource> resources, DatabaseModel<?> model) {
    List<PatientlistLine> patients = new ArrayList<>();

    for (Resource patientResource : resources) {
      PatientlistLine line = new PatientlistLine();

      // set the patientID, in case this has to be displayed (if it is a bridgehead or the patient
      // was imported)
      line.setPatientId(patientResource.getProperty("patientID").asInteger());

      line.setResourceUri(patientResource.getValue());

      // set the psn that can be used by the mainzelliste resolver to query name (first/last) and
      // birthday
      String tmpid = getTmpId(patientResource.getProperty("patientID").getValue());
      line.setPsn(tmpid);

      // set the location name
      String patientLocationName =
          sessionBean.getLocationName(
              patientResource.getProperty(OSSEVocabulary.Patient.Locations).getValue());
      line.setLocation(patientLocationName);

      // set the imported flag for the patient
      try {
        // If the property "Vocabulary.Patient.isImported" is set, copy its value to the line
        line.setImported(patientResource.getProperty(Vocabulary.Patient.isImported).asBoolean());
      } catch (NullPointerException npe) {
        // If the property "Vocabulary.Patient.isImported" is not found on the resource, the patient
        // was not
        // imported. So the value can safely be set to false and the exception be ignored.
        line.setImported(false);
      }

      // Check if there is a case that has the same location as the current user
      boolean mayWrite = false;
      boolean seeIdat = sessionBean.getMayAlwaysSeeIdat();

      // TODO: Right now, there will only be one case per patient. This has to be checked when this
      // changes
      ArrayList<Resource> caseResources =
          patientResource.getResources(OSSEVocabulary.Patient.ReadOnly.Cases, model);

      for (Resource caseResource : caseResources) {
        boolean caseLocationEqualsUserLocation;
        try {
          caseLocationEqualsUserLocation =
              caseResource
                  .getProperty(OSSEVocabulary.Case.Location)
                  .getValue()
                  .equalsIgnoreCase(sessionBean.getCurrentRole().getLocation().getResourceUri());
        } catch (NullPointerException npe) {
          caseLocationEqualsUserLocation = false;
        }
        mayWrite =
            caseLocationEqualsUserLocation
                && sessionBean.mayWrite(patientLocationName, OSSEVocabulary.Type.Patient);
        seeIdat = seeIdat || (caseLocationEqualsUserLocation && sessionBean.getMaySeeIdat());
      }

      line.setMayWrite(mayWrite);
      line.setShowIdat(seeIdat);
      // In the original code, if the patient is not deleted, the right to delete is always
      // identical to may write.
      line.setMayDeleteCase(mayWrite);

      patients.add(line);
    }

    return patients;
  }

  /**
   * Get a temp id for a given patient psn.
   *
   * @param patientId the local pseudonym of the patient
   * @return the tempid which can be used to query the mainzelliste for idat. the idat retrieval
   *     will be done via javascript code
   */
  private String getTmpId(String patientId) {
    // In a bridgehead, the patientID is displayed and nothing else. So no need to try to contact
    // mainzelliste
    if (ApplicationBean.isBridgehead()) {
      return patientId;
    }

    try {
      Session mlSession = sessionBean.getMainzellisteSession(true);
      ID myId = new ID("psn", patientId);

      List<String> fieldsToShow;

      if (Utils.getBooleanOfJsonResource(
          sessionBean.getConfig(), Vocabulary.Config.Mainzelliste.noIdat)) {
        fieldsToShow = Arrays.asList("patientId");
      } else {
        fieldsToShow =
            Arrays.asList("vorname", "nachname", "geburtstag", "geburtsmonat", "geburtsjahr");
      }

      List<String> idsToShow = Arrays.asList("pid");

      return mlSession.getTempId(myId, fieldsToShow, idsToShow);
    } catch (MainzellisteNetworkException | InvalidSessionException e) {
      return "error";
    }
  }
}
