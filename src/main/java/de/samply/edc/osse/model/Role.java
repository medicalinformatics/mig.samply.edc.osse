/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.util.List;

/** Model class for roles. */
public class Role extends Entity {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -6641078835930028684L;

  /** The location of the role. */
  private Location location;

  /**
   * Instantiates a new role.
   *
   * @param database the database
   */
  public Role(AbstractDatabase<?> database) {
    super(database, OSSEVocabulary.Type.Role);
  }

  /**
   * Constructs Patient based on Resource.
   *
   * @param database the database
   * @param roleResource the role resource
   */
  public Role(AbstractDatabase<?> database, Resource roleResource) {
    super(database, OSSEVocabulary.Type.Role, roleResource);
  }

  /**
   * Constructs Patient based on Resource.
   *
   * @param database the database
   * @param roleUri the role uri
   */
  public Role(AbstractDatabase<?> database, String roleUri) {
    super(database, OSSEVocabulary.Type.Role, roleUri);
  }

  /**
   * Loads the role data, and its children (permissions).
   *
   * @return the boolean
   * @see de.samply.edc.model.Entity#load()
   */
  @Override
  public Boolean load(JSONResource myConfig) {
    return load(true, myConfig);
  }

  /**
   * Loads the role data.
   *
   * @param loadPermissions Also load its children (permissions)
   * @return the boolean
   */
  public Boolean load(Boolean loadPermissions, JSONResource myConfig) {
    ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Location);
    query.add(
        Criteria.Equal(
            OSSEVocabulary.Type.Location,
            OSSEVocabulary.ID,
            getResource().getProperty(OSSEVocabulary.Role.Location).asIdentifier().getId()));
    query.setFetchAdjacentResources(false);

    Resource locationResource = getDatabase().getResources(query).get(0);

    if (locationResource != null) {
      location = new Location(getDatabase(), locationResource);
      location.load(myConfig);
    }

    if (loadPermissions) {
      loadChildren(OSSEVocabulary.Type.Permission, false, myConfig);
    }

    return super.load(myConfig);
  }

  /** Loads the permissions of this role. */
  public void loadPermissions(JSONResource myConfig) {
    loadChildren(OSSEVocabulary.Type.Permission, false, myConfig);
  }

  /**
   * Checks if this is a user role.
   *
   * @return the boolean
   */
  public Boolean isUserRole() {
    return getResource().getProperty(OSSEOntology.Role.IsUserRole).asBoolean();
  }

  /**
   * Checks if this is an admin role.
   *
   * @return the boolean
   */
  public Boolean isAdminRole() {
    return getResource()
        .getProperty(OSSEOntology.Role.RoleType)
        .getValue()
        .equalsIgnoreCase(OSSERoleType.ADMINISTRATOR.toString());
  }

  /**
   * Checks if this is a local admin role.
   *
   * @return the boolean
   */
  public Boolean isLocalAdminRole() {
    return getResource()
        .getProperty(OSSEOntology.Role.RoleType)
        .getValue()
        .equalsIgnoreCase(OSSERoleType.LOCAL_ADMINISTRATOR.toString());
  }

  /**
   * Checks if this is a patient role.
   *
   * @return the boolean
   */
  public Boolean isPatientRole() {
    return getResource()
        .getProperty(OSSEOntology.Role.RoleType)
        .getValue()
        .equalsIgnoreCase(OSSERoleType.PATIENT.toString());
  }

  /**
   * Checks if this is a custom role.
   *
   * @return the boolean
   */
  public Boolean isCustomRole() {
    return getResource()
        .getProperty(OSSEOntology.Role.RoleType)
        .getValue()
        .equalsIgnoreCase(OSSERoleType.CUSTOM.toString());
  }

  /**
   * Checks if this is a developer role.
   *
   * @return the boolean
   */
  public Boolean isDeveloperRole() {
    return getResource()
        .getProperty(OSSEOntology.Role.RoleType)
        .getValue()
        .equalsIgnoreCase(OSSERoleType.DEVELOPER.toString());
  }

  /**
   * Checks if this is a developer role.
   *
   * @return the boolean
   */
  public Boolean isSystemRole() {
    return getResource()
        .getProperty(OSSEOntology.Role.RoleType)
        .getValue()
        .equalsIgnoreCase(OSSERoleType.SYSTEM.toString());
  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return getResource().getProperty(OSSEVocabulary.Role.Name).getValue();
  }

  /**
   * Gets the location.
   *
   * @return the location
   */
  public Location getLocation() {
    return location;
  }

  /**
   * Gets the permissions.
   *
   * @return the permissions
   */
  public List<Entity> getPermissions(JSONResource myConfig) {
    return getChildren(OSSEVocabulary.Type.Permission, myConfig);
  }
}
