/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.MdrConnectionException;
import de.samply.common.mdrclient.MdrInvalidResponseException;
import de.samply.common.mdrclient.domain.DataElement;
import de.samply.common.mdrclient.domain.Slot;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.exceptions.FormException;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.utils.FormUtils;
import de.samply.edc.utils.Utils;
import de.samply.form.dto.FormDetails;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.web.mdrfaces.MdrContext;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Optional;
import java.util.concurrent.ExecutionException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Model class for forms. */
public abstract class Form extends Entity {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -3574273866983145331L;

  /**
   * Form properties (data elements and records) that could not be mapped while importing data from
   * a different form.
   */
  private Map<String, Object> notMappedProperties;

  /**
   * Instantiates a new form.
   *
   * @param database the database
   * @param formType the OSSEVocabulary.Type.* form type
   * @param formUri the backend URI of the form
   */
  public Form(AbstractDatabase<?> database, String formType, String formUri) {
    super(database, formType, formUri);
  }

  /**
   * Instantiates a new form.
   *
   * @param database the database
   * @param formType the OSSEVocabulary.Type.* form type
   * @param formRes the backend resource of the form
   */
  public Form(AbstractDatabase<?> database, String formType, Resource formRes) {
    super(database, formType, formRes);
  }

  /**
   * Instantiates a new form.
   *
   * @param database the database
   * @param formType the OSSEVocabulary.Type.* form type
   */
  public Form(AbstractDatabase<?> database, String formType) {
    super(database, formType);
  }

  /**
   * Sets a form state to 'reported'.
   *
   * @throws FormException the form exception
   * @throws DatabaseException the database exception
   */
  public void reportForm() throws FormException, DatabaseException {
    setFormState("reported");
  }

  /**
   * Sets a form status to 'validated'.
   *
   * @throws FormException the form exception
   * @throws DatabaseException the database exception
   */
  public void signForm() throws FormException, DatabaseException {
    setFormState("validated");
  }

  /**
   * Sets a form status to 'refused'.
   *
   * @throws FormException the form exception
   * @throws DatabaseException the database exception
   */
  public void refuseForm() throws FormException, DatabaseException {
    setFormState("open");
  }

  /**
   * Sets a form status to 'open' (used in action "refuse form").
   *
   * @throws FormException the form exception
   * @throws DatabaseException the database exception
   */
  public void reportTakeBackForm() throws FormException, DatabaseException {
    setFormState("open");
  }

  /**
   * Sets a form status to 'open' (used in action "break sign").
   *
   * @throws FormException the form exception
   * @throws DatabaseException the database exception
   */
  public void signTakeBackForm() throws FormException, DatabaseException {
    setFormState("open");
  }

  /**
   * Creates a new form and sets its initial status to the one with the backend ID "1" (which in the
   * original system is "open").
   *
   * @param formName the form name
   * @param formVersion the form version
   */
  public void createForm(String formName, Integer formVersion, JSONResource myConfig) {
    String formNameParam = OSSEVocabulary.CaseForm.Name;
    String formStateParam = OSSEVocabulary.CaseForm.Status;
    String formVersionParam = OSSEVocabulary.CaseForm.Version;
    String formParentParam = OSSEVocabulary.CaseForm.Case;
    String parentType = OSSEVocabulary.Type.Case;

    if (getType() == null || getType().equals(OSSEVocabulary.Type.EpisodeForm)) {
      formNameParam = OSSEVocabulary.EpisodeForm.Name;
      formStateParam = OSSEVocabulary.EpisodeForm.Status;
      formVersionParam = OSSEVocabulary.EpisodeForm.Version;
      formParentParam = OSSEVocabulary.EpisodeForm.Episode;
      parentType = OSSEVocabulary.Type.Episode;
    }

    setProperty(formNameParam, formName);
    Resource statusRes =
        Utils.findResourceByProperty(OSSEVocabulary.Type.Status, OSSEVocabulary.ID, "1");
    setProperty(formStateParam, statusRes);
    setProperty(formVersionParam, formVersion);
    setProperty(formParentParam, getParent(parentType, myConfig).getResource());

    // define if it is a "patientform" or an ordinary registry form
    if (((ApplicationBean) Utils.getAB()).isPatientForm(formName)) {
      setProperty(Vocabulary.Form.formType, Vocabulary.FormTypes.PATIENTFORM);
    } else {
      setProperty(Vocabulary.Form.formType, Vocabulary.FormTypes.DEFAULT);
    }

    getParent(parentType, myConfig).addChild(getType(), this);
  }

  /** Deletes any medical data (i.e. the key is a MDRKey) from the form properties */
  public void deleteFormMedicalData() {
    Iterator<Entry<String, Object>> allKeysIterator = getProperties().entrySet().iterator();
    while (allKeysIterator.hasNext()) {
      Entry<String, Object> entry = allKeysIterator.next();
      String key = entry.getKey();
      String fkey = Utils.fixMdrkeyForSave(key);
      MdrKey mdrKey = new MdrKey(fkey);
      if (mdrKey.getKeyType() == null) {
        continue;
      }

      allKeysIterator.remove();
      // Check if this is a member of a non-repeatable record
      if (fkey.contains("/")) {
        // In this case, the sole record URN has to be marked to be removed
        addRemovedProperty(fkey.split("/")[0]);
      }
      addRemovedProperty(fkey);
    }
  }

  /** Copies the Data of a given previous Episode's Form. */
  public void importPreviousFormData(Form previousForm) {
    String summary = Utils.getResourceBundleString("summary_importfailed");
    String errorFormNotOpen = Utils.getResourceBundleString("error_formnotopen");

    HashMap<String, Boolean> notCopyableDataKeys = FormUtils.getNotCopyableDataKeys();

    if (isOpen()) {
      for (String key : previousForm.getProperties().keySet()) {
        if (notCopyableDataKeys.get(key) != null) {
          continue;
        }

        setProperty(key, previousForm.getProperty(key));
      }
    }

    getDatabase().beginTransaction();
    saveOrUpdate();
    getDatabase().commit();
    storeLastChange();
  }

  /**
   * Import data from another form. Shortcut for {@link #importFrom(Form, boolean)} with parameter
   * {@code saveForm} set to {@code true}.
   */
  public String importPreviousFormData2(Form previousForm) {
    return importFrom(previousForm, true);
  }

  /**
   * Import data from another form. Imports all data elements and records that also exist in this
   * form. When comparing data elements and records, their versions are ignored, as a version change
   * cannot change the value domain.
   *
   * @param previousForm The form from which to import data
   * @param saveForm Whether to save this form after import
   * @return "matched_none"|"matched_fully"|"matched_partially"
   */
  public String importFrom(Form previousForm, boolean saveForm) {
    String summary = Utils.getResourceBundleString("summary_importfailed");
    String errorFormNotOpen = Utils.getResourceBundleString("error_formnotopen");
    String dataMatchStatus = "";
    int matchedElements = 0;
    notMappedProperties = new HashMap<>();
    FormDetails currentForm =
        FormUtils.loadFormFromXml(
            FormUtils.getFormIdFromName((String) getProperty("name")),
            FormUtils.getFormVersionFromName((String) getProperty("name")));
    ApplicationBean ab = (ApplicationBean) Utils.getAB();
    HashMap<String, List<String>> forms = ab.getFormContainsMdrEntity();
    Pattern mdrIdPattern = Pattern.compile("urn:(.*-\\d+):(record|dataelement):(\\d+):(\\d+)");
    Pattern keyPattern = Pattern.compile("urn__(.*-\\d+)__(record|dataelement)__(\\d+)__(\\d+).*");

    HashMap<String, Boolean> notCopyableDataKeys = FormUtils.getNotCopyableDataKeys();
    HashMap<String, List<String>> recordHasMdrEntities =
        ((ApplicationBean) Utils.getAB()).getRecordHasMdrEntities();

    // Taking details from current episode Form like dataelements the form can possibly have even
    // when form is newly created or not saved.
    FormDetails currentFormDetails =
        FormUtils.loadFormFromXml(
            FormUtils.getFormIdFromName(getFormID()),
            FormUtils.getFormVersionFromName(getFormID()));
    if (isOpen()) {
      for (String previousFormKey : previousForm.getProperties().keySet()) {
        if (notCopyableDataKeys.get(previousFormKey) != null) {
          continue;
        }
        // Skip empty values or repeatable records without rows
        Object previousFormValue = previousForm.getProperty(previousFormKey);
        if (previousFormValue == null
            || "".equals(previousFormValue)
            || (previousFormValue instanceof DatatableRows
                && ((DatatableRows) previousFormValue).getRows().size() == 0)) {
          continue;
        }
        String targetFormKey = null;

        Matcher previousFormKeyMatcher =
            FormUtils.getFormPropertyKeyPatternMatcher(previousFormKey);
        if (previousFormKeyMatcher
            .matches()) { // only checking/comparing with mdr dataelements or records of prev form
          boolean matchFound = false;
          for (FormDetails.Item item :
              currentFormDetails
                  .getItems()) { // formDetails: contains dataelements/mdrId of current form
            if (item.getMdrId() != null) { // if there exists any mdrId
              // to match only the data element's (or record's) namespace, type and id (till
              // urn:osse-1:dataelement:1:)
              // i.e. import in spite of different versions of data elements or records
              Matcher itemUrnMatcher = FormUtils.getMdrUrnPatternMatcher(item.getMdrId());
              if (itemUrnMatcher.matches()
                  && itemUrnMatcher.group(1).equals(previousFormKeyMatcher.group(1))
                  && // namespace
                  itemUrnMatcher.group(2).equals(previousFormKeyMatcher.group(2))
                  && // type (record/dataelement)
                  itemUrnMatcher.group(3).equals(previousFormKeyMatcher.group(3))) { // id
                String namespace = itemUrnMatcher.group(1);
                // compares current form mdrId with prev form dataelement/mdrId
                // if matches then set current dataelement to prev form dataelement value
                if (previousFormKeyMatcher.group(2).equals("record")) { // field is a record
                  if (previousFormKeyMatcher.group(5).equals("")) { // repeatable record
                    Object previousPropertyValue = previousForm.getProperty(previousFormKey);
                    // go into the rows of the repeatable record, find every single item and compare
                    // it with the record of the current form
                    // at the same time create new rows with the same data but updated item versions
                    // (except if target is non-repeatable)
                    if (previousPropertyValue instanceof DatatableRows) {
                      DatatableRows previousRows = (DatatableRows) previousPropertyValue;
                      // Automatic mapping only possible if target is also repeatable or if there
                      // is only one row in the source record
                      if (item.isRepeatable() || previousRows.getRows().size() == 1) {
                        try {
                          HashMap<String, Object> propertiesToAdd = new HashMap<>();
                          DatatableRows newRows = new DatatableRows();
                          for (DatatableRow previousRow : previousRows.getRows()) {
                            DatatableRow newRow = new DatatableRow();
                            HashMap<String, Object> newColumns = new HashMap<>();
                            for (String previousColumnKey : previousRow.getColumns().keySet()) {
                              Matcher previousColumnKeyMatcher =
                                  FormUtils.getFormPropertyKeyPatternMatcher(previousColumnKey);
                              if (previousColumnKeyMatcher.matches()) {
                                String newVersion = null;
                                for (String mdrEntity : recordHasMdrEntities.get(item.getMdrId())) {
                                  // check if the same data element is present (has to be, but we
                                  // need
                                  // the version)
                                  Matcher recordElementUrnMatcher =
                                      FormUtils.getMdrUrnPatternMatcher(mdrEntity);
                                  if (recordElementUrnMatcher.matches()
                                      && recordElementUrnMatcher
                                          .group(3)
                                          .equals(previousColumnKeyMatcher.group(3))) {
                                    newVersion = recordElementUrnMatcher.group(4);
                                  }
                                }
                                Object mappedValue =
                                    mapPermittedValues(
                                        namespace,
                                        previousColumnKeyMatcher.group(3),
                                        previousColumnKeyMatcher.group(4),
                                        newVersion,
                                        previousRow.getColumns().get(previousColumnKey));
                                if (item.isRepeatable()) {
                                  newColumns.put(
                                      FormUtils.constructFormKey(
                                          itemUrnMatcher.group(1),
                                          previousColumnKeyMatcher.group(3),
                                          newVersion),
                                      mappedValue);
                                } else {
                                  // If target is non-repeatable, copy value to the form key that
                                  // belongs to this record member. The check
                                  // previousRows.getRows().size() above makes sure that this is
                                  // only done if there is only one row in the source record, i.e.no
                                  // value in the target is overwritten
                                  targetFormKey = FormUtils.constructFormKey(
                                      itemUrnMatcher.group(1),
                                      itemUrnMatcher.group(3),
                                      itemUrnMatcher.group(4),
                                      previousColumnKeyMatcher.group(3),
                                      newVersion);
                                  // Mapped values are collected in this map first until it is
                                  // sure that all record members can be mapped
                                  propertiesToAdd.put(targetFormKey,mappedValue);
                                  continue;
                                }
                              }
                            }
                            newRow.setColumns(newColumns);
                            newRows.add(newRow);
                          }
                          // Assign DataTableRows if target is repeatable
                          if (item.isRepeatable()) {
                            targetFormKey = FormUtils.constructFormKey(
                                itemUrnMatcher.group(1),
                                itemUrnMatcher.group(3),
                                itemUrnMatcher.group(4),
                                null,
                                null);
                            setProperty(targetFormKey, newRows);
                          } else { // Target is non-repeatable
                            // Add all cached non-repeatable record members now that they have all
                            // been mapped
                            propertiesToAdd
                                .entrySet()
                                .forEach(e -> setProperty(e.getKey(), e.getValue()));
                          }
                          matchFound = true;
                        } catch (PermittedValueNotMappableException e) {
                          // If a record member could not be mapped, continue. This will leave the
                          // whole record unmapped -> trade-off between code
                          // complexity and expected frequency (low!) of this case
                          // Continue with next item of the target form
                          continue;
                        }
                      }
                    }
                  } else { // non-repeatable record
                    for (String mdrEntity : recordHasMdrEntities.get(item.getMdrId())) {
                      // check if it's the same data element (ignore version)
                      Matcher recordElementUrnMatcher =
                          FormUtils.getMdrUrnPatternMatcher(mdrEntity);
                      if (recordElementUrnMatcher.matches()
                          && recordElementUrnMatcher
                              .group(1)
                              .equals(previousFormKeyMatcher.group(6))
                          && // namespace
                          recordElementUrnMatcher.group(2).equals(previousFormKeyMatcher.group(7))
                          && // dataelement
                          recordElementUrnMatcher
                              .group(3)
                              .equals(previousFormKeyMatcher.group(8))) { // id
                        // store same value for updated key
                        if (!item.isRepeatable()) {
                          try {
                            targetFormKey = FormUtils.constructFormKey(
                                itemUrnMatcher.group(1),
                                itemUrnMatcher.group(3),
                                itemUrnMatcher.group(4),
                                recordElementUrnMatcher.group(3),
                                recordElementUrnMatcher.group(4));
                            setProperty(targetFormKey,
                                mapPermittedValues(
                                    namespace,
                                    previousFormKeyMatcher.group(8),
                                    previousFormKeyMatcher.group(9),
                                    recordElementUrnMatcher.group(4),
                                    previousForm.getProperty(previousFormKey)));
                            matchFound = true;
                          } catch (PermittedValueNotMappableException e) {
                            break;
                          }
                        } else {
                          // Target is repeatable -> add source values to a single entry in the
                          // target
                          targetFormKey =
                              FormUtils.constructFormKey(
                                  itemUrnMatcher.group(1),
                                  itemUrnMatcher.group(3),
                                  itemUrnMatcher.group(4),
                                  null,
                                  null);
                          // Create record in form data if necessary (done when the first record
                          // member is copied)
                          if (!hasProperty(targetFormKey)) {
                            setProperty(targetFormKey, new DatatableRows());
                          }
                          DatatableRows targetDatatableRows =
                              (DatatableRows) getProperty(targetFormKey);
                          if (targetDatatableRows.getRows().size() == 0) {
                            targetDatatableRows.add(new DatatableRow());
                          }
                          DatatableRow targetRow = targetDatatableRows.getRows().get(0);
                          try {
                            targetRow
                                .getColumns()
                                .put(
                                    FormUtils.constructFormKey(
                                        itemUrnMatcher.group(1),
                                        recordElementUrnMatcher.group(
                                            3), // This is the target dataelement id
                                        recordElementUrnMatcher.group(4)), // and this the version.
                                    mapPermittedValues(
                                        namespace,
                                        previousFormKeyMatcher.group(8),
                                        previousFormKeyMatcher.group(9),
                                        recordElementUrnMatcher.group(4),
                                        previousForm.getProperty(previousFormKey)));
                            matchFound = true;
                          } catch (PermittedValueNotMappableException e) {
                            // Remove row if there are no entries in it
                            // -> Prevents empty row in case no record members could be mapped
                            if (targetRow.getColumns().size() == 0) {
                              removeProperty(targetFormKey);
                            }
                            break;
                          }
                        }
                      }
                    }
                  }
                } else { // field is a data element
                  String elementId = itemUrnMatcher.group(3);
                  String targetVersion = itemUrnMatcher.group(4);
                  String sourceVersion = previousFormKeyMatcher.group(4);
                  targetFormKey =
                      FormUtils.constructFormKey(
                          itemUrnMatcher.group(1),
                          itemUrnMatcher.group(3),
                          itemUrnMatcher.group(4));
                  if (item.isRepeatable()) {
                    // Target element is repeatable. Create DatatableRows instance if not there yet.
                    if (!hasProperty(targetFormKey)) {
                      this.setProperty(targetFormKey, new DatatableRows());
                    }
                    DatatableRows targetDatatable = (DatatableRows) this.getProperty(targetFormKey);
                    if (previousForm.getProperty(previousFormKey) instanceof DatatableRows) {
                      // Source element is also repeatable
                      try {
                        for (DatatableRow sourceRow :
                            ((DatatableRows) previousForm.getProperty(previousFormKey)).getRows()) {
                          DatatableRow newRow = new DatatableRow();
                          for (Object value : sourceRow.getColumns().values()) {
                            newRow
                                .getColumns()
                                .put(
                                    targetFormKey,
                                    mapPermittedValues(
                                        namespace, elementId, sourceVersion, targetVersion, value));
                          }
                          targetDatatable.add(newRow);
                          matchFound = true;
                        }
                      } catch (PermittedValueNotMappableException e) {
                        // If there are any non-mappable permitted values in the repeatable element,
                        // let the whole element fail (i.e. remove already mapped rows). This
                        // will probably happen only rarely, if ever, so the coding effort to map
                        // some entries and some not is not reasonable.
                        this.removeProperty(targetFormKey);
                        // Reset matchFound as it has been set to true if one entry has been mapped
                        // before the failure
                        matchFound = false;
                        continue;
                      }
                    } else {
                      try {
                        // Source is non-repeatable, but target is
                        DatatableRow newRow = new DatatableRow();
                        newRow
                            .getColumns()
                            .put(
                                targetFormKey,
                                mapPermittedValues(
                                    namespace,
                                    elementId,
                                    sourceVersion,
                                    targetVersion,
                                    previousForm.getProperty(previousFormKey)));
                        targetDatatable.add(newRow);
                        matchFound = true;
                      } catch (PermittedValueNotMappableException e) {
                        continue;
                      }
                    }
                  } else {
                    // Source element is repeatable
                    if (previousForm.getProperty(previousFormKey) instanceof DatatableRows) {
                      // Source is repeatable but target not: Import only possible if there is only
                      // one entry
                      DatatableRows sourceRows =
                          (DatatableRows) previousForm.getProperty(previousFormKey);
                      if (sourceRows.getRows().size() == 1) {
                        try {
                          if (sourceRows.getRows().get(0).getColumns().get(previousFormKey)
                              != null) {
                            setProperty(
                                targetFormKey,
                                mapPermittedValues(
                                    namespace,
                                    elementId,
                                    sourceVersion,
                                    targetVersion,
                                    sourceRows.getRows().get(0).getColumns().get(previousFormKey)));
                          }
                          // Mapping is successful though even with null value
                          matchFound = true;
                        } catch (PermittedValueNotMappableException e) {
                          continue;
                        }
                      }
                    } else {
                      // Both source and target are non-repeatable
                      try {
                        setProperty(
                            targetFormKey,
                            mapPermittedValues(
                                namespace,
                                elementId,
                                sourceVersion,
                                targetVersion,
                                previousForm.getProperty(previousFormKey)));
                        matchFound = true;
                      } catch (PermittedValueNotMappableException e) {
                        continue;
                      }
                    }
                  }
                }
              }
            }
          }
          if (matchFound) {
            matchedElements++;
          } else {
            // Add to map of not mapped properties
            notMappedProperties.put(previousFormKey, previousForm.getProperty(previousFormKey));
          }
        }
      }
    }

    if (saveForm) {
      getDatabase().beginTransaction();
      saveOrUpdate();
      this.setVersion(Math.toIntExact(currentFormDetails.getVersion()));
      getDatabase().commit();
      storeLastChange();
    }
    // Utils.getLogger().debug("checking form names and
    // version"+getFormID().substring(0,ind)+getVersion());
    Utils.getLogger()
        .debug(
            "checking previous form names and version"
                + previousForm.getFormRealname()
                + previousForm.getVersion());

    // message status to send according to currently matched elements with previous form
    if (matchedElements == 0) {
      dataMatchStatus = "matched_none";
    } else if (matchedElements > 0 && getVersion().equals(previousForm.getVersion())) {
      dataMatchStatus = "matched_fully";
    } else if (matchedElements > 0) {
      dataMatchStatus = "matched_partially";
    }

    return dataMatchStatus;
  }

  /**
   * Exception to signal when mapping permitted values fails. See {@link #mapPermittedValues(String,
   * String, String, String, Object)}.
   */
  private final class PermittedValueNotMappableException extends Exception {}

  /**
   * Map value to a different version of the data element and respect the input type (single choice
   * / multiple choice).
   *
   * <p>Two versions of a data element with permitted values can differ in the aspect if multiple
   * selections can be made - this is controlled by a slot, which can be changed without creating a
   * whole new data element. Single choice and multiple choice options are stored differently
   * (atomic value vs. array). This method maps values of such elements whenever possible. If the
   * input contains multiple values (i.e. multiple checked checkboxes in the form) but the target
   * element version only supports single choice, it fails with throwing {@link
   * PermittedValueNotMappableException}. If the data element is not of data type "permitted values"
   * at all, the value is returned as it is.
   *
   * @param namespace MDR namespace
   * @param dataElementId ID of the data element
   * @param sourceVersion Version of the source data element
   * @param targetVersion Version of the target data element
   * @param value The value to map
   * @return The mapped value.
   * @throws PermittedValueNotMappableException if mapping is not possible
   */
  private Object mapPermittedValues(
      String namespace,
      String dataElementId,
      String sourceVersion,
      String targetVersion,
      Object value)
      throws PermittedValueNotMappableException {
    // Put source value into an array if it is not one already in order to simplify the code
    Object[] valueArray = value.getClass().isArray() ? (Object[]) value : new Object[] {value};
    MdrClient mdrClient = MdrContext.getMdrContext().getMdrClient();
    String targetUrn = "urn:" + namespace + ":dataelement:" + dataElementId + ":" + targetVersion;
    Boolean sourceHasMultipleValues = valueArray.length > 1;
    try {
      DataElement targetElement =
          mdrClient.getDataElement(targetUrn, Utils.getLocale().getLanguage());
      // For all data types except permitted values, just return the input
      if (targetElement.getValidation().getPermissibleValues().size() == 0) {
        return value;
      }
      // Find slot with inputType and find out if target is a multiple choice element
      Optional<Slot> slotInputType =
          targetElement.getSlots().stream()
              .filter(s -> s.getSlotName().equals("inputType"))
              .findAny();
      Boolean targetIsMultipleChoice =
          slotInputType.isPresent()
              && slotInputType.get().getSlotValue().equals("SELECT_MANY_CHECKBOX");
      if (sourceHasMultipleValues && !targetIsMultipleChoice) {
        // No mapping possible (multiple values to single choice
        throw new PermittedValueNotMappableException();
      } else if (targetIsMultipleChoice) {
        // If the target element allows multiple choice, an array must be returned in any case
        return valueArray;
      } else { // Only case left: target is single choice and source has at most one value
        // Special case: Emtpy array (is the case when source is multiple choice but nothing
        // selected)
        if (valueArray.length == 0) {
          // "" can also represent that nothing is chosen (elsewhere, EDC omits the element)
          return "";
        } else {
          return valueArray[0];
        }
      }
    } catch (MdrConnectionException | MdrInvalidResponseException | ExecutionException e) {
      e.printStackTrace();
      throw new Error("Failure while accessing MDR", e);
    }
  }

  /**
   * Get the form properties (data elements and records) that could not be mapped when importing
   * from a different form. To be invoked after {@link #importFrom(Form, boolean)} or {@link
   * #importPreviousFormData2(Form)}.
   *
   * @return The not mapped entries
   */
  public Map<String, Object> getNotMappedProperties() {
    return notMappedProperties == null ? new HashMap<>() : notMappedProperties;
  }

  /**
   * Checks if form is in the status 'validated'.
   *
   * @return the boolean
   */
  public Boolean isValidated() {
    return getFormState().equals("validated");
  }

  /**
   * Checks if form is in the status 'reported'.
   *
   * @return the boolean
   */
  public Boolean isReported() {
    return getFormState().equals("reported");
  }

  /**
   * Checks if form is in the status 'open'.
   *
   * @return the boolean
   */
  public Boolean isOpen() {
    return getFormState().equals("open");
  }

  /**
   * Gets the form state number (which is basically the ID of the backend resource of the state).
   *
   * @return the form status number.
   */
  public String getFormStateNumber() {
    String statusNumber = ((ApplicationBean) Utils.getAB()).getFormStandardStatusNumber();

    // no type? return the standard status
    if (getType() == null) {
      return statusNumber;
    }

    if (getType().equals(OSSEVocabulary.Type.EpisodeForm)) {
      Object statusRes = getProperty(OSSEVocabulary.EpisodeForm.Status);
      if (statusRes != null) {
        if (statusRes instanceof Resource) {
          return "" + ((Resource) statusRes).getId();
        }
        statusNumber = ((String) statusRes).split(":")[1];
      }
    } else if (getType().equals(OSSEVocabulary.Type.CaseForm)) {
      Object statusRes = getProperty(OSSEVocabulary.CaseForm.Status);
      if (statusRes != null) {
        if (statusRes instanceof Resource) {
          return "" + ((Resource) statusRes).getId();
        }
        statusNumber = ((String) statusRes).split(":")[1];
      }
    }

    return statusNumber;
  }

  /**
   * Gets the form status name (open, reported, validated, refused).
   *
   * @return the form status
   */
  public String getFormState() {
    String formState =
        ((ApplicationBean) Utils.getAB())
            .getFormStatus()
            .get(Integer.parseInt(getFormStateNumber()));

    if (formState == null) {
      return "open";
    }

    return formState;
  }

  /**
   * Sets the form status to a given status, only legal statuses will be accepted.
   *
   * @param state the new form status
   * @throws FormException if an illegal status is provided
   * @throws DatabaseException the database exception
   */
  public void setFormState(String state) throws FormException, DatabaseException {
    if (!((ApplicationBean) Utils.getAB()).getFormStatusReverse().containsKey(state)) {
      throw new FormException(
          "form.setFormState called with illegal state "
              + state
              + " available are only: "
              + ((ApplicationBean) Utils.getAB()).getFormStatus().values());
    }

    Resource statusResource =
        Utils.findResourceByProperty(OSSEVocabulary.Type.Status, "name", state);
    if (statusResource == null) {
      throw new FormException(
          "form.setFormState called with illegal state " + state + ": no Resource found!");
    }

    getDatabase().beginTransaction();
    getDatabase().executeFormAction(getResource(), statusResource);
    getDatabase().commit();
  }

  /**
   * Gets the version of the form.
   *
   * @return the version
   */
  public String getVersion() {
    if (getType() == null || getType().equals(OSSEVocabulary.Type.EpisodeForm)) {
      return getProperty(OSSEVocabulary.EpisodeForm.Version).toString();
    }
    return getProperty(OSSEVocabulary.CaseForm.Version).toString();
  }

  /**
   * Sets the version.
   *
   * @param formVersion the new version
   */
  public void setVersion(Integer formVersion) {
    String versionParam = OSSEVocabulary.CaseForm.Version;
    if (getType() == null || getType().equals(OSSEVocabulary.Type.EpisodeForm)) {
      versionParam = OSSEVocabulary.EpisodeForm.Version;
    }

    getResource().setProperty(versionParam, formVersion);
    setProperty(versionParam, formVersion);
    getDatabase().save(getResource());
  }

  /**
   * Gets the form id/name.
   *
   * @return the form id
   */
  public String getFormID() {
    if (getType() == null || getType().equals(OSSEVocabulary.Type.EpisodeForm)) {
      return (String) getProperty(OSSEVocabulary.EpisodeForm.Name);
    }
    return (String) getProperty(OSSEVocabulary.CaseForm.Name);
  }

  /**
   * Prints out the properties of the form.
   *
   * @return the string
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    String text = "";

    text += getUri();

    for (String key : this.getProperties().keySet()) {
      text = text + "K: " + key + " V: " + getProperty(key) + "\n\n";
    }

    return text;
  }

  /**
   * returns the page title name of a form.
   *
   * @return String the page title name
   */
  public String getFormRealname() {

    String formid = getFormID();
    String[] moo = formid.split("-");
    if (moo.length > 0) {
      return Utils.getAB().getPageTitleName(formid, Utils.getLocale());
    }

    return "";
  }
}
