/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSERoleType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.Criteria;
import de.samply.store.query.ResourceQuery;
import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/** Model class for locations, a representation of a clinic, an organisation or an instutition. */
public class Location extends Entity {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 4148411369720339980L;

  /** The contact data of the location. */
  private LocationContact contact;

  /** The users of the location. */
  private ArrayList<User> users;

  /** The patients of the location. */
  private ArrayList<Patient> patients;

  /**
   * Instantiates a new location.
   *
   * @param database the database
   */
  public Location(AbstractDatabase<?> database) {
    super(database, OSSEVocabulary.Type.Location);
  }

  /**
   * Constructs Patient based on Resource.
   *
   * @param database the database
   * @param locationResource the location resource
   */
  public Location(AbstractDatabase<?> database, Resource locationResource) {
    super(database, OSSEVocabulary.Type.Location, locationResource);
  }

  /**
   * Constructs Patient based on Resource.
   *
   * @param database the database
   * @param locationUri the location uri
   */
  public Location(AbstractDatabase<?> database, String locationUri) {
    super(database, OSSEVocabulary.Type.Location, locationUri);
  }

  /**
   * Instantiates a new location.
   *
   * @param database the database
   * @param locationID the location id
   */
  public Location(AbstractDatabase<?> database, Integer locationID) {
    super(database, OSSEVocabulary.Type.Location, locationID);
  }

  /**
   * Creates a location or loads an existing one by name.
   *
   * @param name the name
   */
  public void createOrLoadByName(String name, JSONResource myConfig) {
    Resource patientLocationResource = entityExistsByProperty(OSSEVocabulary.Location.Name, name);

    if (patientLocationResource == null) {
      // create location
      setProperty(OSSEVocabulary.Location.Name, name);
      saveOrUpdate();

      LocationContact contact = new LocationContact(getDatabase());
      contact.setLocation(this);
      contact.saveOrUpdate();

      Role role = new Role(getDatabase());
      role.setProperty(OSSEOntology.Role.RoleType, OSSERoleType.LOCAL_ADMINISTRATOR);
      role.setProperty(OSSEOntology.Role.IsUserRole, false);
      role.setProperty(OSSEVocabulary.Role.Location, getResource());
      role.setProperty(OSSEVocabulary.Role.Name, name + " Admin");
      role.saveOrUpdate();
    } else {
      setResource(patientLocationResource);
    }
    load(myConfig);
  }

  /**
   * Loads the location data and initiates its contact.
   *
   * @return the boolean
   * @see de.samply.edc.model.Entity#load(JSONResource)
   */
  @Override
  public Boolean load(JSONResource myConfig) {
    if (getResource().getProperty(OSSEVocabulary.Location.ReadOnly.LocationContacts) == null) {
      contact = new LocationContact(getDatabase());
      contact.setLocation(this);
      contact.setProperty(Vocabulary.Attributes.contactStreet, "N/A");
      contact.setProperty(Vocabulary.Attributes.contactPLZ, "N/A");
      contact.setProperty(Vocabulary.Attributes.contactCity, "N/A");
      contact.setProperty(Vocabulary.Attributes.contactFreetext, "N/A");
    } else {
      Resource contactRes =
          getDatabase()
              .getResourceByIdentifier(
                  getResource()
                      .getProperty(OSSEVocabulary.Location.ReadOnly.LocationContacts)
                      .getValue());
      contact = new LocationContact(getDatabase(), contactRes);
      contact.setLocation(this);
      contact.load(myConfig);
    }

    return super.load(myConfig);
  }

  /**
   * Compares a location to another by name.
   *
   * @param otherLocation the compare object
   * @return the int
   */
  public int compareTo(Location otherLocation) {
    Collator collator = Collator.getInstance(Locale.GERMAN);
    collator.setStrength(Collator.SECONDARY);
    if (otherLocation == null || getName() == null || otherLocation.getName() == null) {
      return 0;
    }
    return collator.compare(getName(), otherLocation.getName());
  }

  /** Loads the users of the location. */
  public void loadUsers(JSONResource myConfig) {
    users = new ArrayList<>();

    ResourceQuery allUsersQuery = new ResourceQuery(OSSEVocabulary.Type.User);
    allUsersQuery.add(
        Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.ID, getResource().getId()));

    List<Resource> result = getDatabase().getResources(allUsersQuery);

    if (!result.isEmpty()) {
      for (Resource userRes : result) {
        User user = new User(getDatabase(), userRes);
        user.load(myConfig);

        users.add(user);
      }
    }
  }

  /** Loads the patients of the location. */
  public void loadPatients(JSONResource myConfig) {
    ResourceQuery crit = new ResourceQuery(OSSEVocabulary.Type.Patient);
    crit.add(
        Criteria.Equal(OSSEVocabulary.Type.Location, OSSEVocabulary.ID, getResource().getId()));

    patients = new ArrayList<>();

    List<Resource> patResList = getDatabase().getResources(crit);

    if (patResList != null && patResList.size() > 0) {
      for (Resource p : patResList) {
        Patient pat = new Patient(getDatabase(), p);
        pat.load(myConfig);
        if (pat.isDeleted()) {
          continue;
        }

        patients.add(pat);
      }
    }
  }

  /**
   * Returns the data or if there is none, what you give as standard.
   *
   * @param key the key
   * @param standard the standard
   * @return the contact data
   */
  private String getContactData(String key, String standard) {
    if (contact != null && contact.getProperty(key) != null) {
      return (String) contact.getProperty(key);
    } else {
      return standard;
    }
  }

  /**
   * Returns the data or if there is none, returns N/A.
   *
   * @param key the key
   * @return the contact data
   */
  private String getContactData(String key) {
    return getContactData(key, "N/A");
  }

  /**
   * Gets the street.
   *
   * @return the street
   */
  public String getStreet() {
    return getContactData(Vocabulary.Attributes.contactStreet);
  }

  /**
   * Gets the postal code.
   *
   * @return the postal code.
   */
  public String getPlz() {
    return getContactData(Vocabulary.Attributes.contactPLZ);
  }

  /**
   * Gets the city.
   *
   * @return the city
   */
  public String getCity() {
    return getContactData(Vocabulary.Attributes.contactCity);
  }

  /**
   * Gets the optional freetext data.
   *
   * @return the freetext data
   */
  public String getFreetext() {
    return getContactData(Vocabulary.Attributes.contactFreetext, "");
  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return (String) getProperty(OSSEVocabulary.Location.Name);
  }

  /**
   * Gets the contact.
   *
   * @return the contact
   */
  public LocationContact getContact() {
    return contact;
  }

  /**
   * Sets the contact.
   *
   * @param contact the new contact
   */
  public void setContact(LocationContact contact) {
    this.contact = contact;
  }
}
