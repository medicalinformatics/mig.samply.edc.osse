/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import java.util.HashMap;

/**
 * Singleton class to store tokenIds that are transmitted by the Mainzelliste to the registry via
 * JSON, as there is no other way to transmit it into the sessionBean due to the nature of the
 * communication callback/redirect mechanic used.
 *
 * <p>The idea is, that the token is stored in the sessionBean. This token is used to store the
 * Mainzelliste response data (atm. PID) in this singleton to make it accessable from the session
 * once the user is being redirected back to OSSE from the Mainzelliste.
 *
 * <p>TODO: expire timestamp, and expire-cleanup of this datastorage
 */
public enum TempTokenExchangeStorage {

  /** It's a singleton. */
  instance;

  /** The current objects. */
  private HashMap<String, Object> currentObjects;

  /**
   * Gets the current object.
   *
   * @param name the name
   * @return the current object
   */
  public synchronized Object getCurrentObject(String name) {
    if (currentObjects == null) {
      currentObjects = new HashMap<>();
    }

    if (currentObjects.containsKey(name)) {
      return currentObjects.get(name);
    } else {
      return null;
    }
  }

  /**
   * Sets the current object.
   *
   * @param name the name
   * @param ob the ob
   */
  public synchronized void setCurrentObject(String name, Object ob) {
    if (currentObjects == null) {
      currentObjects = new HashMap<>();
    }

    currentObjects.put(name, ob);
  }
}
