/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.utils.Utils;
import de.samply.store.DatabaseConstants;
import de.samply.store.JSONResource;
import de.samply.store.ResourceUtils;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEModel;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/** The DBmodel. Only used for upgrading a registry to directly manipulate the backend. */
public class UpgradeDbModel extends OSSEModel {

  /**
   * Instantiates a new upgrade db model.
   *
   * @param config the config file
   * @throws DatabaseException the database exception
   */
  public UpgradeDbModel(String config) throws DatabaseException {
    super(config);
  }

  /** Inserts the system role into the registry. */
  public void insertSystemRole() {
    StringBuilder builder =
        new StringBuilder(
            "BEGIN TRANSACTION; "
                + "INSERT INTO roles (data, name, transaction_id, location_id) values "
                + "('{\"roleType\":\"SYSTEM\", \"isUserRole\":false}', 'Systemrole', 1, 1); "
                + "INSERT INTO users_roles (transaction_id, user_id, role_id) VALUES "
                + "(1, 1, currval('\"roles_id_seq\"')); "
                + "COMMIT; ");

    PreparedStatement stmt;
    try {
      stmt = connection.prepareStatement(builder.toString());
      stmt.executeUpdate();
      stmt.close();
      connection.commit();

    } catch (SQLException e) {
      Utils.getLogger().warn("Unable to add system role to database", e);
    }
  }

  /**
   * Save the OSSE config.
   *
   * @param name the name
   * @param config the config
   * @throws DatabaseException the database exception
   */
  public void saveOsseConfig(String name, JSONResource config) throws DatabaseException {
    try {
      String delete = "DELETE FROM \"configs\" WHERE \"name\" = ?";

      PreparedStatement st = connection.prepareStatement(delete);
      st.setString(1, name);

      st.execute();
      st.close();

      String sql =
          "INSERT INTO \"configs\" (\"name\", \"data\") VALUES (?, CAST(? AS "
              + DatabaseConstants.getSQLJson()
              + "));";

      st = connection.prepareStatement(sql);
      st.setString(1, name);
      st.setString(2, ResourceUtils.createJsonElement(config).toString());
      st.execute();
      st.close();

      connection.commit();

    } catch (SQLException e) {
      Utils.getLogger().error("Unable to save osse config to database", e);
      throw new DatabaseException(e);
    }
  }

  /**
   * Gets the database connection.
   *
   * @return the database connection
   */
  public Connection getConnection() {
    return connection;
  }
}
