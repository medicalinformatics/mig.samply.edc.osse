/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.utils.Utils;
import de.samply.store.DatabaseConstants;
import de.samply.store.JSONResource;
import de.samply.store.ResourceUtils;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEModel;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import org.apache.commons.configuration.Configuration;

/** The DBmodel. Only used to install a new registry to directly manipulate the backend. */
public class InitDbModel extends OSSEModel {

  /**
   * Instantiates a new db model.
   *
   * @param config the database config file
   * @throws DatabaseException the database exception
   */
  public InitDbModel(String config) throws DatabaseException {
    super(config);
  }

  /**
   * Inserts the global roles location, the global administration role and adds the users "admin"
   * (with password "admin"), "dev" (commented out, with password "dev"), and "export" (without
   * valid password) into the database. The user "admin" has access to the global administration of
   * the registry. The user "dev" has global access to the medical data. The user "exporter" is only
   * used for the export of medical data.
   */
  public void insertAdminAndDev() {
    StringBuilder builder =
        new StringBuilder("BEGIN TRANSACTION; "
            + "ALTER TABLE transactions DROP CONSTRAINT transactions_user_id_fkey; "
            + "INSERT INTO transactions (\"timestamp\", user_id) values (now(), 1); "
            + "INSERT INTO \"users\" (data, username, password, salt, \"isActivated\", "
            + "\"activationCode\", \"failCounter\", transaction_id) VALUES ('{}', 'admin', "
            + "'6bcf0beed43ffcbb06d778c5e00dab81f901c71162bb1ac06ceb51309829e7466219d7a6af315a6583"
            + "668ddd7016e9bf72bbed1e6ba269d22d8e1c17c8c52acd', 'gyQq5bsnpfE5RkeN5Z3iTknLQqt40VsL',"
            + " 1, '', 0, 1); "
            + "INSERT INTO locations (transaction_id, name, data) VALUES (1, 'Global Roles', '{}');"
            + " INSERT INTO roles (data, name, transaction_id, location_id) values "
            + "('{\"roleType\":\"ADMINISTRATOR\", \"isUserRole\":false}', 'Global Admin', 1, 1); "
            + "INSERT INTO roles (data, name, transaction_id, location_id) values "
            + "('{\"roleType\":\"SYSTEM\", \"isUserRole\":false}', 'Systemrole', 1, 1); "
            + "INSERT INTO users_roles (transaction_id, user_id, role_id) VALUES (1, 1, 1); "
            + "INSERT INTO users_roles (transaction_id, user_id, role_id) VALUES (1, 1, 2); "
            + "ALTER TABLE transactions ADD FOREIGN KEY (user_id) REFERENCES users (id); "
            + "COMMIT; "
            + "INSERT INTO \"users\" (data, username, password, salt, \"isActivated\", "
            + "\"activationCode\", \"failCounter\", transaction_id) VALUES ('{}', 'export','x',"
            + "'9rkq0p8YcJjOyhq21hx6mHBjpClOHzni', 1, '', 0, 1);"
            // +
            // "INSERT INTO \"users\" (data, username, password, salt, \"isActivated\",
            // \"activationCode\", \"failCounter\", transaction_id) VALUES ('{}', 'dev',
            // 'fa0d1fe30d1a64dd523b2ea03ae44bd2d60dcb1d5267cc996690e708dad0581c6ddeda84ac532f191c31
            // 852db42de5128c5d73124b0ad7bb228a559e94204409', '2pF4dexiHh91HdOknT7wPTRMIeFntvUu', 1,
            // '', 0, 1); "
            // +
            // "INSERT INTO \"roles\" (data, name, transaction_id, location_id) VALUES
            // ('{\"roleType\":\"DEVELOPER\", \"isUserRole\":false}','Developer Role', 1, 1); "
            // +
            // "INSERT INTO \"users_roles\" (user_id, role_id, transaction_id) VALUES
            // (currval('users_id_seq'), currval('roles_id_seq'), 1);"
            );

    PreparedStatement stmt;
    try {
      stmt = connection.prepareStatement(builder.toString());
      stmt.executeUpdate();
      stmt.close();
    } catch (SQLException e) {
      Utils.getLogger().warn("Unable to add admin/dev/export user to database", e);
    }
  }

  /** Inserts form statuses into the database. */
  public void insertStatuses() {
    StringBuilder builder =
        new StringBuilder("BEGIN TRANSACTION; "
            + "INSERT INTO statuses (data, transaction_id) values ('{\"name\":\"open\"}', 1); "
            + "INSERT INTO statuses (data, transaction_id) values ('{\"name\":\"reported\"}', 1); "
            + "INSERT INTO statuses (data, transaction_id) values ('{\"name\":\"validated\"}', 1); "
            + "INSERT INTO \"statusChanges\" (data, \"from\", \"to\", transaction_id) values "
            + "('{\"name\":\"Report form\"}', 1, 2, 1); "
            + "INSERT INTO \"statusChanges\" (data, \"from\", \"to\", transaction_id) values "
            + "('{\"name\":\"Validate form\"}', 2, 3, 1); "
            + "INSERT INTO \"statusChanges\" (data, \"from\", \"to\", transaction_id) values "
            + "('{\"name\":\"Refuse form\"}', 2, 1, 1); "
            + "INSERT INTO \"statusChanges\" (data, \"from\", \"to\", transaction_id) values "
            + "('{\"name\":\"Break signature\"}', 3, 1, 1); "
            + "COMMIT;");

    PreparedStatement stmt;
    try {
      stmt = connection.prepareStatement(builder.toString());
      stmt.executeUpdate();
      stmt.close();
    } catch (SQLException e) {
      Utils.getLogger().warn("Unable to add form statuses to database", e);
    }
  }

  /**
   * Insert the basic OSSE configuration into the database.
   *
   * @param baseConfig the apache configuration
   * @throws DatabaseException the database exception
   */
  public void insertOsseConfig(Configuration baseConfig) throws DatabaseException {
    JSONResource config = new JSONResource();
    config.addProperty(Vocabulary.Config.project, "OSSE");
    config.addProperty(Vocabulary.Config.site, "OSSE");

    config.addProperty(Vocabulary.Config.thePackage, baseConfig.getString("instance.package"));

    config.addProperty(
        Vocabulary.Config.FormEditor.formEditorBASE,
        baseConfig.getString(Vocabulary.Config.Standard.formEditorBASE));
    config.addProperty(
        Vocabulary.Config.FormEditor.formEditorRestUrl,
        baseConfig.getString(Vocabulary.Config.Standard.formEditorRestUrl));
    config.addProperty(
        Vocabulary.Config.FormEditor.formEditorClientId,
        baseConfig.getString(Vocabulary.Config.Standard.formEditorClientId));
    config.addProperty(
        Vocabulary.Config.Auth.REST, baseConfig.getString(Vocabulary.Config.Standard.authREST));
    config.addProperty(
        Vocabulary.Config.Auth.REALM, baseConfig.getString(Vocabulary.Config.Standard.authREALM));
    config.addProperty(
        Vocabulary.Config.Auth.UseSamplyAuth,
        baseConfig.getBoolean(Vocabulary.Config.Standard.authUseSamplyAuth));
    config.addProperty(
        Vocabulary.Config.Auth.Pubkey, baseConfig.getString(Vocabulary.Config.Standard.authPubkey));
    config.addProperty(
        Vocabulary.Config.Auth.ClientId,
        baseConfig.getString(Vocabulary.Config.Standard.authClientId));
    config.addProperty(
        Vocabulary.Config.Auth.ClientSecret,
        baseConfig.getString(Vocabulary.Config.Standard.authClientSecret));
    config.addProperty(
        Vocabulary.Config.Auth.RegistryPassword,
        baseConfig.getString(Vocabulary.Config.Standard.authRegistryPassword));
    config.addProperty(
        Vocabulary.Config.Mainzelliste.noIdat,
        baseConfig.getBoolean(Vocabulary.Config.Standard.mainzellisteNoIdat));
    config.addProperty(
        Vocabulary.Config.Mainzelliste.restUrlPublic,
        baseConfig.getString(Vocabulary.Config.Standard.mainzellisteRestUrlPublic));
    config.addProperty(
            Vocabulary.Config.Mainzelliste.restUrlEdc,
            baseConfig.getString(Vocabulary.Config.Standard.mainzellisteRestUrlEdc));
    config.addProperty(
        Vocabulary.Config.Mainzelliste.RESTInternalPort,
        baseConfig.getString(Vocabulary.Config.Standard.RESTInternalPort));
    config.addProperty(
        Vocabulary.Config.Mainzelliste.apikey,
        baseConfig.getString(Vocabulary.Config.Standard.mainzellisteApikey));

    config.addProperty(
        Vocabulary.Config.Mdr.REST, baseConfig.getString(Vocabulary.Config.Standard.mdrREST));

    config.addProperty(
        Vocabulary.Config.Teiler.REST, baseConfig.getString(Vocabulary.Config.Standard.teilerREST));
    config.addProperty(
        Vocabulary.Config.Teiler.internalTeilerPort,
        baseConfig.getString(Vocabulary.Config.Standard.teilerInternalPort));
    config.addProperty(
        Vocabulary.Config.Teiler.apiKey,
        baseConfig.getString(Vocabulary.Config.Standard.teilerApiKey));
    config.addProperty(
        Vocabulary.Config.Teiler.apiKeyExport,
        baseConfig.getString(Vocabulary.Config.Standard.teilerApiKeyExport));

    if (baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPHost) != null) {
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPHost,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPHost));
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPPort,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPPort));
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPUsername,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPUsername));
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPPassword,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPPassword));
    }

    if (baseConfig.getString(Vocabulary.Config.Standard.proxyRealm) != null) {
      config.addProperty(
          Vocabulary.Config.Proxy.Realm,
          baseConfig.getString(Vocabulary.Config.Standard.proxyRealm));
    }

    if (baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPSHost) != null) {
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPSHost,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPSHost));
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPSPort,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPSPort));
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPSUsername,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPSUsername));
      config.addProperty(
          Vocabulary.Config.Proxy.HTTPSPassword,
          baseConfig.getString(Vocabulary.Config.Standard.proxyHTTPSPassword));
    }

    config.addProperty(Vocabulary.Config.Form.patient, "");
    config.addProperty(Vocabulary.Config.Form.patientStandard, "");

    config.addProperty(Vocabulary.Config.Form.visit, "");
    config.addProperty(Vocabulary.Config.Form.visitStandard, "");

    JSONResource formNames = new JSONResource();
    config.addProperty(Vocabulary.Config.formNames, formNames);

    saveOsseConfig("osse", config);
    saveOsseConfig("osse.factory.settings", config);
  }

  /**
   * Saves the OSSE configuration.
   *
   * @param name the name of the configuration
   * @param config the config data
   * @throws DatabaseException the database exception
   */
  public void saveOsseConfig(String name, JSONResource config) throws DatabaseException {
    try {
      String delete = "DELETE FROM \"configs\" WHERE \"name\" = ?";

      PreparedStatement st = connection.prepareStatement(delete);
      st.setString(1, name);

      st.execute();
      st.close();

      String sql =
          "INSERT INTO \"configs\" (\"name\", \"data\") VALUES (?, CAST(? AS "
              + DatabaseConstants.getSQLJson()
              + "));";

      st = connection.prepareStatement(sql);
      st.setString(1, name);
      st.setString(2, ResourceUtils.createJsonElement(config).toString());
      st.execute();
      st.close();

      connection.commit();

    } catch (SQLException e) {
      Utils.getLogger().error("Unable to save osse config to database", e);
      throw new DatabaseException(e);
    }
  }
}
