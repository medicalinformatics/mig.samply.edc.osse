/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

/** The Class OsseMessage. */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "",
    propOrder = {"finalMessage", "patientID", "messages"})
@XmlRootElement(name = "BHMessage")
public class OsseMessage implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -5876412399663026419L;

  /** The final message. */
  @XmlElement(name = "FinalMessage")
  private String finalMessage;

  /** The patient id. */
  @XmlElement(name = "FailedPatient")
  private String patientID;
  /** The messages. */
  @XmlElement(name = "ErrorMessages")
  private OsseMessage.Messages messages;

  /** Instantiates a new OSSE message. */
  public OsseMessage() {
    finalMessage = "";
  }

  /**
   * Gets the patient id.
   *
   * @return the patientID
   */
  public String getPatientID() {
    return patientID;
  }

  /**
   * Sets the patient id.
   *
   * @param patientID the patientID to set
   */
  public void setPatientID(String patientID) {
    this.patientID = patientID;
  }

  /**
   * Gets the messages.
   *
   * @return the messages
   */
  public OsseMessage.Messages getMessages() {
    return messages;
  }

  /**
   * Sets the messages.
   *
   * @param messages the messages to set
   */
  public void setMessages(OsseMessage.Messages messages) {
    this.messages = messages;
  }

  /**
   * Gets the final message.
   *
   * @return the finalMessage
   */
  public String getFinalMessage() {
    return finalMessage;
  }

  /**
   * Sets the final message.
   *
   * @param finalMessage the finalMessage to set
   */
  public void setFinalMessage(String finalMessage) {
    this.finalMessage = finalMessage;
  }

  /**
   * are there any messages?.
   *
   * @return the boolean
   */
  public Boolean hasErrorMessages() {
    return messages != null && !messages.message.isEmpty();
  }

  /**
   * adds a message to the list.
   *
   * @param key the key
   * @param value the value
   * @param message the message
   */
  public void addErrorMessage(String key, String value, String message) {
    if (messages == null) {
      messages = new OsseMessage.Messages();
    }

    Messages.Message temp = new Messages.Message();
    temp.key = key;
    temp.value = value;
    temp.message = message;

    messages.message.add(temp);
  }

  /** The Class Messages. */
  public static class Messages implements Serializable {

    /** The Constant serialVersionUID. */
    private static final long serialVersionUID = -1977792477326481270L;

    /** The message. */
    @XmlElement(name = "ErrorMessage")
    protected List<Message> message;

    /** Instantiates a new messages. */
    public Messages() {
      message = new LinkedList<>();
    }

    public List<Message> getMessage() {
      return message;
    }

    /** The Class Message. */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(
        name = "",
        propOrder = {"key", "value", "message"})
    public static class Message implements Serializable {

      /** The Constant serialVersionUID. */
      private static final long serialVersionUID = 8484902565595050515L;

      /** The key. */
      @XmlElement(name = "mdrKey")
      protected String key;

      /** The value. */
      @XmlElement(name = "value")
      protected String value;

      /** The message. */
      @XmlElement(name = "Message")
      protected String message;

      public String getKey() {
        return key;
      }

      public String getValue() {
        return value;
      }

      public String getMessage() {
        return message;
      }
    }
  }
}
