/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.control.SessionBean;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;
import java.util.List;

/** Model class for cases. */
public class Case extends Entity {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7621208486757821294L;

  /** The location of the case. */
  private Location location;

  /** The patient uri. */
  private String patientUri;

  /**
   * Instantiates a new case.
   *
   * @param database the database wrapper
   */
  public Case(AbstractDatabase<?> database) {
    super(database, OSSEVocabulary.Type.Case);
    setChildrenHashPrimaryKey(OSSEVocabulary.Type.Episode, OSSEVocabulary.Episode.Name);
    setChildrenHashPrimaryKey(OSSEVocabulary.Type.CaseForm, OSSEVocabulary.CaseForm.Name);
  }

  /**
   * Instantiates a new case.
   *
   * @param database the database wrapper
   * @param caseResource the resource of the case
   */
  public Case(AbstractDatabase<?> database, Resource caseResource) {
    super(database, OSSEVocabulary.Type.Case, caseResource);
    setChildrenHashPrimaryKey(OSSEVocabulary.Type.Episode, OSSEVocabulary.Episode.Name);
    setChildrenHashPrimaryKey(OSSEVocabulary.Type.CaseForm, OSSEVocabulary.CaseForm.Name);
  }

  /**
   * Instantiates a new case.
   *
   * @param database the database wrapper
   * @param caseUri the backend URI of the case
   */
  public Case(AbstractDatabase<?> database, String caseUri) {
    super(database, OSSEVocabulary.Type.Case, caseUri);
    setChildrenHashPrimaryKey(OSSEVocabulary.Type.Episode, OSSEVocabulary.Episode.Name);
    setChildrenHashPrimaryKey(OSSEVocabulary.Type.CaseForm, OSSEVocabulary.CaseForm.Name);
  }

  /**
   * Instantiates a new case.
   *
   * @param database the database wrapper
   * @param caseID the backend ID of the case
   */
  public Case(AbstractDatabase<?> database, Integer caseID) {
    super(database, OSSEVocabulary.Type.Case, caseID);

    setChildrenHashPrimaryKey(OSSEVocabulary.Type.Episode, OSSEVocabulary.Episode.Name);
    setChildrenHashPrimaryKey(OSSEVocabulary.Type.CaseForm, OSSEVocabulary.CaseForm.Name);
  }

  /** Reloads a case. */
  public void reload(JSONResource myConfig) {
    super.reloadResource();
    load(myConfig);
  }

  /** Loads the patient of the case. */
  public void loadPatient(JSONResource myConfig) {
    if (patientUri == null) {
      patientUri =
          getDatabase()
              .getResourceByIdentifier(
                  getResource().getProperty(OSSEVocabulary.Case.Patient).getValue())
              .getValue();
    }
    Patient patient = new Patient(getDatabase(), patientUri);
    patient.load(myConfig);
    patient.addCase(this);

    setParent(patient);
  }

  /** Loads the location. */
  public void loadLocation(JSONResource myConfig) {
    location =
        new Location(
            getDatabase(),
            getDatabase()
                .getResourceByIdentifier(
                    getResource().getProperty(OSSEVocabulary.Case.Location).getValue()));
    location.load(myConfig);
  }

  /**
   * Saves the case and adds the location to the case, if we don't have a location defined, we use
   * the current location of the session.
   *
   * @return the boolean
   * @see de.samply.edc.model.Entity#saveOrUpdate()
   */
  //    @Override
  public Boolean saveOrUpdate(JSONResource myConfig) {
    addProperty(
        OSSEVocabulary.Case.Patient,
        getParent(OSSEVocabulary.Type.Patient, myConfig).getResource());
    if (location == null || location.getResource() == null) {
      addProperty(OSSEVocabulary.Case.Location,
          ((SessionBean) Utils.getSB()).getCurrentLocation().getResource());
    } else {
      addProperty(OSSEVocabulary.Case.Location, location.getResource());
    }

    return super.saveOrUpdate();
  }

  /**
   * Returns the usergroup (now renamed to location) of the case.
   *
   * @return the user group
   */
  @Deprecated
  public Location getUserGroup(JSONResource myConfig) {
    Utils.whoCalledMe();
    if (location == null) {
      loadLocation(myConfig);
    }
    return location;
  }

  /**
   * Gets the location.
   *
   * @return the location
   */
  public Location getLocation(JSONResource myConfig) {
    if (location == null) {
      loadLocation(myConfig);
    }
    return location;
  }

  /**
   * Sets the location.
   *
   * @param group the new location
   */
  public void setLocation(Location group) {
    this.location = group;
  }

  /**
   * Gets the visits (now called episodes) of the case.
   *
   * @return the visits
   */
  @Deprecated
  public List<Entity> getVisits(JSONResource myConfig) {
    return getSortedChildren(OSSEVocabulary.Type.Episode, true, myConfig);
  }

  /**
   * Gets the episodes of the case.
   *
   * @return the episodes
   */
  public List<Entity> getEpisodes() {
    return getSortedChildren(OSSEVocabulary.Type.Episode, false, Utils.getSB().getConfig());
  }

  public List<Entity> getEpisodes(JSONResource myConfig) {
    return getSortedChildren(OSSEVocabulary.Type.Episode, false, myConfig);
  }

  /**
   * Gets the case forms of the case.
   *
   * @return the forms
   */
  public List<Entity> getForms(JSONResource myConfig) {
    return getSortedChildren(OSSEVocabulary.Type.CaseForm, true, myConfig);
  }

  /**
   * Searches for a given form in the case.
   *
   * @param formID the form id
   * @return the form
   */
  public Form getForm(String formID, JSONResource myConfig) {
    Entity found = getChild(OSSEVocabulary.Type.CaseForm, formID, myConfig);
    if (found != null) {
      return (Form) found;
    } else {
      return null;
    }
  }
}
