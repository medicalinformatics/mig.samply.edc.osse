/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import java.util.HashMap;
import java.util.Set;

/**
 * Class used to provide a HashMap to JSF while saving values in our entity object (form).
 *
 * @param <K> the key type
 * @param <T> the generic type
 */
public class MyMap<K, T> extends HashMap<K, T> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -4364082945182983394L;

  /** The form this map wants to save data in. */
  private Form form;

  /**
   * Instantiates a new MyMap.
   *
   * @param form the form
   */
  public MyMap(Form form) {
    this.form = form;
  }

  /**
   * Gets the.
   *
   * @param theKey the the key
   * @return the t
   * @see java.util.HashMap#get(java.lang.Object)
   */
  @SuppressWarnings("unchecked")
  @Override
  public T get(final Object theKey) {
    if (form == null) {
      return null;
    }
    return (T) form.getProperty((String) theKey);
  }

  /**
   * Put.
   *
   * @param key the key
   * @param value the value
   * @return the t
   * @see java.util.HashMap#put(java.lang.Object, java.lang.Object)
   */
  @Override
  public T put(final K key, final T value) {
    if (form != null) {
      form.setProperty((String) key, value);
    }
    return value;
  }

  /**
   * Key set.
   *
   * @return the sets the
   * @see java.util.HashMap#keySet()
   */
  @Override
  @SuppressWarnings("unchecked")
  public Set<K> keySet() {
    return (Set<K>) form.getProperties().keySet();
  }

  /**
   * Contains key.
   *
   * @param key the key
   * @return true, if successful
   * @see java.util.HashMap#containsKey(java.lang.Object)
   */
  @Override
  public boolean containsKey(final Object key) {
    return form.hasProperty((String) key);
  }

  /**
   * Gets the form.
   *
   * @return the form
   */
  public Form getForm() {
    return form;
  }

  /**
   * Sets the form.
   *
   * @param form the new form
   */
  public void setForm(Form form) {
    this.form = form;
  }
}
