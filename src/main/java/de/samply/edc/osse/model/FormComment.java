/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.TimestampLiteral;
import java.io.Serializable;

/** The Class FormComment. */
public class FormComment implements Serializable, Comparable<FormComment> {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5585028909855450228L;

  /** The comment. */
  private String comment;

  /** The user uri. */
  private String userUri;

  /** The user. */
  private User theUser;

  /** The timestamp. */
  private long timestamp;

  /**
   * Instantiates a new form comment.
   *
   * @param comment the comment
   * @param userUri the user uri
   * @param timestamp the timestamp
   */
  public FormComment(String comment, String userUri, long timestamp) {
    this.comment = comment;
    this.userUri = userUri;
    this.timestamp = timestamp;
  }

  /**
   * Instantiates a new form comment.
   *
   * @param data the data
   */
  public FormComment(JSONResource data) {
    readJsonResource(data);
  }

  /**
   * Gets the comment.
   *
   * @return the comment
   */
  public String getComment() {
    return comment;
  }

  /**
   * Sets the comment.
   *
   * @param comment the new comment
   */
  public void setComment(String comment) {
    this.comment = comment;
  }

  /**
   * Gets the user real name.
   *
   * @return the user real name
   */
  public String getUserRealName(JSONResource myConfig) {

    // If a comment was added externally, there might not be a user uri. Return "system" in that
    // case.
    if (userUri == null || userUri.length() < 1) {
      return "system";
    }

    if (theUser == null) {
      theUser = new User(Utils.getDatabase(), userUri);
      theUser.load(false, false, myConfig);
    }

    return theUser.getUserRealName();
  }

  /**
   * Get the full name (first and last name) of the user who wrote the comment.
   *
   * @return The comment author's name.
   */
  public String getUserRealName() {
    return getUserRealName(Utils.getSB().getConfig());
  }

  /**
   * Gets the timestamp.
   *
   * @return the timestamp
   */
  public long getTimestamp() {
    return timestamp;
  }

  /**
   * Sets the timestamp.
   *
   * @param timestamp the new timestamp
   */
  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  /**
   * Read json resource.
   *
   * @param data the data
   */
  public void readJsonResource(JSONResource data) {
    if (data == null) {
      return;
    }

    this.comment = data.getProperty(Vocabulary.Comment.commentText).getValue();
    this.timestamp = data.getProperty(Vocabulary.Comment.commentTimestamp).asLong();
    try {
      this.userUri = data.getProperty(Vocabulary.Comment.commentUserURI).getValue();
    } catch (NullPointerException npe) {
      this.userUri = null;
      Utils.getLogger().debug("Property not found.", npe);
    }
  }

  /**
   * To json resource.
   *
   * @return the JSON resource
   */
  public JSONResource toJsonResource() {
    JSONResource commentResource = new JSONResource();
    commentResource.addProperty(
        Vocabulary.Comment.commentTimestamp, new TimestampLiteral(timestamp));
    commentResource.addProperty(Vocabulary.Comment.commentUserURI, userUri);
    commentResource.addProperty(Vocabulary.Comment.commentText, comment);

    return commentResource;
  }

  /* (non-Javadoc)
   * @see java.lang.Comparable#compareTo(java.lang.Object)
   */
  @Override
  public int compareTo(FormComment other) {
    return Long.compare(getTimestamp(), other.getTimestamp());
  }
}
