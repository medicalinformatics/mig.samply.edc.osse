/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import java.io.Serializable;

/**
 * Defines an episode pattern ( label , pattern , regexp , isDatePattern) ({ "ISO date",
 * "yyyy-MM-dd", "(\\d{4})-(\\d{2})-(\\d{2})", true }).
 */
public class EpisodePattern implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 8935923677843549723L;

  /** The name of this pattern. */
  private String name;

  /** The label explains the pattern to a human. */
  private String label;

  /** This pattern is used in Java Date<>String conversions. */
  private String pattern;

  /** This pattern is used in jQuery datepickers. */
  private String patternJQuery;

  /** The regexp is used in the validator. */
  private String regexp;

  /**
   * Defines if a pattern is a clear date which can be converted to a Java Date directly, or not.
   */
  private Boolean isDatePattern;

  /**
   * Instantiates a new episode pattern.
   *
   * @param data the data
   */
  public EpisodePattern(Object[] data) {
    this.name = (String) data[0];
    this.label = (String) data[1];
    this.pattern = (String) data[2];
    this.patternJQuery = (String) data[3];
    this.regexp = (String) data[4];
    this.isDatePattern = (Boolean) data[5];
  }

  /**
   * Instantiates a new episode pattern.
   *
   * @param name the name
   * @param label the label
   * @param pattern the pattern
   * @param patternJQuery the pattern j query
   * @param regexp the regexp
   * @param isDatePattern the is date pattern
   */
  public EpisodePattern(
      String name,
      String label,
      String pattern,
      String patternJQuery,
      String regexp,
      Boolean isDatePattern) {
    this.name = name;
    this.label = label;
    this.pattern = pattern;
    this.patternJQuery = patternJQuery;
    this.regexp = regexp;
    this.isDatePattern = isDatePattern;
  }

  /**
   * Gets the label.
   *
   * @return the label
   */
  public String getLabel() {
    return label;
  }

  /**
   * Sets the label.
   *
   * @param label the label to set
   */
  public void setLabel(String label) {
    this.label = label;
  }

  /**
   * Gets the pattern.
   *
   * @return the pattern
   */
  public String getPattern() {
    return pattern;
  }

  /**
   * Sets the pattern.
   *
   * @param pattern the pattern to set
   */
  public void setPattern(String pattern) {
    this.pattern = pattern;
  }

  /**
   * Gets the regexp.
   *
   * @return the regexp
   */
  public String getRegexp() {
    return regexp;
  }

  /**
   * Sets the regexp.
   *
   * @param regexp the regexp to set
   */
  public void setRegexp(String regexp) {
    this.regexp = regexp;
  }

  /**
   * Gets the checks if is date pattern.
   *
   * @return the isDatePattern
   */
  public Boolean getIsDatePattern() {
    return isDatePattern;
  }

  /**
   * Sets the checks if is date pattern.
   *
   * @param isDatePattern the isDatePattern to set
   */
  public void setIsDatePattern(Boolean isDatePattern) {
    this.isDatePattern = isDatePattern;
  }

  /**
   * Gets the pattern j query.
   *
   * @return the patternJQuery
   */
  public String getPatternJQuery() {
    return patternJQuery;
  }

  /**
   * Sets the pattern j query.
   *
   * @param patternJQuery the patternJQuery to set
   */
  public void setPatternJQuery(String patternJQuery) {
    this.patternJQuery = patternJQuery;
  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the name to set
   */
  public void setName(String name) {
    this.name = name;
  }

  /**
   * Checks if one episode pattern is equal to the other based on their names.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (getClass() != obj.getClass()) {
      return false;
    }

    if (getName() == null) {
      return ((EpisodePattern) obj).getName() == null;
    } else {
      return getName().equals(((EpisodePattern) obj).getName());
    }
  }

  /**
   * Calculates hashCode based on name.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getName() == null) ? 0 : getName().hashCode());
    return result;
  }
}
