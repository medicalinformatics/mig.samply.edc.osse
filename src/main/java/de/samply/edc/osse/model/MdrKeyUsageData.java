/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.io.Serializable;
import java.util.HashMap;

/**
 * Data as to how an MDRKey is used in the registry as a field The data stored for a mdrKey are: -
 * in which form does it occur - what is the type of the form (case, episode, patient) - what is the
 * type of the field - is it mandatory.
 */
public class MdrKeyUsageData implements Serializable {

  /** constants. */
  public static final String FORMNAME = "formName";
  /** The Constant FORMTYPE. */
  public static final String FORMTYPE = "formType";
  /** The Constant FIELDTYPE. */
  public static final String FIELDTYPE = "fieldType";
  /** The Constant MANDATORY. */
  public static final String MANDATORY = "mandatory";
  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 2951632383155957069L;
  /** the data keeper. */
  private HashMap<String, Object> mdrKeyUsageData;

  /** Instantiates a new mdr key usage data. */
  public MdrKeyUsageData() {
    mdrKeyUsageData = new HashMap<>();
  }

  /**
   * Instantiates a new mdr key usage data.
   *
   * @param formName the form name
   * @param formType the form type
   * @param fieldType the field type
   * @param isMandatory the is mandatory
   */
  public MdrKeyUsageData(
      String formName, FormType formType, FieldType fieldType, Boolean isMandatory) {
    mdrKeyUsageData = new HashMap<>();
    set(formName, formType, fieldType, isMandatory);
  }

  /**
   * Sets the.
   *
   * @param formName the form name
   * @param formType the form type
   * @param fieldType the field type
   * @param isMandatory the is mandatory
   */
  public void set(String formName, FormType formType, FieldType fieldType, Boolean isMandatory) {
    mdrKeyUsageData.put(FORMNAME, formName);
    mdrKeyUsageData.put(FORMTYPE, formType);
    mdrKeyUsageData.put(FIELDTYPE, fieldType);
    mdrKeyUsageData.put(MANDATORY, isMandatory);
  }

  /**
   * gets the form name.
   *
   * @return the form name
   */
  public String getFormName() {
    return (String) mdrKeyUsageData.get(FORMNAME);
  }

  /**
   * gets the field's type.
   *
   * @return the field type
   */
  public FieldType getFieldType() {
    return (FieldType) mdrKeyUsageData.get(FIELDTYPE);
  }

  /**
   * As a MDRKey may only be used once per form, the formname becomes the unique property of this
   * item.
   *
   * @return the int
   */
  @Override
  public int hashCode() {
    final int prime = 31;
    int result = 1;
    result = prime * result + ((getFormName() == null) ? 0 : getFormName().hashCode());
    return result;
  }

  /**
   * As a MDRKey may only be used once per form, the formname becomes the unique property of this
   * item.
   *
   * @param obj the obj
   * @return true, if successful
   */
  @Override
  public boolean equals(Object obj) {
    if (this == obj) {
      return true;
    }
    if (obj == null) {
      return false;
    }
    if (!(obj instanceof MdrKeyUsageData)) {
      return false;
    }
    MdrKeyUsageData other = (MdrKeyUsageData) obj;
    if (getFormName() == null) {
      return other.getFormName() == null;
    } else {
      return getFormName().equals(other.getFormName());
    }
  }

  /**
   * Fill this usage data from a JSONResource (used in load).
   *
   * @param data the data
   * @return the boolean
   */
  public Boolean inputAsJsonResource(JSONResource data) {
    try {
      String formName = data.getProperty(FORMNAME).getValue();
      FormType formType = FormType.valueOf(data.getProperty(FORMTYPE).getValue());
      FieldType fieldType = FieldType.valueOf(data.getProperty(FIELDTYPE).getValue());
      Boolean isMandatory = data.getProperty(MANDATORY).asBoolean();

      set(formName, formType, fieldType, isMandatory);
      return true;
    } catch (NullPointerException e) {
      // old input values, need to remake them!
      Utils.getLogger().debug("Was trying to save: " + data);
      e.printStackTrace();
      return false;
    }
  }

  /**
   * JSONResource representation of this usage data (used in save).
   *
   * @return the JSON resource
   */
  public JSONResource asJsonResource() {
    JSONResource data = new JSONResource();
    data.addProperty(FORMNAME, (String) mdrKeyUsageData.get(FORMNAME));
    data.addProperty(FORMTYPE, ((FormType) mdrKeyUsageData.get(FORMTYPE)).name());
    data.addProperty(FIELDTYPE, ((FieldType) mdrKeyUsageData.get(FIELDTYPE)).name());
    data.addProperty(MANDATORY, (Boolean) mdrKeyUsageData.get(MANDATORY));

    return data;
  }

  /**
   * String representation.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder sb = new StringBuilder();
    sb.append((String) mdrKeyUsageData.get(FORMNAME)).append(", ");
    sb.append(((FormType) mdrKeyUsageData.get(FORMTYPE)).name()).append(", ");
    sb.append(((FieldType) mdrKeyUsageData.get(FIELDTYPE)).name()).append(", ");
    sb.append((Boolean) mdrKeyUsageData.get(MANDATORY));
    return sb.toString();
  }

  /** Type of form (episode, case, patient). */
  public enum FormType {

    /** The episode. */
    EPISODE,
    /** The case. */
    CASE,
    /** The patient. */
    PATIENT
  }

  /** Type of the field. */
  public enum FieldType {

    /** The field. */
    FIELD,
    /** The repeatablefield. */
    REPEATABLEFIELD,
    /** The record. */
    RECORD,
    /** The repeatablerecord. */
    REPEATABLERECORD
  }
}
