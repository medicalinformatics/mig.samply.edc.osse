/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;

/** Model class for UserContact data. */
public class UserContact extends Entity {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 6933865874658320922L;

  /** The user this belongs to. */
  private User user;

  /**
   * Instantiates a new user contact.
   *
   * @param database the database
   */
  public UserContact(AbstractDatabase<?> database) {
    super(database, OSSEVocabulary.Type.UserContact);
  }

  /**
   * Instantiates a new user contact.
   *
   * @param database the database
   * @param contactResource the contact resource
   */
  public UserContact(AbstractDatabase<?> database, Resource contactResource) {
    super(database, OSSEVocabulary.Type.UserContact, contactResource);
  }

  /**
   * Instantiates a new user contact.
   *
   * @param database the database
   * @param contactUri the contact uri
   */
  public UserContact(AbstractDatabase<?> database, String contactUri) {
    super(database, OSSEVocabulary.Type.UserContact, contactUri);
  }

  /**
   * Gets the user this contact belongs to.
   *
   * @return the user
   */
  public User getUser() {
    return user;
  }

  /**
   * Sets the user this contact belongs to.
   *
   * @param user the new user
   */
  public void setUser(User user) {
    setParent(user);
    setProperty(OSSEVocabulary.UserContact.User, user.getResource());
    this.user = user;
  }

  /**
   * Gets the firstname.
   *
   * @return the firstname
   */
  public String getFirstname() {
    if (getProperty(Vocabulary.Attributes.contactFirstName) != null) {
      return (String) getProperty(Vocabulary.Attributes.contactFirstName);
    }
    return "Unknown";
  }

  /**
   * Gets the lastname.
   *
   * @return the lastname
   */
  public String getLastname() {
    if (getProperty(Vocabulary.Attributes.contactLastName) != null) {
      return (String) getProperty(Vocabulary.Attributes.contactLastName);
    }
    return "Unknown";
  }

  /**
   * Gets the email.
   *
   * @return the email
   */
  public String getEmail() {
    if (getProperty(Vocabulary.Attributes.contactEMail) != null) {
      return (String) getProperty(Vocabulary.Attributes.contactEMail);
    }
    return "Unknown";
  }

  /**
   * Gets the title.
   *
   * @return the title
   */
  public String getTitle() {
    if (getProperty(Vocabulary.Attributes.contactTitle) != null) {
      return (String) getProperty(Vocabulary.Attributes.contactTitle);
    }
    return "";
  }
}
