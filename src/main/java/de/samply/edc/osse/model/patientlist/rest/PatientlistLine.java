/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model.patientlist.rest;

import java.io.Serializable;

public class PatientlistLine implements Serializable {

  private int patientId;
  private String resourceUri;
  private String psn;
  private String location;
  private boolean isImported;
  private boolean showIdat;
  private boolean mayWrite;
  private boolean mayDeleteCase;

  public int getPatientId() {
    return patientId;
  }

  public void setPatientId(int patientId) {
    this.patientId = patientId;
  }

  public String getResourceUri() {
    return resourceUri;
  }

  public void setResourceUri(String resourceUri) {
    this.resourceUri = resourceUri;
  }

  public String getPsn() {
    return psn;
  }

  public void setPsn(String psn) {
    this.psn = psn;
  }

  public String getLocation() {
    return location;
  }

  public void setLocation(String location) {
    this.location = location;
  }

  public boolean isImported() {
    return isImported;
  }

  public void setImported(boolean imported) {
    isImported = imported;
  }

  public boolean isShowIdat() {
    return showIdat;
  }

  public void setShowIdat(boolean showIdat) {
    this.showIdat = showIdat;
  }

  public boolean isMayWrite() {
    return mayWrite;
  }

  public void setMayWrite(boolean mayWrite) {
    this.mayWrite = mayWrite;
  }

  public boolean isMayDeleteCase() {
    return mayDeleteCase;
  }

  public void setMayDeleteCase(boolean mayDeleteCase) {
    this.mayDeleteCase = mayDeleteCase;
  }
}
