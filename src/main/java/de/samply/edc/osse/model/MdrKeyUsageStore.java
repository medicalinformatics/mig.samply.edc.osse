/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.osse.model.MdrKeyUsageData.FieldType;
import de.samply.edc.osse.model.MdrKeyUsageData.FormType;
import de.samply.store.JSONResource;
import de.samply.store.Value;
import java.io.Serializable;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map.Entry;
import java.util.Set;

/**
 * Contains the configuration of data fields in this registry Each field is represented by its
 * mdrKey
 *
 * <p>The data stored for each mdrKey are: - a set of forms they occur in - what is the type of the
 * form (case, episode, patient) - what is the type of the field - is it mandatory.
 */
public class MdrKeyUsageStore implements Serializable {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = -2866369612631966681L;

  /** The store. */
  private HashMap<String, Set<MdrKeyUsageData>> mdrKeyUsageStore;

  /** Instantiates a new mdr key usage store. */
  public MdrKeyUsageStore() {
    mdrKeyUsageStore = new HashMap<>();
  }

  /**
   * Gets the mdr key usage store.
   *
   * @return the mdrKeyUsageStore
   */
  public HashMap<String, Set<MdrKeyUsageData>> getMdrKeyUsageStore() {
    return mdrKeyUsageStore;
  }

  /**
   * Adds a field If it already exists (means there is another entry for the formname) the entry
   * will be replaced by the new one.
   *
   * @param mdrKey the mdrKey used
   * @param formName in which form was it used
   * @param formType MdrKeyUsageData.FormType
   * @param fieldType MdrKeyUsageData.FieldType
   * @param isMandatory mandatory field?
   * @return 0:error, 1:added, -1:already existed
   */
  public int addField(
      String mdrKey,
      String formName,
      MdrKeyUsageData.FormType formType,
      MdrKeyUsageData.FieldType fieldType,
      Boolean isMandatory) {
    if (mdrKey == null || formName == null) {
      return 0;
    }

    MdrKeyUsageData mdrKeyUsageData =
        new MdrKeyUsageData(formName, formType, fieldType, isMandatory);

    return addMdrKeyUsageData(mdrKey, mdrKeyUsageData);
  }

  /**
   * Adds a field If it already exists (means there is another entry for the formname) the entry
   * will be replaced by the new one.
   *
   * @param mdrKey the mdrKey used
   * @param mdrKeyUsageData the usage data
   * @return 0:error, 1:added, -1:already existed
   */
  public int addMdrKeyUsageData(String mdrKey, MdrKeyUsageData mdrKeyUsageData) {
    if (mdrKey == null || mdrKeyUsageData.getFormName() == null) {
      return 0;
    }

    Set<MdrKeyUsageData> setOfUsages;

    if (!mdrKeyUsageStore.containsKey(mdrKey)) {
      setOfUsages = new HashSet<>();
      setOfUsages.add(mdrKeyUsageData);
      mdrKeyUsageStore.put(mdrKey, setOfUsages);
      return 1;
    }

    setOfUsages = mdrKeyUsageStore.get(mdrKey);

    if (!setOfUsages.add(mdrKeyUsageData)) {
      return -1;
    } else {
      mdrKeyUsageStore.put(mdrKey, setOfUsages);
      return 1;
    }
  }

  /**
   * checks if a mdrkey is used in a form.
   *
   * @param mdrKey the mdr key
   * @param formName the form name
   * @return the boolean
   */
  public Boolean containsField(String mdrKey, String formName) {
    if (!mdrKeyUsageStore.containsKey(mdrKey)) {
      return false;
    }

    MdrKeyUsageData mdrKeyUsageData = new MdrKeyUsageData(formName, null, null, null);
    return mdrKeyUsageStore.get(mdrKey).contains(mdrKeyUsageData);
  }

  /**
   * removes a mdrkey from a form.
   *
   * @param mdrKey the mdr key
   * @param formName the form name
   * @return the boolean
   */
  public Boolean removeField(String mdrKey, String formName) {
    if (!mdrKeyUsageStore.containsKey(mdrKey)) {
      return false;
    }

    MdrKeyUsageData mdrKeyUsageData = new MdrKeyUsageData(formName, null, null, null);
    return mdrKeyUsageStore.get(mdrKey).remove(mdrKeyUsageData);
  }

  /**
   * returns this usage store as JSONResource (used in save).
   *
   * @return the JSON resource
   */
  public JSONResource asJsonResource() {
    JSONResource data = new JSONResource();

    for (Entry<String, Set<MdrKeyUsageData>> entry : mdrKeyUsageStore.entrySet()) {
      for (MdrKeyUsageData field : entry.getValue()) {
        data.addProperty(entry.getKey(), field.asJsonResource());
      }
    }
    return data;
  }

  /**
   * fills this usage store from a JSONResource (used in load).
   *
   * @param data the data
   * @return the boolean
   */
  public Boolean inputAsJsonResource(JSONResource data) {
    mdrKeyUsageStore.clear();

    for (String mdrKey : data.getDefinedProperties()) {
      HashSet<MdrKeyUsageData> usageData = new HashSet<>();

      for (Value value : data.getProperties(mdrKey)) {
        MdrKeyUsageData mud = new MdrKeyUsageData();

        if (value.asJSONResource() != null) {
          mud.inputAsJsonResource(value.asJSONResource());
        } else {
          // old values transformed, just in case

          JSONResource temp = new JSONResource();
          temp.addProperty(MdrKeyUsageData.FORMNAME, value.getValue());
          temp.addProperty(MdrKeyUsageData.FORMTYPE, FormType.CASE.name());
          temp.addProperty(MdrKeyUsageData.FIELDTYPE, FieldType.FIELD.name());
          temp.addProperty(MdrKeyUsageData.MANDATORY, false);

          mud.inputAsJsonResource(temp);
        }
        usageData.add(mud);
      }

      mdrKeyUsageStore.put(mdrKey, usageData);
    }

    return true;
  }

  /**
   * Get a string representation.
   *
   * @return the string
   */
  @Override
  public String toString() {
    StringBuilder builder = new StringBuilder();

    for (Entry<String, Set<MdrKeyUsageData>> entry : mdrKeyUsageStore.entrySet()) {
      builder.append(entry.getKey()).append("\n");

      for (MdrKeyUsageData field : entry.getValue()) {
        builder.append("-> ").append(field.toString()).append("\n");
      }
    }

    return builder.toString();
  }

  /** clear this store. */
  public void clear() {
    mdrKeyUsageStore.clear();
  }

  /**
   * Merges a given MdrKeyUsageStore into this one.
   *
   * @param otherStore The MdrKeyUsageStore to merge into this one
   */
  public void merge(MdrKeyUsageStore otherStore) {
    for (Entry<String, Set<MdrKeyUsageData>> entry : otherStore.getMdrKeyUsageStore().entrySet()) {
      if (!mdrKeyUsageStore.containsKey(entry.getKey())) {
        mdrKeyUsageStore.put(entry.getKey(), entry.getValue());
      } else {
        for (MdrKeyUsageData mkud : entry.getValue()) {
          addMdrKeyUsageData(entry.getKey(), mkud);
        }
      }
    }
  }

  /**
   * Gets the usage data of a mdrkey.
   *
   * @param mdrKey the mdr key
   * @return the usage of mdr key
   */
  public Set<MdrKeyUsageData> getUsageOfMdrKey(String mdrKey) {
    if (mdrKey == null) {
      return null;
    }

    return mdrKeyUsageStore.get(mdrKey);
  }
}
