/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.StringLiteral;
import de.samply.store.Value;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.ResourceQuery;
import java.util.List;

/** Model class for backend permissions. */
public class Permission extends Entity {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 5182152237626156935L;

  /** The location of this permission. */
  private Location location;

  /**
   * Definitions if this permission affects rights on patients, cases, episodes, and/or forms,
   * users.
   */
  private Boolean entityPatient;
  private Boolean entityCase;
  private Boolean entityEpisode;
  private Boolean entityForm;
  private Boolean entityUser;

  /** The access type of this permission (read, write, ...) */
  private String access;

  /** Defines if IDAT for a patient may be displayed. */
  private Boolean showIdat;

  /** The status id this permission is defined for. */
  private Integer statusID;

  /** Name property of the permission. */
  private String name;

  /**
   * Instantiates a new permission object.
   *
   * @param database the database
   */
  public Permission(AbstractDatabase<?> database) {
    super(database, OSSEVocabulary.Type.Permission);
  }

  /**
   * Constructs Patient based on Resource.
   *
   * @param database the database
   * @param permissionResource the permission resource
   */
  public Permission(AbstractDatabase<?> database, Resource permissionResource) {
    super(database, OSSEVocabulary.Type.Permission, permissionResource);
  }

  /**
   * Instantiates a new permission object.
   *
   * @param database the database
   * @param permissionUri the permission uri
   */
  public Permission(AbstractDatabase<?> database, String permissionUri) {
    super(database, OSSEVocabulary.Type.Permission, permissionUri);
  }

  /**
   * Loads the permission data.
   *
   * @return the boolean
   * @see de.samply.edc.model.Entity#load()
   */
  @Override
  public Boolean load(JSONResource myConfig) {
    showIdat = true;

    if (getResource().getProperty("custom") != null) {
      JSONResource customPermissionData = getResource().getProperty("custom").asJSONResource();
      if (customPermissionData != null) {
        showIdat = customPermissionData.getProperty("showidat").asBoolean();
        if (showIdat == null) {
          showIdat = true;
        }
      }
    }

    List<Value> resources = getResource().getProperties(OSSEOntology.Permission.Resources);
    entityUser = resources.contains(new StringLiteral("user"));
    entityPatient = resources.contains(new StringLiteral("patient"));
    entityCase = resources.contains(new StringLiteral("case"));
    entityEpisode = resources.contains(new StringLiteral("episode"));

    // Yet we don't make a difference between case forms and episode forms
    entityForm =
        resources.contains(new StringLiteral("caseForm"))
            || resources.contains(new StringLiteral("episodeForm"));

    access = getResource().getProperty(OSSEOntology.Permission.Access).getValue();

    name = getResource().getString("name");

    List<Value> criteria = getResource().getProperties(OSSEOntology.Permission.Criteria);
    for (Value temp : criteria) {
      JSONResource tempJson = temp.asJSONResource();
      if (tempJson.getProperty(OSSEOntology.CriteriaEntry.Resource).getValue().equals("location")
          && tempJson.getProperty(OSSEOntology.CriteriaEntry.Type).getValue().equals("equal")) {
        Integer locationID = tempJson.getProperty(OSSEOntology.CriteriaEntry.Value).asInteger();

        ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Location);
        query.setFetchAdjacentResources(false);
        query.addEqual(OSSEVocabulary.ID, locationID);

        location = new Location(getDatabase(), getDatabase().getResources(query).get(0));
        location.load(myConfig);
      } else if (tempJson
              .getProperty(OSSEOntology.CriteriaEntry.Resource)
              .getValue()
              .equals("status")
          && tempJson.getProperty(OSSEOntology.CriteriaEntry.Type).getValue().equals("equal")) {
        statusID = tempJson.getProperty(OSSEOntology.CriteriaEntry.Value).asInteger();
      } else if (tempJson.getProperty(OSSEOntology.CriteriaEntry.Resource).getValue().equals("case")
          && tempJson.getProperty(OSSEOntology.CriteriaEntry.Type).getValue().equals("equal")) {

        Integer caseID = tempJson.getProperty(OSSEOntology.CriteriaEntry.Value).asInteger();
        Case theCase = new Case(getDatabase(), caseID);
        theCase.load(myConfig);
        location = theCase.getLocation(myConfig);
      }
    }

    return true;
  }

  /**
   * Saves the permission data.
   *
   * @return the boolean
   * @see de.samply.edc.model.Entity#saveOrUpdate()
   */
  //    @Override
  public Boolean saveOrUpdate(JSONResource myConfig) {

    if (location != null) {
      Integer locationID = location.getResource().getId();
      JSONResource locationResource = new JSONResource();
      locationResource.setProperty(OSSEOntology.CriteriaEntry.Type, "equal");
      locationResource.setProperty(OSSEOntology.CriteriaEntry.Resource, "location");
      locationResource.setProperty(OSSEOntology.CriteriaEntry.Property, "id");
      locationResource.setProperty(OSSEOntology.CriteriaEntry.Value, locationID);
      setProperty(OSSEOntology.Permission.Criteria, locationResource);
    }

    if (showIdat != null) {
      JSONResource tempDataJson = new JSONResource();
      tempDataJson.setProperty("showidat", showIdat);
      setProperty("custom", tempDataJson);
    }

    // If we save a statusID, then there are several things fixed to be set
    // TODO: This changed in the backend. Now actions are saved, not the
    // goal-status
    if (statusID != null) {
      JSONResource statusResource = new JSONResource();
      statusResource.setProperty(OSSEOntology.CriteriaEntry.Type, "equal");
      statusResource.setProperty(OSSEOntology.CriteriaEntry.Resource, "status");
      statusResource.setProperty(OSSEOntology.CriteriaEntry.Property, "id");
      statusResource.setProperty(OSSEOntology.CriteriaEntry.Value, statusID);
      addProperty(OSSEOntology.Permission.Criteria, statusResource);

      setProperty(OSSEOntology.Permission.Resources, "caseForm");
      addProperty(OSSEOntology.Permission.Resources, "episodeForm");
    } else {
      if (entityPatient != null && entityPatient) {
        setProperty(OSSEOntology.Permission.Resources, "patient");
      }
      if (entityCase != null && entityCase) {
        addProperty(OSSEOntology.Permission.Resources, "case");
      }
      if (entityEpisode != null && entityEpisode) {
        addProperty(OSSEOntology.Permission.Resources, "episode");
      }
      if (entityForm != null && entityForm) {
        addProperty(OSSEOntology.Permission.Resources, "caseForm");
        addProperty(OSSEOntology.Permission.Resources, "episodeForm");
      }
      if (entityUser != null && entityUser) {
        addProperty(OSSEOntology.Permission.Resources, "user");
      }
    }

    // TODO: change to OSSEOntology.Permission.Name once that is in the
    // backend
    setProperty("name", name);

    setProperty(OSSEOntology.Permission.Access, access);
    setProperty(OSSEVocabulary.Permission.Role, getParent(myConfig).getResource());
    return super.saveOrUpdate();
  }

  /**
   * Checks if one permission is equal to another.
   *
   * @param otherObject the other object
   * @return true, if successful
   * @see de.samply.edc.model.Entity#equals(java.lang.Object)
   */
  @Override
  public boolean equals(Object otherObject) {
    if (super.equals(otherObject)) {
      return true;
    }

    Permission otherPermission = (Permission) otherObject;

    if (otherPermission.getLocation() == null && this.getLocation() == null) {
      if (this.getEntityPatient() == otherPermission.getEntityPatient()
          && this.getEntityCase() == otherPermission.getEntityCase()
          && this.getEntityEpisode() == otherPermission.getEntityEpisode()
          && this.getEntityForm() == otherPermission.getEntityForm()
          && this.getAccess().equalsIgnoreCase(otherPermission.getAccess())
          && this.getStatusID().equals(otherPermission.getStatusID())) {
        return true;
      }
    }

    if (otherPermission.getLocation() == null || this.getLocation() == null) {
      return false;
    }

    if (this.getLocation().getName().equalsIgnoreCase(otherPermission.getLocation().getName())
        && this.getEntityPatient() == otherPermission.getEntityPatient()
        && this.getEntityCase() == otherPermission.getEntityCase()
        && this.getEntityEpisode() == otherPermission.getEntityEpisode()
        && this.getEntityForm() == otherPermission.getEntityForm()
        && this.getAccess().equalsIgnoreCase(otherPermission.getAccess())) {

      if (this.getStatusID() != null) {
        if (otherPermission.getStatusID() == null) {
          return false;
        }

        return this.getStatusID().equals(otherPermission.getStatusID());
      } else {
        return otherPermission.getStatusID() == null;
      }
    }

    return false;
  }

  /**
   * Gets the location.
   *
   * @return the location
   */
  public Location getLocation() {
    return location;
  }

  /**
   * Sets the location.
   *
   * @param location the new location
   */
  public void setLocation(Location location) {
    this.location = location;
  }

  /**
   * Gets the entity patient.
   *
   * @return the entity patient
   */
  public Boolean getEntityPatient() {
    return entityPatient;
  }

  /**
   * Sets the entity patient.
   *
   * @param entityPatient the new entity patient
   */
  public void setEntityPatient(Boolean entityPatient) {
    this.entityPatient = entityPatient;
  }

  /**
   * Gets the entity case.
   *
   * @return the entity case
   */
  public Boolean getEntityCase() {
    return entityCase;
  }

  /**
   * Sets the entity case.
   *
   * @param entityCase the new entity case
   */
  public void setEntityCase(Boolean entityCase) {
    this.entityCase = entityCase;
  }

  /**
   * Gets the entity episode.
   *
   * @return the entity episode
   */
  public Boolean getEntityEpisode() {
    return entityEpisode;
  }

  /**
   * Sets the entity episode.
   *
   * @param entityEpisode the new entity episode
   */
  public void setEntityEpisode(Boolean entityEpisode) {
    this.entityEpisode = entityEpisode;
  }

  /**
   * Gets the entity form.
   *
   * @return the entity form
   */
  public Boolean getEntityForm() {
    return entityForm;
  }

  /**
   * Sets the entity form.
   *
   * @param entityForm the new entity form
   */
  public void setEntityForm(Boolean entityForm) {
    this.entityForm = entityForm;
  }

  /**
   * Gets the access.
   *
   * @return the access
   */
  public String getAccess() {
    return access;
  }

  /**
   * Sets the access.
   *
   * @param access the new access
   */
  public void setAccess(String access) {
    this.access = access;
  }

  /**
   * Gets the status id.
   *
   * @return the status id
   */
  public Integer getStatusID() {
    return statusID;
  }

  /**
   * Sets the status id.
   *
   * @param statusID the new status id
   */
  public void setStatusID(Integer statusID) {
    this.statusID = statusID;
  }

  /**
   * Gets the show idat.
   *
   * @return the show idat
   */
  public Boolean getShowIdat() {
    return showIdat;
  }

  /**
   * Sets the show idat.
   *
   * @param showIdat the new show idat
   */
  public void setShowIdat(Boolean showIdat) {
    this.showIdat = showIdat;
  }

  /**
   * Gets the entity user.
   *
   * @return the entity user
   */
  public Boolean getEntityUser() {
    return entityUser;
  }

  /**
   * Sets the entity user.
   *
   * @param entityUser the new entity user
   */
  public void setEntityUser(Boolean entityUser) {
    this.entityUser = entityUser;
  }

  /**
   * Gets the name.
   *
   * @return the name
   */
  public String getName() {
    return name;
  }

  /**
   * Sets the name.
   *
   * @param name the new name
   */
  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    String text =
        "booleans: "
            + entityPatient
            + ", "
            + entityCase
            + ", "
            + entityEpisode
            + ", "
            + entityForm
            + ", "
            + entityUser;
    text += "\n Location: " + location.getName();
    text += " Access: " + access;
    text += " showIdat: " + showIdat;
    text += " statusID: " + statusID;
    text += " name: " + name;

    return text;
  }
}
