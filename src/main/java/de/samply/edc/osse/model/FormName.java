/*
 * Copyright (C) 2022 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/** Utility class for form names with easy access to id, version and string representation. */
public class FormName {

  private final int id;
  private final int version;

  /**
   * Create an immutable instance.
   *
   * @param id Form id
   * @param version Form version
   */
  public FormName(int id, int version) {
    this.id = id;
    this.version = version;
  }

  /**
   * Create an instance from a complete form name (form_{id}_ver-{version}).
   *
   * @param formName The complete form name.
   * @throws IllegalArgumentException if the argument is not a valid form name.
   */
  public FormName(String formName) {
    Pattern formIdPattern = Pattern.compile("form_(\\d+)_ver-(\\d+)");
    Matcher formIdMatcher = formIdPattern.matcher(formName);
    if (!formIdMatcher.find()) {
      throw new IllegalArgumentException(formName + " is not a valid form name!");
    }
    id = Integer.parseInt(formIdMatcher.group(1));
    version = Integer.parseInt(formIdMatcher.group(2));
  }

  /**
   * Get the form id.
   *
   * @return The form id
   */
  public int getId() {
    return id;
  }

  /**
   * Get the form version.
   *
   * @return The form version
   */
  public int getVersion() {
    return version;
  }

  /**
   * Get the complete form name (form_{id}_ver-{version}).
   *
   * @return The complete form name
   */
  public String toString() {
    return String.format("form_%d_ver-%d", id, version);
  }
}
