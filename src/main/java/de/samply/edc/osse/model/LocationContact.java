/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEVocabulary;

/** Model class for location contact data. */
public class LocationContact extends Entity {

  /** The Constant serialVersionUID. */
  private static final long serialVersionUID = 7662900532203471873L;

  /** The location of this contact. */
  private Location location;

  /**
   * Instantiates a new location contact.
   *
   * @param database the database
   */
  public LocationContact(AbstractDatabase<?> database) {
    super(database, OSSEVocabulary.Type.LocationContact);
  }

  /**
   * Instantiates a new location contact.
   *
   * @param database the database
   * @param contactResource the contact resource
   */
  public LocationContact(AbstractDatabase<?> database, Resource contactResource) {
    super(database, OSSEVocabulary.Type.LocationContact, contactResource);
  }

  /**
   * Instantiates a new location contact.
   *
   * @param database the database
   * @param contactUri the contact uri
   */
  public LocationContact(AbstractDatabase<?> database, String contactUri) {
    super(database, OSSEVocabulary.Type.LocationContact, contactUri);
  }

  /**
   * Gets the location.
   *
   * @return the location
   */
  public Location getLocation() {
    return location;
  }

  /**
   * Sets the location.
   *
   * @param location the new location
   */
  public void setLocation(Location location) {
    setParent(location);
    setProperty(OSSEVocabulary.Type.Location, location.getResource());
    this.location = location;
  }
}
