/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.control.AbstractDatabase;
import de.samply.edc.model.Entity;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.osse.OSSEOntology;
import de.samply.store.osse.OSSEUserType;
import de.samply.store.osse.OSSEVocabulary;
import de.samply.store.query.ResourceQuery;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionBindingEvent;
import javax.servlet.http.HttpSessionBindingListener;

/** Model class for users. */
public class User extends Entity implements HttpSessionBindingListener {

  /** Static map of all logged in users and their sessions. */
  private static Map<User, HttpSession> logins = new HashMap<>();

  /** The user contact data. */
  private UserContact contact;

  /** The roles of the user. */
  private List<Role> roles;

  /** The locations of the user (basically the locations of the roles of the user). */
  private List<Location> locations;

  /**
   * Instantiates a new user.
   *
   * @param database the database
   * @param userResource the user resource
   */
  public User(AbstractDatabase<?> database, Resource userResource) {
    super(database, OSSEVocabulary.Type.User, userResource);
  }

  /**
   * Instantiates a new user.
   *
   * @param database the database
   */
  public User(AbstractDatabase<?> database) {
    super(database, OSSEVocabulary.Type.User);
  }

  /**
   * Instantiates a new user.
   *
   * @param database the database
   * @param userUri the user uri
   */
  public User(AbstractDatabase<?> database, String userUri) {
    super(database, OSSEVocabulary.Type.User, userUri);
  }

  /**
   * Gets the logins.
   *
   * @return the logins
   */
  public static Map<User, HttpSession> getLogins() {
    return logins;
  }

  /**
   * Checks if a username is currently in the logged in sessions.
   *
   * @param username the username
   * @return the boolean
   */
  public static Boolean isLoggedIn(String username) {
    if ("".equals(username) || username == null) {
      return false;
    }

    for (User u : getLogins().keySet()) {
      if (u.getUsername().equalsIgnoreCase(username)) {
        return true;
      }
    }

    return false;
  }

  /**
   * Checks if a default role is defined for the user.
   *
   * @return the boolean
   */
  public Boolean hasDefaultRole() {
    return getResource().getProperty(Vocabulary.User.defaultRole) != null;
  }

  /**
   * Gets the default role of this user.
   *
   * @return the name of the default role or an empty String if no default role is defined.
   */
  public String getDefaultRole() {
    if (hasDefaultRole()) {
      return getResource().getProperty(Vocabulary.User.defaultRole).getValue();
    }

    if (roles != null && !roles.isEmpty()) {
      return roles.get(0).getName();
    }

    return "";
  }

  /**
   * Loads the user data.
   *
   * <p>If withRoles is true, loads also his roles (and his locations, which basically are the
   * locations of his roles).
   *
   * @param withRoles Load the User with his roles
   * @param doNotLoadPatientUser If it is a patient user, do not load the user
   * @return the boolean
   */
  public Boolean load(Boolean withRoles, Boolean doNotLoadPatientUser, JSONResource myConfig) {
    if (doNotLoadPatientUser) {
      String userType = getResource().getString("userType");
      if (userType != null && userType.equalsIgnoreCase(OSSEUserType.PATIENT.toString())) {
        return false;
      }
    }

    if (withRoles) {
      getDatabase().startCaching();

      /* Preload all locations */
      ResourceQuery locationCacheQuery = new ResourceQuery(OSSEVocabulary.Type.Location);
      locationCacheQuery.setFetchAdjacentResources(false);
      getDatabase().getResources(locationCacheQuery);

      List<Resource> roleResources =
          getResource().getResources(OSSEVocabulary.User.Roles, getDatabase().getDatabaseModel());

      // sorting the role resources by name
      Collections.sort(
          roleResources,
          new Comparator<Resource>() {
            @Override
            public int compare(Resource o1, Resource o2) {
              return o1.getProperty(OSSEVocabulary.Role.Name)
                  .getValue()
                  .compareTo(o2.getProperty(OSSEVocabulary.Role.Name).getValue());
            }
          });

      roles = new ArrayList<>();
      locations = new ArrayList<>();
      for (Resource roleResource : roleResources) {
        Role role = new Role(getDatabase(), roleResource);
        role.load(myConfig);
        roles.add(role);

        if (role.isCustomRole()) {
          String locationUri = (String) role.getProperty(OSSEVocabulary.Role.Location);

          ResourceQuery query = new ResourceQuery(OSSEVocabulary.Type.Location);
          query.setFetchAdjacentResources(false);
          query.addEqual(
              OSSEVocabulary.ID,
              roleResource.getProperty(OSSEVocabulary.Role.Location).asIdentifier().getId());

          Resource locationRes = getDatabase().getResources(query).get(0);
          Location location = new Location(getDatabase(), locationRes);
          location.load(myConfig);
          locations.add(location);
        }
      }

      getDatabase().stopCaching();
    }

    if (getResource().getProperty(OSSEVocabulary.User.ReadOnly.UserContacts) == null) {
      contact = new UserContact(getDatabase());
      contact.setUser(this);
      contact.addProperty(Vocabulary.Attributes.contactFirstName, "Unknown");
      contact.addProperty(Vocabulary.Attributes.contactLastName, "Unknown");
      contact.addProperty(Vocabulary.Attributes.contactTitle, "Unknown");
      contact.addProperty(Vocabulary.Attributes.contactEMail, "Unknown");
    } else {
      Resource contactRes =
          getDatabase()
              .getResourceByIdentifier(
                  getResource().getProperty(OSSEVocabulary.User.ReadOnly.UserContacts).getValue());
      contact = new UserContact(getDatabase(), contactRes);
      contact.setUser(this);
      contact.load(myConfig);
    }

    return super.load(myConfig);
  }

  /**
   * Loads the user data, his roles (and his locations, which basically are the locations of his
   * roles).
   *
   * @return the boolean
   * @see de.samply.edc.model.Entity#load()
   */
  @Override
  public Boolean load(JSONResource myConfig) {
    return load(true, false, myConfig);
  }

  /**
   * Unloads user data.
   *
   * @return the boolean
   * @see de.samply.edc.model.Entity#unload()
   */
  @Override
  public Boolean unload() {
    roles = null;
    contact = null;

    return super.unload();
  }

  /**
   * Deletes a user (sets him as not activated).
   *
   * @return true, if successful
   */
  public boolean deleteUser(JSONResource myConfig) {
    load(myConfig);

    getResource().setProperty(OSSEVocabulary.User.IsActivated, 0);
    getDatabase().beginTransaction();
    getDatabase().saveOrUpdateResource(getResource());
    getDatabase().commit();

    setProperty(OSSEVocabulary.User.IsActivated, 0);

    return true;
  }

  /**
   * Undeletes a user (sets him as activated).
   *
   * @return true, if successful
   */
  public boolean undeleteUser(JSONResource myConfig) {
    load(myConfig);
    getResource().setProperty(OSSEVocabulary.User.IsActivated, 1);
    getDatabase().beginTransaction();
    getDatabase().saveOrUpdateResource(getResource());
    getDatabase().commit();

    setProperty(OSSEVocabulary.User.IsActivated, 1);

    Utils.addContextMessage(
        Utils.getResourceBundleString("undelete_successful_title"),
        Utils.getResourceBundleString("undelete_successful"));
    return true;
  }

  /**
   * Checks if the user is activated.
   *
   * @return the boolean
   */
  public Boolean isActivated() {
    if (getProperty(OSSEVocabulary.User.IsActivated) == null) {
      return false;
    }

    if (getProperty(OSSEVocabulary.User.IsActivated) instanceof Number) {
      return (1 == ((Number) getProperty(OSSEVocabulary.User.IsActivated)).intValue());
    }

    return "1".equalsIgnoreCase((String) getProperty(OSSEVocabulary.User.IsActivated));
  }

  /**
   * Gets the user real name ("Admin" for user admin, or "firstname lastname" for other users, or
   * "Unknown" if so).
   *
   * @return the user real name
   */
  public String getUserRealName() {
    if (getUsername().equals("admin")) {
      return "System";
    } else {
      if (contact != null) {
        return ""
            + contact.getProperty(Vocabulary.Attributes.contactFirstName)
            + " "
            + contact.getProperty(Vocabulary.Attributes.contactLastName);
      }
    }

    return "Unknown";
  }

  /**
   * Checks if the user is a developer.
   *
   * @return the boolean
   */
  public Boolean isDeveloper() {
    for (Role role : roles) {
      if ("DEVELOPER".equalsIgnoreCase((String) role.getProperty(OSSEOntology.Role.RoleType))) {
        return true;
      }
    }

    return false;
  }

  /**
   * Checks if user is a global admin.
   *
   * @return the boolean
   */
  public Boolean isGlobalAdmin() {
    for (Role role : roles) {
      if ("ADMINISTRATOR".equalsIgnoreCase((String) role.getProperty(OSSEOntology.Role.RoleType))) {
        return true;
      }
    }

    return false;
  }

  /**
   * Checks if the user is a local admin.
   *
   * @return the boolean
   */
  public Boolean isLocalAdmin() {
    for (Role role : roles) {
      if ("LOCAL_ADMINISTRATOR"
          .equalsIgnoreCase((String) role.getProperty(OSSEOntology.Role.RoleType))) {
        return true;
      }
    }

    return false;
  }

  /** Reset fail counter. The fail counter counts the illegal login attempts (wrong password) */
  public void resetFailCounter() {
    getResource().setProperty(OSSEVocabulary.User.FailCounter, "0");
    getDatabase().save(getResource());
  }

  public void activateUser() {
    getResource().setProperty(OSSEVocabulary.User.IsActivated, "1");
    getDatabase().save(getResource());
  }

  /**
   * Gets the locations.
   *
   * @return the locations
   */
  public List<Location> getLocations() {
    if (locations == null) {
      return new ArrayList<>();
    }
    return locations;
  }

  /**
   * Gets the username.
   *
   * @return the username
   */
  public String getUsername() {
    return (String) getProperty(OSSEVocabulary.User.Username);
  }

  /**
   * Gets the failcounter. The fail counter counts the illegal login attempts (wrong password)
   *
   * @return the failcounter
   */
  public Integer getFailcounter() {
    if (getProperty(OSSEVocabulary.User.FailCounter) == null) {
      return 0;
    }

    if (getProperty(OSSEVocabulary.User.FailCounter) instanceof Number) {
      return ((Number) getProperty(OSSEVocabulary.User.FailCounter)).intValue();
    }

    return Integer.parseInt((String) getProperty(OSSEVocabulary.User.FailCounter));
  }

  /**
   * Gets the contact.
   *
   * @return the contact
   */
  public UserContact getContact() {
    return contact;
  }

  /**
   * Sets the contact.
   *
   * @param contact the new contact
   */
  public void setContact(UserContact contact) {
    this.contact = contact;
  }

  /**
   * Gets the roles.
   *
   * @return the roles
   */
  public List<Role> getRoles() {
    return roles;
  }

  /**
   * Sets the roles.
   *
   * @param roles the new roles
   */
  public void setRoles(List<Role> roles) {
    this.roles = roles;
  }

  /**
   * This removes all previous sessions of the currently logging in user and saves his session into
   * a static list.
   *
   * @param event the event
   */
  @Override
  public void valueBound(HttpSessionBindingEvent event) {
    HttpSession session = logins.remove(this);
    if (session != null) {
      session.invalidate();
    }
    logins.put(this, event.getSession());
  }

  /**
   * This removes the user and his session from the static list.
   *
   * @param event the event
   */
  @Override
  public void valueUnbound(HttpSessionBindingEvent event) {
    logins.remove(this);
  }

  /** Removes this user from the static list. */
  public void logMeOut() {
    if (logins.get(this) == null) {
      return;
    }

    logins.get(this).invalidate();
  }
}
