/*
 * Copyright (C) 2018 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.model.patientlist.rest;

import java.util.List;

public class Patientlist {

  private int draw;
  private int recordsTotal;
  private int recordsFiltered;
  private boolean showIdat;
  private boolean isBridgehead;
  private List<PatientlistLine> data;

  public int getDraw() {
    return draw;
  }

  public void setDraw(int draw) {
    this.draw = draw;
  }

  public int getRecordsTotal() {
    return recordsTotal;
  }

  public void setRecordsTotal(int recordsTotal) {
    this.recordsTotal = recordsTotal;
  }

  public int getRecordsFiltered() {
    return recordsFiltered;
  }

  public void setRecordsFiltered(int recordsFiltered) {
    this.recordsFiltered = recordsFiltered;
  }

  public boolean isShowIdat() {
    return showIdat;
  }

  public void setShowIdat(boolean showIdat) {
    this.showIdat = showIdat;
  }

  public boolean isBridgehead() {
    return isBridgehead;
  }

  public void setBridgehead(boolean bridgehead) {
    isBridgehead = bridgehead;
  }

  public List<PatientlistLine> getData() {
    return data;
  }

  public void setData(List<PatientlistLine> data) {
    this.data = data;
  }
}
