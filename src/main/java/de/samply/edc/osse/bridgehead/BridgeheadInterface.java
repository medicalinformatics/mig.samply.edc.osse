/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.bridgehead;

import com.thoughtworks.xstream.XStream;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.model.DatatableRow;
import de.samply.edc.model.DatatableRows;
import de.samply.edc.model.Entity;
import de.samply.edc.osse.bridgehead.exception.ImportException;
import de.samply.edc.osse.control.Database;
import de.samply.edc.osse.model.Case;
import de.samply.edc.osse.model.CaseForm;
import de.samply.edc.osse.model.Episode;
import de.samply.edc.osse.model.EpisodeForm;
import de.samply.edc.osse.model.Form;
import de.samply.edc.osse.model.Location;
import de.samply.edc.osse.model.OsseMessage;
import de.samply.edc.osse.model.Patient;
import de.samply.edc.osse.model.SessionsKillerSingleton;
import de.samply.edc.osse.model.User;
import de.samply.edc.osse.rest.AbstractRestInterface;
import de.samply.edc.osse.utils.MapEntryConverter;
import de.samply.edc.osse.utils.XmlHelper;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import de.samply.store.Resource;
import de.samply.store.Value;
import de.samply.store.osse.OSSEVocabulary;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.core.SecurityContext;
import javax.xml.parsers.ParserConfigurationException;
import org.codehaus.jettison.json.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/** REST interface for bridgehead functions. */
@Path("")
public class BridgeheadInterface extends AbstractRestInterface {

  /** The error messages. */
  private OsseMessage errorMessages;

  /** The episode and case form name. */
  private String caseFormName;
  private String episodeFormName;

  /** This is a bridgehead. */
  private Boolean isBridgehead;

  /** The mdr Namespace. */
  private String mdrNS;

  /** The status one resource. */
  private Resource statusOneResource = null;

  /**
   * Import patients.
   *
   * @param requestContext the request context
   * @param context the context
   * @param headers the headers
   * @param patientsXml the patients xml
   * @return the response
   */
  @POST
  @Path("/patients")
  @Consumes(MediaType.APPLICATION_XML)
  @Produces({MediaType.APPLICATION_XML, MediaType.APPLICATION_JSON})
  public Response importPatients(
      @Context HttpServletRequest requestContext,
      @Context SecurityContext context,
      @Context HttpHeaders headers,
      String patientsXml) {

    Status accessStatus = accessAllowed(headers);
    if (!accessStatus.equals(Status.OK)) {
      return Response.status(accessStatus).build();
    }

    Database database = new Database(true);
    JSONResource config = database.getConfig("osse");

    errorMessages = new OsseMessage();
    isBridgehead = Utils.isBridgehead(config);

    String keywordHeader = headers.getHeaderString("use_keywords");
    boolean useKeyWords = false;
    if (keywordHeader != null && keywordHeader.equals("true")) {
      useKeyWords = true;
    }


    // Fetch the XSD for the import
    JSONResource osseXsdStorage = database.getConfig("osse.xsd.storage");
    String xsd = Utils.getStringOfJsonResource(osseXsdStorage, Vocabulary.Config.specificXSD);
    if (xsd == null) {
      errorMessages.setFinalMessage("No XSD found!");
      return Response.status(Status.INTERNAL_SERVER_ERROR).entity(errorMessages).build();
    }

    // validate the xml and create a Document from it
    XmlHelper xmlHelper = new XmlHelper(errorMessages);

    Document dom = xmlHelper.validateXml(xsd, patientsXml);
    errorMessages = xmlHelper.getErrorMessages();

    // validation failed, bail out
    if (dom == null || errorMessages.hasErrorMessages()) {
      return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
    }

    // We need some information if this is a bridgehead and the episode and
    // case form names
    caseFormName = config.getProperty(Vocabulary.Config.Form.patientStandard).getValue();
    episodeFormName = config.getProperty(Vocabulary.Config.Form.visitStandard).getValue();

    Element docEle = dom.getDocumentElement();

    // Read out MDR Namespace
    Element mdr = (Element) docEle.getElementsByTagName("Mdr").item(0);
    mdrNS = mdr.getElementsByTagName("Namespace").item(0).getTextContent();

    NodeList patientNodeList = docEle.getElementsByTagName("BHPatient");
    Integer returnCode = 0;

    Integer counterOverwritten = 0;
    Integer counterNewPatients = 0;
    Integer counterAddPatientData = 0;

    database.beginTransaction();

    for (int i = 0; i < patientNodeList.getLength(); i++) {
      Node node = patientNodeList.item(i);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element patient = (Element) node;
        Patient patientEntity = new Patient(database);

        String pid = patient.getElementsByTagName("Identifier").item(0).getTextContent();
        Utils.getLogger().debug("Patient PID = " + pid);

        NodeList locations = patient.getElementsByTagName("Location");

        if (locations == null || locations.getLength() == 0) {
          continue;
        }

        for (int l = 0; l < locations.getLength(); l++) {

          node = locations.item(l);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element location = (Element) node;
            String locationName = location.getAttribute("name");
            Utils.getLogger().debug("Location " + locationName);

            Location patientLocation = new Location(database);

            // on the first run, create the patient and add the
            // location as the patient's own location
            if (l == 0) {
              patientLocation.createOrLoadByName(locationName, config);

              // In the bridgehead we want to delete the patient first and
              // then add in the data
              // In a registry, we do not want to delete the data but only
              // overwrite the form data (or add new forms)
              returnCode = patientEntity.importPatient(pid, patientLocation, isBridgehead, config);
            } else {
              patientLocation = new Location(database);
              patientLocation.createOrLoadByName(locationName, config);
            }

            // Prepare the case
            Entity myCase = patientEntity.getCaseOfLocation(patientLocation, config);
            if (myCase == null) {
              if (isBridgehead) {
                myCase = patientEntity.addCaseOfLocation(patientLocation);
              } else {
                errorMessages.setFinalMessage(
                    "Could not find a location with the name '" + locationName + "'.");
                Utils.getLogger().error("No case for location: " + locationName);
                return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
              }
            }

            NodeList basicData = location.getElementsByTagName("BasicData");
            if (basicData == null || basicData.getLength() == 0) {
              Utils.getLogger().debug("No basic data");
            } else {
              Utils.getLogger().debug("Importing basic data...");
              // iterate through all available forms
              if (basicData.getLength() == 1) {
                NodeList formElementList = basicData.item(0).getChildNodes();
                int formElementListLength = formElementList.getLength();
                for (int counter = 0; counter < formElementListLength; counter++) {
                  if (formElementList.item(counter).getNodeType() == Node.ELEMENT_NODE) {
                    Element formElement = (Element) formElementList.item(counter);
                    if (formElement.getNodeName().toLowerCase().contains("form")) {
                      Utils.getLogger().debug("Importing " + formElement.getNodeName() + " ...");
                      if (!readCaseForms(
                          database,
                          (Case) myCase,
                          (Element) basicData.item(0),
                          formElement.getNodeName(),
                          config)) {
                        Utils.getLogger().error("Could not import " + formElement.getNodeName());
                      }
                    }
                  }
                }
              }
            }

            // episodes
            NodeList episodes = location.getElementsByTagName("Episode");
            if (episodes == null || episodes.getLength() == 0) {
              Utils.getLogger().debug("No episodes");
            }

            for (int e = 0; e < episodes.getLength(); e++) {
              Element episode = (Element) episodes.item(e);
              String episodeName = episode.getAttribute("name");
              String episodeOptionalText = episode.getAttribute("optionalText");
              Utils.getLogger().debug("Episode: " + episodeName + " " + episodeOptionalText);

              Episode episodeEntity;

              if (useKeyWords && episodeName.equals("#latest#")) {
                List<Entity> episodeList = ((Case) myCase).getEpisodes(config);
                episodeEntity = (Episode) episodeList.get(episodeList.size() - 1);
              } else {
                episodeEntity = new Episode(database);
                episodeEntity.createOrLoadByName(episodeName, (Case) myCase, config);
                if (episodeOptionalText != null) {
                  episodeEntity.setProperty(Vocabulary.Episode.optionalText, episodeOptionalText);
                  episodeEntity.saveOrUpdate();
                }
              }

              NodeList longitudinalData = episode.getElementsByTagName("LogitudinalData");
              if (longitudinalData == null || longitudinalData.getLength() == 0) {
                Utils.getLogger().debug("No longitudinal data");
              } else {
                Utils.getLogger().debug("Importing longitudinal data...");
                // iterate through all available forms
                if (longitudinalData.getLength() == 1) {
                  NodeList formElementList = longitudinalData.item(0).getChildNodes();
                  int formElementListLength = formElementList.getLength();
                  for (int counter = 0; counter < formElementListLength; counter++) {
                    if (formElementList.item(counter).getNodeType() == Node.ELEMENT_NODE) {
                      Element formElement = (Element) formElementList.item(counter);
                      if (formElement.getNodeName().toLowerCase().contains("form")) {
                        Utils.getLogger().debug("Importing " + formElement.getNodeName() + " ...");
                        if (!readEpisodeForms(
                            database,
                            episodeEntity,
                            (Element) longitudinalData.item(0),
                            formElement.getNodeName(),
                            config)) {
                          Utils.getLogger().error("Could not import " + formElement.getNodeName());
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }

        if (errorMessages.hasErrorMessages()) {
          database.rollback();
          errorMessages.setPatientID(pid);
          errorMessages.setFinalMessage("The import was interrupted due to validation errors. The "
              + "following errors occured in the patient " + pid + ":");
          Utils.getLogger()
              .error("The import of patient " + pid + " was interrupted due to validation errors.");

          for (OsseMessage.Messages.Message message : errorMessages.getMessages().getMessage()) {
            Utils.getLogger()
                .debug(
                    "Key: "
                        + message.getKey()
                        + " | Value: "
                        + message.getValue()
                        + " | Message: "
                        + message.getMessage());
          }

          return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
        } else {
          Utils.getLogger().debug("Done importing patient " + pid + ".");
        }

        switch (returnCode) {
          case -1:
            counterOverwritten++;
            break;
          case 1:
            counterNewPatients++;
            break;
          case 0:
            counterAddPatientData++;
            break;
          default:
            break;
        }
      }
    }

    database.commit();
    Utils.getLogger().debug("Import finished.");

    // put upload date into DB
    config.setProperty(Vocabulary.Config.uploadStats, new Date().getTime());
    database.saveConfig("osse", config);

    if (counterNewPatients > 0) {
      errorMessages.setFinalMessage(
          counterNewPatients
              + " patients were imported, "
              + counterOverwritten
              + " patients were overwritten, and "
              + counterAddPatientData
              + " patients had data added or forms overwritten.");
      return Response.status(Status.CREATED).entity(errorMessages).build();
    }

    Utils.getLogger().debug("Kill all current user sessions!");
    SessionsKillerSingleton.instance.setDoKillAllSessions(true);

    errorMessages.setFinalMessage(
        counterOverwritten
            + " patients were overwritten, and "
            + counterAddPatientData
            + " patients had data added or forms overwritten.");
    return Response.status(Status.OK).entity(errorMessages).build();
  }

  /**
   * Puts a dataelement into a given form and returns the form.
   *
   * @param database the database
   * @param theForm the the form
   * @param mdrKey the mdr key
   * @param entry the entry
   * @return the form
   */
  private Form readDataelement(
      Database database, Form theForm, String mdrKey, Element entry, JSONResource myConfig) {
    Boolean isCaseForm = (Objects.equals(theForm.getType(), OSSEVocabulary.Type.CaseForm));

    NodeList entryValues = entry.getElementsByTagName("Value");
    if (entryValues != null && entryValues.getLength() > 0) {
      // repeatable Field

      DatatableRows rows = new DatatableRows();

      if (entryValues.getLength() > 0) {
        for (int ev = 0; ev < entryValues.getLength(); ev++) {
          Node node = entryValues.item(ev);
          if (node.getNodeType() == Node.ELEMENT_NODE) {
            Element repValue = (Element) node;
            String value = repValue.getTextContent();

            Utils.getLogger().debug("- " + mdrKey + " = " + value);

            if (value == null || value.equals("")) {
              Utils.getLogger().debug("Value null or emtpy. Moving on...");
              continue;
            }

            try {
              Object theValue =
                  ImportValidator.convertAndValidate(
                      database, isCaseForm, mdrKey, value, null, myConfig);
              HashMap<String, Object> columns = new HashMap<>();
              DatatableRow row = new DatatableRow();
              columns.put(mdrKey, theValue);
              row.setColumns(columns);
              rows.add(row);
            } catch (ImportException e) {
              errorMessages.addErrorMessage(mdrKey, value, e.getMessage());
            }
          }
        }

        theForm.addProperty(mdrKey, rows);
      }
    } else {
      // field
      String value = entry.getTextContent();
      Utils.getLogger().debug(mdrKey + " = " + value);

      try {
        Object theValue =
            ImportValidator.convertAndValidate(database, isCaseForm, mdrKey, value, null, myConfig);
        theForm.addProperty(mdrKey, theValue);
      } catch (ImportException e) {
        errorMessages.addErrorMessage(mdrKey, value, e.getMessage());
      }
    }

    return theForm;
  }

  /**
   * Puts a record into a form and returns the form.
   *
   * @param database the database
   * @param theForm the the form
   * @param mdrKey the mdr key
   * @param entry the entry
   * @param myConfig config json
   * @return the form
   */
  private Form readRecord(
      Database database, Form theForm, String mdrKey, Element entry, JSONResource myConfig) {
    Utils.getLogger().debug("Record " + mdrKey);
    Boolean isCaseForm = (theForm.getType() == OSSEVocabulary.Type.CaseForm);

    NodeList recordRows = entry.getElementsByTagName("Row");
    if (recordRows == null || recordRows.getLength() == 0) {
      // non repeatable record
      Utils.getLogger().debug("===SINGLE===");
      NodeList recordEntries = entry.getChildNodes();
      for (int re = 0; re < recordEntries.getLength(); re++) {
        Node node = recordEntries.item(re);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          Element recordEntry = (Element) node;
          String recordEntryMdrKey = recordEntry.getNodeName();
          recordEntryMdrKey = fixMdrKey(mdrNS, recordEntryMdrKey);
          String value = recordEntry.getTextContent();

          Utils.getLogger().debug("- " + recordEntryMdrKey + " = " + value);

          try {
            Object theValue =
                ImportValidator.convertAndValidate(
                    database, isCaseForm, recordEntryMdrKey, value, mdrKey, myConfig);
            Utils.getLogger().debug("Field: " + recordEntryMdrKey + " Value: " + value);

            String finalKey = mdrKey + "/" + recordEntryMdrKey;
            theForm.addProperty(finalKey, theValue);
          } catch (ImportException e) {
            Utils.getLogger().error("Could not use value '" + value + "'", e);
            errorMessages.addErrorMessage(
                mdrKey + " -> " + recordEntryMdrKey, value, e.getMessage());
          }
        }
      }
    } else {
      // repeatable record
      DatatableRows rows = new DatatableRows();

      for (int rr = 0; rr < recordRows.getLength(); rr++) {
        Utils.getLogger().debug("===ROW===");
        DatatableRow row = new DatatableRow();
        HashMap<String, Object> columns = new HashMap<>();

        Node node = recordRows.item(rr);
        if (node.getNodeType() == Node.ELEMENT_NODE) {
          NodeList recordEntries = ((Element) node).getChildNodes();

          for (int re = 0; re < recordEntries.getLength(); re++) {
            node = recordEntries.item(re);

            if (node.getNodeType() == Node.ELEMENT_NODE) {
              Element recordEntry = (Element) node;
              String recordEntryMdrKey = recordEntry.getNodeName();
              recordEntryMdrKey = fixMdrKey(mdrNS, recordEntryMdrKey);

              String value = recordEntry.getTextContent();

              Utils.getLogger().debug("- " + recordEntryMdrKey + " = " + value);

              try {
                Object theValue =
                    ImportValidator.convertAndValidate(
                        database, isCaseForm, recordEntryMdrKey, value, mdrKey, myConfig);
                Utils.getLogger().debug("Field: " + recordEntryMdrKey + " Value: " + value);
                columns.put(recordEntryMdrKey, theValue);
              } catch (ImportException e) {
                Utils.getLogger().error("ERRORVALUE = '" + value + "'", e);
                errorMessages.addErrorMessage(
                    mdrKey + " -> " + recordEntryMdrKey, value, e.getMessage());
              }
            }
          }
        }
        row.setColumns(columns);
        rows.add(row);
      }

      theForm.addProperty(mdrKey, rows);
    }

    return theForm;
  }

  /**
   * fills a form and saves it to the backend.
   *
   * @param database the database
   * @param theForm the the form
   * @param formEntries the form entries
   * @param myConfig config json
   */
  private void readForm(
      Database database, Form theForm, NodeList formEntries, JSONResource myConfig) {
    for (int fe = 0; fe < formEntries.getLength(); fe++) {
      Node node = formEntries.item(fe);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element entry = (Element) node;
        String mdrKey = entry.getNodeName();

        mdrKey = fixMdrKey(mdrNS, mdrKey);

        if (mdrKey.contains("dataelement")) {
          // dataelement
          theForm = readDataelement(database, theForm, mdrKey, entry, myConfig);

        } else {

          theForm = readRecord(database, theForm, mdrKey, entry, myConfig);
        }
      }
    }
    theForm.saveOrUpdate();
  }

  /**
   * fills an episode form with values and saves it to the backend.
   *
   * @param database the database
   * @param myEpisode the my episode
   * @param dataElement the data element
   * @param elementName the name of the form tag to import
   * @param myConfig config json
   * @return true, if successful
   */
  private boolean readEpisodeForms(
      Database database,
      Episode myEpisode,
      Element dataElement,
      String elementName,
      JSONResource myConfig) {
    NodeList forms = dataElement.getElementsByTagName(elementName);

    if (forms == null || forms.getLength() == 0) {
      return false;
    }

    for (int f = 0; f < forms.getLength(); f++) {
      Node node = forms.item(f);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element form = (Element) node;
        String formName = form.getAttribute("name");
        Utils.getLogger().debug("Form " + formName);

        if (isBridgehead) {
          // The bridgehead has only one fixed episode form, so all
          // data will be put into it
          // no matter what form names the xml brings along
          Utils.getLogger()
              .debug(
                  "XML has episode form name "
                      + formName
                      + ". We are a bridgehead, so putting it into "
                      + episodeFormName);
          formName = episodeFormName;
        }

        EpisodeForm episodeForm = (EpisodeForm) myEpisode.getForm(formName, myConfig);
        if (episodeForm == null) {
          episodeForm = new EpisodeForm(database);
        } else {
          episodeForm.load(myConfig);
          episodeForm.deleteFormMedicalData();
        }

        // Prepare the caseForm
        episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Name, formName);
        Resource statusRes = getStatusOneResource(database);
        episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Status, statusRes);
        episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Version, 1);
        episodeForm.setProperty(OSSEVocabulary.EpisodeForm.Episode, myEpisode.getResource());

        Date lastChangedTime = Calendar.getInstance().getTime();
        User systemUser = new User(database, "user:1");
        systemUser.load(false, true, myConfig);
        episodeForm.setProperty(Vocabulary.Attributes.lastChangedBy, systemUser.getResource());
        episodeForm.setProperty(Vocabulary.Attributes.lastChangedDate, lastChangedTime);

        NodeList formEntries = form.getChildNodes();

        readForm(database, episodeForm, formEntries, myConfig);

        if (errorMessages.hasErrorMessages()) {
          return true;
        }
      }
    }

    return true;
  }

  /**
   * fills a case form with values and saves it to the backend.
   *
   * @param database the database
   * @param myCase the my case
   * @param dataElement the data element
   * @param elementName the name of the form tag to import
   * @param myConfig config json
   * @return true, if successful
   */
  private boolean readCaseForms(
      Database database,
      Case myCase,
      Element dataElement,
      String elementName,
      JSONResource myConfig) {
    NodeList forms = dataElement.getElementsByTagName(elementName);
    if (forms == null || forms.getLength() == 0) {
      return false;
    }

    for (int f = 0; f < forms.getLength(); f++) {
      Node node = forms.item(f);
      if (node.getNodeType() == Node.ELEMENT_NODE) {
        Element form = (Element) node;
        String formName = form.getAttribute("name");
        Utils.getLogger().debug("Form " + formName);

        if (isBridgehead) {
          // The bridgehead has only one fixed case form, so all data
          // will be put into it
          // no matter what form names the xml brings along
          Utils.getLogger()
              .debug(
                  "XML has case form name "
                      + formName
                      + ". We are a bridgehead, so putting it into "
                      + caseFormName);
          formName = caseFormName;
        }

        CaseForm caseForm = (CaseForm) myCase.getForm(formName, myConfig);
        if (caseForm == null) {
          caseForm = new CaseForm(database);
        } else {
          caseForm.load(myConfig);
          caseForm.deleteFormMedicalData();
        }

        // Prepare the caseForm
        caseForm.setProperty(OSSEVocabulary.CaseForm.Name, formName);
        Resource statusRes = getStatusOneResource(database);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Status, statusRes);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Version, 1);
        caseForm.setProperty(OSSEVocabulary.CaseForm.Case, myCase.getResource());

        Date lastChangedTime = Calendar.getInstance().getTime();
        User systemUser = new User(database, "user:1");
        systemUser.load(false, true, myConfig);
        caseForm.setProperty(Vocabulary.Attributes.lastChangedBy, systemUser.getResource());
        caseForm.setProperty(Vocabulary.Attributes.lastChangedDate, lastChangedTime);

        NodeList formEntries = form.getChildNodes();

        readForm(database, caseForm, formEntries, myConfig);

        if (errorMessages.hasErrorMessages()) {
          return true;
        }
      }
    }

    return true;
  }

  /**
   * Gets the resource for "status = 1 = open".
   *
   * @param database the database
   * @return the status one resource
   */
  private Resource getStatusOneResource(Database database) {
    if (statusOneResource == null) {
      statusOneResource =
          Utils.findResourceByPropertyInDatabase(
              database, OSSEVocabulary.Type.Status, OSSEVocabulary.ID, "1", false);
    }

    return statusOneResource;
  }

  /**
   * REST interface to completely delete a patient (this is a true delete).
   *
   * @param pseudonym the pseudonym
   * @return the response
   */
  @DELETE
  @Path("/patients/{pseudonym}")
  public Response deletePatient(@PathParam("pseudonym") String pseudonym,
      @Context HttpHeaders headers) {

    Status accessStatus = accessAllowed(headers);
    if (!accessStatus.equals(Status.OK)) {
      return Response.status(accessStatus).build();
    }

    Database database = new Database(true);
    Patient patientEntity = new Patient(database);
    Resource patientResource = patientEntity.entityExistsByProperty("patientID", pseudonym);

    if (patientResource != null) {
      patientEntity.setResource(patientResource);

      database.beginTransaction();
      patientEntity.wipeoutPatient();
      database.commit();

      return Response.ok().build();
    } else {
      errorMessages = new OsseMessage();
      errorMessages.setFinalMessage(
          "The patient with the ID " + pseudonym + " could not be deleted.");
      return Response.status(Status.BAD_REQUEST).entity(errorMessages).build();
    }
  }

  /**
   * Reads status information of the bridgehead.
   *
   * @return A map with status information on software version.
   */
  private Map<String, String> getStatusInformation() {
    Database database = new Database(true);
    JSONResource config = database.getConfig("osse");

    Map<String, String> out = new HashMap<>();

    Value version = config.getProperty(Vocabulary.Config.version);
    if (version == null) {
      out.put("version", "1.0.0");
    } else {
      out.put("version", version.getValue());
    }

    Value uploadStats = config.getProperty(Vocabulary.Config.uploadStats);
    if (uploadStats != null) {
      Date date = new Date(uploadStats.asLong());
      // we need a date format like: Tue, 15 Nov 1994 08:12:31 GMT
      String datePattern = "EEE, dd MMM yyyy HH:mm:ss zzz";
      SimpleDateFormat sdf = new SimpleDateFormat(datePattern);
      out.put("uploadStats", sdf.format(date));
    } else {
      out.put("uploadStats", "No upload yet");
    }

    return out;
  }

  /**
   * Output status information as JSON.
   *
   * @return A JSON object with the following members:
   *     <ul>
   *       <li>version: Software version of this Bridgehead instance.
   *     </ul>
   */
  @GET
  @Produces(MediaType.APPLICATION_JSON)
  public Response helloJson() {
    Map<String, String> versionMap = new HashMap<>();
    versionMap.put("version", getStatusInformation().get("version"));
    JSONObject answer = new JSONObject(versionMap);
    return Response
        .status(Status.OK)
        .entity(answer.toString())
        .build();
  }

  /**
   * Output status information as XML.
   *
   * @return A XML containing the software version of this Bridgehead instance. Example: <root>
   *     <version>1.1.2</version> </root>
   * @throws ParserConfigurationException the parser configuration exception
   */
  @GET
  @Produces(MediaType.APPLICATION_XML)
  public String helloXml() {
    Map<String, String> versionMap = new HashMap<>();
    versionMap.put("version", getStatusInformation().get("version"));

    XStream magicApi = new XStream();
    magicApi.registerConverter(new MapEntryConverter());
    magicApi.alias("root", Map.class);

    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    xml += magicApi.toXML(versionMap);

    return xml;
  }

  /**
   * Output status information as text.
   *
   * @return A status message containing the software version of this Bridgehead instance.
   */
  @GET
  @Produces({MediaType.TEXT_HTML, MediaType.TEXT_PLAIN})
  public String helloHtml() {
    return String.format(
        "This is OSSE-Bridgehead running version %s.", getStatusInformation().get("version"));
  }

  /**
   * Get the timestamp for the last successful upload to determine which patients have to be
   * uploaded. Output as JSON.
   *
   * @return A JSON object with the following members:
   *     <ul>
   *       <li>LastUploadTimestamp: Timestamp of the last upload
   *     </ul>
   */
  @GET
  @Path("/uploadStats")
  @Produces(MediaType.APPLICATION_JSON)
  public Response getUploadStatsJson() {
    Map<String, String> versionMap = new HashMap<>();
    versionMap.put("LastUploadTimestamp", getStatusInformation().get("uploadStats"));
    JSONObject answer = new JSONObject(versionMap);
    return Response
        .status(Status.OK)
        .entity(answer.toString())
        .build();
  }

  /**
   * Get the timestamp for the last successful upload to determine which patients have to be
   * uploaded. Output as XML.
   *
   * @return A XML containing the software version of this Bridgehead instance. Example:
   *     <Uploadstats> <LastUploadTimestamp>Tue, 15 Nov 1994 08:12:31 GMT</LastUploadTimestamp>
   *     </Uploadstats>
   * @throws ParserConfigurationException the parser configuration exception
   */
  @GET
  @Path("/uploadStats")
  @Produces(MediaType.APPLICATION_XML)
  public String getUploadStatsXml() {
    Map<String, String> versionMap = new HashMap<>();
    versionMap.put("LastUploadTimestamp", getStatusInformation().get("uploadStats"));

    XStream magicApi = new XStream();
    magicApi.registerConverter(new MapEntryConverter());
    magicApi.alias("Uploadstats", Map.class);

    String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n";
    xml += magicApi.toXML(versionMap);

    return xml;
  }

  /**
   * Transforms XML-MDRKey fragments with underscores to MDRKeys.
   *
   * @param mdrNS the MDR Namespace
   * @param mdrKey the XML-MDRKey fragment
   * @return the string
   */
  private String fixMdrKey(String mdrNS, String mdrKey) {
    mdrKey = mdrKey.replace("_", ":");
    mdrKey = Utils.lowerCaseFirstChar(mdrKey);

    mdrKey = mdrNS + ":" + mdrKey;

    return mdrKey;
  }

  /**
   * Checks if this REST interface shall allow access currently it's only for the OSSE Bridgehead.
   *
   * @return the boolean
   */
  @Override
  protected Boolean allowRest() {
    Database database = new Database(true);
    JSONResource config = database.getConfig("osse");

    return Utils.isBridgehead(config);
  }
}
