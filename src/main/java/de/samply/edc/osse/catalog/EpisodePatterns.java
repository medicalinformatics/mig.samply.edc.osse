/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.catalog;

import de.samply.edc.osse.model.EpisodePattern;
import java.util.TreeMap;

/** static list of possible episode patterns. */
public class EpisodePatterns {
  // define your patternData here as string array (0 = name, 1 = label, 2 =
  // pattern for Java, 3 = pattern for jQuery, 4 = regexp, 5 = classic
  /** The Constant patterns. */
  // and add them to this map
  public static final TreeMap<String, EpisodePattern> patterns = new TreeMap<>();
  /** The Constant UKDATE. */
  // datestring pattern?)
  private static final Object[] UKDATE = {
    "UKDATE", "English date", "dd/MM/yyyy", "dd/mm/yy", "^(\\d{2})\\/(\\d{2})\\/(\\d{4})$", true
  };
  /** The Constant ISODATE. */
  private static final Object[] ISODATE = {
    "ISODATE", "ISO date", "yyyy-MM-dd", "yy-mm-dd", "^(\\d{4})-(\\d{2})-(\\d{2})$", true
  };
  /** The Constant MONTHYEAR. */
  private static final Object[] MONTHYEAR = {
    "MONTHYEAR", "Month and year", "MM/yyyy", "mm/yy", "^(\\d{2})\\/(\\d{4})$", true
  };
  /** The Constant ROMANQUARTERS. */
  private static final Object[] ROMANQUARTERS = {
    "ROMANQUARTERS", "Roman Quarters", "I-IV\\yyyy", "", "^(IV|I{1,3})\\/(\\d{4})$", false
  };
  /** The Constant QUARTERS. */
  private static final Object[] QUARTERS = {
    "QUARTERS", "Quarters", "Qx/yyyy", "", "^(Q\\d{1})\\/(\\d{4})$", false
  };
  /** The Constant Index. */
  private static final Object[] INDEX = {
      "INDEX", "Index", "dd", "", "^(\\d*)$", false
  };

  static {
    patterns.put((String) UKDATE[0], new EpisodePattern(UKDATE));
    patterns.put((String) ISODATE[0], new EpisodePattern(ISODATE));
    patterns.put((String) MONTHYEAR[0], new EpisodePattern(MONTHYEAR));
    patterns.put((String) QUARTERS[0], new EpisodePattern(QUARTERS));
    patterns.put((String) ROMANQUARTERS[0], new EpisodePattern(ROMANQUARTERS));
    patterns.put((String) INDEX[0], new EpisodePattern(INDEX));
  }
}
