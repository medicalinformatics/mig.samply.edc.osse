/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.osse.catalog;

/** Contains patterns for stringformatters. */
public class Patterns {

  /** The Constant patternXSDField. */
  /*
   * XSD patterns used to create the specific data import XSD
   */
  public static final String patternXSDField =
      "                                      <xs:element name=\"%s\" minOccurs=\"0\">\n"
          + "                                        <xs:annotation>\n"
          + "                                          <xs:documentation>%s</xs:documentation>\n"
          + "                                        </xs:annotation>\n"
          + "                                        <xs:complexType>\n"
          + "                                          <xs:simpleContent>\n"
          + "                                            <xs:extension base=\"xs:string\">\n"
          + "                                              <xs:attribute type=\"xs:string\" "
          + "name=\"name\" fixed=\"%s\"/>\n"
          + "                                            </xs:extension>\n"
          + "                                          </xs:simpleContent>\n"
          + "                                        </xs:complexType>\n"
          + "                                      </xs:element>\n\n";

  /** The Constant patternXSDRepeatableField. */
  public static final String patternXSDRepeatableField =
      "                                      <xs:element name=\"%s\" minOccurs=\"0\">\n"
          + "                                        <xs:annotation>\n"
          + "                                          <xs:documentation>%s</xs:documentation>\n"
          + "                                        </xs:annotation>\n"
          + "                                        <xs:complexType>\n"
          + "                                          <xs:sequence>\n"
          + "                                            <xs:element type=\"xs:string\" "
          + "name=\"Value\" maxOccurs=\"unbounded\" minOccurs=\"0\"/>\n"
          + "                                          </xs:sequence>\n"
          + "                                          <xs:attribute type=\"xs:string\" "
          + "name=\"name\" fixed=\"%s\"/>\n"
          + "                                        </xs:complexType>\n"
          + "                                      </xs:element>\n\n";

  /** The Constant patternXSDRecord. */
  public static final String patternXSDRecord =
      "                                      <xs:element name=\"%s\" minOccurs=\"0\">\n"
          + "                                        <xs:annotation>\n"
          + "                                          <xs:documentation>%s</xs:documentation>\n"
          + "                                        </xs:annotation>\n"
          + "                                        <xs:complexType>\n"
          + "                                          <xs:sequence>\n"
          + "                                            %s"
          + "                                          </xs:sequence>\n"
          + "                                          <xs:attribute type=\"xs:string\" "
          + "name=\"name\" fixed=\"%s\"/>\n"
          + "                                        </xs:complexType>\n"
          + "                                      </xs:element>\n\n";

  /** The Constant patternXSDRepeatableRecord. */
  public static final String patternXSDRepeatableRecord =
      "                                      <xs:element name=\"%s\" minOccurs=\"0\">\n"
          + "                                        <xs:annotation>\n"
          + "                                          <xs:documentation>%s</xs:documentation>\n"
          + "                                        </xs:annotation>\n"
          + "                                        <xs:complexType>\n"
          + "                                          <xs:sequence>\n"
          + "                                            <xs:element name=\"Row\" "
          + "maxOccurs=\"unbounded\" minOccurs=\"0\">\n"
          + "                                              <xs:complexType>\n"
          + "                                                <xs:sequence>\n"
          + "                                                  %s"
          + "                                                </xs:sequence>\n"
          + "                                              </xs:complexType>\n"
          + "                                            </xs:element>\n"
          + "                                          </xs:sequence>\n"
          + "                                          <xs:attribute type=\"xs:string\" "
          + "name=\"name\" fixed=\"%s\"/>\n"
          + "                                        </xs:complexType>\n"
          + "                                      </xs:element>\n\n";

  /*
   * XSD Patterns end
   */

}
