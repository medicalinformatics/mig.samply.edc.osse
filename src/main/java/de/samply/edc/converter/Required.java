/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.converter;

import de.samply.edc.utils.Utils;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;

/**
 * Converter that allows validation of empty fields by own code, as JSF does not trigger validators
 * on empty fields and only offers required="true" as sole solution with no simple solution to build
 * an own validation around that.
 *
 * <p>This is included for future use.
 */
public class Required implements Converter {

  public static final String VALIDATION_ERROR_HEADER = "validation_error_header";
  public static final String REQUIRED_MISSING_ERROR = "convert_required_no_value_error";

  /*
   * (non-Javadoc)
   *
   * @see
   * javax.faces.convert.Converter#getAsObject(javax.faces.context.FacesContext
   * , javax.faces.component.UIComponent, java.lang.String)
   */
  @Override
  public Object getAsObject(FacesContext context, UIComponent component, String value) {

    if (Utils.isNullOrEmpty(value)) {
      String summary = Utils.getResourceBundleString(VALIDATION_ERROR_HEADER);
      String message = Utils.getResourceBundleString(REQUIRED_MISSING_ERROR);

      FacesMessage msg = new FacesMessage(summary, message);
      msg.setSeverity(FacesMessage.SEVERITY_ERROR);
      throw new ConverterException(msg);
    }

    return value;
  }

  /*
   * (non-Javadoc)
   *
   * @see
   * javax.faces.convert.Converter#getAsString(javax.faces.context.FacesContext
   * , javax.faces.component.UIComponent, java.lang.Object)
   */
  @Override
  public String getAsString(FacesContext context, UIComponent component, Object value) {
    if (value == null) {
      return null;
    }

    return value.toString();
  }
}
