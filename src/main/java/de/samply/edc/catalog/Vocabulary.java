/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.catalog;

/** Vocabulary class, providing static constants for strings used in database or configurations. */
public class Vocabulary {
  public static final String caseIsCleared = "caseIsCleared";
  public static final String querySearchesGrading = "querySearchesGrading";
  public static final String querySearchesICD10primary = "querySearchesICD10primary";
  public static final String querySearchesICD10secondary = "querySearchesICD10secondary";
  public static final String querySearchesICDO3Morphology = "querySearchesICDO3Morphology";
  public static final String querySearchesICDO3Topography = "querySearchesICDO3Topography";
  public static final String querySearchesLocationOfMetastases =
      "querySearchesLocationOfMetastases";
  public static final String querySearchesSexMale = "showMale";
  public static final String querySearchesSexFemale = "showFemale";
  public static final String querySearchesSexUnknown = "showUndefined";
  public static final String querySearchesTNM = "querySearchesTNM";
  public static final String querySearchesSampleType = "querySearchesSampleType";
  public static final String querySearchesYearOfBirth = "querySearchesYearOfBirth";
  public static final String querySearchesResultText = "querySearchesResultText";
  public static final String attr_contextLocked = "locked";
  private static final String attr_yearOfBirth = "yearOfBirth";
  private static final String attr_owner = "owner";
  private static final String attr_diagnosis = "diagnosis";
  private static final String attr_icd10primary = "icd10primary";
  private static final String attr_icd10secondary = "icd10secondary";
  private static final String attr_locationOfMetastases = "locationOfMetastases";
  private static final String attr_icdo3Topography = "icdo3Topography";
  private static final String attr_icdo3Morphology = "icdo3Morphology";
  private static final String attr_therapy = "therapy";
  private static final String attr_caseDate = "caseDate";
  private static final String attr_patientAgeAtCase = "patientAgeAtCase";
  private static final String attr_patientUserAccount = "patientUserAccount";
  private static final String attr_isImported = "isImported";
  private static final String attr_caseType = "caseType";
  private static final String attr_tnm = "tnm";
  private static final String attr_sampleType = "sampleType";
  private static final String attr_grading = "grading";
  private static final String attr_gender = "gender";
  private static final String attr_fixationTime = "fixationTime";
  private static final String attr_extractionTime = "extractionTime";
  private static final String attr_fixationType = "fixationType";
  private static final String attr_treatment = "treatment";
  private static final String attr_resultText = "resultText";
  private static final String attr_resultDate = "resultDate";
  private static final String attr_tumorLocation = "tumorLocation";
  private static final String attr_pseudonymDescribesResource = "pseudonymDescribesResource";
  private static final String attr_contactFirstName = "firstName";
  private static final String attr_contactLastName = "lastName";
  private static final String attr_contactEMail = "eMail";
  private static final String attr_contactPhone = "phone";
  private static final String attr_contactTitle = "title";
  private static final String attr_requestStatus = "status";
  private static final String attr_requestShortDescriptionText = "description";
  private static final String attr_requestExposeURL = "requestExposeURL";
  private static final String attr_site = "site";
  private static final String attr_siteAdmin = "admin";
  private static final String attr_siteThis = "this";
  private static final String attr_isDeleted = "isDeleted";
  private static final String opt_male = "male";
  private static final String opt_female = "female";
  private static final String opt_undefined = "undefined";
  private static final String opt_resectionbiopsy = "resectionbiopsy";
  private static final String opt_punch = "punch";
  private static final String opt_needle = "needle";
  private static final String opt_other = "other_specify";
  private static final String opt_unknown = "unknown";
  private static final String opt_unknown_origin = "unknown_origin";
  private static final String opt_misc = "misc";
  private static final String opt_freshFrozen = "freshFrozen";
  private static final String opt_paraffin = "paraffin";
  private static final String opt_EDTA = "EDTA";
  private static final String opt_DNA = "DNA";
  private static final String opt_RNA = "RNA";
  private static final String opt_pax = "pax";
  private static final String opt_primary = "primary";
  private static final String opt_relapse = "relapse_specify";
  private static final String opt_secondaryMalignant = "secondaryMalignant_specify";
  private static final String opt_ewing = "ewing";
  private static final String opt_osteo = "osteo";
  private static final String opt_rhabdomyo = "rhabdomyo";
  private static final String opt_synovial = "synovial";
  private static final String opt_DRCT = "DRCT";
  private static final String opt_ewing2008 = "ewing_ewing2008";
  private static final String opt_ewing_ee99 = "ewing_ee99";
  private static final String opt_ewing_eicess92 = "ewing_eicess92";
  private static final String opt_ewing_relapse = "ewing_relapse";
  private static final String opt_ewing_other = "ewing_other";
  private static final String opt_ewing_unknown = "ewing_unknown";
  private static final String opt_osteo_euramos1 = "osteo_euramos1";
  private static final String opt_osteo_euroboss = "osteo_euroboss";
  private static final String opt_osteo_coss96 = "osteo_coss96";
  private static final String opt_osteo_relapse = "osteo_relapse";
  private static final String opt_osteo_other = "osteo_other";
  private static final String opt_osteo_unknown = "osteo_unknown";
  private static final String opt_soft_sotisar = "soft_sotisar";
  private static final String opt_soft_cws2007 = "soft_cws2007";
  private static final String opt_soft_cws2002 = "soft_cws2002";
  private static final String opt_soft_cws96 = "soft_cws96";
  private static final String opt_soft_relapse = "soft_relapse";
  private static final String opt_soft_other = "soft_other";
  private static final String opt_soft_unknown = "soft_unknown";
  private static final String opt_bone = "bone";
  private static final String opt_softTissue = "softTissue";
  private static final String opt_boneMarrow = "boneMarrow";
  private static final String opt_blood = "blood";
  private static final String opt_metastasis = "metastasis";
  private static final String opt_cellLine = "cellLine";
  private static final String opt_xenograft = "xenograft";
  private static final String opt_whole = "wholeBlood";
  private static final String opt_serum = "serum";
  private static final String opt_plasma = "plasma";
  private static final String opt_pellet = "pellet";
  private static final String opt_msc = "msc";
  private static final String opt_pelvis = "pelvis";
  private static final String opt_abdomen = "abdomen";
  private static final String opt_spine = "spine";
  private static final String opt_thorax = "thorax";
  private static final String opt_headNeckArea = "headNeckArea";
  private static final String opt_upperExtremities = "upperExtremities";
  private static final String opt_lowerExtremities = "lowerExtremities";
  private static final String opt_osIlium = "osIlium";
  private static final String opt_ischiopubis = "ischiopubis";
  private static final String opt_acetabulum = "acetabulum";
  private static final String opt_osSacrum = "osSacrum";
  private static final String opt_hwk = "hwk";
  private static final String opt_bwk = "bwk";
  private static final String opt_lwk = "lwk";
  private static final String opt_scapula = "scapula";
  private static final String opt_clavicula = "clavicula";
  private static final String opt_rib = "rib";
  private static final String opt_sternum = "sternum";
  private static final String opt_skull = "skull";
  private static final String opt_mandibula = "mandibula";
  private static final String opt_maxilla = "maxilla";
  private static final String opt_humerus = "humerus";
  private static final String opt_ulna = "ulna";
  private static final String opt_radius = "radius";
  private static final String opt_carpus = "carpus";
  private static final String opt_metacarpus = "metacarpus";
  private static final String opt_finger = "finger";
  private static final String opt_femur = "femur";
  private static final String opt_tibia = "tibia";
  private static final String opt_fibula = "fibula";
  private static final String opt_calcanaeum = "calcanaeum";
  private static final String opt_talus = "talus";
  private static final String opt_metatarsus = "metatarsus";
  private static final String opt_toe = "toe";
  private static final String opt_pelviabdominal = "pelviabdominalSofttissue";
  private static final String opt_fundament = "fundament";
  private static final String opt_hipInguinal = "hipInguinal";
  private static final String opt_perineum = "perineum";
  private static final String opt_intraAbdominal = "intraAbdominal";
  private static final String opt_retroperitoneal = "retroperitoneal";
  private static final String opt_abdominalWall = "abdominalWall";
  private static final String opt_kidney = "kidney";
  private static final String opt_cervical = "cervical";
  private static final String opt_dorsal = "dorsal";
  private static final String opt_lumbal = "lumbal";
  private static final String opt_shoulder = "shoulder";
  private static final String opt_axilla = "axilla";
  private static final String opt_thoraxWall = "thoraxWall";
  private static final String opt_scalp = "scalp";
  private static final String opt_neck = "neck";
  private static final String opt_face = "face";
  private static final String opt_arm = "arm";
  private static final String opt_elbow = "elbow";
  private static final String opt_forearm = "forearm";
  private static final String opt_wrist = "wrist";
  private static final String opt_hand = "hand";
  private static final String opt_thigh = "thigh";
  private static final String opt_knee = "knee";
  private static final String opt_shank = "shank";
  private static final String opt_ankle = "ankle";
  private static final String opt_foot = "foot";
  private static final String attr_birthDate = "birthDate";
  private static final String attr_name = "name";
  private static final String attr_lastChangedBy = "lastChangedBy";
  private static final String attr_lastChangedDate = "lastChangedDate";
  private static final String attr_personalGroup = "self";
  private static final String qs_new = "new";
  private static final String qs_sent = "sent";
  private static final String qs_accepted = "accepted";
  private static final String qs_denied = "denied";
  private static final String qs_closed = "closed";
  private static final String qs_ignored = "ignored";
  private static final String attr_street = "street";
  private static final String attr_plz = "plz";
  private static final String attr_city = "city";
  private static final String attr_freetext = "freetext";

  public enum FormTypes {
    DEFAULT,
    PATIENTFORM
  }

  public static class Attributes {
    public static final String yearOfBirth = Vocabulary.attr_yearOfBirth;
    public static final String diagnosis = Vocabulary.attr_diagnosis;
    public static final String icd10primary = Vocabulary.attr_icd10primary;
    public static final String icd10secondary = Vocabulary.attr_icd10secondary;
    public static final String locationOfMetastases = Vocabulary.attr_locationOfMetastases;
    public static final String icdo3Topography = Vocabulary.attr_icdo3Topography;
    public static final String icdo3Morphology = Vocabulary.attr_icdo3Morphology;
    public static final String therapy = Vocabulary.attr_therapy;
    public static final String caseDate = Vocabulary.attr_caseDate;
    public static final String patientAgeAtCase = Vocabulary.attr_patientAgeAtCase;
    public static final String caseType = Vocabulary.attr_caseType;
    public static final String tnm = Vocabulary.attr_tnm;
    public static final String sampleType = Vocabulary.attr_sampleType;
    public static final String grading = Vocabulary.attr_grading;
    public static final String gender = Vocabulary.attr_gender;
    public static final String fixationTime = Vocabulary.attr_fixationTime;
    public static final String extractionTime = Vocabulary.attr_extractionTime;
    public static final String fixationType = Vocabulary.attr_fixationType;
    public static final String treatment = Vocabulary.attr_treatment;
    public static final String resultText = Vocabulary.attr_resultText;
    public static final String resultDate = Vocabulary.attr_resultDate;
    public static final String tumorLocation = Vocabulary.attr_tumorLocation;
    public static final String pseudonymDescribesResource =
        Vocabulary.attr_pseudonymDescribesResource;
    public static final String contactFirstName = Vocabulary.attr_contactFirstName;
    public static final String contactLastName = Vocabulary.attr_contactLastName;
    public static final String contactEMail = Vocabulary.attr_contactEMail;
    public static final String contactPhone = Vocabulary.attr_contactPhone;
    public static final String contactTitle = Vocabulary.attr_contactTitle;
    public static final String contactStreet = Vocabulary.attr_street;
    public static final String contactPLZ = Vocabulary.attr_plz;
    public static final String contactCity = Vocabulary.attr_city;
    public static final String contactFreetext = Vocabulary.attr_freetext;

    public static final String requestStatus = Vocabulary.attr_requestStatus;
    public static final String requestShortDescriptionText =
        Vocabulary.attr_requestShortDescriptionText;
    public static final String requestExposeURL = Vocabulary.attr_requestExposeURL;
    public static final String site = Vocabulary.attr_site;

    public static final String birthDate = Vocabulary.attr_birthDate;
    public static final String name = Vocabulary.attr_name;
    public static final String lastChangedBy = Vocabulary.attr_lastChangedBy;
    public static final String lastChangedDate = Vocabulary.attr_lastChangedDate;
  }

  public static class Generic {
    public static final String other = Vocabulary.opt_other;
    public static final String unknown = Vocabulary.opt_unknown;
    public static final String isDeleted = Vocabulary.attr_isDeleted;
    public static final String misc = Vocabulary.opt_misc;
    public static final String owner = Vocabulary.attr_owner;
    public static final String systemLocation = "Global Roles";
  }

  public static class Episode {
    public static final String optionalText = "optionalText";
  }

  public static class Form {
    public static final String formComments = "formComments";
    public static final String formComment = "formComment";
    public static final String formType = "formType";
  }

  public static class Comment {
    public static final String commentTimestamp = "commentTimestamp";
    public static final String commentUserURI = "commentUserURI";
    public static final String commentText = "commentText";
  }

  public static class Development {
    public static final String testFormName = "test-1";
  }

  public static class Context {
    public static final String locked = Vocabulary.attr_contextLocked;
  }

  public static class Site {
    public static final String admin = Vocabulary.attr_siteAdmin;
    public static final String this_ = Vocabulary.attr_siteThis;
  }

  public static class Patient {
    public static final String User = Vocabulary.attr_patientUserAccount;
    public static final String isImported = Vocabulary.attr_isImported;

    public static class Gender {
      public static final String male = Vocabulary.opt_male;
      public static final String female = Vocabulary.opt_female;
      public static final String undefined = Vocabulary.opt_undefined;
    }
  }

  public static class Case {
    public static class Type {
      public static final String primary = Vocabulary.opt_primary;
      public static final String relapse = Vocabulary.opt_relapse;
      public static final String secondary = Vocabulary.opt_secondaryMalignant;
    }

    public static class Diagnosis {
      public static final String ewing = Vocabulary.opt_ewing;
      public static final String osteo = Vocabulary.opt_osteo;
      public static final String rhabdomyo = Vocabulary.opt_rhabdomyo;
      public static final String synovial = Vocabulary.opt_synovial;
      public static final String DRCT = Vocabulary.opt_DRCT;
    }

    public static class Therapy {
      public static class Ewing {
        public static final String ewing2008 = Vocabulary.opt_ewing2008;
        public static final String ee99 = Vocabulary.opt_ewing_ee99;
        public static final String eicess92 = Vocabulary.opt_ewing_eicess92;
        public static final String relapse = Vocabulary.opt_ewing_relapse;
        public static final String other = Vocabulary.opt_ewing_other;
        public static final String unknown = Vocabulary.opt_ewing_unknown;
      }

      public static class Osteo {
        public static final String euramos1 = Vocabulary.opt_osteo_euramos1;
        public static final String euroboss = Vocabulary.opt_osteo_euroboss;
        public static final String coss96 = Vocabulary.opt_osteo_coss96;
        public static final String relapse = Vocabulary.opt_osteo_relapse;
        public static final String other = Vocabulary.opt_osteo_other;
        public static final String unknown = Vocabulary.opt_osteo_unknown;
      }

      public static class SoftTissue {
        public static final String sotisar = Vocabulary.opt_soft_sotisar;
        public static final String cws2007 = Vocabulary.opt_soft_cws2007;
        public static final String cws2002 = Vocabulary.opt_soft_cws2002;
        public static final String cws96 = Vocabulary.opt_soft_cws96;
        public static final String relapse = Vocabulary.opt_soft_relapse;
        public static final String other = Vocabulary.opt_soft_other;
        public static final String unknown = Vocabulary.opt_soft_unknown;
      }
    }
  }

  public static class Request {
    public static final String sample_ids = "sample_ids";

    public static class Status {
      public static final String _new = Vocabulary.qs_new;
      public static final String accepted = Vocabulary.qs_accepted;
      public static final String denied = Vocabulary.qs_denied;
      public static final String sent = Vocabulary.qs_sent;
      public static final String closed = Vocabulary.qs_closed;
      public static final String ignored = Vocabulary.qs_ignored;
    }
  }

  public static class User {
    public static final String defaultRole = "default.role";
  }

  public static class Usergroup {
    public static final String personalGroup = attr_personalGroup;
  }

  public static class Sample {
    public static class ExtractionMethod {
      public static final String resectionbiopsy = Vocabulary.opt_resectionbiopsy;
      public static final String punch = Vocabulary.opt_punch;
      public static final String needle = Vocabulary.opt_needle;
    }

    public static class Treatment {
      public static final String freshFrozen = Vocabulary.opt_freshFrozen;
      public static final String paraffin = Vocabulary.opt_paraffin;
      public static final String EDTA = Vocabulary.opt_EDTA;
      public static final String DNA = Vocabulary.opt_DNA;
      public static final String RNA = Vocabulary.opt_RNA;
      public static final String pax = Vocabulary.opt_pax;
    }

    public static class Type {

      public static final String bone = Vocabulary.opt_bone;
      public static final String softTissue = Vocabulary.opt_softTissue;
      public static final String boneMarrow = Vocabulary.opt_boneMarrow;
      public static final String blood = Vocabulary.opt_blood;
      public static final String metastasis = Vocabulary.opt_metastasis;
      public static final String cellLine = Vocabulary.opt_cellLine;
      public static final String xenograft = Vocabulary.opt_xenograft;

      public static class Area {
        public static final String pelvis = Vocabulary.opt_pelvis;
        public static final String abdomen = Vocabulary.opt_abdomen;
        public static final String spine = Vocabulary.opt_spine;
        public static final String thorax = Vocabulary.opt_thorax;
        public static final String headNeckArea = Vocabulary.opt_headNeckArea;
        public static final String upperExtremities = Vocabulary.opt_upperExtremities;
        public static final String lowerExtremities = Vocabulary.opt_lowerExtremities;
        public static final String unknown = Vocabulary.opt_unknown_origin;
      }

      public static class Bone {
        public static final String osIlium = Vocabulary.opt_osIlium;
        public static final String ischiopubis = Vocabulary.opt_ischiopubis;
        public static final String acetabulum = Vocabulary.opt_acetabulum;
        public static final String osSacrum = Vocabulary.opt_osSacrum;
        public static final String hwk = Vocabulary.opt_hwk;
        public static final String bwk = Vocabulary.opt_bwk;
        public static final String lwk = Vocabulary.opt_lwk;
        public static final String scapula = Vocabulary.opt_scapula;
        public static final String clavicula = Vocabulary.opt_clavicula;
        public static final String rib = Vocabulary.opt_rib;
        public static final String sternum = Vocabulary.opt_sternum;
        public static final String skull = Vocabulary.opt_skull;
        public static final String mandibula = Vocabulary.opt_mandibula;
        public static final String maxilla = Vocabulary.opt_maxilla;
        public static final String humerus = Vocabulary.opt_humerus;
        public static final String ulna = Vocabulary.opt_ulna;
        public static final String radius = Vocabulary.opt_radius;
        public static final String carpus = Vocabulary.opt_carpus;
        public static final String metacarpus = Vocabulary.opt_metacarpus;
        public static final String finger = Vocabulary.opt_finger;
        public static final String femur = Vocabulary.opt_femur;
        public static final String tibia = Vocabulary.opt_tibia;
        public static final String fibula = Vocabulary.opt_fibula;
        public static final String calcanaeum = Vocabulary.opt_calcanaeum;
        public static final String talus = Vocabulary.opt_talus;
        public static final String metatarsus = Vocabulary.opt_metatarsus;
        public static final String toe = Vocabulary.opt_toe;
        public static final String unknown = Vocabulary.opt_unknown;
      }

      public static class SoftTissue {
        public static final String pelviabdominal = Vocabulary.opt_pelviabdominal;
        public static final String fundament = Vocabulary.opt_fundament;
        public static final String hipInguinal = Vocabulary.opt_hipInguinal;
        public static final String perineum = Vocabulary.opt_perineum;
        public static final String intraAbdominal = Vocabulary.opt_intraAbdominal;
        public static final String retroperitoneal = Vocabulary.opt_retroperitoneal;
        public static final String abdominalWall = Vocabulary.opt_abdominalWall;
        public static final String kidney = Vocabulary.opt_kidney;
        public static final String cervical = Vocabulary.opt_cervical;
        public static final String dorsal = Vocabulary.opt_dorsal;
        public static final String lumbal = Vocabulary.opt_lumbal;
        public static final String shoulder = Vocabulary.opt_shoulder;
        public static final String axilla = Vocabulary.opt_axilla;
        public static final String thoraxWall = Vocabulary.opt_thoraxWall;
        public static final String scalp = Vocabulary.opt_scalp;
        public static final String neck = Vocabulary.opt_neck;
        public static final String face = Vocabulary.opt_face;
        public static final String arm = Vocabulary.opt_arm;
        public static final String elbow = Vocabulary.opt_elbow;
        public static final String forearm = Vocabulary.opt_forearm;
        public static final String wrist = Vocabulary.opt_wrist;
        public static final String hand = Vocabulary.opt_hand;
        public static final String thigh = Vocabulary.opt_thigh;
        public static final String knee = Vocabulary.opt_knee;
        public static final String shank = Vocabulary.opt_shank;
        public static final String ankle = Vocabulary.opt_ankle;
        public static final String foot = Vocabulary.opt_foot;
        public static final String unknown = Vocabulary.opt_unknown;
        public static final String other = Vocabulary.opt_other;
      }

      public static class Blood {
        public static final String whole = Vocabulary.opt_whole;
        public static final String serum = Vocabulary.opt_serum;
        public static final String plasma = Vocabulary.opt_plasma;
        public static final String pellet = Vocabulary.opt_pellet;
        public static final String pax = Vocabulary.opt_pax;
        public static final String msc = Vocabulary.opt_msc;
      }
    }
  }

  public static class Upgrade {
    public static final String toVersion = "version.to";
    public static final String fromVersion = "version.from";
  }

  public static class Config {
    public static final String version = "version";
    public static final String uploadStats = "uploadStats";
    public static final String executedUpgradeUUID = "executedUpgradeUUID";

    public static final String registryName = "registryName";
    public static final String registryEmail = "registryEmail";
    public static final String project = "project";
    public static final String site = "site";
    public static final String thePackage = "package";
    public static final String formNames = "form.names.matrix";
    public static final String archivedFormNames = "form.names.matrix.archive";
    public static final String mdrEntityIsInForm = "mdrEntityIsInForm";
    public static final String recordHasMdrEntities = "recordHasMdrEntities";
    public static final String mdrEntityHasValidation = "mdrEntityHasValidation";
    public static final String warnDoubleMDRKeyUsage = "warnDoubleMDRKeyUsage";
    public static final String specificXSD = "specificXSD";
    public static final String useDashboard = "osse.useDashboard";

    public static class Package {
      public static final String bridgehead = "bridgehead";
      public static final String registry = "registry";
    }

    public static class Standard {
      // Formeditor
      public static final String formEditorBASE = "standard.formeditor.base";
      public static final String formEditorRestUrl = "standard.formeditor.rest";
      public static final String formEditorClientId = "standard.formeditor.clientId";

      // AUTH
      public static final String authREST = "standard.auth.rest";
      public static final String authREALM = "standard.auth.realm";
      public static final String authUseSamplyAuth = "standard.auth.useSamplyAuth";
      public static final String authPubkey = "standard.auth.pubkey";
      public static final String authClientId = "standard.auth.clientId";
      public static final String authClientSecret = "standard.auth.clientSecret";
      public static final String authRegistryPassword = "standard.auth.registryPassword";

      // Mainzelliste
      public static final String mainzellisteNoIdat = "standard.mainzelliste.noIdat";
      public static final String RESTInternalPort = "standard.mainzelliste.restInternalPort";
      /**
       * Initialization value for the port number to use when the Mainzelliste is accessible via
       * <tt>http://localhost:_port_/mainzelliste</tt>. Property deprecated by using the full URL
       * {@link Vocabulary.Config.Standard#mainzellisteRestUrlEdc}.
       *
       * @deprecated Use full URL including port number in
       * {@link Vocabulary.Config.Standard#mainzellisteRestUrlEdc} instead.
       */
      @Deprecated
      public static final String mainzellisteInternalPort =
              "standard.mainzelliste.internalMainzellistePort";
      public static final String mainzellisteRestUrlPublic = "standard.mainzelliste.rest";
      public static final String mainzellisteRestUrlEdc = "standard.mainzelliste.restEdc";
      /**
       * Initialization value for the REST URL for accessing the Mainzelliste.
       * Property replaced by {@link Vocabulary.Config.Standard#mainzellisteRestUrlPublic}.
       *
       * @deprecated Use {@link Vocabulary.Config.Standard#mainzellisteRestUrlPublic} instead.
       */
      @Deprecated
      public static final String mainzellisteREST = mainzellisteRestUrlPublic;
      public static final String mainzellisteApikey = "standard.mainzelliste.domain";

      // MDR
      public static final String mdrREST = "standard.mdr.rest";

      // Teiler
      public static final String teilerREST = "standard.teiler.rest";
      public static final String teilerInternalPort = "standard.teiler.internalTeilerPort";
      public static final String teilerApiKey = "standard.teiler.apikey";
      public static final String teilerApiKeyExport = "standard.teiler.apikeyExport";

      public static final String proxyHTTPHost = "standard.proxy.http.host";
      public static final String proxyHTTPPort = "standard.proxy.http.port";
      public static final String proxyHTTPUsername = "standard.proxy.http.username";
      public static final String proxyHTTPPassword = "standard.proxy.http.password";
      public static final String proxyRealm = "standard.proxy.realm";
      public static final String proxyHTTPSHost = "standard.proxy.https.host";
      public static final String proxyHTTPSPort = "standard.proxy.https.port";
      public static final String proxyHTTPSUsername = "standard.proxy.https.username";
      public static final String proxyHTTPSPassword = "standard.proxy.https.password";

    }

    public static class Proxy {
      public static final String HTTPHost = "proxy.http.host";
      public static final String HTTPPort = "proxy.http.port";
      public static final String HTTPUsername = "proxy.http.username";
      public static final String HTTPPassword = "proxy.http.password";
      public static final String Realm = "proxy.realm";
      public static final String HTTPSHost = "proxy.https.host";
      public static final String HTTPSPort = "proxy.https.port";
      public static final String HTTPSUsername = "proxy.https.username";
      public static final String HTTPSPassword = "proxy.https.password";
    }

    public static class Teiler {
      public static final String REST = "teiler.rest";
      public static final String internalTeilerPort = "teiler.internalTeilerPort";
      public static final String apiKey = "teiler.apikey";
      public static final String apiKeyExport = "teiler.apikeyExport";
    }

    public static class Mdr {
      public static final String REST = "mdr.rest";
    }

    public static class Auth {
      public static final String REST = "auth.rest";
      public static final String REALM = "auth.realm";
      public static final String UseSamplyAuth = "auth.useSamplyAuth";
      public static final String Pubkey = "auth.pubkey";
      public static final String ServerPubKey = "auth.server.pubkey";
      public static final String KeyId = "auth.keyid";
      public static final String UserId = "auth.userid";
      public static final String MyPubKey = "auth.my.pubkey";
      public static final String MyPrivKey = "auth.my.privkey";
      public static final String ClientId = "auth.clientId";
      public static final String ClientSecret = "auth.clientSecret";
      public static final String RegistryPassword = "auth.registryPassword";
    }

    public static class Mainzelliste {
      public static final String noIdat = "mainzelliste.noIdat";
      public static final String RESTInternalPort = "mainzelliste.restInternalPort";
      /**
       * Port number to use when the Mainzelliste is accessible via
       * <tt>http://localhost:_port_/mainzelliste</tt>. Property deprecated by using the full URL
       * {@link Vocabulary.Config.Mainzelliste#restUrlEdc}.
       *
       * @deprecated Use full URL including port number in
       * {@link Vocabulary.Config.Mainzelliste#restUrlEdc} instead.
       */
      @Deprecated
      public static final String RESTInternalMainzellistePort =
              "mainzelliste.internalMainzellistePort";
      public static final String restUrlPublic = "mainzelliste.rest";
      public static final String restUrlEdc = "mainzelliste.restEdc";
      /**
       * The REST URL for accessing the Mainzelliste.
       * Property replaced by {@link Vocabulary.Config.Mainzelliste#restUrlPublic}.
       *
       * @deprecated Use {@link Vocabulary.Config.Mainzelliste#restUrlPublic} instead.
       */
      @Deprecated
      public static final String REST = restUrlPublic;
      public static final String apikey = "mainzelliste.domain";
    }

    public static class FormEditor {
      public static final String formEditorBASE = "formeditor.base";
      public static final String formEditorRestUrl = "formeditor.rest";
      public static final String formEditorClientId = "formeditor.clientId";
    }

    public static class Xsd {
      public static final String storage = "xsd.storage";
    }

    public static class Episode {
      public static final String pattern = "episode.pattern";
    }

    public static class PatientForm {
      public static final String list = "patientformlist";
      public static final String archivedList = "archived.patientformlist";
    }

    public static class Form {
      public static final String patient = "form.patient";
      public static final String archivedPatient = "archived.form.patient";
      public static final String patientStandard = "form.patient.standard";
      public static final String visit = "form.visit";
      public static final String archivedVisit = "archived.form.visit";
      public static final String visitStandard = "form.visit.standard";
      public static final String possibleStatus = "form.status";
      public static final String statusTransition = "form.status.transition";
      public static final String statusTransitionName = "form.status.transition.name";
      public static final String statusTransitionFrom = "form.status.transition.from";
      public static final String statusTransitionTo = "form.status.transition.to";
      public static final String storage = "form.storage";
      public static final String formNameMatrix = "form.name.matrix";
    }

    public static class Importer {
      public static final String apikey = "importer.apikey";
    }

    public static class Mail {
      public static final String smtpHost = "smtp.host";
      public static final String smtpPort = "smtp.port";
      public static final String smtpUser = "smtp.user";
      public static final String smtpPassword = "smtp.password";
      public static final String smtpFrom = "smtp.from";
      public static final String smtpSendername = "smtp.sendername";
    }

    public static class Password {
      public static final String minLength = "password.min.length";
      public static final String maxLength = "password.max.length";
      public static final String minLowercase = "password.min.lowercase";
      public static final String minUppercase = "password.min.uppercase";
      public static final String minSpecial = "password.min.special";
      public static final String minNumerical = "password.min.numerical";
    }

    public static class Authentication {
      public static final String systemTotpActive = "authentication.totp";
    }

    public static class Kidsafe {
      public static final String documentUploadUrl = "document_upload_url";
    }
  }
}
