/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.utils;

import de.samply.edc.catalog.Vocabulary;

/** WebUtils JSF helper. */
public final class WebUtils {

  /** Instantiates a new web utils. */
  private WebUtils() {}

  /**
   * Translates a string (which is a comma separated list of keys) into clear text.
   *
   * @param in the text
   * @param split translates all parts of the string, or only the final one
   * @return the string
   */
  public static String translate(String in, Boolean split) {
    if (split) {
      return translateSplit(in);
    } else {
      return translate(in);
    }
  }

  /**
   * Translates a string (which is a comma separated list of keys) into clear text.
   *
   * @param in the text
   * @return the string
   */
  public static String translate(String in) {
    if (Utils.isNullOrEmpty(in)) {
      return "";
    }
    if (in.contains(":")) {
      return Utils.getResourceText(in.substring(in.indexOf(':') + 1), true);
    }

    return Utils.getResourceText(in, true);
  }

  /**
   * Translates all parts of a string (which is a list of keys) into clear text.
   *
   * @param in the in
   * @return the string
   */
  public static String translateSplit(String in) {
    return Utils.translateSplit(in);
  }

  /**
   * Transforms the values m, f, u into predefined string texts.
   *
   * @param in the gender abbreviation
   * @return the string
   */
  public static String genderText(String in) {
    String out;
    switch (in) {
      case "f":
        out = translate(Vocabulary.Patient.Gender.female);
        break;
      case "m":
        out = translate(Vocabulary.Patient.Gender.male);
        break;
      case "u":
      default:
        out = translate(Vocabulary.Patient.Gender.undefined);
        break;
    }
    return out;
  }
}
