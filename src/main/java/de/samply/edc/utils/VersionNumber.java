/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.utils;

/**
 * Comparable Class to be able to compare version numbers like "4.2.1" with each other on the fly.
 *
 * <p>Usage: VersionNumber foo = new VersionNumber("4.2.1"); VersionNumber bar = new
 * VersionNumber("3.99.5");
 *
 * <p>and you can use foo.compareTo(bar) as Comparable
 */
public class VersionNumber implements Comparable<VersionNumber> {

  public static final String SEPARATOR = ".";
  /** The major, minor, and sub parts of a version number. */
  private Integer major;

  private Integer minor;
  private Integer sub;

  /**
   * Instantiates a new version number.
   *
   * @param version the version number, format: "major.minor.sub".
   */
  public VersionNumber(String version) {
    major = 0;
    minor = 0;
    sub = 0;

    if (!Utils.isNullOrEmpty(version)) {
      String[] split = version.split("\\.");

      if (split.length > 0) {
        major = Integer.parseInt(split[0]);
        if (split.length > 1) {
          minor = Integer.parseInt(split[1]);
          if (split.length > 2) {
            sub = Integer.parseInt(split[2]);
          }
        }
      }
    }
  }

  /**
   * Gets the major component.
   *
   * @return the major component
   */
  public Integer getMajor() {
    return major;
  }

  /**
   * Sets the major component.
   *
   * @param major the new major component
   */
  public void setMajor(Integer major) {
    this.major = major;
  }

  /**
   * Gets the minor component.
   *
   * @return the minor component
   */
  public Integer getMinor() {
    return minor;
  }

  /**
   * Sets the minor component.
   *
   * @param minor the new minor component
   */
  public void setMinor(Integer minor) {
    this.minor = minor;
  }

  /**
   * Gets the sub component.
   *
   * @return the sub component
   */
  public Integer getSub() {
    return sub;
  }

  /**
   * Sets the sub component.
   *
   * @param sub the new sub component
   */
  public void setSub(Integer sub) {
    this.sub = sub;
  }

  /**
   * Returns this versionnumber as a string like "4.1.1"
   *
   * @return the string
   */
  @Override
  public String toString() {
    return "" + major + SEPARATOR + minor + SEPARATOR + sub;
  }

  /**
   * Compares a version number to another.
   *
   * @param other the other
   * @return the int
   */
  @Override
  public int compareTo(VersionNumber other) {
    if (other.getMajor() > getMajor()) {
      return -1;
    } else if (other.getMajor() < getMajor()) {
      return 1;
    } else if (other.getMinor() > getMinor()) {
      return -1;
    } else if (other.getMinor() < getMinor()) {
      return 1;
    } else if (other.getSub() > getSub()) {
      return -1;
    } else if (other.getSub() < getSub()) {
      return 1;
    } else {
      return 0;
    }
  }
}
