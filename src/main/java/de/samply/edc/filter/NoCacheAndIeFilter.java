/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.filter;

import java.io.IOException;
import javax.faces.application.ResourceHandler;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.HttpHeaders;

/**
 * A filter to always set certain header fields.
 *
 * <p>TODO: It should be considered to use http://showcase.omnifaces.org/filters/CacheControlFilter
 */
@WebFilter("/*")
public class NoCacheAndIeFilter implements Filter {

  public static final String CONTENT_TYPE_JAVA_ARCHIVE = "application/x-java-archive";
  public static final String HEADER_X_UA_COMPATIBLE = "X-UA-Compatible";
  public static final String HEADER_VALUE_IE_EDGE = "IE=edge";
  public static final String HEADER_VALUE_NO_CACHE_NO_STORE_MUST_REVALIDATE =
      "no-cache, no-store, must-revalidate";
  public static final String HEADER_VALUE_NO_CACHE = "no-cache";
  public static final String HEADER_CONTENT_TYPE_OPTIONS = "X-Content-Type-Options";
  public static final String HEADER_VALUE_NOSNIFF = "nosniff";

  /*
   * (non-Javadoc)
   *
   * @see javax.servlet.Filter#doFilter(javax.servlet.ServletRequest,
   * javax.servlet.ServletResponse, javax.servlet.FilterChain)
   */
  @Override
  public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
      throws IOException, ServletException {
    HttpServletRequest httpReq = (HttpServletRequest) request;
    HttpServletResponse httpRes = (HttpServletResponse) response;

    if (!httpReq
        .getRequestURI()
        .startsWith(httpReq.getContextPath() + ResourceHandler.RESOURCE_IDENTIFIER)) {
      // Skip JSF resources (CSS/JS/Images/etc)

      // also skip jar files
      if (httpReq.getContentType() == null
          || !httpReq.getContentType().equals(CONTENT_TYPE_JAVA_ARCHIVE)) {
        httpRes.setHeader(HEADER_X_UA_COMPATIBLE, HEADER_VALUE_IE_EDGE);
        httpRes.setHeader(
            HttpHeaders.CACHE_CONTROL, HEADER_VALUE_NO_CACHE_NO_STORE_MUST_REVALIDATE);
        httpRes.setHeader(com.google.common.net.HttpHeaders.PRAGMA, HEADER_VALUE_NO_CACHE);
        httpRes.setDateHeader(HttpHeaders.EXPIRES, 0);
      }
    }

    // check mime types always (yet only used in IE and Chrome)
    httpRes.setHeader(HEADER_CONTENT_TYPE_OPTIONS, HEADER_VALUE_NOSNIFF);

    //add HSTS Header
    httpRes.setHeader("Strict-Transport-Security", "max-age=31536000; includeSubDomains");
    // prevent clickjacking
    httpRes.setHeader("X-Frame-Options", "sameorigin");

    chain.doFilter(request, response);
  }

  /*
   * (non-Javadoc)
   *
   * @see javax.servlet.Filter#init(javax.servlet.FilterConfig)
   */
  @Override
  public void init(FilterConfig filterConfig) {}

  /*
   * (non-Javadoc)
   *
   * @see javax.servlet.Filter#destroy()
   */
  @Override
  public void destroy() {}
}
