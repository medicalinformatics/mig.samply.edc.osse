/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.phaselistener;

import de.samply.edc.control.AbstractSessionBean;
import de.samply.edc.osse.model.User;
import de.samply.edc.osse.utils.MultiFactorAuthBean;
import de.samply.edc.utils.Utils;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpSession;

/**
 * The listener interface for receiving authorization events. The class that is
 * interested in processing an authorization event implements this interface, and
 * the object created with that class is registered with a component using the
 * component's <code>addAuthorizationListener</code> method. When
 * the authorization event occurs, that object's appropriate
 * method is invoked.
 * This listener is used to ensure that pages are only displayed
 * if a user is logged into the system.
 * If not, it will refer back to the login page.
 *
 * @see AuthorizationEvent
 */
public class AuthorizationListener implements PhaseListener {

  public static final String PAGE_LOGIN = "login.xhtml";
  public static final String PAGE_NO_JAVASCRIPT = "nojavascript.xhtml";
  public static final String PAGE_PATIENTLIST_SEARCH = "patientlistSearchView.xhtml";
  public static final String PAGE_ACTIVATE = "activate.xhtml";
  public static final String PAGE_TEST = "test.xhtml";
  public static final String PAGE_FORGOTPASSWORD = "forgotpassword.xhtml";
  public static final String CURRENT_OBJECT_USER = "user";
  public static final String OUTCOME_LOGIN = "loginPage";
  public static final String BEAN_NAME_SESSIONBEAN = "sessionBean";

  /*
   * (non-Javadoc)
   *
   * @see
   * javax.faces.event.PhaseListener#afterPhase(javax.faces.event.PhaseEvent)
   */
  @Override
  public void afterPhase(PhaseEvent event) {

    FacesContext facesContext = event.getFacesContext();

    boolean isLoginPage = Utils.isCurrentPage(PAGE_LOGIN, facesContext);
    boolean isnoJsPage = Utils.isCurrentPage(PAGE_NO_JAVASCRIPT, facesContext);
    boolean isSearchViewPage = Utils.isCurrentPage(PAGE_PATIENTLIST_SEARCH, facesContext);

    //do not check for a valid login on certain pages
    if (Utils.isCurrentPage(PAGE_ACTIVATE, facesContext)
        || Utils.isCurrentPage(PAGE_TEST, facesContext)
        || Utils.isCurrentPage(PAGE_FORGOTPASSWORD, facesContext)
        || Utils.isCurrentPage("login_mfa", facesContext)
        || isSearchViewPage
        || isLoginPage
        || isnoJsPage) {
      return;
    }

    HttpSession session = (HttpSession) facesContext.getExternalContext().getSession(false);
    AbstractSessionBean sessionBean =
        (AbstractSessionBean) Utils.getBackingBean(BEAN_NAME_SESSIONBEAN, facesContext);

    //if no session exists logout
    if (session == null) {
      sessionBean.getDatabase().logout();
      Utils.navigateToPage(OUTCOME_LOGIN, facesContext, false);
    }
    //if username is not set i.e. login was not okay logout
    if (sessionBean.getCurrentObject(CURRENT_OBJECT_USER) == null) {
      sessionBean.getDatabase().logout();
      Utils.navigateToPage(OUTCOME_LOGIN, facesContext, false);
    }
    //if user needs mfa and it was not set logout
    if (isMfaActiveForUser(sessionBean) && sessionBean.getCurrentObject("mfa_successful") == null) {
      sessionBean.getDatabase().logout();
      Utils.navigateToPage(OUTCOME_LOGIN, facesContext, false);
    }

  }


  private boolean isMfaActiveForUser(AbstractSessionBean sessionBean) {
    if (!MultiFactorAuthBean.isToTpActived()) {
      return false;
    }
    return ((User) sessionBean.getCurrentObject(CURRENT_OBJECT_USER)).getResource()
        .getProperty(MultiFactorAuthBean.authMfaAuthenticator) != null
        && ((User) sessionBean.getCurrentObject(
        CURRENT_OBJECT_USER)).getResource().getProperty(MultiFactorAuthBean.authMfaAuthenticator)
        .asBoolean();
  }


  /*
   * (non-Javadoc)
   *
   * @see
   * javax.faces.event.PhaseListener#beforePhase(javax.faces.event.PhaseEvent)
   */
  @Override
  public void beforePhase(PhaseEvent event) {}

  /*
   * (non-Javadoc)
   *
   * @see javax.faces.event.PhaseListener#getPhaseId()
   */
  @Override
  public PhaseId getPhaseId() {
    return PhaseId.RESTORE_VIEW;
  }
}
