/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.model;

import java.io.Serializable;

/** Menu entry model for context menus. */
public class ContextMenuEntry implements Serializable {

  /** The link, icon, text of the context menu entry. */
  private String link;

  private String icon;
  private String text;

  /** Data for a confirmation dialog. */
  private String confirmTitle;

  private String confirmText;
  private String confirmYes;
  private String confirmNo;

  /**
   * Boolean to decide whether to use a confirmation dialog when clicking on a context menu entry.
   */
  private Boolean sure;

  /** Boolean to decide whether to import all Data when clicking on a context menu entry. */
  // private Boolean allData = false;

  /**
   * Instantiates a new context menu entry.
   *
   * @param glink The URL link
   * @param gicon The icon to display
   * @param gtext The text to display
   * @param gsure Has this entry a confirmation dialog?
   * @param gcTitle Confirmation dialog title
   * @param gcText Confirmation dialog text
   * @param gcYes Confirmation dialog accept button text
   * @param gcNo Confirmation dialog deny button text
   */
  public ContextMenuEntry(
      String glink,
      String gicon,
      String gtext,
      Boolean gsure,
      String gcTitle,
      String gcText,
      String gcYes,
      String gcNo) {
    this.link = glink;
    this.icon = gicon;
    this.text = gtext;
    this.sure = gsure;
    confirmTitle = gcTitle;
    confirmText = gcText;
    confirmYes = gcYes;
    confirmNo = gcNo;
  }

  /**
   * Confirmation needed yes/no.
   *
   * @return true if confirmation is needed
   */
  public Boolean getSure() {
    return sure;
  }

  /**
   * Confirmation needed yes/no.
   *
   * @param sure whether confirmation is needed
   */
  public void setSure(Boolean sure) {
    this.sure = sure;
  }

  /**
   * Gets the confirm title.
   *
   * @return the confirm title
   */
  public String getConfirmTitle() {
    return confirmTitle;
  }

  /**
   * Sets the confirm title.
   *
   * @param confirmTitle the new confirm title
   */
  public void setConfirmTitle(String confirmTitle) {
    this.confirmTitle = confirmTitle;
  }

  /**
   * Gets the confirm text.
   *
   * @return the confirm text
   */
  public String getConfirmText() {
    return confirmText;
  }

  /**
   * Sets the confirm text.
   *
   * @param confirmText the new confirm text
   */
  public void setConfirmText(String confirmText) {
    this.confirmText = confirmText;
  }

  /**
   * Gets the confirm accept button text.
   *
   * @return the confirm accept button text
   */
  public String getConfirmYes() {
    return confirmYes;
  }

  /**
   * Sets the confirm accept button text.
   *
   * @param confirmYes the new confirm accept button text
   */
  public void setConfirmYes(String confirmYes) {
    this.confirmYes = confirmYes;
  }

  /**
   * Gets the confirm deny button text.
   *
   * @return the confirm deny button text
   */
  public String getConfirmNo() {
    return confirmNo;
  }

  /**
   * Sets the confirm deny button text.
   *
   * @param confirmNo the new confirm deny button text
   */
  public void setConfirmNo(String confirmNo) {
    this.confirmNo = confirmNo;
  }

  /**
   * Gets the link.
   *
   * @return the link
   */
  public String getLink() {
    return link;
  }

  /**
   * Sets the link.
   *
   * @param link the new link
   */
  public void setLink(String link) {
    this.link = link;
  }

  /**
   * Gets the icon.
   *
   * @return the icon
   */
  public String getIcon() {
    return icon;
  }

  /**
   * Sets the icon.
   *
   * @param icon the new icon
   */
  public void setIcon(String icon) {
    this.icon = icon;
  }

  /**
   * Gets the text.
   *
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * Sets the text.
   *
   * @param text the new text
   */
  public void setText(String text) {
    this.text = text;
  }
}
