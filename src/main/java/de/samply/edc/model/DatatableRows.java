/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/** Model class for a list of datatable rows. */
public class DatatableRows implements Serializable {

  /** The rows. */
  private List<DatatableRow> rows;

  /** The sorting order. */
  private boolean sortAscending = true;

  /** Create an empty instance (i.e. with an empty list of rows). */
  public DatatableRows() {
    rows = new ArrayList<>();
  }

  /**
   * getRows used to be called getColumns in the past as there may be still forms out there which
   * use the old naming, we need to keep this for compatibility reasons.
   *
   * @return the columns
   */
  @Deprecated
  public List<DatatableRow> getColumns() {
    return rows;
  }

  /**
   * setRows used to be called setColumns in the past as there may be still forms out there which
   * use the old naming, we need to keep this for compatibility reasons.
   *
   * @param rows the new columns
   */
  @Deprecated
  public void setColumns(List<DatatableRow> rows) {
    this.rows = rows;
  }

  /**
   * Gets the rows represented by this instance.
   *
   * @return the rows
   */
  public List<DatatableRow> getRows() {
    return rows;
  }

  /**
   * Sets the rows.
   *
   * @param rows the new rows
   */
  public void setRows(List<DatatableRow> rows) {
    this.rows = rows;
  }

  /**
   * Adds a row to this instance.
   *
   * @param row the row to add.
   */
  public void add(DatatableRow row) {
    rows.add(row);
  }

  /**
   * Removes a given row object from this instance.
   *
   * @param row the row to remove
   */
  public void remove(DatatableRow row) {
    rows.remove(row);
  }

  /*
   * (non-Javadoc)
   *
   * @see java.lang.Object#toString()
   */
  @Override
  public String toString() {
    StringBuilder text = new StringBuilder();
    for (DatatableRow c : rows) {
      text.append(c.toString()).append(", ");
    }

    if ("".equals(text.toString())) {
      text = new StringBuilder("Empty Row, ");
    }

    text = new StringBuilder(text.substring(0, text.length() - 2));

    return text.toString();
  }

  /**
   * Sorts the rows by column key.
   *
   * @param key the column key
   * @return winning object
   */
  public Object sortyBy(final String key) {
    // TODO: check for date strings (stringformat by MDR), numbers

    if (sortAscending) {
      Collections.sort(
          rows,
          new Comparator<DatatableRow>() {

            @Override
            public int compare(DatatableRow o1, DatatableRow o2) {
              if (o1.getColumns().get(key) == null) {
                return -1;
              }

              return ((String) o1.getColumns().get(key))
                  .compareTo((String) o2.getColumns().get(key));
            }
          });
      sortAscending = false;
    } else {
      Collections.sort(
          rows,
          new Comparator<DatatableRow>() {

            @Override
            public int compare(DatatableRow o1, DatatableRow o2) {
              if (o2.getColumns().get(key) == null) {
                return -1;
              }

              return ((String) o2.getColumns().get(key))
                  .compareTo((String) o1.getColumns().get(key));
            }
          });
      sortAscending = true;
    }
    return null;
  }
}
