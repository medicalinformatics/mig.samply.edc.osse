/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.model;

import java.io.Serializable;

/** Model Class for items in our menu (in design the tabs for the forms with their names). */
public class MenuItem implements Serializable {

  /** The url the link leads to. */
  private String url;

  /** The text displayed. */
  private String text;

  /** Is the entry selected (form currently displayed)? Used for css style. */
  private String selected;

  /** Is it an episode form entry (called subitem for historical reasons). */
  private String subitem;

  /** The icon to display. */
  private String icon;

  /** The icon mouseover title text. */
  private String iconText;

  /**
   * Instantiates a new menu item.
   *
   * @param url the url
   * @param text the text
   * @param selected the selected
   * @param subitem the subitem
   * @param icon the icon
   * @param iconText the icon text
   */
  public MenuItem(
      String url, String text, String selected, String subitem, String icon, String iconText) {
    this.url = url;
    this.text = text;
    this.selected = selected;
    this.subitem = subitem;
    this.icon = icon;
    this.iconText = iconText;
  }

  /**
   * Gets the selected css style class text.
   *
   * @return the selected
   */
  public String getSelected() {
    return selected;
  }

  /**
   * Sets the selected css style class text.
   *
   * @param selected the new selected
   */
  public void setSelected(String selected) {
    this.selected = selected;
  }

  /**
   * Gets the subitem.
   *
   * @return the subitem
   */
  public String getSubitem() {
    return subitem;
  }

  /**
   * Sets the subitem.
   *
   * @param subitem the new subitem
   */
  public void setSubitem(String subitem) {
    this.subitem = subitem;
  }

  /**
   * Gets the url.
   *
   * @return the url
   */
  public String getUrl() {
    return url;
  }

  /**
   * Sets the url.
   *
   * @param url the new url
   */
  public void setUrl(String url) {
    this.url = url;
  }

  /**
   * Gets the text.
   *
   * @return the text
   */
  public String getText() {
    return text;
  }

  /**
   * Sets the text.
   *
   * @param text the new text
   */
  public void setText(String text) {
    this.text = text;
  }

  /**
   * Gets the icon.
   *
   * @return the icon
   */
  public String getIcon() {
    return icon;
  }

  /**
   * Sets the icon.
   *
   * @param icon the new icon
   */
  public void setIcon(String icon) {
    this.icon = icon;
  }

  /**
   * Gets the icon text.
   *
   * @return the icon text
   */
  public String getIconText() {
    return iconText;
  }

  /**
   * Sets the icon text.
   *
   * @param iconText the new icon text
   */
  public void setIconText(String iconText) {
    this.iconText = iconText;
  }
}
