/*
 * Copyright (C) 2015 Working Group on Joint Research, University Medical Center Mainz
 * Copyright (C) since 2016 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.edc.model;

import java.io.Serializable;
import java.util.HashMap;

/** Model class for form entries which are to be stored in a hash map. */
public class Formvars implements Serializable {

  /** Entries made into the edit page of a form. */
  private HashMap<String, Object> entries = new HashMap<>();

  /**
   * Delete entry.
   *
   * @param key the key
   */
  public void deleteEntry(String key) {
    entries.remove(key);
  }

  /**
   * Gets the entries.
   *
   * @return the entries
   */
  public HashMap<String, Object> getEntries() {
    return entries;
  }

  /**
   * Sets the entries.
   *
   * @param entries the entries
   */
  public void setEntries(HashMap<String, Object> entries) {
    this.entries = entries;
  }

  /**
   * Gets the entry with the given key.
   *
   * @param key the key
   * @return the entry or an empty String if no entry exists for this key.
   */
  public Object getEntry(String key) {
    if (entries.containsKey(key)) {
      return entries.get(key);
    } else {
      return "";
    }
  }

  /**
   * Sets the entry.
   *
   * @param key the key
   * @param value the value
   */
  public void setEntry(String key, Object value) {
    entries.put(key, value);
  }

  /** Clear entries. Empties the underlying map. */
  public void clearEntries() {
    entries.clear();
  }
}
