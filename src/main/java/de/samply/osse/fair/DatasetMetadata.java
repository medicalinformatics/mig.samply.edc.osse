/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.osse.fair;

/**
 * The Dataset Metadata can have a number of Distribution Metadata. The Dataset Metadata Retrieval
 * function lead to the distribution Metadata Retrieval function by containing the URIs of the
 * Distribution Metadata in the Dataset Metadata content. Also, the Dataset Metadata can have a Data
 * Record Metadata. The Dataset Metadata retrieval function can lead to the Data Record Metadata
 * Retrieval function by appending the URIs of the Datarecord Metadata in the Dataset Metadata
 * content
 */
public class DatasetMetadata {

  private String title; // title of the dataset
  private String identifier; //
  private String version;
  private String description;
  private String language = "http://id.loc.gov/vocabulary/iso639-1/en";
  private String rdfLabel;
  private String contactPoint;
  private String keyword;
  private String distribution;
  private String modified;
  private String issued;
  private String theme;

  public String getModified() {
    return modified;
  }

  public void setModified(String modified) {
    this.modified = modified;
  }

  public String getIssued() {
    return issued;
  }

  public void setIssued(String issued) {
    this.issued = issued;
  }

  public String getTheme() {
    return theme;
  }

  public void setTheme(String theme) {
    this.theme = theme;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getRdfLabel() {
    return rdfLabel;
  }

  public void setRdfLabel(String rdfLabel) {
    this.rdfLabel = rdfLabel;
  }

  public String getContactPoint() {
    return contactPoint;
  }

  public void setContactPoint(String contactPoint) {
    this.contactPoint = contactPoint;
  }

  public String getKeyword() {
    return keyword;
  }

  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }

  public String getDistribution() {
    return distribution;
  }

  public void setDistribution(String distribution) {
    this.distribution = distribution;
  }
}
