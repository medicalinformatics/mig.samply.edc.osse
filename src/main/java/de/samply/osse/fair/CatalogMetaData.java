/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.osse.fair;

/**
 * The catalog is composed by a number of Dataset Metadata. The Catalog Metadata Retrieval function
 * lead to the Dataset Metadata function by containing the URIs of the Dataset Metadata in the
 * Catalog Metadata content
 */
public class CatalogMetaData {

  private String title; // The name of the Catalog
  private String identifier; // A unambigous and persistent identifier to the catalog
  private String version; // version
  private String description; // short description
  private String language = "http://id.loc.gov/vocabulary/iso639-1/en";
  private String license = "http://purl.org/NET/rdflicense/MIT1.0";
  private String rdfLabel; //
  private String website; // website of your registry
  private String dataset; // URI of the dataset
  private String themeTaxonomy = "http://dbpedia.org/resource/Disease_registry";
  private String issued; // date of issue
  private String modified; // date of modify

  public String getIssued() {
    return issued;
  }

  public void setIssued(String issued) {
    this.issued = issued;
  }

  public String getModified() {
    return modified;
  }

  public void setModified(String modified) {
    this.modified = modified;
  }

  public String getTitle() {
    return title;
  }

  public void setTitle(String title) {
    this.title = title;
  }

  public String getIdentifier() {
    return identifier;
  }

  public void setIdentifier(String identifier) {
    this.identifier = identifier;
  }

  public String getVersion() {
    return version;
  }

  public void setVersion(String version) {
    this.version = version;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getLanguage() {
    return language;
  }

  public void setLanguage(String language) {
    this.language = language;
  }

  public String getLicense() {
    return license;
  }

  public void setLicense(String license) {
    this.license = license;
  }

  public String getRdfLabel() {
    return rdfLabel;
  }

  public void setRdfLabel(String rdfLabel) {
    this.rdfLabel = rdfLabel;
  }

  public String getWebsite() {
    return website;
  }

  public void setWebsite(String website) {
    this.website = website;
  }

  public String getDataset() {
    return dataset;
  }

  public void setDataset(String dataset) {
    this.dataset = dataset;
  }

  public String getThemeTaxonomy() {
    return themeTaxonomy;
  }

  public void setThemeTaxonomy(String themeTaxonomy) {
    this.themeTaxonomy = themeTaxonomy;
  }
}
