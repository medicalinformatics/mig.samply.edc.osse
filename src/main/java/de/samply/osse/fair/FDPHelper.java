/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.osse.fair;

import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.store.JSONResource;
import de.samply.store.exceptions.DatabaseException;
import de.samply.store.osse.OSSEModel;

/** Helper Methods for access to OSSE-Configuration and other stuff. */
public class FdpHelper {

  /**
   * Reads the OSSEConfig without JSF Context.
   */
  public JSONResource getOsseConfig() throws DatabaseException {

    JSONResource osseConfig = null;
    OSSEModel model = null;
    model = new OSSEModel(ApplicationBean.getConfigFilenameBackend());
    // get the Config from the model
    osseConfig = model.getConfig("osse");
    return osseConfig;
  }

  /**
   * Reads the Configuration for the fdpmetadata out of the OSSECOnfig.
   */
  public FdpMetaData getFdpMetaDataFromConfig(JSONResource osseConfig) {

    FdpMetaData fdpMetaData = new FdpMetaData();
    MdrHelper mdrHelper = new MdrHelper();

    fdpMetaData.setTitle(osseConfig.getProperty(Vocabulary.Config.registryName).getValue());
    fdpMetaData.setIdentifier(fdpMetaData.getTitle() + ":" + mdrHelper.getNameSpace(osseConfig));
    fdpMetaData.setVersion(osseConfig.getProperty("fair.fdpmetadata.version").getValue());
    fdpMetaData.setDescription(osseConfig.getProperty("fair.fdpmetadata.description").getValue());
    fdpMetaData.setPublisher(osseConfig.getProperty("fair.fdpmetadata.publisher").getValue());
    fdpMetaData.setRegistryUrl(osseConfig.getProperty("fair.fdpmetadata.url").getValue());
    fdpMetaData.setWebsite(osseConfig.getProperty("fair.fdpmetadata.website").getValue());
    fdpMetaData.setModified(osseConfig.getProperty("fair.fdpmetadata.modified").getValue());
    fdpMetaData.setIssued(osseConfig.getProperty("fair.fdpmetadata.issued").getValue());

    String language = osseConfig.getProperty("fair.fdpmetadata.language").getValue();
    if (language.equals("German")) {
      fdpMetaData.setLanguage("http://id.loc.gov/vocabulary/iso639-1/de");
    } else {
      fdpMetaData.setLanguage("http://id.loc.gov/vocabulary/iso639-1/en");
    }

    return fdpMetaData;
  }

  /**
   * * Reads the Configuration for the catalogmetadata out of the OSSECOnfig.
   */
  public CatalogMetaData getCatalogMetaDataFromConfig(JSONResource osseConfig) {

    CatalogMetaData catalogMetaData = new CatalogMetaData();
    catalogMetaData.setTitle(osseConfig.getProperty(Vocabulary.Config.registryName).getValue());
    catalogMetaData.setIdentifier(catalogMetaData.getTitle());
    catalogMetaData.setDescription(osseConfig.getProperty("fair.catalog.description").getValue());
    catalogMetaData.setVersion(osseConfig.getProperty("fair.catalog.version").getValue());
    catalogMetaData.setModified(osseConfig.getProperty("fair.catalog.modified").getValue());
    catalogMetaData.setIssued(osseConfig.getProperty("fair.catalog.issued").getValue());

    return catalogMetaData;
  }

  /**
   * * Reads the Configuration for the datasetmetadata out of the OSSEConfig.
   */
  public DatasetMetadata getDatasetMetaDataFromConfig(JSONResource osseConfig) {

    DatasetMetadata datasetMetadata = new DatasetMetadata();
    datasetMetadata.setDescription(osseConfig.getProperty("fair.dataset.description").getValue());
    datasetMetadata.setTitle(osseConfig.getProperty(Vocabulary.Config.registryName).getValue());
    datasetMetadata.setVersion(osseConfig.getProperty("fair.dataset.version").getValue());
    datasetMetadata.setContactPoint(osseConfig.getProperty("fair.dataset.contactpoint").getValue());
    datasetMetadata.setKeyword(osseConfig.getProperty("fair.dataset.keyword").getValue());
    datasetMetadata.setTheme(osseConfig.getProperty("fair.dataset.theme").getValue());
    datasetMetadata.setModified(osseConfig.getProperty("fair.dataset.modified").getValue());
    datasetMetadata.setIssued(osseConfig.getProperty("fair.dataset.issued").getValue());

    return datasetMetadata;
  }

  /**
   * * Reads the Configuration for the distributionmetadata out of the OSSEConfig.
   */
  public DistributionMetadata getDistributionMetaDataFromConfig(JSONResource osseConfig) {

    DistributionMetadata distributionMetadata = new DistributionMetadata();
    distributionMetadata.setDescription(
        osseConfig.getProperty("fair.distribution.description").getValue());
    distributionMetadata.setVersion(osseConfig.getProperty("fair.distribution.version").getValue());
    distributionMetadata.setTitle(
        osseConfig.getProperty(Vocabulary.Config.registryName).getValue());
    distributionMetadata.setMediatype(
        osseConfig.getProperty("fair.distribution.mediatype").getValue());
    distributionMetadata.setFormat(osseConfig.getProperty("fair.distribution.format").getValue());
    distributionMetadata.setModified(
        osseConfig.getProperty("fair.distribution.modified").getValue());
    distributionMetadata.setIssued(osseConfig.getProperty("fair.distribution.issued").getValue());

    return distributionMetadata;
  }

  /**
   * * Checks if the Fair Data Point is activated or not.
   */
  public boolean checkFdpActivation(JSONResource osseConfig) {

    boolean activated = true;
    String activation = osseConfig.getProperty("fair.fdp.activation").getValue();

    if (activation.equals("Deactivate")) {

      activated = false;
    }

    return activated;
  }
}
