/*
 * Copyright (C) 2017 The Samply Community
 *
 * This program is free software; you can redistribute it and/or modify it under the terms of the
 * GNU Affero General Public License as published by the Free Software Foundation; either version 3
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License along with this
 * program; if not, see <http://www.gnu.org/licenses>.
 *
 * Additional permission under GNU GPL version 3 section 7:
 *
 * If you modify this Program, or any covered work, by linking or combining it with Jersey
 * (https://jersey.java.net) (or a modified version of that library), containing parts covered by
 * the terms of the General Public License, version 2.0, the licensors of this Program grant you
 * additional permission to convey the resulting work.
 */

package de.samply.osse.fair;

import de.samply.auth.rest.AccessTokenDto;
import de.samply.common.http.HttpConnector;
import de.samply.common.mdrclient.MdrClient;
import de.samply.common.mdrclient.domain.Result;
import de.samply.common.mdrclient.domain.ResultList;
import de.samply.edc.catalog.Vocabulary;
import de.samply.edc.osse.auth.Auth;
import de.samply.edc.osse.control.ApplicationBean;
import de.samply.edc.osse.model.MdrKey;
import de.samply.edc.utils.Utils;
import de.samply.store.JSONResource;
import java.util.List;
import javax.ws.rs.client.Client;
import org.apache.commons.configuration.Configuration;

/** Helper Class for FAIR-Data point to get directly informations from the MDR. */
public class MdrHelper {

  /**
   * Returns the Datalements from the MDR.
   */
  public ResultList getResultListMdr(JSONResource osseConfig) throws Exception {
    Configuration config = Utils.getConfig();
    ResultList resultList = new ResultList();

    // set Proxy settings
    String[] proxySettings = {
      Vocabulary.Config.Standard.proxyHTTPHost,
      Vocabulary.Config.Standard.proxyHTTPSHost,
      Vocabulary.Config.Standard.proxyHTTPPort,
      Vocabulary.Config.Standard.proxyHTTPSPort,
      Vocabulary.Config.Standard.proxyHTTPUsername,
      Vocabulary.Config.Standard.proxyHTTPPassword,
      Vocabulary.Config.Standard.proxyHTTPSUsername,
      Vocabulary.Config.Standard.proxyHTTPSPassword,
      Vocabulary.Config.Standard.proxyRealm
    };

    // inject proxy settings, so they dont get overloaded in the next step
    for (String proxySetting : proxySettings) {
      osseConfig.setProperty(proxySetting, config.getString(proxySetting));
    }

    // inject OSSEConfig into Config, so both ways of configuration are
    // returning the same values
    for (String key : osseConfig.getDefinedProperties()) {
      config.setProperty(key, osseConfig.getProperty(key).getValue());
    }

    // get AuthUserID from Config
    String authUserId = osseConfig.getProperty(Vocabulary.Config.Auth.UserId).getValue();
    AccessTokenDto accessToken;
    HttpConnector httpConnector = ApplicationBean.getHttpConnector();

    if (httpConnector != null) {
      Client client = httpConnector.getJerseyClientForHTTPS(false);
      accessToken = Auth.getAccessToken(client, config);

      Client mdrJerseyClient =
          httpConnector.getJerseyClient(
              osseConfig.getProperty(Vocabulary.Config.Mdr.REST).getValue(), false);

      // Init MdrFacesClient
      MdrClient mdrClient =
          new MdrClient(
              osseConfig.getProperty(Vocabulary.Config.Mdr.REST).getValue(), mdrJerseyClient);

      // Create mdr key to Init MDR-Client

      MdrKey key = new MdrKey();
      key.initMdrClient(mdrClient, accessToken, authUserId);

      // get all data elements
      resultList =
          key.getMdrClient().getUserRootElements("en", accessToken.getAccessToken(), authUserId);
    } else {
      Utils.getLogger().error("Couldn't establish http connection.");
    }

    return resultList;
  }

  /**
   * * Returns the namespace of the registry off the MDR.
   */
  public String getNameSpace(JSONResource osseConfig) {

    String namespace = "";
    try {
      ResultList resultList = getResultListMdr(osseConfig);
      List<Result> results = resultList.getResults();
      String id = results.get(0).getId();
      MdrKey mdrKey = new MdrKey(id);
      namespace = mdrKey.getNamespace();
    } catch (Exception e) {
      Utils.getLogger().error("Unable to get namespace from the MDR",e);
    }
    return namespace;
  }
}
