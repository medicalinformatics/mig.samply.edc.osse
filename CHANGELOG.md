# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## [3.3.2] - 2025-01-14
### Changed
- Updated PrimeFaces themes to 1.0.10

## [3.3.1] - 2023-06-20
### Fixed
- Login with wrong username lead to exception and message on failed login did not appear.
- Users with a password that contains certain special characters were not able to log in.

## [3.3.0] - 2023-04-26
### Added
- Deactivate login for a single user by an administrator.
- Added dummy files for custom CSS and JS changes.
- Added support for multi-factor authentication based on TOTP.

### Changed
- The project de.mig.samply.edc was incorporated into de.mig.samply.edc-osse due to ease of maintenance and development.
- Temporarily stored episode form values are no longer accessible across episodes.
- Relaxed database constraint to allow PRO and default forms to exist simultaneously.
- It's now possible to configure two different URLs to access the Mainzelliste if necessary. One for generic or public access and one specifically for the EDC.
- Layout of repeatable data elements has been improved.
- The configuration for the form editor now uses two separate URLs. One for the login and forward to the form editor's frontend and one for the REST API of the form editor.

### Fixed
- Added token ID to callback URL for compatibility with Mainzelliste 1.9 and later.
- Hiding a repeatable element with elements in it via form conditions lead to empty rows being stored.
- Fixed rare bug which could prevent loading of forms in a different language.
- Form conditions within repeatable record tables did not work for newly added rows.
- Added missing translations.
- In no-IDAT mode, the patient table could not be sorted by the local identifier.
- Fixed anchoring of save and cancel button in basic and episode forms.
- Fixed a bug that could prevent patient loading if the optional episode text was missing in the database.

### Removed
- The configuration option for a localhost port number to access the Mainzelliste has been removed in favour of an additional full URL specifically to be used by the EDC.

### Security
- Updated XStream, fixing several CVEs.
- Updated FasterXML components (CVE-2022-42003, CVE-2022-42004, CVE-2020-36518).
- Updated Apache commons-codec, commons-fileupload.
- Updated Apache Jena.

## [3.2.0] - 2022-11-10
### Added
- Framework functions to be used as helpers in project specific dashboards.
- New modal of the notification "Session timeout".
- Added invisible row index to table view of repeatable data elements and records so that by default the rows appear in the order in which they have been added.
- `use_keywords` header for the import API. Set to true to allow keywords.
- Option to import data into the latest episode of a patient via `#latest#` keyword as episode name in the import XML file.
- Localization support for the dashboard configuration.
- Configurable links for the dashboard.
- New index episode pattern i.e. simple integer.
- Data elements in the forms appear deactivated if the user has no data entry permission.
- The admin view shows the internal IDs of locations and users.
- New JavaScript methods to access a patient's age in days.
- Patient's age at episode date in different formats accessible in JavaScript for more episode date formats.
- Patient's age at given date in different formats accessible in JavaScript.
- Added a function to copy OSSE version information to the clipboard.
- Forms with an outdated version can be upgraded to the current version.
- Load spinner for adding repeatable entries.
- An existing patient's place of birth can be edited if the Mainzelliste supports it.
- Dialog to search for HPO terms, activated by adding a slot `renderType` with value `catalog_HPO` to a data element in the MDR.
- Dialog to search for Orpha codes, activated adding a slot `renderType` with value `catalog_ORPHA` to a data element in the MDR.

### Changed
- Adjusted heights of dashboard panels.
- Loading of xss processing script optimized to load only once.
- Improved startup performance.
- Extended length of episode description.
- Patient's age at episode date removed from local storage, instead calculated as for a given date.
- Forms are no longer created and saved in the database just by viewing them, i.e. a form stays in status "unused" as long as it is not intentionally saved.
- Added constraint to database to prevent storing duplicated forms.

### Removed
- Removed "Overview" menu item in the top navigation bar when the dashboard is not active.
- Option to import data from the previous episode is only available when creating a new one.

### Fixed
- The notice on unsaved changes was sometimes shown even when no changes had been made in a form.
- Show correct form status after saving a previously unused form.
- Disabled the option to transfer data from a previous episode when there is no episode available.
- Spelling of some German labels.
- The sorting of the numbers of patients per location in the table on the dashboard is correct.
- Fixed processing of records in statistic framework.
- Users get sent back to the login page after a session timeout.
- Fixed the browser's history after switching the language.
- Repeatable entries with validation errors will no longer be deleted by adding a new entry.
- The triangle episode indicator automatically hides if you click on < or > in Episode browsing carousel.
- Fixed the number of deleted patients in the patient list.
- Xss filter script sometimes failed to load.
- Fixed an issue with the Mainzelliste connection that could lead to missing IDAT.
- The Patient's age at the date of the episode is removed from local storage if episode date can't be parsed.
- Fixed missing dashboard link after switching the user role.
- When importing all forms from the previous episode, the form version property was set to 1 regardless of the correct version.
- Fixed an exception that could prevent OSSE from being initialized correctly.
- Fix Javascript errors for some combinations of form conditions involving stored values from other forms.
- The use of a deprecated global variable caused an error while processing form conditions in some cases.

### Security
- When users change their password, the new one has to be different from the old one.

## [3.1.8] - 2022-07-22
### Security
- Users might have been able to access a patient's IDAT without the proper permission.

## [3.1.7] - 2022-04-01
### Security
- Updated PostgreSQL JDBC Driver (CVE-2022-21724)

## [3.1.6] - 2022-01-11
### Security
- Updated Log4j (CVE-2021-44832)

## [3.1.5] - 2021-12-18
### Security
- Updated Log4j (CVE-2021-45105)

## [3.1.4] - 2021-12-15
### Security
- Updated Log4j (CVE-2021-45046)

## [3.1.3] - 2021-12-10
### Security
- Updated Log4j to fix potential remote code execution vulnerability.

## [3.1.2] - 2021-06-07
### Fixed
- Temporary values from hidden data elements remained after saving a form.
- Values in a repeatable record were saved even when the record was hidden by form conditions.
- IDAT resolving failed if edc did not use root path of Tomcat.

## [3.1.1] - 2021-05-21
### Fixed
- Due to a dependency issue, an error occurred when creating a patient.

## [3.1.0] - 2021-05-18
### Added
- Admins can now generate a random API key for the import interface.
- A new REST endpoint provides a quick way to determine whether any users are currently logged in.
- Admins can now (de)activate the dashboard and customize it.
- Admins can now customize the login.
- New JavaScript methods allow access to the patient's age in the frontend and via conditional visibility of data elements.
- A new form allows admins to change the share.client API keys.
- The patient password letter is now also available in German.
- Admins can now set an imprint and privacy link to show on all webpages.
- Repeatable records can be rendered as tabs by setting a slot `renderType` with the value `TABS` on the record.
- Data elements can be made read-only, i.e. such elements cannot be changed by the user, but their values are still stored with the form.
- Added a direct link to the MDR on forms admin page.

### Changed
- Improved error reporting and logging.
- Date fields are no longer automatically filled with the current date.
- Version string is no longer visible in login screen. It now shows on admin pages.
- Patients can't transfer form data from one episode to the next anymore.
- Catalogues are now fetched dynamically, if supported by the MDR.

### Fixed
- Fixed a silent NullPointerException during first start of the registry.
- Fixed a problem with permitted values data elements not properly being hidden and shown using conditional visibility.
- Fixed a problem with values of hidden comboboxes being saved.
- Fixed warning before leaving a page after unsuccessfully trying to save a form.
- The role selection list and the user profile menu could disappear on small screens or with high zoom level.
- Form comments were not visible.
- A newly added form comment was not visible until reloading the page.
- Fixed the format of episode timestamps when transferring data from a previous episode.
- Fixed date format `MMM d, yyyy` from mdrFaces.
- Fixed the language of the error message after an unsuccessful login attempt.
- Fixed incorrect api response on version and uploadstats endpoints.
- Fixed missing api key check in some end points.
- An error message did not appear when replacing forms.
- Fixed replacing forms with their newer versions in admin view.
- Fixed language change after refreshing a patient form page.
- Fixed redirection of the user back to patient list page.
- Fixed a possible error when un-deleting a patient.
- Fixed some German translations in the patient list.
- Fixed an error when creating patients in a registry without any forms.
- Deleting forms in the form editor might prevent users from logging in.

### Removed
- Checkboxes (boolean data elements) can no longer have the state "indeterminate".

### Security
- Users with global read access in one of their roles could still see data of other locations' patients when switching to a role without global read access.
- Location admins can no longer see users from other locations.
- Added sanitization for Mainzelliste response in browser to prevent XSS (js from jsxss.com).

## [3.0.0] - 2020-05-29
### Added
- Possibility to make some modifications to forms (conditional hiding/showing, disabling of data elements, setting default values, temporarily store values) by editing a JavaScript object. See file ```formConditions.js``` for an example.
- Form HTML files now contain form id and version as HTML attributes.
- When creating a new episode, it's possible to copy all values from the previous episode.
- A data element's unit of measure is now visible next to input fields
- Form ID visible as tooltip of "Version" string in a form's header.
- When leaving a form page without saving, a confirmation message pops up.
- The patient identifier (PID) is now also shown when OSSE is configured to use local identifiers instead of IDAT.
- A new dashboard can be configured to show after logging in. The dashboard contains links to the most important functions and is customizable to some extent. There's also an example for utilizing a JavaScript charting library to visualize data.

### Changed
- edc-osse now requires Java 8.
- Transferring data from a previous episode's form to a new one now works even if versions of forms, records or data elements differ.
- "Transfer 'ALL' forms' data from last episode" also functions properly, i.e. same way as individual form data transfer works.
- The browser won't show autocomplete suggestions in medical forms anymore.
- Tab of currently selected episode has a slightly different appearance from the other tabs.
- "Catalogue Report" tab removed from admin view, since RoR isn't running anymore.
- Items of permitted values data elements are now sorted alphabetically by their value instead of seemingly being in no particular order.
- Users can now log in to the EDC even if none of the central registry components (MDR, Formeditor, Authentication Server) are available.
- A random key for the import API can now be generated automatically.

### Fixed
- When editing an existing episode, the German date field label read as if a new episode was being created.
- Triangle to highlight episode doesn't "cover" the episode tab anymore.
- Radio buttons in repeatable records are now properly aligned.
- It was possible for the admin to use illegal characters (one of {}()[]<>&) when setting a user's password and the user could not change such a password afterwards.

## [2.0.2] - 2019-04-12
### Fixed
- Problem when upgrading from older 1.4.* version.
- Error when trying to import a patient that doesn't already exist.

## [2.0.1] - 2019-03-21
### Fixed
- Connection Pool Error

## [2.0.0] - 2019-01-18
### Added
- Keycloak support
- Admins can download a CSV file containing all forms and data elements used in the registry. The CSV file is created during the form rebuilding cycle. So, if the file is not available, clicking on the save button in the registry design admin view will trigger rebuilding.
- By pressing a keyboard shortcut (Ctrl+Shift+F4) when viewing a form, MDR URNs for all data elements and records become visible. This might be useful for development purposes.

### Changed
- The import XSD file now includes every form with every record and data element, regardless if data elements occur in more than one form.
- The name attribute for data elements and records is no longer mandatory in import XML files.
- Considerable speedup of loading episode forms, especially in registries with a lot of forms and data elements.
- Slight reduction in page loading time throughout the registry.
- When adding a new patient, the location to which the patient will be added is explicitly shown in the dialog window.
- Admin forms are no longer hidden until a certain criteria is met
- Show the role editor before the user entered a first character for the role name
- Mandatory form fields are marked with an asterisk.
- Migrate from Jersey 1 to Jersey 2

### Fixed
- In some cases, newly entered data was not visible after switching between forms.
- Patient creation failed silently when the same patient already existed in another location. An error message is now shown.
- Error loading the patient list after changing a location's name.
- In user's details, the email address was not visible.
- Import data from previous/last was not working.

## [1.4.3] - 2017-10-26
### Added
- Every input element now has an attribute called ```data-mdrid```, containing the data element's mdr id.

### Changed
- Caching of database queries used to speed up user and roles lists in admin section.
- List of user roles in frontend now sorted alphabetically.

### Fixed
- Sorting of patients by name or pseudonym was not always working correctly.
- Some minor translation issues.

## [1.4.2] - 2017-09-04
### Fixed
- One rather prominent string always showed in english.

## [1.4.1] - 2017-08-29
### Added
- Forms show title and description texts as well as rich text items in the language set in EDC, given that they are available in that language. (requires Form Editor 1.5.0 or higher)
- Patients' pseudonyms can now be copied to the clipboard easily.

### Changed
- Loading patients and locations now incorporates caching methods to speed things up.
- Patients' pseudonyms now displayed in a different font which makes it easier to distinguish some characters.
- Changed design of language select box.
- jQuery imported by mainzelliste-client is ignored from now on.
- Changed several dependencies to the latest component versions (edc, store-osse, mdrclient).
- Flag icons replaced by [flag-icon-css](http://flag-icon-css.lip.is/).

### Fixed
- "Password Complexity" setting appeared twice in admin view.
- Some minor i18n-related string corrections.
- Timeout when connecting to samply.auth prevents possible freeze on startup.

## [1.4.0] - 2017-06-01
### Added
- Additional config parameters editable for admin.
- OSSE can now act as a FAIR data point.
- New select box to change ui language (currently available: English and German).
- The user's current language setting gets saved.
- Added ability to import entire last episode.
- Use gzip compression in data transfer for some uncompressed formats.

### Changed
- Filters of patient list repositioned.
- Config panels rearranged to keep things clear in admin view.
- Records' titles are now fetched dynamically from the MDR.
- Forms now show title and description in header.
- Layout works better on higher resolutions now.
- Version number in login screen now shows build version.

### Fixed
- Fixed a bug occurring when adding a new patient.
- Users cannot be added without a role anymore.
- Special characters in utf-8 now display correctly.

## [1.3.3] - 2016-12-15
### Fixed
- Disabled failure on deserialisation on unknown or unignorable properties in Jersey (needed for new AUTH version).
- Some CSS fixes.

## [1.3.2] - 2016-04-21
### Added
- Patient list can now be filtered.
- Edit a new patient's medical data directly after creating the patient.
- Added the possibility to create another patient right after creating a new one.

## [1.3.1] - 2016-03-17
### Fixed
- Proxy usage for mdrfaces fixed.

## [1.3.0] - 2015-12-21
### Added
- New ```mainzelliste.noIdat``` setting in osseconfig. Will automatically be set to false, if it doesn't exist yet or isn't a boolean.
- New postgres functions for samply.store.

### Changed
- MDR rest interface updated to version v3.
- Incorporated changes in form-data-storage (all forms will be recreated).
- Changes the timestamps in all episodes to the timestamp version of the name.

## [1.2.0] - 2015-11-06
### Added
- New ```RESTLocalhostToMainzelliste``` configuration value for local HTTP connection to Mainzelliste.

### Fixed
- ```TeilerURL``` and port settings fixed.
