# Samply.EDC.OSSE

Samply.EDC.OSSE as a web based electronic data capturing system focuses on establishing a disease specific registry for collaborational use. The underlying data model is highly adaptable and can therefore be tailored to the specific needs of the respective observed disease. This is possible due to the integration of Samply.MDR and Samply.FormRepository.

# Features

- set up a registry by going through a configuration wizard
- define locations, roles and users to facilitate the collection of patients and their data at multiple cooperating sites
- integration with the Mainzelliste as a pseudonymization service where the access on the identifying patient data depends on the users rights
- import and usage of forms which are using data elements from the Samply.MDR and are designed in the Samply.FormRepository

# Build

All the necessary libraries to build this project are either available in the central maven 
repository or in the maven repository of the Institute of Medical Informatics at:

https://maven.dev.imi-frankfurt.de/

To configure this repository copy and paste the following content into your settings.xml file 
($HOME/.m2/settings.xml).

```xml
<?xml version="1.0" encoding="UTF-8"?>
<settings
    xsi:schemaLocation="http://maven.apache.org/SETTINGS/1.1.0 http://maven.apache.org/xsd/settings-1.1.0.xsd"
    xmlns="http://maven.apache.org/SETTINGS/1.1.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">

    <profiles>
        <profile>
            <activation>
                <activeByDefault>true</activeByDefault>
            </activation>
            <repositories>
                <repository>
                    <releases>
                        <enabled>true</enabled>
                    </releases>
                    <snapshots>
                        <enabled>false</enabled>
                    </snapshots>
                    <id>imi-maven-releases</id>
                    <url>https://maven.dev.imi-frankfurt.de/oss-releases</url>
                </repository>
            </repositories>
        </profile>
    </profiles>
</settings>
```

Use maven to build the war file:

``` 
mvn clean package
```

# Configuration

You find the configuration XML files in /src/main/resources/

In order to execute OSSE, you will have to create the files "backend.xml" and "osse.xml" in the directory and fill it with the necessary access data and URLs to the services. You can find example files in the directory that you can simply copy and rename.

The XML files in the directory above you can copy to /etc/osse/ (linux) or to a directory that is defined in the registry entry HKLM/Software/OSSE/ConfDir (Windows) in order to preserve them. Files in these system directory will be used over the files in the /src/main/resources/ directory, which only works as a fallback, if the system directories and its files are not found.


## Tomcat context file (e.g. ossebh.xml)
You can also define a context for tomcat in order to shift the configuration directory (see above) to another name.
With the following entry, you can for example tell the system to look for its configuration files in /etc/ossebh (or in the Windows Registry HKLM/Software/OSSEBH/ConfDir)

<?xml version="1.0" encoding="UTF-8"?>
<Context>
        <Parameter name="de.samply.projectName" value="OSSEBH"
        override="false" />
</Context>


## backend.xml

This file contains the access data to your database server, used by the backend. Fill in the hostname, username, password, and database name used.

## osse.xml

This file contains the URLs to the services used by OSSE. Fill in the URLs and necessary keys where needed. This file is only read before the first execution of OSSE, and will not be read anymore later on, as the configuration entries are stored in the database. You will have to use OSSE configuration pages to change the values at a later time, or use the Upgrade mechanism.

## theOneUpgrade.xml

This file contains information for upgrades. The UUID defines if it already has been run.
This file is only used by OS-based reconfiguration! Do not touch any values unless you know what you do.



# Remarks

## adding new postgres functions/changes in the backend

You must make sure that those postgres functions are added before any new functionality is used.
The idea is, that the provided upgrades of EDC are run before anything fancy is used from the backend.
This also means, that changes in the backend need to be done the way, that starting EDC and running the upgrades 
should basically run "on version 1.0", where the new functions are not used.
Otherwise you end up having a hen-and-egg problem adding functions while EDC crashes before, because the backend
wants to use them already.
Upgrade 9 provides the idea of such a problem, if you change the order of what it runs and have the adding of the new postgres functions done after the other stuff. Then you'll know what I mean here.

